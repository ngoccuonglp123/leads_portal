

<!DOCTYPE html>
<html lang="vi_VN">
<head>
<meta charset=UTF-8>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Kênh tư vấn và giao dịch bất động sản Vinhomes</title>
<link rel="icon" type="image/png" href="custom/include/custom_view/regis_advice/images/favicon.ico" sizes="48x48" />
<link defer rel="stylesheet" href="custom/include/custom_view/regis_advice/css/style.css" />
</head>
<body>
<div class="main" >
	<div class="main-pad">
		<div class="header">
			<div class="logo">
				<a href="https://online.vinhomes.vn/" title="online.vinhomes.vn">
					<img src="custom/include/custom_view/regis_advice/images/logo.png">
				</a>
			</div>
			<div class="header-title">
				<h1>Kênh tư vấn và giao dịch bất động sản Vinhomes</h1>
			</div>
		</div>
		<div class="content">
			<div class="table-respon">
				<table style="width:100%">
					<thead>
						<tr>
						    <th>
						    	<span>Nội dung</span>
						    	<span>Hình thức</span>
						    </th>
						    <th>Kênh Online <br> (mua qua online.vinhomes.vn)</th>
						    <th>Kênh Offline <br> (truyền thống)</th>
						 </tr>
					</thead>
					<tbody>
						<tr>
						    <td>Định nghĩa</td>
						    <td>
						    	<p>Khách hàng tự thao tác giao dịch trên kênh <a href="https://online.vinhomes.vn/" title="online.vinhomes.vn">online.vinhomes.vn</a>, hoặc app <a href="http://vho.app.link/" title="app Vinhomes Online">Vinhomes Online</a></p></p>
						    </td>
						    <td>
						    	<p>Khách hàng có chuyên viên kinh doanh tư vấn, hỗ trợ hướng dẫn mua bất động sản 
						    		<span class="fwb">Vinhomes</span>.</p>
						    </td>
						</tr>
						<tr>
						    <td>Dịch vụ tư vấn/hỗ trợ</td>
						    <td><p>Tư vấn online, thăm quan dự án qua nhân viên hướng dẫn trải nghiệm dự án</p></td>
						    <td><p>Tư vấn theo các chương trình hình thức đa kênh trực tiếp, một nhân sự theo xuyên suốt.</p></td>
						</tr>
						<tr>
						    <td>Dịch vụ hỗ trợ <br> Khách hàng giao dịch</td>
						    <td><p>Khách hàng tự thao tác đặt mua online</p></td>
						    <td><p>Chuyên viên kinh doanh đặt mua cho Khách hàng.</p></td>
						</tr>
						<tr>
						    <td>Ưu đãi</td>
						    <td><p>Bảng hàng đầy đủ, giá gốc chủ đầu tư, được hưởng đầy đủ các chương trình ưu đãi theo sản phẩm hoặc theo chính sách ưu đãi chủ đầu tư</p> 
						    	<p>Được hưởng các ưu đãi dành riêng cho khách mua qua kênh online:</p>
						    	<p class="margin-bottom-0">- Giảm giá <span class="fwb">1%*</span></p>
						    	<p class="margin-top-0">- Và các ưu đãi khác</p>
						    </td>
						    <td><p>Bảng hàng đầy đủ, giá gốc chủ đầu tư, được hưởng đầy đủ các chương trình ưu đãi theo sản phẩm hoặc theo chính sách ưu đãi chủ đầu tư.</p></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="footer">
			<p class="copyright">(<span class="red">*</span>) tính trên giá bán căn hộ (đã bao gồm VAT và phí bảo trì). Ưu đãi giảm giá mua online còn 
				<span class="fwb">0,5%</span> khi khách hàng đăng ký giao dịch với chuyên viên kinh doanh kênh Offline (truyền thống) nhưng giao dịch mua nhà <span class="fwb">Vinhomes</span> trên <a href="https://online.vinhomes.vn/" title="online.vinhomes.vn">online.vinhomes.vn</a> hoặc app <span class="fwb">Vinhomes Online</span> trong vòng <span class="fwb">20 ngày</span> kể từ ngày đăng ký.
			</p>
		</div>
	</div>
</div>
</body>
</html>
