$(document).ready(function () {

    // Get url parameter
    var urlParams = [];
    urlParams = getUrlVars();

    // Ten phong kinh doanh
    var department_name = 'Phòng kinh doanh';
    if (urlParams.length > 0) {
        var department_param = urlParams['department'];
        switch (department_param) {
            case 'dangkytuvanPKD2':
                department_name = 'Phòng kinh doanh 2';
                break;
            case 'dangkytuvanPKD3':
                department_name = 'Phòng kinh doanh 3';
                break;
            case 'dangkytuvanPKD5':
                department_name = 'Phòng kinh doanh 5';
                break;
            case 'dangkytuvanPKD8':
                department_name = 'Phòng kinh doanh 8';
                break;
            case 'dangkytuvanPKD9':
                department_name = 'Phòng kinh doanh 9';
                break;
            case 'dangkytuvanPKD10':
                department_name = 'Phòng kinh doanh 10';
                break;
            case 'dangkytuvanPKDO1':
                if (urlParams['type'] === 'online') {
                    department_name = 'Phòng kinh doanh online 1';
                }
                break;
            case 'dangkytuvanPKDO2':
                if (urlParams['type'] === 'online'){
                    department_name = 'Phòng kinh doanh online 2';
                }
                break;
            default: 
                break;
        }
        $('#department').text(decodeURIComponent(department_name));

        // Tieu de online / offline
        var vin_header = 'Vinhomes';
        if (urlParams['type'] === 'online') {
            vin_header = '<br/> Vinhomes online';
        }
        $('#vin_header_title').html(vin_header);
    }

    $('input[type=number][max]:not([max=""])').on('input', function (ev) {
        var $this = $(this);
        var maxlength = $this.attr('max').length;
        var value = $this.val();
        if (value && value.length >= maxlength) {
            $this.val(value.substr(0, maxlength));
        }
    });

    // Get url param
    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    $('#btn_submit').click(function () {
        saveRegisForm();
    });

    function saveRegisForm() {
        var customer_name = '';
        var couple_name = '';
        var customer_dob = '';
        var couple_dob = '';

        customer_name = $('#customer_name').val();
        couple_name = $('#couple_name').val();
        var customer_dob_day = $('#customer_dob_day').val();
        var customer_dob_month = $('#customer_dob_month').val();
        var customer_dob_year = $('#customer_dob_year').val();
        var couple_dob_day = $('#couple_dob_day').val();
        var couple_dob_month = $('#couple_dob_month').val();
        var couple_dob_year = $('#couple_dob_year').val();

        if(customer_name === ''){
            showToast('Nhập họ và tên!', 'warning');
            return;
        } else {
            if(hasNumber(customer_name)) {
                showToast('Tên không được chứa ký tự số !', 'warning');
                return;
            }
            if (customer_dob_day !== '' && customer_dob_month !== '' && customer_dob_year !== '') {
                customer_dob = validateDoB(customer_dob_day, customer_dob_month, customer_dob_year, false);
                if(customer_dob === '') {
                    return;
                }
            } else {
                showToast('Nhập đầy đủ ngày tháng năm sinh !', 'warning');
                return;
            }

            if (couple_name !== '') {
                if (hasNumber(couple_name)){
                    showToast('Tên vợ/chồng không được chứa ký tự số !', 'warning');
                    return;
                }
                if (couple_dob_day !== '' && couple_dob_month !== '' && couple_dob_year !== '') {
                    couple_dob = validateDoB(couple_dob_day, couple_dob_month, couple_dob_year, true);
                    if (couple_dob === '') {
                        return;
                    }
                }
            }
        }
            showToast('Trân trọng cảm ơn quý khách đã lựa chọn dịch vụ tư vấn của Vinhomes '+(urlParams['type'] === 'online' ? 'Online.': '.'), 'success');
            $('#regis_advice_form').trigger('reset');
    }

    function validateDoB(day, month, year, isCouple) {
        var result = '';
        if(month > 12 || month < 1) {
            showToast('Tháng sinh'+(isCouple ? ' vợ/chồng ' : ' ')+'không chính xác !', 'warning');
            return '';
        }
        result += year + '-' + month + '-';
        if (month % 2 != 0) {
            if (day <= 31) {
                result += day;
            } else {
                showToast('Ngày sinh' + (isCouple ? ' vợ/chồng ' : ' ') + 'không chính xác !', 'warning');
                return '';
            }
        } else {
            if (month == 2) {
                if (day <= 29) {
                    result += day;
                }else {
                    showToast('Ngày sinh' + (isCouple ? ' vợ/chồng ' : ' ') + 'không chính xác !', 'warning');
                    return '';
                }
            }else {
                if (day <= 30) {
                    result += day;
                }else {
                    showToast('Ngày sinh' + (isCouple ? ' vợ/chồng ' : ' ') + 'không chính xác !', 'warning');
                    return '';
                }
            }
        }
        return result;
    }

    function showToast(msg, status) {
        $('#snackbar').removeClass();
        switch (status) {
            case 'success' :
                $('#snackbar').addClass('success-toast');
                break;

            case 'warning' :
                $('#snackbar').addClass('warning-toast');
                break;

            default :
                break;
        }
        if(status === 'success') {
            $('#snackbar').html('<span> <b style = "font-size: 18px">ĐĂNG KÝ THÀNH CÔNG !</b><br/><p style="font-size: 14">' + msg + '</p></span>');
        }else {
            $('#snackbar').html(msg);
        }
        
        $('#snackbar').addClass('show');
        setTimeout(function () {
            $('#snackbar').removeClass('show');
         }, 3000);
        
    }

    function hasNumber(myString) {
        return /\d/.test(myString);
    }

});