<?php 
	$current_date = getdate(date("U"));
	$day = $current_date['mday'];
    $month = $current_date['mon'];

?>
<html lang="vi_VN">

<head>
    <meta charset=UTF-8>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Đăng ký sử dụng dịch vụ tư vấn Vinhomes</title>
    <link rel="icon" type="image/png" href="custom/include/custom_view/regis_advice/images/favicon.ico" sizes="48x48" />
    <link defer rel="stylesheet" href="custom/include/custom_view/regis_advice/css/style.css" />
    <script type="text/javascript" src="cache/include/javascript/sugar_grp1_jquery.js"> </script>
    <script type="text/javascript" src="custom/include/custom_view/regis_advice/js/custom.js"></script>
</head>

<body>
    <form id="regis_advice_form">
        <div class="main main-fix">
            <div class="main-pad">
                <div class="header">
                    <div class="logo">
                        <a href="https://online.vinhomes.vn/" title="online.vinhomes.vn">
                            <img src="custom/include/custom_view/regis_advice/images/logo.png">
                        </a>
                    </div>
                    <div class="header-title">
                        <h1>Đăng ký sử dụng dịch vụ tư vấn <span id="vin_header_title"></span></h1>
                        <div class="group">
                            <div>
                                <label>Ngày</label>
                                <input class="w-30 text-center" id="current_date" type="number" min="1" max="31"
                                    value="<?php echo $day ?>" name="">
                            </div>
                            <div>
                                <label>Tháng</label>
                                <input class="w-30 text-center" id="current_month" type="number" min="1" max="12"
                                    value="<?php echo $month ?>" name="">
                            </div>
                            <div>
                                <label>Năm 2020</label>
                            </div>
                        </div>
                        <p class="text-center"><i class="line"></i></p>
                    </div>
                </div>
                <div class="content">
                    <div class="group">
                        <label>Họ tên khách hàng: </label>
                        <input type="text" id="customer_name" required name="">
                    </div>
                    <div class="group">
                        <label>Ngày sinh: </label>
                        <div class="group">
                            <div>
                                <label class="fwn">Ngày</label>
                                <input class="w-60 text-center" id="customer_dob_day" type="number" min="1" max="31"
                                    name="">
                            </div>
                            <div>
                                <label class="fwn">Tháng</label>
                                <input class="w-60 text-center" id="customer_dob_month" type="number" min="1" max="12"
                                    name="">
                            </div>
                            <div>
                                <label class="fwn">Năm</label>
                                <input class="w-60 text-center" id="customer_dob_year" type="number" min="1" max="2020"
                                    name="">
                            </div>
                        </div>
                    </div>
                    <div class="group">
                        <label>Họ tên vợ/chồng (nếu có): </label>
                        <input type="text" id="couple_name" name="">
                    </div>
                    <div class="group">
                        <label>Ngày sinh: </label>
                        <div class="group">
                            <div>
                                <label class="fwn">Ngày</label>
                                <input class="w-60 text-center" id="couple_dob_day" type="number" min="1" max="31"
                                    name="">
                            </div>
                            <div>
                                <label class="fwn">Tháng</label>
                                <input class="w-60 text-center" id="couple_dob_month" type="number" min="1" max="12"
                                    name="">
                            </div>
                            <div>
                                <label class="fwn">Năm</label>
                                <input class="w-60 text-center" id="couple_dob_year" type="number" min="1" max="2020"
                                    name="">
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <p class="copyright"><span class="red fwb fsn">Lưu ý:</span> Đăng ký của vợ hoặc chồng được xem
                            là đăng ký cho cả hai vợ chồng.</p>
                    </div>
                    <div class="group group-fix">
                        <input class="un-flex" type="checkbox" name="">
                        <label class="fwn f14">Bằng phiếu đăng ký này, Khách hàng đồng ý sử dụng dịch vụ tư vấn của
                            <span class="fwb" id="department"></span>
                            , và đồng ý với các điều kiện và chính sách tư vấn <span class="fwb">Vinhomes</span>.
                        </label>
                    </div>
                    <div class="box-ul">
                        <ul>
                            <li>Phiếu đăng ký này có giá trị <span class="fwb">20 ngày</span> kể từ ngày ký phiếu đăng
                                ký.</li>
                            <li>Để biết thêm các quy định khác về chính sách của <span class="fwb">Vinhomes</span>, xin
                                Khách hàng vui lòng truy cập vào đường link <a href="https://sales.vinhomes.vn/index.php?entryPoint=kenhtuvan_GD_BDS_Vinhomes"
                                    title="online.vinhomes.vn">https://sales.vinhomes.vn/kenhtuvan_GD_BDS_Vinhomes.</a></li>
                        </ul>
                    </div>
                    <div class="text-center margin-top-50">
                        <a class="button" id="btn_submit">XÁC NHẬN</a>
                    </div>
                    <div class="text-center margin-top-25">
                        <p class="text-bottom">Vinhomes Trân Trọng Cám Ơn!</p>
                    </div>

                </div>

            </div>
            <div id="snackbar"></div>
        </div>
    </form>
</body>

</html>