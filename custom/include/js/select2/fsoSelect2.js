/**
 * FSO_Custom
 * AnhDT39
 * item: Select2Control
 */

$(document).ready(function () {
    $('input[inputselect2-enable="1"]').each(function (i, el) {

        let moduleDir = $(el).attr('moduleselect2');
        let el_vname = $(el).attr('vnameselect2');

        let el_id = $(el).attr('id_nameselect2');
        let el_id_val = $('#' + el_id).val();

        let el_idName = $(el).attr('id');
        let el_idName_val = $(el).val();

        // console.log({
        //     el_vname: el_vname,
        //     el_id: el_id,
        //     el_id_val: el_id_val,
        //     el_idName: el_idName,
        //     el_idName_val: el_idName_val,
        // });
        $(el)
            .select2({
                allowClear: true,
                placeholder: 'Please select an ' + el_vname,
                ajax: {
                    url: 'index.php?entryPoint=apiGetListPopup&moduleDir='
                        + moduleDir+'&record='+ $('input[name="record"').val(),
                    dataType: 'json',
                    data: function (term) {
                        var query = {
                            vin_csrf_token: vin_csrf_token,
                            q: term
                        }
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    allowClear: true,
                    results: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            })
            .on('change', function (obj) {
                // console.log(el_idName + ' >> change', obj);
                if (obj.added) {
                    // add or change
                    // console.log(el_idName + ' ==> add or update');

                    // set id value
                    $("#" + el_id).val(obj.added.id);
                    $("#" + el_idName).val(obj.added.text);

                    if (obj.removed == null) {
                        // console.log(el_idName + ' ==> add new');
                    } else {
                        // console.log(el_idName + ' ==> update');
                    }
                } else {
                    // console.log(el_idName + ' ==> clear');
                    $("#" + el_id).val('');
                    $("#" + el_idName).val('');
                }
            });

        if (el_id_val != '' && el_idName_val != '') {
            $(el)
                .select2('data', {
                    id: el_id_val,
                    text: el_idName_val
                });
        }
    })
})