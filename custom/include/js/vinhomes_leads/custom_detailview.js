$(document).ready(function () {
    if(document.getElementById("fs_custom_header_buttons") == null){
        $( '<div id="fs_custom_header_buttons"></div>' ).insertBefore( ".moduleTitle .clear" );
    }

    if(document.getElementById("actionMenuSidebar") != null){
        if($('#actionMenuSidebar .actionmenulinks a').eq(1) != undefined){
            var aTagCreate = $('#actionMenuSidebar .actionmenulinks a').eq(1).attr('href');
            if(aTagCreate != null && aTagCreate != undefined && aTagCreate != ''){
                if(aTagCreate.indexOf('index.php?module=vin_vin_leads&action=index') !== -1){
                    var createButton = '<button type="button" id="" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="customGoToListview()">List</button>';
                    $('#fs_custom_header_buttons').append(createButton);
                }
            }
        }
    }

    if(document.getElementById("custom_header_above") != null){
        var custom_header_above = $("#custom_header_above").html();
        $("#custom_header_above").html("");
        $( custom_header_above ).insertAfter( ".moduleTitle" );

    }

    if(document.getElementById("btn_lead_change_owner") == null){
        //$( lead_change_owner_btn ).insertBefore( ".moduleTitle .clear" );
        $("#fs_custom_header_buttons").append(lead_change_owner_btn);
    }

    $('#vin_budget_c').html($('#fs_currency').val()+$('#vin_budget_c').html());

    select2_sale_agent();
    select2_change_owner();

    //Lock
    if($('#fs_lock_permission').val() == '1'){
        var lock_status = 'Lock';
        if($('#fs_lock_status').val() == '1'){
            lock_status = 'Unlock';
        }
        var btn_lock_lead = '<li><input id="btn_lock_lead" title="" class="button" onclick="lockLead()" type="button" value="'+lock_status+'"></li>';
        $('#btn_view_change_log').parent().parent().append(btn_lock_lead);
    }
    refreshLockStatus();

    //Hide default find duplicate
    $('#merge_duplicate_button').parent().css('display','none');
    //Add custom find duplicate
    var custom_find_duplicate = '<li><input id="btn_view_change_log_custom" title="View Change Log" class="button" onclick="viewListDuplicate()" type="button" value="Find Duplicates"></li>';
    $('#btn_view_change_log').parent().parent().append(custom_find_duplicate);

    //Facebook Zalo
    if(document.getElementById('vin_facebook_c') != null){
        $('#vin_facebook_c').html('');
        var ofb = '';
        if($('#vin_primary_fb_c').val() != ''){
            ofb += '<span style="font-weight: bold;color: #23468c;">'+$('#vin_primary_fb_c').val()+'</span>';
            ofb += '<p>';
        }
        if($('#vin_other_fb_c').val() != ''){
            var ofb_array =  $('#vin_other_fb_c').val().split(";");
            ofb_array.forEach(function(entry) {
                if(entry != ''){
                    ofb += '<span>'+entry+'</span>';
                    ofb += '<p>';
                }
            });
        }
        $('#vin_facebook_c').html(ofb);
    }

    if(document.getElementById('vin_zalo_c') != null){
        $('#vin_zalo_c').html('');
        var ozl = '';
        if($('#vin_primary_zalo_c').val() != ''){
            ozl += '<span style="font-weight: bold;color: #23468c;">'+$('#vin_primary_zalo_c').val()+'</span>';
            ozl += '<p>';
        }
        if($('#vin_other_zalo_c').val() != ''){
            var ozl_array = $('#vin_other_zalo_c').val().split(";");
            ozl_array.forEach(function(entry) {
                if(entry != ''){
                    ozl += '<span>'+entry+'</span>';
                    ozl += '<p>';
                }
            });
        }
        $('#vin_zalo_c').html(ozl);
    }

    //Other Email - Phone
    if(document.getElementById('vin_other_email_c') != null){
        var cs_vin_other_email_c = $('#vin_other_email_c').html();
        var ozl_array = cs_vin_other_email_c.split(";");
        var ozl = '';
        ozl_array.forEach(function(entry) {
            if(entry != ''){
                ozl += '<span>'+entry+'</span>';
                ozl += '<p>';
            }
        });
        $('#vin_other_email_c').parent().html(ozl);
    }

    if(document.getElementById('vin_other_phone_c') != null){
        var cs_vin_other_phone_c = $('#vin_other_phone_c').html();
        var ozl_array = cs_vin_other_phone_c.split(";");
        var ozl = '';
        ozl_array.forEach(function(entry) {
            if(entry != ''){
                ozl += '<span>'+entry+'</span>';
                ozl += '<p>';
            }
        });
        $('#vin_other_phone_c').parent().html(ozl);
    }
    
    //Hidden Fields
    var hidden_fields = $('#lead_hidden_fields').val();
    if(hidden_fields != '' && hidden_fields != undefined){
        hidden_fields = hidden_fields.split(",");
        hidden_fields.forEach(function(entry) {
            if(entry != ''){
                if(document.getElementById(entry) != null){
                    $('#'+entry).parent().parent().parent().css('display','none');
                }
            }
        });
    }

});

function workflowChangeStatus(status, status_display, step){
    $("#wf_status").val(status);
    $("#wf_status_display").val(status_display);
    $("#wf_step").val(step);

    $('.fs_workflow_step').removeClass('fs_workflow_current');
    $('#fs_workflow_step_'+step).addClass('fs_workflow_current');
    return;
}

function workflow_change_status_save(){
    var status = $("#wf_status").val();
    var status_display = $("#wf_status_display").val();
    var step = parseInt($("#wf_step").val());
    var valid = 1;

    $("#s2id_sale_agent_select").attr("style", "border-color: #c4e3f5!important");
    $("#working_status").attr("style", "border-color: #a5e8d6!important");
    $("#s2id_sale_agent_select").attr("style", "border-color: #c4e3f5!important");
    $("#reason_unqualified").attr("style", "border-color: #c4e3f5!important");

    if(status == 'assigned'){
        if($('#sale_agent_select').val().trim() == ""){
            $("#s2id_sale_agent_select").attr("style", "border-color: red!important");
            valid = 0;
        }
    }
    else if(status == 'working'){
        if($('#header_sales_in_charge').html().trim() == '' || $('#header_sales_in_charge').html() == null){
            if($('#sale_agent_select').val() == ""){
                $("#s2id_sale_agent_select").attr("style", "border-color: red!important");
                valid = 0;
            }
        }
        if($('#working_status').val().trim() == ''){
            $("#working_status").attr("style", "border-color: red!important");
            valid = 0;
        }
    }
    else if(status == 'unqualified'){
        if($('#reason_unqualified').val().trim() == ''){
            $("#reason_unqualified").attr("style", "border-color: red!important");
            valid = 0;
        }
    }
    else if(status == 'converted'){
        if($('#header_sales_in_charge').html().trim() == '' || $('#header_sales_in_charge').html() == null){
            if($('#sale_agent_select').val() == ""){
                $("#s2id_sale_agent_select").attr("style", "border-color: red!important");
                valid = 0;
            }
        }
        if($('#converted_status_change_input').val().trim() == ''){
            $("#converted_status_change_input").attr("style", "border-color: red!important");
            valid = 0;
        }
    }

    if(valid == 1){
        var confirmChangeStatus = confirm('Are you sure change status Lead to '+status_display+'?');
        if (confirmChangeStatus == true) {
            SUGAR.ajaxUI.showLoadingPanel();
            setTimeout(function(){ 
                $.ajax({
                    url: "index.php?entryPoint=vinhomes_leads&subaction=workflowChangeStatus",
                    data: { id: $('#qkt_record_id').val(), 
                            status: status, 
                            saleId: $('#sale_agent_select').val(),
                            working_status: $('#working_status').val(),
                            reason_unqualified: $('#reason_unqualified').val(),
                            vin_csrf_token: vin_csrf_token,
                            orderId: $('#converted_status_change_input').val(),
                        },
                    method: "POST",
                    success: function (result) {
                        SUGAR.ajaxUI.hideLoadingPanel(); 
                        if(result == 1 || result == '1'){
                            var i;
                            for (i = 1; i <= step; i++) { 
                                $("#fs_workflow_step_"+i).removeClass("fs_workflow_current");
                                $("#fs_workflow_step_"+i).addClass("fs_workflow_completed");
                            }
                            for (i = step + 1; i <= 5; i++) { 
                                $("#fs_workflow_step_"+i).removeClass("fs_workflow_completed");
                            }
                            $("#fs_workflow_step_"+step).addClass("fs_workflow_current");
                            $("#fs_workflow_step_"+step).removeClass("fs_workflow_completed");
                            getSaleInfoWF($('#sale_agent_select').val());
                            $('.fs_workflow_current').click();
                            if(status == 'converted'){
                                $('#fs_order_id').val($('#converted_status_change_input').val());
                            }

                            var set_vin_lead_status_c = '<input type="hidden" class="sugar_field" id="vin_lead_status_c" value="'+status+'">'+status_display;
                            $('div[field=vin_lead_status_c]').html(set_vin_lead_status_c);

                            if(status == 'unqualified'){
                                $('#fs_workflow_step_4').prop('onclick','');
                                $('.fs_workflow_completed').prop('onclick','');
                                $('.fs_workflow_current').prop('onclick','');
                            }
                            else if(status == 'converted'){
                                $('#fs_workflow_step_4').prop('onclick','');
                                $('.fs_workflow_completed').prop('onclick','');
                                $('.fs_workflow_current').prop('onclick','');
                            }
                            else{
                                if($('#fs_workflow_step_4').prop('onclick') == null || $('#fs_workflow_step_4').prop('onclick') == ""){
                                    $('#fs_workflow_step_4').on('click', function () {
                                        workflowChangeStatus('unqualified', 'Unqualified', 4);
                                    });
                                }
                                if($('#fs_workflow_step_5').prop('onclick') == null || $('#fs_workflow_step_5').prop('onclick') == ""){
                                    $('#fs_workflow_step_5').on('click', function () {
                                        workflowChangeStatus('converted', 'Converted', 5);
                                    });
                                }
   
                            }
                        }
                        else{
                            alert('Something went wrong! \nPlease contact Admintrator for more information!');
                        }
                    },
                    error: function() {
                        alert('Error occured!');
                        SUGAR.ajaxUI.hideLoadingPanel(); 
                    }
                });
                $("#workflow_action_modal").modal('hide');
            }, 0);
        }
    }
}

function select2_sale_agent() {
    $('#sale_agent_select')
        .select2({
            allowClear: true,
            dropdownParent: $("#sale_agent_select_modal"),
            placeholder: 'Select a Sales In Charge...',
            ajax: {
                url: 'index.php?entryPoint=vinhomes_leads&subaction=getSaleAgent',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
                getSaleInfo(obj.val);
            } else {
                getSaleInfo('');
            }
        }
    );
}

function getSaleInfo(id){
    $('#sale_agent_select_email').html('');
    if(id != ''){
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=getSaleInfo",
            data: { vin_csrf_token: vin_csrf_token, id: id },
            method: "POST",
            success: function (result) {
                $('#sale_agent_select_email').html(result.email);
                $('#sale_agent_select_department').html(result.dept);
            },
            error: function() {
                alert('Error occured!');
            }
        });
    }
}

function getSaleInfoWF(id){
    if(id != ''){
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=getSaleInfo",
            data: { vin_csrf_token: vin_csrf_token, id: id },
            method: "POST",
            success: function (result) {
                $('#header_sales_in_charge').html(result.user_name);
                $('#user_id4_c').html(result.user_name);
                $('#user_id4_c').attr('data-id-value',id);
                var old_id = $('#vin_vin_department_id_c').attr('data-id-value');
                $('#vin_vin_department_id_c').html(result.dept);
                $('#vin_vin_department_id_c').attr('data-id-value',result.dept_id);
                var aTag = $('#vin_vin_department_id_c').parent().prop('href');
                if(aTag != null && aTag != ''){
                    aTag = aTag.replace(old_id, result.dept_id);
                    $('#vin_vin_department_id_c').parent().attr('href', aTag);
                }

            },
            error: function() {
                alert('Error occured!');
            }
        });
    }
}

function onclickChangeOwner(){
    $("#change_owner_modal").modal('show');
}

function select2_change_owner() {
    $('#change_owner_select')
        .select2({
            allowClear: true,
            dropdownParent: $("#change_owner_modal"),
            placeholder: 'Select a lead owner...',
            ajax: {
                url: 'index.php?entryPoint=vinhomes_leads&subaction=getSaleAgent',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
            } else {
            }
        }
    );
}

function change_owner_submit(){
    $("#s2id_change_owner_select").attr("style", "border-color: #c4e3f5!important");
    if($('#change_owner_select').val() == ""){
        $("#s2id_change_owner_select").attr("style", "border-color: red!important");
        return;
    }
    else{
        SUGAR.ajaxUI.showLoadingPanel();
        setTimeout(function(){ 
            $.ajax({
                url: "index.php?entryPoint=vinhomes_leads&subaction=submitChangeOwner",
                data: { vin_csrf_token: vin_csrf_token, ownerId: $('#change_owner_select').val(), id: $('#qkt_record_id').val() },
                method: "POST",
                success: function (result) {
                    $('#change_owner_current').html($('#change_owner_select').select2('data').text);
                    $('#user_id3_c').html($('#change_owner_select').select2('data').text);
                    $('#user_id3_c').attr('data-id-value',$('#change_owner_select').val());
                    SUGAR.ajaxUI.hideLoadingPanel(); 
                },
                error: function() {
                    alert('Error occured!');
                    SUGAR.ajaxUI.hideLoadingPanel(); 
                }
            });
        }, 0);
        $("#change_owner_modal").modal('hide');
    }
}

function workflowMarkStatus(){
    var status = $("#wf_status").val();
    var status_display = $("#wf_status_display").val();
    var step = parseInt($("#wf_step").val());
    var is_need_popup = 1;

    if($('#vin_lead_status_c').val() == 'unqualified' || $('#vin_lead_status_c').val() == 'converted'){
        return;
    }

    $('.sales_in_charge_tr').css('display','none');
    $('#working_status_tr').css('display','none');
    $('#reason_unqualified_tr').css('display','none');
    $('#converted_status_change').css('display','none');

    if(status == 'assigned'){
        $('.sales_in_charge_tr').css('display','');
    }
    else if(status == 'working'){
        if($('#header_sales_in_charge').html() == '' || $('#header_sales_in_charge').html() == null){
            $('.sales_in_charge_tr').css('display','');
        }
        else{
            is_need_popup = 0; 
        }
        $('#working_status_tr').css('display','');
    }
    else if(status == 'unqualified'){
        if($('#fs_order_id').val() == '' || $('#fs_order_id').val() == null){
            $('#reason_unqualified_tr').css('display','');
        }
        else{
            alert('Cannot change status. This Lead had Order.');
            is_need_popup = 0; 
        }
    }
    else if(status == 'converted'){
        if($('#header_sales_in_charge').html() == '' || $('#header_sales_in_charge').html() == null){
            $('.sales_in_charge_tr').css('display','');
        }
        $('#converted_status_change').css('display','');
        $('#converted_status_change_input').val($('#fs_order_id').val());
    }
    if(is_need_popup == 1){
        $("#workflow_action_modal").modal('show');
    }
    else{
        if(status == 'working'){
            //workflow_change_status_save();
            $("#workflow_action_modal").modal('show');
        }
    }
    return;
}

function customGoToListview(){
    if($('#actionMenuSidebar .actionmenulinks a').eq(1) != undefined){
        var aTagCreate = $('#actionMenuSidebar .actionmenulinks a').eq(1).attr('href');
        if(aTagCreate != null && aTagCreate != undefined && aTagCreate != ''){
            if(aTagCreate.indexOf('index.php?module=vin_vin_leads&action=index') !== -1){
                window.location.href = aTagCreate;
            }
        }
    }
}

function viewListDuplicate(){
    var record_id = '';
    var email = '';
    var phone = '';
    record_id = $('input[name=record]').val();
    email = $('#vin_email_c').html();
    phone = $('#vin_phone_number_c').html();
    $('#list_duplicate_modal_data').html();
    SUGAR.ajaxUI.showLoadingPanel();
    setTimeout(function(){ 
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=viewListDuplicateLead",
            data: { 
                email: email, 
                phone: phone, 
                vin_csrf_token: vin_csrf_token, 
                record_id: record_id 
            },
            method: "POST",
            success: function (result) {
                var data = result;
                var data_count = Object.keys(data).length;
                var tr_data = '';
                for (i = 0; i < data_count; i++) {
                    if(data[i] != undefined){
                        tr_data += '<tr>';
                        tr_data += '<td style="width:5%;">'+(i+1)+'</td>';
                        tr_data += '<td style="width:30%;"><a href="index.php?module=vin_vin_leads&action=DetailView&record='+data[i].id+'" target="_blank">'+data[i].name+'</a></td>';
                        tr_data += '<td style="width:23%;">'+data[i].owner+'</td>';
                        tr_data += '<td style="width:25%;">'+data[i].email+'</td>';
                        tr_data += '<td style="width:17%;">'+data[i].phone+'</td>';
                        tr_data += '</tr>';
                    }
                }
                $('#list_duplicate_modal_data').html(tr_data);
                $("#list_duplicate_modal").modal('show');
                SUGAR.ajaxUI.hideLoadingPanel(); 
            },
            error: function() {
                alert('Error occured!');
                SUGAR.ajaxUI.hideLoadingPanel(); 
            }
        });
    }, 0);
}

function lockLead(){
    record_id = $('input[name=record]').val();
    SUGAR.ajaxUI.showLoadingPanel();
    setTimeout(function(){ 
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=lockLead",
            data: { 
                vin_csrf_token: vin_csrf_token, 
                record_id: record_id 
            },
            method: "POST",
            success: function (result) {
                if(result == 1 || result == '1'){
                    $('#fs_lock_status').val(result);
                    $('#btn_lock_lead').val('Unlock');
                }
                else if(result == 0 || result == '0'){
                    $('#fs_lock_status').val(result);
                    $('#btn_lock_lead').val('Lock');
                }
                refreshLockStatus();
                SUGAR.ajaxUI.hideLoadingPanel(); 
            },
            error: function() {
                alert('Error occured!');
                SUGAR.ajaxUI.hideLoadingPanel(); 
            }
        });
    }, 0);
}

function refreshLockStatus(){
    if($('#fs_lock_permission').val() == '0'){
        if($('#fs_lock_status').val() == '1'){
            $('#edit_button').css('display','none');
            $('#btn_lead_change_owner').css('display','none');
            $('.fs_workflow_mark_status').css('display','none');
        }
        else{
            $('#edit_button').css('display','block');
            $('#btn_lead_change_owner').css('display','block');
            $('.fs_workflow_mark_status').css('display','block');
        }
    }
}