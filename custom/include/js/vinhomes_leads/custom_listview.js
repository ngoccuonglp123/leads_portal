$(document).ready(function () {

    if(document.getElementById("fs_custom_header_buttons") == null){
        $( '<div id="fs_custom_header_buttons"></div>' ).insertBefore( ".moduleTitle .clear" );
    }

    if(document.getElementById("actionMenuSidebar") != null){
        var aTagCreate = $('#actionMenuSidebar .actionmenulinks a').attr('href');
        if(aTagCreate != null && aTagCreate != undefined && aTagCreate != ''){
            if(aTagCreate.indexOf('index.php?module=vin_vin_leads&action=EditView') !== -1){
                var createButton = '<button type="button" id="" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="customCreateLead()">Create</button>';
                $('#fs_custom_header_buttons').append(createButton);
                var importButton = '<button type="button" id="" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="customImportLead()">Import</button>';
                $('#fs_custom_header_buttons').append(importButton);
            }
        }
    }

    if(document.getElementById("btn_change_status_button") == null){
        $('#fs_custom_header_buttons').append(btn_change_status_button);
    }

    if(document.getElementById("btn_change_owner_button") == null){
        $('#fs_custom_header_buttons').append(btn_change_owner_button);
    }

    if(document.getElementById("btn_assign_leads_button") == null){
        $('#fs_custom_header_buttons').append(assign_leads_button);
    }

    if(document.getElementById("btn_encode_modal") == null){
        $('#fs_custom_header_buttons').append(btn_encode_modal);
    }

    if(document.getElementById("btn_allocate_button") == null){
        $('#fs_custom_header_buttons').append(btn_allocate_button);
    }

    select2_sale_agent();
    select2_change_owner();

    if(document.getElementById("btn_lead_report") == null && btn_lead_report != undefined && btn_lead_report != ''){
        $('#fs_custom_header_buttons').append(btn_lead_report);

        var date = new Date();
        date.setDate(date.getDate() - 8);
        var lastweek = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        $('#lead_report_from').datepicker({
            format: 'mm/dd/yyyy'
        });
        $('#lead_report_from').datepicker('setDate', lastweek);
    
        var date = new Date();
        date.setDate(date.getDate() - 1);
        var yesterday = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        $('#lead_report_to').datepicker({
            format: 'mm/dd/yyyy'
        });
        $('#lead_report_to').datepicker('setDate', yesterday);
    }
});

function allocate_matrix(){
    window.location.href = "index.php?module=vin_vin_leads&action=allocate_matrix";
}

function showAssignLeadsModal(){
    var checked = sugarListView.get_checks();
    var selected_ids = document.MassUpdate.uid.value;
    selected_ids = selected_ids.split(',');
    if(selected_ids.length < 1){
        alert('Please select at least one lead!');
    }
    else{
        if(selected_ids.length == 1){
            if(selected_ids[0] ==  null || selected_ids[0] == ""){
                alert('Please select at least one lead!');
            }
            else{
                $("#sale_agent_select_modal").modal('show');
            }
        }
        else{
            $("#sale_agent_select_modal").modal('show');
        }
    }
}

function select2_sale_agent() {
    $('#sale_agent_select')
        .select2({
            allowClear: true,
            dropdownParent: $("#sale_agent_select_modal"),
            placeholder: 'Select a Sales In Charge...',
            ajax: {
                url: 'index.php?entryPoint=vinhomes_leads&subaction=getSaleAgent',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
                getSaleInfo(obj.val);
            } else {
                getSaleInfo('');
            }
        }
    );
}

function getSaleInfo(id){
    $('#sale_agent_select_email').html('');
    if(id != ''){
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=getSaleInfo",
            data: { vin_csrf_token: vin_csrf_token, id: id },
            method: "POST",
            success: function (result) {
                $('#sale_agent_select_email').html(result.email);
                $('#sale_agent_select_department').html(result.dept);
            },
            error: function() {
                alert('Error occured!');
            }
        });
    }
}

function assignLeadsSubmit(){
    $("#s2id_sale_agent_select").attr("style", "border-color: #c4e3f5!important");
    if($('#sale_agent_select').val() == ""){
        $("#s2id_sale_agent_select").attr("style", "border-color: red!important");
        return;
    }
    else{
        var checked = sugarListView.get_checks();
        var listLeads = document.MassUpdate.uid.value;
        SUGAR.ajaxUI.showLoadingPanel();
        setTimeout(function(){ 
            $.ajax({
                url: "index.php?entryPoint=vinhomes_leads&subaction=submitAssignSale",
                data: { vin_csrf_token: vin_csrf_token, saleId: $('#sale_agent_select').val(), listLeads: listLeads},
                method: "POST",
                success: function (result) {
                    SUGAR.ajaxUI.hideLoadingPanel(); 
                    location.reload();
                },
                error: function() {
                    alert('Error occured!');
                    SUGAR.ajaxUI.hideLoadingPanel(); 
                }
            });
        }, 0);
        $("#sale_agent_select_modal").modal('hide');
    }
}

function customCreateLead(){
    var aTagCreate = $('#actionMenuSidebar .actionmenulinks a').attr('href');
    window.location.href = aTagCreate;
}

function onclickChangeOwner(){
    var checked = sugarListView.get_checks();
    var selected_ids = document.MassUpdate.uid.value;
    selected_ids = selected_ids.split(',');
    if(selected_ids.length < 1){
        alert('Please select at least one lead!');
    }
    else{
        if(selected_ids.length == 1){
            if(selected_ids[0] ==  null || selected_ids[0] == ""){
                alert('Please select at least one lead!');
            }
            else{
                $("#change_owner_modal").modal('show');
            }
        }
        else{
            $("#change_owner_modal").modal('show');
        }
    }
}

function select2_change_owner() {
    $('#change_owner_select')
        .select2({
            allowClear: true,
            dropdownParent: $("#change_owner_modal"),
            placeholder: 'Select a lead owner...',
            ajax: {
                url: 'index.php?entryPoint=vinhomes_leads&subaction=getSaleAgent',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
            } else {
            }
        }
    );
}

function change_owner_submit(){
    $("#s2id_change_owner_select").attr("style", "border-color: #c4e3f5!important");
    if($('#change_owner_select').val() == ""){
        $("#s2id_change_owner_select").attr("style", "border-color: red!important");
        return;
    }
    else{
        SUGAR.ajaxUI.showLoadingPanel();
        var checked = sugarListView.get_checks();
        var listLeads = document.MassUpdate.uid.value;
        setTimeout(function(){ 
            $.ajax({
                url: "index.php?entryPoint=vinhomes_leads&subaction=submitChangeOwnerList",
                data: { vin_csrf_token: vin_csrf_token, ownerId: $('#change_owner_select').val(), listLeads: listLeads },
                method: "POST",
                success: function (result) {
                    SUGAR.ajaxUI.hideLoadingPanel(); 
                    location.reload();
                },
                error: function() {
                    alert('Error occured!');
                    SUGAR.ajaxUI.hideLoadingPanel(); 
                }
            });
        }, 0);
        $("#change_owner_modal").modal('hide');
    }
}

function showChangeStatusModal(){
    var checked = sugarListView.get_checks();
    var selected_ids = document.MassUpdate.uid.value;
    selected_ids = selected_ids.split(',');
    $("#working_status").attr("style", "border-color: #a5e8d6!important");
    if(selected_ids.length < 1){
        alert('Please select at least one lead!');
    }
    else{
        $('#working_status_tr').css('display','none');
        $('#reason_unqualified_tr').css('display','none');
        if(selected_ids.length == 1){
            if(selected_ids[0] ==  null || selected_ids[0] == ""){
                alert('Please select at least one lead!');
            }
            else{
                working_status_change()
                $("#change_status_modal").modal('show');
            }
        }
        else{
            working_status_change()
            $("#change_status_modal").modal('show');
        }
    }
}

function change_status_submit(){
    var checked = sugarListView.get_checks();
    var listLeads = document.MassUpdate.uid.value;
    $("#working_status").attr("style", "border-color: #a5e8d6!important");
    if($('#status_change').val() == 'working'){
        if($('#working_status').val() == ''){
            $("#working_status").attr("style", "border-color: red!important");
            return;
        }
    }

    SUGAR.ajaxUI.showLoadingPanel();
    setTimeout(function(){ 
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=submitChangeStatusList",
            data: { vin_csrf_token: vin_csrf_token, 
                status: $('#status_change').val(), 
                working_status: $('#working_status').val(), 
                reason_unqualified: $('#reason_unqualified').val(), 
                listLeads: listLeads },
            method: "POST",
            success: function (result) {
                SUGAR.ajaxUI.hideLoadingPanel(); 
                location.reload();
            },
            error: function() {
                alert('Error occured!');
                SUGAR.ajaxUI.hideLoadingPanel(); 
            }
        });
    }, 0);
    $("#change_status_modal").modal('hide');
}

function customImportLead(){
    $('#upload_file3').val('')
    $('#lblMsg3').html('');
    $('#lblError3').html('');
    $('#btn_master_import_invoices').css('display','none');
    $('#btn_verify_master_import_invoices').css('display','');
    $('#master_import_modal_popup').modal('show');
}

function onClickVerifyMasterImportInvoices(){
    if (validateFile3()){
        $('#btn_verify_master_import_invoices').prop('disabled',true);
        SUGAR.ajaxUI.showLoadingPanel();
        $("#loaderModal").modal('show');
        setTimeout(function(){
            var fd = new FormData();
            fd.append('upload_file',$('#upload_file3')[0].files[0]);
            fd.append('vin_csrf_token',vin_csrf_token);
            $.ajax({
                type: 'POST',
                url: "index.php?entryPoint=vinhomes_leads&subaction=importLeads",
                data: fd,
                processData: false,
                contentType: false,
                success: function(result){
                    SUGAR.ajaxUI.hideLoadingPanel();
                    $("#loaderModal").modal('hide');
                    $('#btn_verify_master_import_invoices').prop('disabled',false);
                    if(result.valid == 1 || result.valid == '1'
                    || result.valid == 0 || result.valid == '0'){
                        var valid = 1;
                        var lblErrorHTML = "";
                        if(result.name_require != '' && result.name_require != null){
                            lblErrorHTML += "Last Name is mandatory.";
                            lblErrorHTML += "<br />";
                            valid = 0;
                        }
                        if(result.email_require != '' && result.email_require != null){
                            lblErrorHTML += "Email is mandatory.";
                            lblErrorHTML += "<br />";
                            valid = 0;
                        }
                        if(result.project_require != '' && result.project_require != null){
                            lblErrorHTML += "Project is mandatory.";
                            lblErrorHTML += "<br />";
                            valid = 0;
                        }
                        if(result.email_invalid != '' && result.email_invalid != null){
                            lblErrorHTML += "Invalid Email: " + result.email_invalid;
                            lblErrorHTML += "<br />";
                            valid = 0;
                        }
                        if(result.phone_invalid != '' && result.phone_invalid != null){
                            lblErrorHTML += "Invalid Phone: " + result.phone_invalid;
                            lblErrorHTML += "<br />";
                            valid = 0;
                        }
                        if(result.project_invalid != '' && result.project_invalid != null){
                            lblErrorHTML += "Project not existed: " + result.project_invalid;
                            lblErrorHTML += "<br />";
                            valid = 0;
                        }

                        if(valid == 1){
                            $.notify("Import successful. Listview will be refreshed.", "success");
                            location.reload();
                        }
                        else{
                            $("#lblError3").html(lblErrorHTML);
                        }
                    }
                    else{
                        alert('Something went wrong!!! Cannot import files!');
                    }
                },
                error: function() {
                    $("#loaderModal").modal('hide');
                    alert('Something went wrong!!!');
                    SUGAR.ajaxUI.hideLoadingPanel();
                    $('#btn_verify_master_import_invoices').prop('disabled',false);
                }
            });
        }, 0);
    } else {
        alert('File uploaded not support!');
    }
}

function validateFile3() {
    $('#lblMsg3').html('');
    $('#btn_master_import_invoices').css('display','none');
    $('#btn_verify_master_import_invoices').css('display','');

    var allowedFiles = [".xls", ".xlsx"];
    var fileUpload = $("#upload_file3");
    var lblError = $("#lblError3");
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:()])+(" + allowedFiles.join('|') + ")$");

    if (fileUpload.val() != '') {
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>(" + allowedFiles.join(', ') + ")</b> only.");
            return false;
        }
    } else {
        lblError.html('');
    }
    lblError.html('');
    return true;
}

function onclickEncrypt(){
    $("#encrypt_value_raw").css("border-color", "#c4e3f5");
    $("#encrypt_value_raw").val("");
    $("#encrypt_value").html("");
    $("#encrypt_modal").modal('show');
}

function onClickEncrypt(){
    $("#encrypt_value_raw").css("border-color", "#c4e3f5");
    if($('#encrypt_value_raw').val() == ""){
        $("#encrypt_value_raw").css("border-color", "red");
        return;
    }
    else{
        $("#loaderModal").modal('show');
        setTimeout(function(){ 
            $.ajax({
                url: "index.php?entryPoint=vinhomes_leads&subaction=encrypt",
                data: { vin_csrf_token: vin_csrf_token, value: $('#encrypt_value_raw').val()},
                method: "POST",
                success: function (result) {
                    $("#encrypt_value").html(result);
                    $("#loaderModal").modal('hide');
                },
                error: function() {
                    $("#loaderModal").modal('hide');
                    alert('Error occured!');
                }
            });
        }, 0);
    }
}

function working_status_change(){
    $('#working_status_tr').css('display','none');
    $('#reason_unqualified_tr').css('display','none');
    if($('#status_change').val() == "working"){
        $('#working_status_tr').css('display','contents');
    }
    else if($('#status_change').val() == "unqualified"){
        $('#reason_unqualified_tr').css('display','contents');
    }
}

function showLeadsReportModal(){
    $("#leads_report_modal").modal('show');
}

function export_report(){
    $("#loaderModal").modal('show');
    setTimeout(function(){ 
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=export_report",
            data: { vin_csrf_token: vin_csrf_token, 
                from: $('#lead_report_from').val(),
                to: $('#lead_report_to').val()},
            method: "POST",
            success: function (result) {
                try {
                    var dataResult = JSON.parse(result);
                    window.downloadFile(dataResult.file);
                } catch (error) {
                }
                $("#loaderModal").modal('hide');
            },
            error: function() {
                $("#loaderModal").modal('hide');
                alert('Error occured!');
            }
        });
    }, 0);
}

window.downloadFile = function (sUrl) {

    //If in Chrome or Safari - download via virtual link click
    if (window.downloadFile.isChrome || window.downloadFile.isSafari) {
        //Creating new link node.
        var link = document.createElement('a');
        link.href = sUrl;

        if (link.download !== undefined) {
            //Set HTML5 download attribute. This will prevent file from opening if supported.
            var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
            link.download = fileName;
        }

        //Dispatching click event.
        if (document.createEvent) {
            var e = document.createEvent('MouseEvents');
            e.initEvent('click', true, true);
            link.dispatchEvent(e);
            return true;
        }
    }

    // Force file download (whether supported by server).
    var query = '?download';

    window.open(sUrl + query);
}

window.downloadFile.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
window.downloadFile.isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
