var fb_gen = 0;
var zl_gen = 0;
var email_gen = 0;
var phone_gen = 0;
$(document).ready(function () {
    $('#EditView').prop('autocomplete','off');
    
    //Default lead owner
    if($('input[name=record]').val() =='' || $('input[name=record]').val() == null){
        $('#vin_lead_owner_c').val($('#fs_current_user_name').val());
        $('#user_id3_c').val($('#fs_current_user_id').val());
    }

    //Duplcate
    var duplicate_warning = '<div id="duplicate_warning_msg" class="duplicate_warning_msg alert alert-warning"><span>It looks as if duplicate exist for this lead. <span class="view_duplicate" onclick="viewListDuplicate()">View duplicate.</span></span><div>';
    $('#EditView_tabs').prepend(duplicate_warning);

    disableFiels();

    $("#vin_email_c").change(function () {
        checkDuplicate();
    });
    $("#vin_phone_number_c").change(function () {
        checkDuplicate();
    });
    $("#vin_full_name_c").change(function () {
        checkDuplicate();
    });
    $("#vin_dob_c_trigger").focusout(function () {
        checkDuplicate();
    });

    select2_sale_in_charge();
    select2_department();
    $('#btn_vin_department_c').css('display','none');
    $('#btn_clr_vin_department_c').css('display','none');

    if($('#user_id4_c').val() != '' &&  $('#user_id4_c').val() != null){
        $("#vin_sales_in_charge_c").select2("data", {
            id: $('#user_id4_c').val(),
            text: $('#vin_sales_in_charge_c').val()
        });
    }

    if($('#vin_vin_department_id2_c').val() != '' &&  $('#vin_vin_department_id2_c').val() != null){
        $("#vin_department_c").select2("data", {
            id: $('#vin_vin_department_id2_c').val(),
            text: $('#vin_department_c').val()
        });
    }

    //Facebook Zalo
    if(document.getElementById('vin_facebook_c') != null){
        if(document.getElementById('vin_primary_fb_c') == null){
            var input_pfb = '<input type="text" name="vin_primary_fb_c" id="vin_primary_fb_c" style="display:none;">';
            $('#vin_facebook_c').parent().append(input_pfb);
        }
        if(document.getElementById('vin_other_fb_c') == null){
            var input_ofb = '<input type="text" name="vin_other_fb_c" id="vin_other_fb_c" style="display:none;">';
            $('#vin_zalo_c').parent().append(input_ofb);
        }
        $('#vin_other_fb_c').val($('#cs_vin_other_fb_c').val());
        $('#vin_facebook_c').css('display','none');

        var input_zl_db = ' <span style="font-style: italic;font-size: 12px;">DoubleClick to mark as primary</span>';
        if($('#cs_vin_primary_fb_c').val() != ''){
            fb_gen = fb_gen + 1;
            var input_fb = '<input type="text" name="custom_facebook" ondblclick="doubleClickFB(this)" value="'+$('#cs_vin_primary_fb_c').val()+'" style="width:50%;background-color: #90d3fc;" onchange="custom_facebook_add('+fb_gen+', this)">'+input_zl_db;
            $('#vin_facebook_c').parent().append(input_fb);
        }
        else{
            if($('#cs_vin_other_fb_c').val() == ''){
                fb_gen = 1;
                var input_fb = '<input type="text" name="custom_facebook" style="width:50%;" ondblclick="doubleClickFB(this)" onchange="custom_facebook_add('+fb_gen+', this)"> <span style="font-style: italic;font-size: 12px;">DoubleClick to mark as primary</span>';
                $('#vin_facebook_c').parent().append(input_fb);
            }
        }

        if($('#cs_vin_other_fb_c').val() == ''){

        }
        else{
            if($('#cs_vin_other_fb_c').val() != ''){
                var ofb_array =  $('#cs_vin_other_fb_c').val().split(";");
                ofb_array.forEach(function(entry) {
                    if(entry != ''){
                        fb_gen = fb_gen + 1;
                        var input_fb = '';
                        if(fb_gen > 1){
                            input_fb = '<p>';
                            input_zl_db = '';
                        }
                        input_fb += '<input type="text" name="custom_facebook" ondblclick="doubleClickFB(this)" value="'+entry+'" style="width:50%;" onchange="custom_facebook_add('+fb_gen+', this)">'+input_zl_db;
                        $('#vin_facebook_c').parent().append(input_fb);
                    }
                });

                fb_gen = fb_gen + 1;
                var input_fb2 = '<input type="text" name="custom_facebook" ondblclick="doubleClickFB(this)" value="" style="width:50%;" onchange="custom_facebook_add('+fb_gen+', this)">';
                $('#vin_facebook_c').parent().append(input_fb2);
            }
        }
    }

    if(document.getElementById('vin_zalo_c') != null){
        if(document.getElementById('vin_primary_zalo_c') == null){
            var input_pzl = '<input type="text" name="vin_primary_zalo_c" id="vin_primary_zalo_c" style="display:none;">';
            $('#vin_facebook_c').parent().append(input_pzl);
        }
        if(document.getElementById('vin_other_zalo_c') == null){
            var input_ozl = '<input type="text" name="vin_other_zalo_c" id="vin_other_zalo_c" style="display:none;">';
            $('#vin_zalo_c').parent().append(input_ozl);
        }
        $('#vin_other_zalo_c').val($('#cs_vin_other_zalo_c').val());
        $('#vin_zalo_c').css('display','none');

        var input_zl_db = ' <span style="font-style: italic;font-size: 12px;">DoubleClick to mark as primary</span>';
        if($('#cs_vin_primary_zalo_c').val() != ''){
            zl_gen = zl_gen + 1;
            var input_zl = '<input type="text" name="custom_zalo" ondblclick="doubleClickZalo(this)" value="'+$('#cs_vin_primary_zalo_c').val()+'" style="width:50%;background-color: #90d3fc;" onchange="custom_zalo_add('+zl_gen+', this)">'+input_zl_db;
            $('#vin_zalo_c').parent().append(input_zl);
        }
        else{
            if($('#cs_vin_other_zalo_c').val() == ''){
                zl_gen = 1;
                var input_zl = '<input type="text" name="custom_zalo" style="width:50%;" ondblclick="doubleClickZalo(this)" onchange="custom_zalo_add('+zl_gen+', this)"> <span style="font-style: italic;font-size: 12px;">DoubleClick to mark as primary</span>';
                $('#vin_zalo_c').parent().append(input_zl);
            }
        }

        if($('#cs_vin_other_zalo_c').val() == ''){

        }
        else{
            if($('#cs_vin_other_zalo_c').val() != ''){
                var ofb_array =  $('#cs_vin_other_zalo_c').val().split(";");
                ofb_array.forEach(function(entry) {
                    if(entry != ''){
                        zl_gen = zl_gen + 1;
                        var input_zl = '';
                        if(zl_gen > 1){
                            input_zl = '<p>';
                            input_zl_db = '';
                        }
                        input_zl += '<input type="text" name="custom_zalo" ondblclick="doubleClickZalo(this)" value="'+entry+'" style="width:50%;" onchange="custom_zalo_add('+zl_gen+', this)">'+input_zl_db;
                        $('#vin_zalo_c').parent().append(input_zl);
                    }
                });

                zl_gen = zl_gen + 1;
                var input_zl2 = '<input type="text" name="custom_zalo" ondblclick="doubleClickZalo(this)" value="" style="width:50%;" onchange="custom_zalo_add('+zl_gen+', this)">';
                $('#vin_zalo_c').parent().append(input_zl2);
            }
        }
    }

    //Other Email - Phone
    if(document.getElementById('vin_other_email_c') != null){
        $('#vin_other_email_c').css('display','none');
        if($('#cs_vin_other_email_c').val() != ''){
            var ofb_array =  $('#cs_vin_other_email_c').val().split(";");
            ofb_array.forEach(function(entry) {
                if(entry != ''){
                    email_gen = email_gen + 1;
                    var input_zl = '';
                    if(email_gen > 1){
                        input_zl = '<p>';
                    }
                    input_zl += '<input type="text" name="custom_email" value="'+entry+'" style="width:50%;" onchange="custom_email_add('+email_gen+', this)">';
                    $('#vin_other_email_c').parent().append(input_zl);
                }
            });

            email_gen = email_gen + 1;
            var input_zl2 = '<input type="text" name="custom_email" value="" style="width:50%;" onchange="custom_email_add('+email_gen+', this)">';
            $('#vin_other_email_c').parent().append(input_zl2);
        }
        else{
            email_gen = email_gen + 1;
            var input_zl2 = '<input type="text" name="custom_email" value="" style="width:50%;" onchange="custom_email_add('+email_gen+', this)">';
            $('#vin_other_email_c').parent().append(input_zl2); 
        }     
    }

    if(document.getElementById('vin_other_phone_c') != null){
        $('#vin_other_phone_c').css('display','none');
        if($('#cs_vin_other_phone_c').val() != ''){
            var ofb_array =  $('#cs_vin_other_phone_c').val().split(";");
            ofb_array.forEach(function(entry) {
                if(entry != ''){
                    phone_gen = phone_gen + 1;
                    var input_zl = '';
                    if(phone_gen > 1){
                        input_zl = '<p>';
                    }
                    input_zl += '<input type="text" name="custom_phone" value="'+entry+'" style="width:50%;" onchange="custom_phone_add('+phone_gen+', this)">';
                    $('#vin_other_phone_c').parent().append(input_zl);
                }
            });

            phone_gen = phone_gen + 1;
            var input_zl2 = '<input type="text" name="custom_phone" value="" style="width:50%;" onchange="custom_phone_add('+phone_gen+', this)">';
            $('#vin_other_phone_c').parent().append(input_zl2);
        }
        else{
            phone_gen = phone_gen + 1;
            var input_zl2 = '<input type="text" name="custom_phone" value="" style="width:50%;" onchange="custom_phone_add('+phone_gen+', this)">';
            $('#vin_other_phone_c').parent().append(input_zl2);
        }    
    }
})

function disableFiels(){
    $('#vin_lead_owner_c').prop('disabled',true);
    $('#vin_sales_dept_of_owner_c').prop('disabled',true);
    $('#vin_priority_c').prop('disabled',true);
    $('#vin_sales_dept_c').prop('disabled',true);
    $('#vin_lead_duplicate_c').prop('disabled', true);
}

function validateSaveLead(){
    var _form = document.getElementById('EditView');
    _form.action.value = 'Save';
    ret ="1";
    $(".br_required").remove();

    if (!validateEmail($("#vin_email_c").val())) {
        $("[field=vin_email_c]").append("<br id='' class='br_required'><div id='' class='br_required' style='color:red;padding:10px 0px 10px 0px' class='stage_require'>Email is invalid</div>");
        ret ="0";
    }
    if (!validatePhoneNumber($("#vin_phone_number_c").val()) && $("#vin_phone_number_c").val() != '') {
        $("[field=vin_phone_number_c]").append("<br id='' class='br_required'><div id='' class='br_required' style='color:red;padding:10px 0px 10px 0px' class='stage_require'>Phone Number is invalid</div>");
        ret ="0";
    }
    if (!validatePhoneNumber($("#vin_mobile_c").val()) && $("#vin_mobile_c").val() != '') {
        $("[field=vin_mobile_c]").append("<br id='' class='br_required'><div id='' class='br_required' style='color:red;padding:10px 0px 10px 0px' class='stage_require'>Mobile is invalid</div>");
        ret ="0";
    }

    if (check_form('EditView') && ret=="1") {
        SUGAR.ajaxUI.submitForm(_form);
    }
    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhoneNumber(phone) {
    var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    return re.test(phone);
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('/');
}
  
function checkDuplicate(){
    var record_id = '';
    var email = '';
    var phone = '';
    var fullName = '';
    var dob = '';
    $('#duplicate_warning_msg').css('display', 'none');
    if ($('input[name=isDuplicate]').val() == "false") {
        record_id = $('input[name=record]').val();
    }

    email = $('#vin_email_c').val();

    phone = $('#vin_phone_number_c').val();

    fullName = $('#vin_full_name_c').val();

    if ($('#vin_dob_c').val() != null && $('#vin_dob_c').val() != "") {
        dob = formatDate($('#vin_dob_c').val());
    }

    $.ajax({
        url: "index.php?entryPoint=vinhomes_leads&subaction=checkDuplicateLead",
        data: {
            email: email,
            phone: phone,
            fullName: fullName,
            dob: dob,
            vin_csrf_token: vin_csrf_token,
            record_id: record_id
        },
        method: "POST",
        async: true,
        success: function (result) {
            if (result == 1 || result == "1") {
                $.notify("It looks as if duplicate exist for this lead.", "warn");
                $('#duplicate_warning_msg').css('display', 'block');
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr.responseText);
            alert('Error occured!');
        }
    });
}

function viewListDuplicate(){
    var record_id = '';
    var email = '';
    var phone = '';
    var fullName = '';
    var dob = '';
    record_id = $('input[name=record]').val();

    email = $('#vin_email_c').val();

    phone = $('#vin_phone_number_c').val();

    fullName = $('#vin_full_name_c').val();

    if ($('#vin_dob_c').val() != null && $('#vin_dob_c').val() != "") {
        dob = formatDate($('#vin_dob_c').val());
    }
    $('#list_duplicate_modal_data').html();
    SUGAR.ajaxUI.showLoadingPanel();
    setTimeout(function(){ 
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=viewListDuplicateLead",
            data: { 
                email: email, 
                phone: phone, 
                dob: dob,
                fullName: fullName,
                vin_csrf_token: vin_csrf_token, 
                record_id: record_id 
            },
            method: "POST",
            success: function (result) {
                var data = result;
                var data_count = Object.keys(data).length;
                var tr_data = '';
                for (i = 0; i < data_count; i++) {
                    if(data[i] != undefined){
                        tr_data += '<tr>';
                        tr_data += '<td style="width:5%;">'+(i+1)+'</td>';
                        tr_data += '<td style="width:30%;"><a href="index.php?module=vin_vin_leads&action=DetailView&record='+data[i].id+'" target="_blank">'+data[i].name+'</a></td>';
                        tr_data += '<td style="width:23%;">'+data[i].owner+'</td>';
                        tr_data += '<td style="width:25%;">'+data[i].email+'</td>';
                        tr_data += '<td style="width:17%;">'+data[i].phone+'</td>';
                        tr_data += '</tr>';
                    }
                }
                $('#list_duplicate_modal_data').html(tr_data);
                $("#list_duplicate_modal").modal('show');
                SUGAR.ajaxUI.hideLoadingPanel(); 
            },
            error: function() {
                alert('Error occured!');
                SUGAR.ajaxUI.hideLoadingPanel(); 
            }
        });
    }, 0);
}

function select2_sale_in_charge() {
    $('#vin_sales_in_charge_c')
        .select2({
            allowClear: true,
            placeholder: 'Select a Sales In Charge...',
            ajax: {
                url: 'index.php?entryPoint=vinhomes_leads&subaction=getSaleAgent',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
                getSaleInfo(obj.val);
                $('#user_id4_c').val(obj.val);
            } else {
                $('#user_id4_c').val('');
                $('#vin_sales_dept_c').val('');
                $('#vin_vin_department_id_c').val('');
            }
        }
    );
}

function getSaleInfo(id){
    if(id != ''){
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=getSaleInfo",
            data: { vin_csrf_token: vin_csrf_token, id: id },
            method: "POST",
            success: function (result) {
                $('#vin_sales_dept_c').val(result.dept);
                $('#vin_vin_department_id_c').val(result.dept_id);
            },
            error: function() {
                alert('Error occured!');
            }
        });
    }
}

function custom_facebook_add(x, e){
    if(e.val != '' && x >= fb_gen){
        fb_gen = fb_gen + 1;
        var input_fb = '<p><input type="text" name="custom_facebook" ondblclick="doubleClickFB(this)" style="width:50%;" onchange="custom_facebook_add('+fb_gen+', this)">';
        $('#vin_facebook_c').parent().append(input_fb);
    }

    var fb = '';
    var fb_p = '';
    $( "input[name=custom_facebook]" ).each(function( index ) {
        if($( this ).val() != ''){
            if($( this ).prop('primary') == '1'){
                fb_p = $( this ).val();
            }
            else{
                fb += $( this ).val() + ';';
            }
        }
        else{
            $( this ).prop('primary','0');
            $( this ).css('background-color','#eaf7ff'); 
        }
    });
    $('#vin_other_fb_c').val(fb);
    $('#vin_primary_fb_c').val(fb_p);
}

function custom_zalo_add(x, e){
    if(e.val != '' && x >= zl_gen){
        zl_gen = zl_gen + 1;
        var input_zl = '<p><input type="text" name="custom_zalo" ondblclick="doubleClickZalo(this)" style="width:50%;" onchange="custom_zalo_add('+zl_gen+', this)">';
        $('#vin_zalo_c').parent().append(input_zl);
    }

    var zl = '';
    var zl_p = '';
    $( "input[name=custom_zalo]" ).each(function( index ) {
        if($( this ).val() != ''){
            if($( this ).prop('primary') == '1'){
                zl_p = $( this ).val();
            }
            else{
                zl += $( this ).val() + ';';
            }
        }
        else{
            $( this ).prop('primary','0');
            $( this ).css('background-color','#eaf7ff'); 
        }
    });
    $('#vin_other_zalo_c').val(zl);
    $('#vin_primary_zalo_c').val(zl_p);
}

function doubleClickFB(e){
    if($( e ).prop('primary') == '1'){
        $( e ).prop('primary','0');
        $( e ).css('background-color','#eaf7ff');
    }
    else{
        $( "input[name=custom_facebook]" ).each(function( index ) {
            $( this ).prop('primary','0');
            $( this ).css('background-color','#eaf7ff'); 
        });
        if($( e ).val() != ''){
            $( e ).prop('primary','1');
            $( e ).css('background-color','#90d3fc');
        }
    }

    var fb = '';
    var fb_p = '';
    $( "input[name=custom_facebook]" ).each(function( index ) {
        if($( this ).val() != ''){
            if($( this ).prop('primary') == '1'){
                fb_p = $( this ).val();
            }
            else{
                fb += $( this ).val() + ';';
            }
        }
        else{
            $( this ).prop('primary','0');
            $( this ).css('background-color','#eaf7ff'); 
        }
    });
    $('#vin_other_fb_c').val(fb);
    $('#vin_primary_fb_c').val(fb_p);
}

function doubleClickZalo(e){
    if($( e ).prop('primary') == '1'){
        $( e ).prop('primary','0');
        $( e ).css('background-color','#eaf7ff');
    }
    else{
        $( "input[name=custom_zalo]" ).each(function( index ) {
            $( this ).prop('primary','0');
            $( this ).css('background-color','#eaf7ff'); 
        });
        if($( e ).val() != ''){
            $( e ).prop('primary','1');
            $( e ).css('background-color','#90d3fc');
        }
    }

    var zl = '';
    var zl_p = '';
    $( "input[name=custom_zalo]" ).each(function( index ) {
        if($( this ).val() != ''){
            if($( this ).prop('primary') == '1'){
                zl_p = $( this ).val();
            }
            else{
                zl += $( this ).val() + ';';
            }
        }
        else{
            $( this ).prop('primary','0');
            $( this ).css('background-color','#eaf7ff'); 
        }
    });
    $('#vin_other_zalo_c').val(zl);
    $('#vin_primary_zalo_c').val(zl_p);
}

function custom_email_add(x, e){
    if(e.val != '' && x >= email_gen){
        email_gen = email_gen + 1;
        var input_zl = '<p><input type="text" name="custom_email" style="width:50%;" onchange="custom_email_add('+email_gen+', this)">';
        $('#vin_other_email_c').parent().append(input_zl);
    }
    $('span[name=custom_email_invalid]').remove();
    var zl = '';
    $( "input[name=custom_email]" ).each(function( index ) {
        if($( this ).val() != ''){
            if (!validateEmail($( this ).val())) {
                $( this ).css('border','1px solid red');
                $( this ).after( '<span name="custom_email_invalid" style="font-style: italic;font-size: 12px; color:red;"> Invalid Email. Wont be counted</span>' );
            }
            else{
                $( this ).css('border','1px solid #a5e8d6');
                zl += $( this ).val() + ';';
            }
        }
    });
    $('#vin_other_email_c').val(zl);
}

function custom_phone_add(x, e){
    if(e.val != '' && x >= phone_gen){
        phone_gen = phone_gen + 1;
        var input_zl = '<p><input type="text" name="custom_phone" style="width:50%;" onchange="custom_phone_add('+phone_gen+', this)">';
        $('#vin_other_phone_c').parent().append(input_zl);
    }
    $('span[name=custom_phone_invalid]').remove();
    var zl = '';
    $( "input[name=custom_phone]" ).each(function( index ) {
        if($( this ).val() != ''){
            if (!validatePhoneNumber( $( this ).val())){
                $( this ).css('border','1px solid red');
                $( this ).after( '<span name="custom_phone_invalid" style="font-style: italic;font-size: 12px; color:red;"> Invalid Phone. Wont be counted</span>' );
            }
            else{
                $( this ).css('border','1px solid #a5e8d6');
                zl += $( this ).val() + ';';
            }
        }
    });
    $('#vin_other_phone_c').val(zl);
}


function select2_department() {
    $('#vin_department_c')
        .select2({
            allowClear: true,
            placeholder: 'Select a Department...',
            ajax: {
                url: 'index.php?entryPoint=vinhomes_leads&subaction=getDepartment',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
                $('#vin_vin_department_id2_c').val(obj.val);
            } else {
                $('#vin_vin_department_id2_c').val('');
            }
        }
    );
}