var lead_funel_generate = 0;
var lead_funel_scope_series = [];
var lead_funel_scope_data = [];

var lead_convert1_generate = 0;
var lead_convert1_scope_series = [];
var lead_convert1_scope_data = [];
var lead_convert2_generate = 0;
var lead_convert2_scope_series = [];
var lead_convert2_scope_data = [];

var lead_performance_generate = 0;
var lead_performance_scope_series = [];
var lead_performance_scope_data = [];

$(document).ready(function () {

    var custom_list_view = $('#custom_list_view_report').clone();
    $('#custom_list_view_report').remove();
    $('.listViewBody').append(custom_list_view).html();
     
    angular.module('app', ['chart.js']);

    //Funel
    angular.module('app')
    .controller('lead_funel', function ($scope, $timeout) {
        $scope.labels = [];
        $scope.data = [];
        $scope.series = [];

        $scope.options = {
            legend: {
                display: true,
                position: "bottom"
            },
            "hover": {
                "animationDuration":0 
            },
            "animation": {
                "duration": 800,
                "onComplete": function() {
                  var chartInstance = this.chart,
                    ctx = chartInstance.ctx;
   
                  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                  ctx.textAlign = 'center';
                  ctx.textBaseline = 'bottom';
                  ctx.fillStyle = "black";
   
                  this.data.datasets.forEach(function(dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function(bar, index) {
                      var data = dataset.data[index];
                      ctx.fillText(data, bar._model.x, bar._model.y - 5);
                    });
                  });
                }
              },
        };

        for (var key in status_list) {
            if (!status_list.hasOwnProperty(key)) continue;
            var obj = status_list[key];
            $scope.labels.push(obj);
        }
        window.setInterval(function(){
            if(lead_funel_generate == 0){
                $timeout(function () {
                    $scope.series = lead_funel_series();
                    $scope.data = lead_funel_data();
                }, 100);
            }
        }, 1000);
    });

    //Convert
    angular.module('app')
    .controller('lead_convert_1', function ($scope, $timeout) {
        $scope.labels = [];
        $scope.data = [];
        //$scope.series = [];

        $scope.options = {
            legend: {
                display: true,
                position: "bottom",
            },
            "animation": {
                "duration": 800,
                "onComplete": function() {
                var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
 
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';
                ctx.fillStyle = "black";
 
                this.data.datasets.forEach(function(dataset, i) {
                  var meta = chartInstance.controller.getDatasetMeta(i);
                  meta.data.forEach(function(bar, index) {
                    var data = dataset.data[index];
                    var midAngle =  bar._model.startAngle + (bar._model.endAngle - bar._model.startAngle) / 2;
                    var labelRadius = bar._model.outerRadius * 0.5;
                    var x = bar._model.x + (labelRadius) * Math.cos(midAngle);
                    var y = bar._model.y + (labelRadius) * Math.sin(midAngle);
                    ctx.fillText(data, x, y);
                  });
                });
              }
            },
            //title: { display: true, text: 'Legend', position: 'bottom', padding: 5 }
        };

        $scope.labels.push('Đang được chăm sóc');
        $scope.labels.push('Đã chuyển đổi');
        $scope.labels.push('Bị mất');
        $scope.colors = [
           "rgb(255,195,0)","rgb(0,0,255)","rgb(255,0,0)"
        ];

        window.setInterval(function(){
            if(lead_convert1_generate == 0){
                $timeout(function () {
                    //$scope.series = lead_convert1_series();
                    $scope.data = lead_convert1_data();
                }, 100);
            }
        }, 1000);
    });

    angular.module('app')
    .controller('lead_convert_2', function ($scope, $timeout) {
        $scope.labels = [];
        $scope.data = [];
        //$scope.series = [];

        $scope.options = {
            legend: {
                display: true,
                position: "bottom"
            },
            "animation": {
                "duration": 800,
                "onComplete": function() {
                var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
 
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';
                ctx.fillStyle = "black";
 
                this.data.datasets.forEach(function(dataset, i) {
                  var meta = chartInstance.controller.getDatasetMeta(i);
                  meta.data.forEach(function(bar, index) {
                    var data = dataset.data[index];
                    var midAngle =  bar._model.startAngle + (bar._model.endAngle - bar._model.startAngle) / 2;
                    var labelRadius = bar._model.outerRadius * 0.5;
                    var x = bar._model.x + (labelRadius) * Math.cos(midAngle);
                    var y = bar._model.y + (labelRadius) * Math.sin(midAngle);
                    ctx.fillText(data+"%", x, y);
                  });
                });
              }
            },
        };

        $scope.labels.push('Tỉ lệ chuyển đổi');
        $scope.labels.push('Tỉ lệ bị mất');
        $scope.labels.push('Khác');
        $scope.colors = [
            "rgb(0,0,255)","rgb(255,0,0)","rgb(155,193,200)"
         ];
        window.setInterval(function(){
            if(lead_convert2_generate == 0){
                $timeout(function () {
                    //$scope.series = lead_convert2_series();
                    $scope.data = lead_convert2_data();
                }, 100);
            }
        }, 1000);
    });

    //Performance
    angular.module('app')
    .controller('lead_performance', function ($scope, $timeout) {
        $scope.labels = [];
        $scope.data = [];
        $scope.series = [];

        $scope.options = {
            legend: {
                display: true,
                position: "bottom"
            },
            "hover": {
                "animationDuration": 0
              },
            "animation": {
                "duration": 800,
                "onComplete": function() {
                  var chartInstance = this.chart,
                    ctx = chartInstance.ctx;
   
                  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                  ctx.textAlign = 'center';
                  ctx.textBaseline = 'bottom';
                  ctx.fillStyle = "black";
   
                  this.data.datasets.forEach(function(dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function(bar, index) {
                      var data = dataset.data[index];
                      ctx.fillText(data, bar._model.x, bar._model.y - 5);
                    });
                  });
                }
            },
        };

        for (var key in status_list) {
            if (!status_list.hasOwnProperty(key)) continue;
            var obj = status_list[key];
            $scope.labels.push(obj);
        }
        window.setInterval(function(){
            if(lead_performance_generate == 0){
                $timeout(function () {
                    $scope.series = lead_performance_series();
                    $scope.data = lead_performance_data();
                }, 100);
            }
        }, 1000);
    });

    angular.element(document).ready(function(){
    angular.bootstrap(document, ['app']);
    });

    //Funel
    var date = new Date();
    date.setDate(date.getDate() - 8);
    var lastweek = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('#lead_funel_from').datepicker({
        format: 'mm/dd/yyyy'
    });
    $('#lead_funel_from').datepicker('setDate', lastweek);

    var date = new Date();
    date.setDate(date.getDate() - 1);
    var yesterday = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('#lead_funel_to').datepicker({
        format: 'mm/dd/yyyy'
    });
    $('#lead_funel_to').datepicker('setDate', yesterday);

    //Performance
    var date = new Date();
    date.setDate(date.getDate() - 8);
    var lastweek = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('#lead_performance_from').datepicker({
        format: 'mm/dd/yyyy'
    });
    $('#lead_performance_from').datepicker('setDate', lastweek);

    var date = new Date();
    date.setDate(date.getDate() - 1);
    var yesterday = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('#lead_performance_to').datepicker({
        format: 'mm/dd/yyyy'
    });
    $('#lead_performance_to').datepicker('setDate', yesterday);

    //Convert
    var date = new Date();
    date.setDate(date.getDate() - 8);
    var lastweek = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('#lead_convert_from').datepicker({
        format: 'mm/dd/yyyy'
    });
    //$('#lead_convert_from').datepicker('setDate', lastweek);

    var date = new Date();
    date.setDate(date.getDate() - 1);
    var yesterday = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('#lead_convert_to').datepicker({
        format: 'mm/dd/yyyy'
    });
    //$('#lead_convert_to').datepicker('setDate', yesterday);

    select2_employee();
    select2_employee_performance();
    select2_employee_convert();
});

function select2_employee() {
    $('#lead_funel_employee')
        .select2({
            allowClear: true,
            multiple: true,
            placeholder: 'Select employee...',
            ajax: {
                url: 'index.php?entryPoint=vinhomes_leads&subaction=getEmployee',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
            } else {
            }
        }
    );
}

function select2_employee_performance() {
    $('#lead_performance_employee')
        .select2({
            allowClear: true,
            multiple: true,
            placeholder: 'Select employee...',
            ajax: {
                url: 'index.php?entryPoint=vinhomes_leads&subaction=getEmployee',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
            } else {
            }
        }
    );
}

function select2_employee_convert() {
    $('#lead_convert_employee')
        .select2({
            allowClear: true,
            multiple: true,
            placeholder: 'Select employee...',
            ajax: {
                url: 'index.php?entryPoint=vinhomes_leads&subaction=getEmployee',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
            } else {
            }
        }
    );
}

function lead_funel_data(){
    var temp = [];
    var r = [];
    if(lead_funel_scope_data.length == 0){
        for (var key2 in status_list) {
            if (!status_list.hasOwnProperty(key2)) continue;
            temp.push(0);
        }
        r.push(temp);
    }
    else{
        lead_funel_generate = 1;
        return lead_funel_scope_data;
    }
    lead_funel_generate = 1;
    return r;
}

function lead_funel_series(){
    var r = [];
    if(lead_funel_scope_series.length == 0){
        r.push('');
    }
    else{
        return lead_funel_scope_series;
    }
    return r;
}

function viewReportLeadFunel(){
    $("#lead_funel_from").css("border-color", "#c4e3f5");
    $("#lead_funel_to").css("border-color", "#c4e3f5");
    if($('#lead_funel_from').val() == ""){
        $("#lead_funel_from").css("border-color", "red");
        //return;
    }
    else if($('#lead_funel_to').val() == ""){
        $("#lead_funel_to").css("border-color", "red");
        //return;
    }
    else{
        SUGAR.ajaxUI.showLoadingPanel();
        setTimeout(function(){ 
            $.ajax({
                url: "index.php?entryPoint=vinhomes_leads&subaction=viewLeadFunelReport",
                data: { vin_csrf_token: vin_csrf_token, 
                    from: $('#lead_funel_from').val(),
                    to: $('#lead_funel_to').val(),
                    employee: $('#lead_funel_employee').val(),
                    },
                method: "POST",
                success: function (result) {
                    if(typeof status_list == "object" && typeof result == "object"){
                        SUGAR.ajaxUI.hideLoadingPanel(); 

                        var series = [];
                        for (var key in result) {
                            if (!result.hasOwnProperty(key)) continue;
                            series.push(key);
                        }

                        var data = [];
                        for (var key in result) {
                            if (!result.hasOwnProperty(key)) continue;
                            var obj = result[key];
                            var temp = [];
                            for (var key2 in status_list) {
                                if (!status_list.hasOwnProperty(key2)) continue;
                                var count = 0;
                                for (var prop in obj) {
                                    if (!obj.hasOwnProperty(prop)) continue;
                                    if(key2 == prop){
                                        count = obj[prop];
                                    }
                                }
                                temp.push(count);
                            }
                            data.push(temp);
                        }
                        lead_funel_scope_series = series;
                        lead_funel_scope_data = data;
                        lead_funel_generate = 0;
                    }
                },
                error: function() {
                    alert('Error occured!');
                    SUGAR.ajaxUI.hideLoadingPanel(); 
                }
            });
        }, 0);
    }
}

function lead_performance_data(){
    var temp = [];
    var r = [];
    if(lead_performance_scope_data.length == 0){
        for (var key2 in status_list) {
            if (!status_list.hasOwnProperty(key2)) continue;
            temp.push(0);
        }
        r.push(temp);
    }
    else{
        lead_performance_generate = 1;
        return lead_performance_scope_data;
    }
    lead_performance_generate = 1;
    return r;
}

function lead_performance_series(){
    var r = [];
    if(lead_performance_scope_series.length == 0){
        r.push('');
    }
    else{
        return lead_performance_scope_series;
    }
    return r;
}

function viewReportLeadPerformance(){
    $("#lead_performance_from").css("border-color", "#c4e3f5");
    $("#lead_performance_to").css("border-color", "#c4e3f5");
    if($('#lead_performance_from').val() == ""){
        $("#lead_performance_from").css("border-color", "red");
        return;
    }
    else if($('#lead_performance_to').val() == ""){
        $("#lead_performance_to").css("border-color", "red");
        return;
    }
    else{
        SUGAR.ajaxUI.showLoadingPanel();
        setTimeout(function(){ 
            $.ajax({
                url: "index.php?entryPoint=vinhomes_leads&subaction=viewLeadPerformanceReport",
                data: { vin_csrf_token: vin_csrf_token, 
                    from: $('#lead_performance_from').val(),
                    to: $('#lead_performance_to').val(),
                    employee: $('#lead_performance_employee').val(),
                    },
                method: "POST",
                success: function (result) {
                    if(typeof status_list == "object" && typeof result == "object"){
                        SUGAR.ajaxUI.hideLoadingPanel(); 

                        var series = [];
                        for (var key in result) {
                            if (!result.hasOwnProperty(key)) continue;
                            series.push(key);
                        }

                        var data = [];
                        for (var key in result) {
                            if (!result.hasOwnProperty(key)) continue;
                            var obj = result[key];
                            var temp = [];
                            for (var key2 in status_list) {
                                if (!status_list.hasOwnProperty(key2)) continue;
                                var count = 0;
                                for (var prop in obj) {
                                    if (!obj.hasOwnProperty(prop)) continue;
                                    if(key2 == prop){
                                        count = obj[prop];
                                    }
                                }
                                temp.push(count);
                            }
                            data.push(temp);
                        }
                        lead_performance_scope_series = series;
                        lead_performance_scope_data = data;
                        lead_performance_generate = 0;
                    }
                },
                error: function() {
                    alert('Error occured!');
                    SUGAR.ajaxUI.hideLoadingPanel(); 
                }
            });
        }, 0);
    }
}

function lead_convert1_data(){
    var temp = [];
    var r = [];
    if(lead_convert1_scope_data.length == 0){

    }
    else{
        lead_convert1_generate = 1;
        return lead_convert1_scope_data;
    }
    lead_convert1_generate = 1;
    return r;
}

function lead_convert1_series(){
    var r = [];
    if(lead_convert1_scope_series.length == 0){
        r.push('');
    }
    else{
        return lead_convert1_scope_series;
    }
    return r;
}

function lead_convert2_data(){
    var temp = [];
    var r = [];
    if(lead_convert2_scope_data.length == 0){

    }
    else{
        lead_convert2_generate = 1;
        return lead_convert2_scope_data;
    }
    lead_convert2_generate = 1;
    return r;
}

function lead_convert2_series(){
    var r = [];
    if(lead_convert2_scope_series.length == 0){
        r.push('');
    }
    else{
        return lead_convert2_scope_series;
    }
    return r;
}

function viewReportLeadConvert(){
    SUGAR.ajaxUI.showLoadingPanel();
    setTimeout(function(){ 
        $.ajax({
            url: "index.php?entryPoint=vinhomes_leads&subaction=viewLeadConvertReport",
            data: { vin_csrf_token: vin_csrf_token, 
                from: $('#lead_convert_from').val(),
                to: $('#lead_convert_to').val(),
                employee: $('#lead_convert_employee').val(),
                },
            method: "POST",
            success: function (result) {
                if(typeof status_list == "object" && typeof result == "object"){
                    SUGAR.ajaxUI.hideLoadingPanel(); 

                    var s1 = 0;
                    var s2 = 0;
                    var s3 = 0;
                    var g1 = 0;
                    var g2 = 0;
                    var g3 = 0;
                    var total = 0;
                    for (var key in result) {
                        if (!result.hasOwnProperty(key)) continue;
                        var obj = result[key];
                        for (var prop in obj) {
                            if (!obj.hasOwnProperty(prop)) continue;
                            if(prop == 'assigned'){
                                s1 = Number(s1) + Number(obj[prop]);
                            }
                            else if(prop == 'converted'){
                                s2 = Number(s2) + Number(obj[prop]);
                            }
                            else if(prop == 'unqualified'){
                                s3 = Number(s3) + Number(obj[prop]);
                            }
                            total = Number(total) + Number(obj[prop]);
                        }
                    }

                    g1 = Number((s2/total)*100).toFixed(2);
                    g2 = Number((s3/total)*100).toFixed(2);
                    g3 = Number(100 - g1 - g2).toFixed(2);

                    lead_convert1_scope_data = [s1,s2,s3];
                    lead_convert2_scope_data = [g1,g2,g3];
                    lead_convert1_generate = 0;
                    lead_convert2_generate = 0;
                }
            },
            error: function() {
                alert('Error occured!');
                SUGAR.ajaxUI.hideLoadingPanel(); 
            }
        });
    }, 0);
}

