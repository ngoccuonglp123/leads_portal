$(document).ready(function () {
    //CSRF TOKEN
    if (typeof vin_csrf_token !== 'undefined'){
        if(vin_csrf_token != "" && vin_csrf_token != null){
            var vin_csrf_token_input = '<input type="hidden" name="vin_csrf_token" value="'+vin_csrf_token+'" />';
            $('form').append(vin_csrf_token_input);
        }
    }

    var arrow_custom = $('#buttontoggle').attr('class');
    if(arrow_custom != null && arrow_custom != undefined){
        if(arrow_custom.indexOf('expanded') !== -1){
            $('#buttontoggle').click();
        }
    }

    //Editview buttons
    if(document.getElementById('EditView') != null){
        if($('.dcQuickEdit').val() != undefined){
            var buttons = $('.dcQuickEdit tbody tr td div').html();
            if(buttons != '' && buttons != undefined){
                buttons = '<div id="fs_custom_header_buttons">' + buttons + '</div>';
                $( buttons ).insertBefore( ".moduleTitle .clear" );
                $('.dcQuickEdit tbody tr td div').html('');
            }
        }
        else{

        }
    }
    else{

    }

    if($('.panel-content').val() != undefined){
        if($('.panel-content div:first').html() == '&nbsp;'){
            $('.panel-content div:first').css('line-height','3px');
        }
    }

    //Detailview created_by/modified_by
    if($('.detail-view').val() != undefined){
        if(document.getElementById('created_by') != null){
            if($('#created_by').html() != '' && $('#created_by').val() != undefined
            && $('#fs_created_by').val() != '' && $('#fs_created_by').val() != undefined){
                $('#created_by').html($('#fs_created_by').val());
            }
        }
        if(document.getElementById('modified_user_id') != null){
            if($('#modified_user_id').html() != '' && $('#modified_user_id').val() != undefined
            && $('#fs_modified_by').val() != '' && $('#fs_modified_by').val() != undefined){
                $('#modified_user_id').html($('#fs_modified_by').val());
            }
        }
    }

    //Detailview tab action
    if(document.getElementById('tab-actions') != null && $('#tab-actions').html() != ''
    && document.getElementById('fs_custom_header_buttons') != null){
        //Edit button edit_button
        if(document.getElementById('edit_button') != null){
            var fs_edit_button = $('#edit_button').parent().html();
            if(fs_edit_button != ''){
                $('#edit_button').parent().html('');
                $('#fs_custom_header_buttons').append(fs_edit_button);
            }
        }

        //Clone button duplicate_button
        if(document.getElementById('duplicate_button') != null){
            $('#duplicate_button').val('Clone');
            var fs_duplicate_button = $('#duplicate_button').parent().html();
            if(fs_duplicate_button != ''){
                $('#duplicate_button').parent().html('');
                $('#fs_custom_header_buttons').append(fs_duplicate_button);
            }
        }

        var actions_tab = $('#tab-actions').html();
        if(actions_tab != ''){
            actions_tab = '<div id="tab-actions" class="dropdown">'+actions_tab+'</div>';
            $('#tab-actions').css('display','none');
            $('#tab-actions').html('');
            $('#tab-actions').prop('id','');

            $('#fs_custom_header_buttons').append(actions_tab);
            $('#fs_custom_header_buttons div').css('background-color','#F2930D');
            $('#fs_custom_header_buttons div').css('height','32px');
            $('#fs_custom_header_buttons div a').html('&nbsp<span class="suitepicon suitepicon-action-caret"></span>');

        }
    }

    //Listview Custom Delete button
    if(document.getElementById('delete_listview_top') != null){
        var new_delete = '';
        new_delete += '<ul class="clickMenu selectmenu searchLink SugarActionMenu listViewLinkButton listViewLinkButton_top">';
        new_delete += '<li class="sugar_action_button">';
        new_delete += '<a href="javascript:void(0)" class="glyphicon glyphicon-trash parent-dropdown-handler" onclick="custom_listview_delete_trigger()" title="Delete"></a>';
        new_delete += '</li>';
        new_delete += '</ul>';
        $('#massall_top').parent().parent().parent().append(new_delete);
    }
    if(document.getElementById('delete_listview_top') != null){
        var new_delete = '';
        new_delete += '<ul class="clickMenu selectmenu searchLink SugarActionMenu listViewLinkButton listViewLinkButton_top">';
        new_delete += '<li class="sugar_action_button">';
        new_delete += '<a href="javascript:void(0)" class="glyphicon glyphicon-trash parent-dropdown-handler" onclick="custom_listview_delete_trigger()" title="Delete"></a>';
        new_delete += '</li>';
        new_delete += '</ul>';
        $('#massall_bottom').parent().parent().parent().append(new_delete);
    }

    //Listview Hide About
    $('#employees_link').parent().css('display','none');
    $('#training_link').parent().css('display','none');
    $('#about_link').parent().css('display','none');
    $('#employees_link').css('display','none');
    $('#training_link').css('display','none');
    $('#about_link').css('display','none');
    
    //Edit module name
    //$('.quickcreatetop ul li a').html('Create Vinhomes Leads');

    //Edit Home
    $('.suitepicon-action-home').prop('href','index.php?module=vin_vin_leads&action=index&parentTab=Sales');

    //Global Search
    var submitUnified = $('form[name=UnifiedSearchAdvancedMain] input[type=submit]');
    submitUnified.prop('type','button');
    submitUnified.click(function(){
        unifiedSearchAdvancedMainCustom();
    });

    //Override remove user
    if($('#formDetailView input[name=module]').val() == "Users"){
        var origConfirmDelete = confirmDelete;
        confirmDelete = function(str) {
            var handleYes = function () {
                SUGAR.util.hrefURL("?module=Users&action=delete&record=" + document.forms.DetailView.record.value+"&vin_csrf_token="+vin_csrf_token);
              };
            
              var handleNo = function () {
                confirmDeletePopup.hide();
                return false;
              };
              var user_portal_group = '{$usertype}';
              var confirm_text = SUGAR.language.get('Users', 'LBL_DELETE_USER_CONFIRM');
              if (user_portal_group == 'GroupUser') {
                confirm_text = SUGAR.language.get('Users', 'LBL_DELETE_GROUP_CONFIRM');
              }
            
              $('#title-generic').html(SUGAR.language.get("Users", "LBL_DELETE_USER"));
              $('.modal-generic .modal-body .container-fluid').html(confirm_text);
              $('#btn-generic').html(SUGAR.language.get("Users", "LBL_OK"));
            
              $('#btn-generic').unbind().click(function(){
                handleYes()
              });
            
              $('.modal-generic').modal('show');
            //return origConfirmDelete(str);
        }
    }

    //Add btn Change Password
    var changePasswordBtn = '<li role="presentation"><a id="fs_change_password" href="index.php?action=ChangePassword&module=Users">Change Password</a></li>';
    $('.globalLinks-desktop #about_link').parent().after(changePasswordBtn);

    //Login Logo A Tag
    $('.p_login_top a').prop('title','Leads Portal');
    $('.p_login_top a').prop('href','#');

    //Quick Create
    $('#quickcreatetop ul li').css('display','none');
    var leads_quick_create = '<li><a href="index.php?module=vin_vin_leads&action=EditView&return_module=vin_vin_leads&return_action=DetailView">Create Vinhomes Leads</a></li>';
    $('#quickcreatetop ul').append(leads_quick_create);

    //All
    $('#All_sp_tab').parent().css('display','none');
});

function custom_listview_delete_trigger(){
    //$('#delete_listview_top').click();
    $('#delete_listview_top[class=parent-dropdown-action-handler]').click();
}

function unifiedSearchAdvancedMainCustom(){
    $('input[name=query_string]').prop('value',$('#searchFieldMain').val());
    $('#searchformdropdown div[class=input-group] span[class=input-group-btn] button[type=submit]').click();
}

