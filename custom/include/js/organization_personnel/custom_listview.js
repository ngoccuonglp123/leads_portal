$(document).ready(function () {

    if(document.getElementById("fs_custom_header_buttons") == null){
        $( '<div id="fs_custom_header_buttons"></div>' ).insertBefore( ".moduleTitle .clear" );
    }

    if(document.getElementById("btn_create_user_button") == null){
        $('#fs_custom_header_buttons').append(btn_create_user_button);
    }

    var custom_list_view = $('#custom_list_view_organization_personnel').clone();
    $('#custom_list_view_organization_personnel').remove();
    $('.listViewBody').append(custom_list_view).html();
    select2_sales_admin();
    onChangeType();


});

function custom_create_user(){
    refreshPopupCreateUser();
    $('#cs-modal-title').html(SUGAR.language.get('vin_vin_organization_personnel', 'LBL_CREATE_USER'));
    $("#create_user_modal").modal('show');
}

function create_user_submit(e){

    $('#create_user_name').css('border','none');
    $('#create_user_last_name').css('border','none');
    $('#create_user_email').css('border','none');
    $('#create_user_password').css('border','none');
    $('#create_user_repassword').css('border','none');
    $('#create_user_first_name').css('border','none');
    $('#create_user_mobile').css('border','none');
    $('#create_user_employee_id').css('border','none');
    $('#create_user_msg').html('');

    var valid = 1;
    if(trim($('#create_user_name').val()) == ''){
        $('#create_user_name').css('border','1px solid red');
        valid = 0;
    }
    else{
        if(trim($('#create_user_name').val()).length < 3){
            $('#create_user_msg').html('Username is too short.');
            valid = 0;
        }
    }
    if(trim($('#create_user_last_name').val()) == ''){
        $('#create_user_last_name').css('border','1px solid red');
        valid = 0;
    }
    if(trim($('#create_user_email').val()) == ''){
        $('#create_user_email').css('border','1px solid red');
        valid = 0;
    }
    else{
        if (!validateEmail(trim($("#create_user_email").val()))) {
            $('#create_user_email').css('border','1px solid red');
            $('#create_user_msg').html('Email is wrong');
            valid = 0;
        }
    }

    if($('#create_user_id').val() == '' || $('#create_user_id').val() == null){
        if(trim($('#create_user_password').val()) == ''){
            $('#create_user_password').css('border','1px solid red');
            valid = 0;
        }
        if(trim($('#create_user_repassword').val()) == ''){
            $('#create_user_repassword').css('border','1px solid red');
            valid = 0;
        }
    
        if(trim($('#create_user_password').val()) != trim($('#create_user_repassword').val())){
            $('#create_user_repassword').css('border','1px solid red');
            $('#create_user_msg').html('Password and Re-Password does not match');
            valid = 0;
        }
    }
    else if(trim($('#create_user_password').val()) != ''){
        if(trim($('#create_user_password').val()) != trim($('#create_user_repassword').val())){
            $('#create_user_repassword').css('border','1px solid red');
            $('#create_user_msg').html('Password and Re-Password does not match');
            valid = 0;
        }
    }

    if(trim($('#create_user_first_name').val()) == ''){
        $('#create_user_first_name').css('border','1px solid red');
        valid = 0;
    }
    if(trim($('#create_user_mobile').val()) == ''){
        $('#create_user_mobile').css('border','1px solid red');
        valid = 0;
    }
    if(trim($('#create_user_employee_id').val()) == ''){
        $('#create_user_employee_id').css('border','1px solid red');
        valid = 0;
    }

    if(valid == 1){
        $("#loaderModal").modal('show');
        setTimeout(function(){ 
            $.ajax({
                url: "index.php?entryPoint=organization_personnel&subaction=createUser",
                data: { 
                    type: trim($('#create_user_type').val()), 
                    user_name: trim($('#create_user_name').val()), 
                    last_name: trim($('#create_user_last_name').val()), 
                    email: trim($('#create_user_email').val()), 
                    password: trim($('#create_user_password').val()), 
                    sales_admin: $('#create_user_sales_admin').val(),
                    mobile: $('#create_user_mobile').val(),
                    employee_id: $('#create_user_employee_id').val(),
                    status: $('#create_user_status').val(),
                    vin_csrf_token: vin_csrf_token,
                    user_id: $('#create_user_id').val(),
                    first_name: trim($('#create_user_first_name').val())
                },
                method: "POST",
                success: function (result) {
                    $("#loaderModal").modal('hide');
                    if(result == '1' || result == 1){
                        $('#create_user_msg').html('Username has been exited.');
                    }
                    else if(result == '2' || result == 2){
                        $("#loaderModal").modal('hide');
                        if(e == 1 || e == '1'){
                            $("#create_user_modal").modal('hide');
                            location.reload();
                        }
                        else{
                            refreshPopupCreateUser();
                        }
                    }
                    else if(result != '' && result != null){
                        $('#create_user_msg').html(result);
                        $('#create_user_password').css('border','1px solid red');
                        $('#create_user_repassword').css('border','1px solid red');
                    }
                    else{
                        $("#loaderModal").modal('hide');
                        $("#create_user_modal").modal('hide');
                        if(e == 1 || e == '1'){
                            location.reload();
                        }
                        else{
                            refreshPopupCreateUser();
                        }
                    }
                },
                error: function() {
                    $("#loaderModal").modal('hide');
                    alert('Error occured!');
                }
            });
        }, 0);
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function select2_sales_admin() {
    $('#create_user_sales_admin')
        .select2({
            allowClear: true,
            dropdownParent: $("#create_user_modal"),
            placeholder: 'Select a user...',
            ajax: {
                url: 'index.php?entryPoint=organization_personnel&subaction=getSalesAdmin',
                dataType: 'json',
                data: function (term) {
                    var query = {
                        vin_csrf_token: vin_csrf_token,
                        q: term
                    }
                    return query;
                },
                allowClear: true,
                results: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        })
        .on('change', function (obj) {
            if (obj.added) {
            } else {
            }
        }
    );
}

function onChangeType(){
    if($('#custom_role').val() != '1'){
        $('#create_user_sales_admin_lable').css('display','none')
        $('#s2id_create_user_sales_admin').css('display','none')
        $('#create_user_type').css('display','none')
        $('#create_user_type_lable').css('display','none')
    }
    else{
        if($('#create_user_type').val() == '2'){
            $('#s2id_create_user_sales_admin').css('display','inline-block')
            $('#create_user_sales_admin_lable').css('display','inline-block')
        }
        else{
            $('#s2id_create_user_sales_admin').css('display','none')
            $('#create_user_sales_admin_lable').css('display','none')
        }
    }
}

function refreshPopupCreateUser(){
    $('#create_user_name').css('border','none');
    $('#create_user_last_name').css('border','none');
    $('#create_user_email').css('border','none');
    $('#create_user_password').css('border','none');
    $('#create_user_repassword').css('border','none');
    $('#create_user_first_name').css('border','none');
    $('#create_user_mobile').css('border','none');
    $('#create_user_employee_id').css('border','none');
    $('#create_user_msg').html('');

    $('#create_user_name').val('');
    $('#create_user_name').removeAttr("disabled");
    $('#create_user_last_name').val('');
    $('#create_user_email').val('');
    $('#create_user_password').val('');
    $('#create_user_repassword').val('');
    $('#create_user_first_name').val('');
    $('#create_user_mobile').val('');
    $('#create_user_employee_id').val('');
    $('#create_user_id').val('');

    $('#create_user_type').val('1');
    $('#create_user_status').val('Active');
    $('#create_user_sales_admin').val('');
    $("#create_user_sales_admin").select2("data", null);
    onChangeType();

}

function custom_edit_user(id){
    refreshPopupCreateUser();
    $("#loaderModal").modal('show');
    setTimeout(function(){ 
        $.ajax({
            url: "index.php?entryPoint=organization_personnel&subaction=loadUser",
            data: { 
                id: id, 
                vin_csrf_token: vin_csrf_token
            },
            method: "POST",
            success: function (result) {
                $("#loaderModal").modal('hide');
                $('#create_user_name').val(result.user_name);
                $('#create_user_last_name').val(result.last_name);
                $('#create_user_email').val(result.user_email);
                $('#create_user_password').val('');
                $('#create_user_repassword').val('');
                $('#create_user_first_name').val(result.first_name);
                $('#create_user_mobile').val(result.phone_mobile);
                $('#create_user_employee_id').val(result.vin_employee_id_c);
                $('#create_user_status').val(result.status);
                $('#create_user_id').val(result.id)
                $('#create_user_sales_admin').val('');
                if(result.user_id_c != null && result.user_id_c != ''){
                    $("#create_user_sales_admin").select2("data", {
                        id: result.user_id_c,
                        text: result.vin_sale_admin_c
                    });

                    $('#create_user_type').val('2');
                }
                else{
                    $('#create_user_type').val('1');
                }
                $('#create_user_name').attr('disabled', 'disabled');
                onChangeType();
                $('#cs-modal-title').html(SUGAR.language.get('vin_vin_organization_personnel', 'LBL_EDIT_USER'));
                $("#create_user_modal").modal('show');
            },
            error: function() {
                $("#loaderModal").modal('hide');
                alert('Error occured!');
            }
        });
    }, 0);
}

function custom_delete_user(id, user_name){
    var confirmChangeStatus = confirm('Are you sure delete this user ('+user_name+')? \nDeleting a User record cannot be undone.');
    if (confirmChangeStatus == true) {
        $("#loaderModal").modal('show');
        setTimeout(function(){ 
            $.ajax({
                url: "index.php?entryPoint=organization_personnel&subaction=deleteUser",
                data: { 
                    id: id, 
                    vin_csrf_token: vin_csrf_token
                },
                method: "POST",
                success: function (result) {
                    $("#loaderModal").modal('hide');
                    if(result == '1' || result == 1){
                        location.reload();
                    }
                    else{
                        alert(result);
                    }
                },
                error: function() {
                    $("#loaderModal").modal('hide');
                    alert('Error occured!');
                }
            });
        }, 0);
    }
}