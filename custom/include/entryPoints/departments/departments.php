<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('custom/include/global_function/fs_generate_cache.php');

global $db, $current_user, $app_list_strings;
$subAction = $_REQUEST["subaction"];

$digital = 0;
$list_roles = getRoleByUser($current_user->id);
foreach($list_roles as $key => $value){
    if($value == 'DIGITAL_MARKETING'){
        $digital = 1;
    }
}
if($current_user->is_admin != 1 && $digital != 1){
    echo "You dont have permission to access this site.";
    die;
}

if($subAction == "loadSource"){
    $fs_generate_cache = new fs_generate_cache;
    echo '<link href="'.$fs_generate_cache->get_cache_version('custom/include/css/departments/departments.css','').'" rel="stylesheet">';
    echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/library/rowspanizer/jquery.rowspanizer.js','').'"></script>';
    echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/library/rowspanizer/jquery.rowspanizer.min.js','').'"></script>';
    if(session_id() != '' && isset($_SESSION)) {
        if(isset($_SESSION["VIN_CSRF_TOKEN"])){
            echo '
                <script>
                    var vin_csrf_token = "'.$_SESSION["VIN_CSRF_TOKEN"].'";
                </script>
            ';
        }
    }

    //Department
    $querySearch = "SELECT id, name FROM vin_vin_department WHERE deleted = 0 ORDER BY name;";
    $result = $db->query($querySearch);
    $dataSet = array();
    $row = $db->fetchByAssoc($result);
    while ($row != null){
        $dataSet[] = $row;
        $row = $db->fetchByAssoc($result);
    }
    $departments_list = 'var departments_list = {';
    $count = 0;
    foreach($dataSet as $key => $value)
    {
        $departments_list .= $count.':{';
        $departments_list .= 'id: "'.$value["id"].'",';
        $departments_list .= 'name: "'.$value["name"].'"';
        $departments_list .= '},';
        $count = $count + 1;
    }
    $departments_list .= '};';
    echo "
        <script>
            ".$departments_list."
        </script>
    ";

    //Source
    $allocate_source = 'var allocate_source = {';
    $count = 0;
    foreach($app_list_strings['allocate_dept_source'] as $key => $value)
    {
        $allocate_source .= $count.':{';
        $allocate_source .= 'key: "'.$key.'",';
        $allocate_source .= 'value: "'.$value.'"';
        $allocate_source .= '},';
        $count = $count + 1;
    }
    $allocate_source .= '};';
    echo "
        <script>
            ".$allocate_source."
        </script>
    ";

    //Level 1
    $allocate_level1= 'var allocate_level1 = {';
    $count = 0;
    foreach($app_list_strings['allocate_dept_level1'] as $key => $value)
    {
        $allocate_level1 .= $count.':{';
        $allocate_level1 .= 'key: "'.$key.'",';
        $allocate_level1 .= 'value: "'.$value.'"';
        $allocate_level1 .= '},';
        $count = $count + 1;
    }
    $allocate_level1 .= '};';
    echo "
        <script>
            ".$allocate_level1."
        </script>
    ";

    //Level 2
    $allocate_level2 = 'var allocate_level2 = {';
    $count = 0;
    foreach($app_list_strings['allocate_dept_level2'] as $key => $value)
    {
        $allocate_level2 .= $count.':{';
        $allocate_level2 .= 'key: "'.$key.'",';
        $allocate_level2 .= 'value: "'.$value.'"';
        $allocate_level2 .= '},';
        $count = $count + 1;
    }
    $allocate_level2 .= '};';
    echo "
        <script>
            ".$allocate_level2."
        </script>
    ";

    //Level 3
    $allocate_level3 = 'var allocate_level3 = {';
    $count = 0;
    foreach($app_list_strings['allocate_dept_level3'] as $key => $value)
    {
        $allocate_level3 .= $count.':{';
        $allocate_level3 .= 'key: "'.$key.'",';
        $allocate_level3 .= 'value: "'.$value.'"';
        $allocate_level3 .= '},';
        $count = $count + 1;
    }
    $allocate_level3 .= '};';
    echo "
        <script>
            ".$allocate_level3."
        </script>
    ";

    //Current Allocate
    $querySearch = "SELECT a.*, b.index FROM vin_allocate_matrix a
        LEFT JOIN vin_allocate_flow b ON a.id = b.matrix_id
        WHERE a.deleted = 0;";
    $result = $db->query($querySearch);
    $dataSet = array();
    $row = $db->fetchByAssoc($result);
    while ($row != null){
        $dataSet[] = $row;
        $row = $db->fetchByAssoc($result);
    }
    $allocate_list = 'var allocate_list = {';
    $count = 0;
    foreach($dataSet as $key => $value)
    {
        $allocate_list .= $count.':{';
        $allocate_list .= 'id: "'.$value["id"].'",';
        $allocate_list .= 'index: '.$count.',';
        $allocate_list .= 'source: "'.$value["source"].'",';
        $allocate_list .= 'source_disp: "'.$app_list_strings['allocate_dept_source'][$value["source"]].'",';
        $allocate_list .= 'level1: "'.$value["level1"].'",';
        $allocate_list .= 'level1_disp: "'.$app_list_strings['allocate_dept_level1'][$value["level1"]].'",';
        $allocate_list .= 'level2: "'.$value["level2"].'",';
        $allocate_list .= 'level2_disp: "'.$app_list_strings['allocate_dept_level2'][$value["level2"]].'",';
        $allocate_list .= 'level3: "'.$value["level3"].'",';
        $allocate_list .= 'level3_disp: "'.$app_list_strings['allocate_dept_level3'][$value["level3"]].'",';
        $allocate_list .= 'allocate: "'.$value["allocate"].'",';
        $allocate_list .= 'current_index: "'.$value["index"].'"';
        $allocate_list .= '},';
        $count = $count + 1;
    }
    $allocate_list .= '};';
    echo "
        <script>
            ".$allocate_list."
        </script>
    ";

    die;
}
else{
    if(!csrf_valid_token()){
        echo "CSRF BLOCKED.";
        die;
    }
}

if($subAction != null && $subAction != "")
{
    switch ($subAction) {
        case "save_allocate_matrix":
            $return = array();

            $data = $_REQUEST["data"];
            if($data == '' || $data == null || !is_array($data)){
            }
            else{
                foreach ($data as $key => $value) {
                    if($value["id"] == "" || $value["id"] == null){
                        $query_insert = "INSERT INTO vin_allocate_matrix (id,source,level1,level2,
                                level3,allocate,date_created,created_by,date_modified,modified_by,deleted)
                            VALUES ((SELECT UUID()),'".$value["source"]."','".$value["level1"]."','".$value["level2"]."','".$value["level3"]."',
                                '".$value["allocate"]."',NOW(),'".$current_user->id."', NOW(), '".$current_user->id."', 0);";
                        $db->query($query_insert);
                    }
                    else{
                        $query_update = " UPDATE vin_allocate_matrix SET 
                            allocate = '".$value["allocate"]."' ,date_modified = NOW(),modified_by = '".$current_user->id."'
                            WHERE id = '".$value["id"]."' AND allocate <> '".$value["allocate"]."';";
                        $db->query($query_update);
                    }
                }
            }

            //Current Allocate
            $querySearch = "SELECT a.*, b.index FROM vin_allocate_matrix a
                        LEFT JOIN vin_allocate_flow b ON a.id = b.matrix_id
                        WHERE a.deleted = 0;";
            $result = $db->query($querySearch);
            $dataSet = array();
            $row = $db->fetchByAssoc($result);
            while ($row != null){
                $dataSet[] = $row;
                $row = $db->fetchByAssoc($result);
            }
            $count = 0;
            foreach($dataSet as $key => $value)
            {
                $return[$count]["id"] = $value["id"];
                $return[$count]["index"] = $count;
                $return[$count]["source"] = $value["source"];
                $return[$count]["source_disp"] = $app_list_strings['allocate_dept_source'][$value["source"]];
                $return[$count]["level1"] = $value["level1"];
                $return[$count]["level1_disp"] = $app_list_strings['allocate_dept_level1'][$value["level1"]];
                $return[$count]["level2"] = $value["level2"];
                $return[$count]["level2_disp"] = $app_list_strings['allocate_dept_level2'][$value["level2"]];
                $return[$count]["level3"] = $value["level3"];
                $return[$count]["level3_disp"] = $app_list_strings['allocate_dept_level3'][$value["level3"]];
                $return[$count]["allocate"] = $value["allocate"];
                $return[$count]["current_index"] = $value["index"];
                $count = $count + 1;
            }

            ob_start();
            ob_clean();
            header('Content-type: application/json');
            echo json_encode($return);
            break;
        default:
            echo "Something went wrong! Can not find Action!";
    }
}

function csrf_valid_token(){
    if(session_id() != '' && isset($_SESSION)) {
        if(isset($_SESSION["VIN_CSRF_TOKEN"])){
            if(!isset($_REQUEST)){
                return false;
            }
            if(!isset($_REQUEST['vin_csrf_token'])){
                return false;
            }
            else{
                if($_REQUEST['vin_csrf_token'] == null || $_REQUEST['vin_csrf_token'] == ''){
                    return false;
                }
                else if($_REQUEST['vin_csrf_token'] != $_SESSION["VIN_CSRF_TOKEN"]){
                    return false;
                }
            }
        }
    }
    return true;
}

function getRoleByUser($id){
    global $db, $current_user;

    $list_roles = array();
    $query = "SELECT acl.name FROM acl_roles acl 
        JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
        JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
        JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
        WHERE acl.deleted = 0 AND sgu.user_id = '".$id."';";

    $result = $db->query($query);
    $dataSet = array();
    $row = $db->fetchByAssoc($result);
    while ($row != null){
        $dataSet[] = $row;
        $row = $db->fetchByAssoc($result);
    }
    foreach($dataSet as $key => $value){
        array_push($list_roles,$value['name']);
    }
    return $list_roles;
}
