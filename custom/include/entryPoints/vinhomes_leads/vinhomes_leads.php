<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
global $db, $current_user, $app_list_strings;

if(!csrf_valid_token() && $_REQUEST["subaction"] != 'loadSource'  && $_REQUEST["subaction"] != 'loadNotification'){
    echo "CSRF BLOCKED.";
    die;
}

$subAction = $_REQUEST["subaction"];
if($subAction != null && $subAction != "")
{
    switch ($subAction) {
        case "getSaleAgent":
            $bm = 0;
            $sa = 0;
            $sg = 0;
            $query = "";
            $list_roles = getRoleByUser($current_user->id);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
            }
            if($bm == 1){
                $query = "select DISTINCT u.id, u.user_name as text 
                from users u
                left join users_cstm uc ON u.id=uc.id_c
                where u.deleted = 0 AND u.status = 'Active' 
                AND (
                    uc.user_id1_c = '".$current_user->id."'
                    OR uc.user_id_c IN (
                        select u2.id
                        from users u2
                        left join users_cstm uc2 ON u2.id=uc2.id_c
                        where u2.deleted = 0 AND u2.status = 'Active' 
                        AND uc2.user_id1_c = '".$current_user->id."'
                    )
                    OR (uc.user_id1_c IS NULL AND uc.user_id_c IS NULL)
                    OR (uc.user_id1_c IS NULL AND uc.user_id_c = '')
                    OR (uc.user_id1_c = '' AND uc.user_id_c = '')
                    OR (uc.user_id1_c = '' AND uc.user_id_c  IS NULL)
                )
                AND u.is_admin <> 1
                AND u.user_name LIKE '%".$_GET['q']."%' 
                ORDER BY u.user_name LIMIT 30";
            }
            else if($sa == 1){
                $bus_mana = getBusinessManagementUser($current_user->id);
                // $query = "select u.id, u.user_name as text 
                //         from users u
                //         left join users_cstm uc ON u.id=uc.id_c
                //         where u.deleted = 0 AND u.status = 'Active' 
                //         AND (
                //             uc.user_id1_c = '".$bus_mana."'
                //             OR uc.user_id_c = '".$current_user->id."'
                //             OR u.id = '".$current_user->id."'
                //             )
                //         AND u.user_name LIKE '%".$_GET['q']."%' 
                //         ORDER BY u.user_name LIMIT 30";
                $query = "select u.id, u.user_name as text 
                        from users u
                        left join users_cstm uc ON u.id=uc.id_c
                        where u.deleted = 0 AND u.status = 'Active' 
                        AND (
                            uc.user_id_c = '".$current_user->id."'
                            OR u.id = '".$current_user->id."'
                            )
                        AND u.user_name LIKE '%".$_GET['q']."%' 
                        ORDER BY u.user_name LIMIT 30";
            }
            else if($sg == 1){
                $sale_admin = getSaleAdminUser($current_user->id);
                $query = "select u.id, u.user_name as text 
                        from users u
                        left join users_cstm uc ON u.id=uc.id_c
                        where u.deleted = 0 AND u.status = 'Active' 
                        AND (
                            uc.user_id_c = '".$sale_admin."'
                            OR u.id = '".$current_user->id."'
                            )
                        AND u.user_name LIKE '%".$_GET['q']."%' 
                        ORDER BY u.user_name LIMIT 30";
            }


            $bean = new SugarBean();
            $result = $bean->db->query($query,true," Error filling in additional detail fields: ");
            $rows = array();

            while($row=$bean->db->fetchByAssoc($result) ) 
            {
                $rows[] = $row;
            }
            ob_start();
            ob_clean();
            header('Content-type: application/json');
            echo json_encode($rows);
            break;
        case "getDepartment":
            $query = "select id, name as text 
                from vin_vin_department
                where deleted = 0 
                AND name LIKE '%".$_GET['q']."%' 
                ORDER BY name LIMIT 30";

            $bean = new SugarBean();
            $result = $bean->db->query($query,true," Error filling in additional detail fields: ");
            $rows = array();

            while($row=$bean->db->fetchByAssoc($result) ) 
            {
                $rows[] = $row;
            }
            ob_start();
            ob_clean();
            header('Content-type: application/json');
            echo json_encode($rows);
            break;
        case "submitAssignSale":
            if(!checkPermission()){
                die;
            }
            $saleId = $_REQUEST['saleId'];
            $listLeads = $_REQUEST['listLeads'];
            if($saleId != '' && $saleId != null
            && $listLeads != '' && $listLeads != null){
                $listLeads = explode(",",$listLeads);
                foreach ($listLeads as $key => $value)
                {
                    $beanLead = BeanFactory::getBean('vin_vin_leads',$value);
                    if($beanLead != null && $beanLead != false && $beanLead->ACLAccess('EditView')){
                        $beanLead->user_id4_c = $saleId;
                        if($beanLead->vin_lead_status_c == '' || $beanLead->vin_lead_status_c == null || $beanLead->vin_lead_status_c == 'new'){
                            $beanLead->vin_lead_status_c = 'assigned';
                        }
                        $beanLead->save();
                    }
                }
            }
            break;
        case "workflowChangeStatus":
            if(!checkPermission()){
                die;
            }
            $bm = 0;
            $sa = 0;
            $sg = 0;
            $return = -1;
            $list_roles = getRoleByUser($current_user->id);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
            }

            $return = 0;
            $id = $_REQUEST['id'];
            $status = $_REQUEST['status'];
            $saleId = $_REQUEST['saleId'];
            $working_status = $_REQUEST['working_status'];
            $reason_unqualified = $_REQUEST['reason_unqualified'];
            $orderId = $_REQUEST['orderId'];
            if($id != '' && $id != null
            && $status != '' && $status != null){
                $valid = 0;
                foreach ($app_list_strings['vin_lead_status_c_list'] as $key => $value){
                    if($key == $status){
                        $valid = 1;
                    }
                }
                if($valid == 1){
                    $beanLead = BeanFactory::getBean('vin_vin_leads',$id);
                    if($beanLead != null && $beanLead != false 
                    && $beanLead->vin_lead_status_c != 'unqualified' 
                    && $beanLead->vin_lead_status_c != 'converted' && $beanLead->ACLAccess('EditView')){
                        if($current_user->is_admin == 1 || $bm == 1 || $sa == 1){

                        }
                        else{
                            if($beanLead->vin_lead_lock_c == 1){
                                die;
                            }
                        }
                        $beanLead->vin_lead_status_c = $status;
                        $dt = new DateTime();
                        $beanLead->vin_change_status_date_c = $dt->format('Y-m-d H:i:s');
                        if($beanLead->user_id4_c == '' || $beanLead->user_id4_c == null){
                            $beanLead->user_id4_c = $saleId;
                        }
                        if($status == 'working'){
                            // $beanLead->vin_working_substatus_c = $beanLead->vin_rating_c;
                            $beanLead->vin_working_substatus_c = $working_status;
                        }
                        else if($status == 'unqualified'){
                            $beanLead->vin_unqualified_reason_c = $reason_unqualified;
                        }
                        else if($status == 'converted'){
                            $beanLead->vin_order_id_c = $orderId;
                        }
                        $beanLead->save();
                        $return = 1;
                    }
                }
            }
            echo $return;
            break;
        case "getSaleInfo":
            if(!checkPermission()){
                die;
            }
            $id = $_REQUEST['id'];
            $bean = BeanFactory::getBean('Users',$id);
            $temp = array();
            $sea = new SugarEmailAddress;
            $temp['email'] = $sea->getPrimaryAddress($bean);
            $temp['user_name'] = $bean->full_name;
            $deparment = getDepartmentOfUser($id);
            $temp['dept_id'] = $deparment['id'];
            $temp['dept'] = $deparment['name'];

            header('Content-type: application/json');
            echo json_encode($temp);
            break;
        case "submitChangeOwner":
            //Disabled Change Owner
            die;
            if(!checkPermission()){
                die;
            }
            $bm = 0;
            $sa = 0;
            $sg = 0;
            $return = -1;
            $list_roles = getRoleByUser($current_user->id);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
            }

            $ownerId = $_REQUEST['ownerId'];
            $id = $_REQUEST['id'];
            if($ownerId != '' && $ownerId != null
            && $id != '' && $id != null){
                $beanLead = BeanFactory::getBean('vin_vin_leads',$id);
                if($beanLead != null && $beanLead != false && $beanLead->ACLAccess('EditView')){
                    if($current_user->is_admin == 1 || $bm == 1 || $sa == 1){

                    }
                    else{
                        if($beanLead->vin_lead_lock_c == 1){
                            die;
                        }
                    }
                    $beanLead->user_id3_c = $ownerId;
                    $beanLead->save();
                }
            }
            break;
        case "submitChangeOwnerList":
            if(!checkPermission()){
                die;
            }
            $bm = 0;
            $sa = 0;
            $sg = 0;
            $return = -1;
            $list_roles = getRoleByUser($current_user->id);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
            }

            $ownerId = $_REQUEST['ownerId'];
            $listLeads = $_REQUEST['listLeads'];
            if($ownerId != '' && $ownerId != null
            && $listLeads != '' && $listLeads != null){
                $listLeads = explode(",",$listLeads);
                foreach ($listLeads as $key => $value)
                {
                    $beanLead = BeanFactory::getBean('vin_vin_leads',$value);
                    if($beanLead != null && $beanLead != false && $beanLead->ACLAccess('EditView')){
                        if($current_user->is_admin == 1 || $bm == 1 || $sa == 1){
                            $beanLead->user_id3_c = $ownerId;
                            $beanLead->save();
                        }
                        else{
                            if($beanLead->vin_lead_lock_c != 1){
                                $beanLead->user_id3_c = $ownerId;
                                $beanLead->save();
                            }
                        }
                    }
                }
            }
            break;
        case "submitChangeStatusList":
            if(!checkPermission()){
                die;
            }
            $bm = 0;
            $sa = 0;
            $sg = 0;
            $return = -1;
            $list_roles = getRoleByUser($current_user->id);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
            }

            $status = $_REQUEST['status'];
            $listLeads = $_REQUEST['listLeads'];
            if($status != '' && $status != null
            && $listLeads != '' && $listLeads != null){
                $valid = 0;
                foreach ($app_list_strings['vin_lead_status_c_list'] as $key => $value){
                    if($key == $status){
                        $valid = 1;
                    }
                }
                if($valid == 1){
                    $listLeads = explode(",",$listLeads);
                    foreach ($listLeads as $key => $value)
                    {
                        $beanLead = BeanFactory::getBean('vin_vin_leads',$value);
                        if($beanLead != null && $beanLead != false && $beanLead->ACLAccess('EditView')){
                            if($status == 'working'){
                                $beanLead->vin_working_substatus_c = $_REQUEST['working_status'];
                            }
                            else if($status == 'unqualified'){
                                $beanLead->vin_unqualified_reason_c = $_REQUEST['reason_unqualified'];
                            }
                            if($current_user->is_admin == 1 || $bm == 1 || $sa == 1){
                                $beanLead->vin_lead_status_c = $status;
                                $beanLead->save();
                            }
                            else{
                                if($beanLead->vin_lead_lock_c != 1){
                                    $beanLead->vin_lead_status_c = $status;
                                    $beanLead->save();
                                }
                            }
                        }
                    }
                }
            }
            break;
        case "checkDuplicateLead":
            if (!checkPermission()) {
                die;
            }
            $email = $_REQUEST['email'];
            $phone = $_REQUEST['phone'];
            $fullName = $_REQUEST['fullName'];
            $dob = $_REQUEST['dob'];
            $record_id = $_REQUEST['record_id'];

            if ($email != '' || $phone != '' || $fullName != '' || $dob != '') {
                $bm = 0;
                $sa = 0;
                $sg = 0;
                $list_roles = getRoleByUser($current_user->id);
                foreach ($list_roles as $key => $value) {
                    if ($value == 'BUSINESS_MANAGER') {
                        $bm = 1;
                    } else if ($value == 'SALE_ADMIN') {
                        $sa = 1;
                    } else if ($value == 'SALE_AGENT') {
                        $sg = 1;
                    }
                }

                $bm_id = '';
                if ($bm == 1) {
                    $bm_id = $current_user->id;
                } else if ($sa == 1) {
                    $bm_id = getBusinessManagementUser($current_user->id);
                } else {
                    $sa_id = getSaleAdminUser($current_user->id);
                    if ($sa_id != '' && $sa_id != null) {
                        $bm_id = getBusinessManagementUser($sa_id);
                    }
                }

                $query = "SELECT count(l.id) total FROM vin_vin_leads l
                    JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c
                    WHERE l.deleted = 0 ";
                if (($email != '' && $phone != '') || ($email != '' && $fullName != '') || ($email != '' && $dob != '')
                    || ($fullName != '' && $phone != '') || ($fullName != '' && $dob != '') || ($phone != '' && $dob != '')) {
                    $query .= " AND (";
                    $checkOr = 0;
                    if ($email != '' && $phone != '') {
                        $checkOr ++;
                        $query .= " (lower(lc.vin_email_c) = '" . $email . "' AND lc.vin_phone_number_c = '" . $phone . "')";
                    } if ($email != '' && $fullName != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $checkOr++;
                        $query .= "(lower(lc.vin_email_c) = lower('" . $email . "') AND lower(lc.vin_full_name_c) = lower('" . $fullName . "'))";
                    } if ($email != '' && $dob != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $checkOr++;
                        $query .= "(lower(lc.vin_email_c) = lower('" . $email . "') AND lc.vin_dob_c = '" . $dob . "')";
                    } if ($fullName != '' && $phone != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $checkOr++;
                        $query .= "(lower(lc.vin_full_name_c) = lower('" . $fullName . "') AND lc.vin_phone_number_c = '" . $phone . "')";
                    } if ($fullName != '' && $dob != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $checkOr++;
                        $query .= "(lower(lc.vin_full_name_c) = lower('" . $fullName . "') AND lc.vin_dob_c = '" . $dob . "')";
                    } if ($phone != '' && $dob != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $query .= "(lc.vin_phone_number_c = '" . $phone . "' AND lc.vin_dob_c = '" . $dob . "')";
                    }
                    $query .= " ) AND datediff(now(), l.date_entered) <= 20";

                    $result = $db->query($query);
                    $row = $db->fetchByAssoc($result);

                    if ($row != null && $row['total'] > 0) {
                        echo 1;
                    } else {
                        echo 0;
                    }

                }
            } else {
                echo 0;
            }
            break;
        case "viewListDuplicateLead":
            if(!checkPermission()){
                die;
            }
            $email = $_REQUEST['email'];
            $phone = $_REQUEST['phone'];
            $fullName = $_REQUEST['fullName'];
            $dob = $_REQUEST['dob'];
            $record_id = $_REQUEST['record_id'];
            $return_list = array();
            if($email != '' || $phone != '' || $fullName !== '' || $dob !== ''){
                $bm = 0;
                $sa = 0;
                $sg = 0;
                $list_roles = getRoleByUser($current_user->id);
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }

                $bm_id = '';
                if($bm == 1){
                    $bm_id = $current_user->id;
                }
                else if($sa == 1){
                    $bm_id = getBusinessManagementUser($current_user->id);
                }
                else{
                    $sa_id = getSaleAdminUser($current_user->id);
                    if($sa_id != '' && $sa_id != null){
                        $bm_id = getBusinessManagementUser($sa_id);
                    }
                }

                $query = "SELECT l.name, lc.vin_email_c,
                    lc.vin_phone_number_c, l.id,
                    u.user_name  AS owner
                    FROM vin_vin_leads l
                    JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c
                    JOIN users u ON lc.user_id3_c = u.id AND u.deleted = 0 AND u.status = 'Active'
                    WHERE l.deleted = 0 AND l.id <> '".$record_id."'"; // AND lc.user_id_c = '".$bm_id."' 
                if (($email !== '' && $phone !== '') || ($email !=='' && $fullName !== '') || ($email !== '' && $dob !=='')
                    || ($fullName !== '' && $phone !== '') || ($fullName !== '' && $dob !== '') || ($phone !== '' && $dob !== '')) {
                    $query .= " AND (";
                    $checkOr = 0;
                    if ($email != '' && $phone != '') {
                        $checkOr ++;
                        $query .= " (lower(lc.vin_email_c) = '" . $email . "' AND lc.vin_phone_number_c = '" . $phone . "')";
                    } if ($email != '' && $fullName != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $checkOr++;
                        $query .= "(lower(lc.vin_email_c) = lower('" . $email . "') AND lower(lc.vin_full_name_c) = lower('" . $fullName . "'))";
                    } if ($email != '' && $dob != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $checkOr++;
                        $query .= "(lower(lc.vin_email_c) = lower('" . $email . "') AND lc.vin_dob_c = '" . $dob . "')";
                    } if ($fullName != '' && $phone != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $checkOr++;
                        $query .= "(lower(lc.vin_full_name_c) = lower('" . $fullName . "') AND lc.vin_phone_number_c = '" . $phone . "')";
                    } if ($fullName != '' && $dob != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $checkOr++;
                        $query .= "(lower(lc.vin_full_name_c) = lower('" . $fullName . "') AND lc.vin_dob_c = '" . $dob . "')";
                    } if ($phone != '' && $dob != '') {
                        if($checkOr > 0) { $query.=" OR ";}
                        $query .= "(lc.vin_phone_number_c = '" . $phone . "' AND lc.vin_dob_c = '" . $dob . "')";
                    }
                    $query.=")";
                }
                $result = $db->query($query);
                $dataSet = array();
                $row = $db->fetchByAssoc($result);
                while ($row != null){
                    $dataSet[] = $row;
                    $row = $db->fetchByAssoc($result);
                }

                foreach ($dataSet as $key => $value) {
                    $temp = array();
                    $temp['id'] = $value['id'];
                    $temp['name'] = $value['name'];
                    $temp['owner'] = $value['owner'];
                    //if($bm == 1){
                        $temp['email'] = $value['vin_email_c'];
                        $temp['phone'] = $value['vin_phone_number_c'];
                    //}
                    //else{
                       // $temp['email'] = '';
                        //$temp['phone'] = '';
                    //}
                    array_push($return_list, $temp);
                }
            }
            ob_start();
            ob_clean();
            header('Content-type: application/json');
            echo json_encode($return_list);
            break;
        case "importLeads":
            if(!checkPermission()){
                die;
            }
            $result = array();
            $bean = BeanFactory::newBean('vin_vin_leads');
            if($bean->ACLAccess('EditView')){
                $result = importLeads();
            }
            ob_start();
            ob_clean();
            ob_end_clean();
            header('Content-type: application/json');
            echo json_encode($result);
            break;
        case "lockLead":
            $record_id = $_REQUEST['record_id'];
            $bm = 0;
            $sa = 0;
            $sg = 0;
            $return = -1;
            $list_roles = getRoleByUser($current_user->id);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
            }

            if($current_user->is_admin == 1 || $bm == 1 || $sa == 1){
                $beanLead = BeanFactory::getBean('vin_vin_leads',$record_id);
                if($beanLead != null && $beanLead != false && $beanLead->ACLAccess('EditView')){
                    if($beanLead->vin_lead_lock_c == 1){
                        $beanLead->vin_lead_lock_c = 0;
                        $return = 0;
                    }
                    else{
                        $beanLead->vin_lead_lock_c = 1;
                        $return = 1;
                    }
                    $beanLead->save();
                }
            }
            echo $return;
            break;
        case "test":
            //require_once('custom/include/custom_jobs/vin_vin_leads/vin_vin_leads_jobs.php');
            //$f = new vin_vin_leads_jobs;
            //$f->lead_unqualified_refresh();
            //$f->salesforce_sync_leads_to_portal();
            //die;
            require_once('custom/include/global_function/vinhomes_salesforce_api.php');
            $f = new vinhomes_salesforce_api;
            $token = $f->get_access_token();
            $leads = $f->get_leads_from_salesforce($token);
            die;
            $data = array();
            $data['vin_first_name_c'] = 'FName223';
            $data['vin_last_name_c'] = 'LNam223';
            $data['vin_email_c'] = 'abc2134453751@gmail.com';
            $data['vin_company_name_c'] = 'Company Name';
            $data['vin_country_c'] = 'VN';
            $data['vin_lead_source_c'] = 'Advertisement';
            $data['name'] = 'FName1 Mname LNam1 Suffix';
            $data['vin_street_c'] = 'HN2';
            $data['vin_city_text_c'] = 'HN';
            $data['vin_state_province_c'] = '23123';
            $data['vin_mobile_c'] = '0977261899';
            $data['vin_phone_number_c'] = '0977261890';
            $data['vin_vin_currency_id_c'] = 'VND';
            $data['vin_lead_status_c'] = 'New';
            $data['vin_rating_c'] = 'Hot';
            $data['vin_zip_postal_code_c'] = '312312312';
            $data['vin_id_number_c'] = '213331231231231';
            $data['vin_project_c'] = 'Vinhomes Grand Park';
            $data['vin_house_type_list_c'] = 'Shop';
            $data['vin_budget_c'] = 100000000;
            $data['vin_lead_scoring_c'] = 90;
            $data['vin_source_details_c'] = 'abv';
            $data['vin_tax_code_c'] = '213123123132121';
            $leads = $f->sync_new_lead_to_salesforce($token,$data);
            var_dump($leads); 
            break;
        case "test2":
            die;
            require_once('custom/include/custom_jobs/vin_vin_leads/vin_vin_leads_jobs.php');
            $f = new vin_vin_leads_jobs;
            $f->salesforce_sync_leads_to_salesforce();
            break;
        case "encrypt":
            if($current_user->is_admin == 1){
                $value = $_REQUEST['value'];
                if($value == '' || $value == null){
    
                }
                else{
                    require_once('custom/include/global_function/encrypt_decrypt.php');
                    $f = new encrypt_decrypt;
                    $en = $f->dec_enc('encrypt', $value);
                    echo $en;
                }
            }
            break;
        case "getEmployee":
                $bm = 0;
                $sa = 0;
                $sg = 0;
                $query = "";
                $list_roles = getRoleByUser($current_user->id);
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }
                if($bm == 1){
                    $query = "select DISTINCT u.id, u.user_name as text 
                    from users u
                    left join users_cstm uc ON u.id=uc.id_c
                    where u.deleted = 0 AND u.status = 'Active' 
                    AND (uc.user_id_c IN (
                            select u2.id
                            from users u2
                            left join users_cstm uc2 ON u2.id=uc2.id_c
                            where u2.deleted = 0 AND u2.status = 'Active' 
                            AND uc2.user_id1_c = '".$current_user->id."'
                        )
                        OR uc.user_id1_c = '".$current_user->id."'
                    )
                    AND u.user_name LIKE '%".$_GET['q']."%' 
                    ORDER BY u.user_name LIMIT 30";
                }
                else if($sa == 1){
                    $bus_mana = getBusinessManagementUser($current_user->id);
                    $query = "select u.id, u.user_name as text 
                            from users u
                            left join users_cstm uc ON u.id=uc.id_c
                            where u.deleted = 0 AND u.status = 'Active' 
                            AND (
                                uc.user_id_c = '".$current_user->id."'
                                )
                            AND u.user_name LIKE '%".$_GET['q']."%' 
                            ORDER BY u.user_name LIMIT 30";
                }
    
                $rows = array();
                if($bm == 1 || $sa == 1){
                    $bean = new SugarBean();
                    $result = $bean->db->query($query,true," Error filling in additional detail fields: ");
                    while($row=$bean->db->fetchByAssoc($result) ) 
                    {
                        $rows[] = $row;
                    }
                }

                ob_start();
                ob_clean();
                header('Content-type: application/json');
                echo json_encode($rows);
                break;
            case "viewLeadFunelReport":
                // $from = $_REQUEST['from'];
                // $to = $_REQUEST['to'];
                $from = "01/01/0001";
                $to = date("m/d/Y");
                $employee = $_REQUEST['employee'];
                $return = array();
                if($from != "" && $to != ""){
                    $bm = 0;
                    $sa = 0;
                    $sg = 0;
                    $query = "";
                    $list_roles = getRoleByUser($current_user->id);
                    foreach($list_roles as $key => $value){
                        if($value == 'BUSINESS_MANAGER'){
                            $bm = 1;
                        }
                        else if($value == 'SALE_ADMIN'){
                            $sa = 1;
                        }
                        else if($value == 'SALE_AGENT'){
                            $sg = 1;
                        }
                    }

                    if($bm == 1){
                        $query = "select u.id, u.user_name
                            from users u
                            left join users_cstm uc ON u.id=uc.id_c
                            where u.deleted = 0 AND u.status = 'Active' 
                            AND (uc.user_id_c IN (
                                    select u2.id
                                    from users u2
                                    left join users_cstm uc2 ON u2.id=uc2.id_c
                                    where u2.deleted = 0 AND u2.status = 'Active' 
                                    AND uc2.user_id1_c = '".$current_user->id."'
                                )
                                OR uc.user_id1_c = '".$current_user->id."'
                                OR uc.id_c = '".$current_user->id."'
                            ) ";
                        if($employee != ''){
                            $employee = explode(",",$employee);
                            if(count($employee) > 0){
                                $query .= " AND ( ";
                                foreach($employee as $key => $value){
                                    if($key == 0 || $key == '0'){
                                        $query .= " u.id = '".$value."' ";
                                    }
                                    else{
                                        $query .= " OR u.id = '".$value."' ";
                                    }
                                }
                                $query .= " ) ";
                            }
                        }
                        else{
                            $query .= " AND uc.id_c = '".$current_user->id."' ";
                        }

                        $query .= " ORDER BY u.user_name ";
                        $result = $db->query($query);
                        $dataSet = array();
                        $row = $db->fetchByAssoc($result);
                        while ($row != null){
                            $dataSet[] = $row;
                            $row = $db->fetchByAssoc($result);
                        }
                        if($employee != ''){
                            foreach($dataSet as $key => $value){
                                $query2 = "SELECT vin_lead_status_c, count(l.id) AS total
                                        FROM vin_vin_leads l
                                        JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c 
                                        WHERE l.deleted = 0 
                                        AND (lc.user_id1_c = '".$value['id']."' OR lc.user_id2_c = '".$value['id']."'  OR lc.user_id_c = '".$value['id']."')
                                        AND l.date_entered >= STR_TO_DATE('".$from."', '%m/%d/%Y')
                                        AND l.date_entered <= STR_TO_DATE('".$to." 23:59:59', '%m/%d/%Y %H:%i:%s')
                                        GROUP BY vin_lead_status_c";
                                        
                                $result2 = $db->query($query2);
                                $dataSet2 = array();
                                $row2 = $db->fetchByAssoc($result2);
                                while ($row2 != null){
                                    $dataSet2[] = $row2;
                                    $row2 = $db->fetchByAssoc($result2);
                                }
                                $return[$value['user_name']] = array();
                                foreach($dataSet2 as $key2 => $value2){
                                    $return[$value['user_name']][$value2['vin_lead_status_c']] = $value2['total'];
                                }
                            }
                        }
                        else{
                            $return['Leads'] = array();
                            foreach ($app_list_strings['vin_lead_status_c_list'] as $key => $value){
                                $return['Leads'][$key] = 0;
                            }
                            foreach($dataSet as $key => $value){
                                $query2 = "SELECT vin_lead_status_c, count(l.id) AS total
                                        FROM vin_vin_leads l
                                        JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c 
                                        WHERE l.deleted = 0 
                                        AND (lc.user_id1_c = '".$value['id']."' OR lc.user_id2_c = '".$value['id']."'  OR lc.user_id_c = '".$value['id']."')
                                        AND l.date_entered >= STR_TO_DATE('".$from."', '%m/%d/%Y')
                                        AND l.date_entered <= STR_TO_DATE('".$to." 23:59:59', '%m/%d/%Y %H:%i:%s')
                                        GROUP BY vin_lead_status_c";
                                        
                                $result2 = $db->query($query2);
                                $dataSet2 = array();
                                $row2 = $db->fetchByAssoc($result2);
                                while ($row2 != null){
                                    $dataSet2[] = $row2;
                                    $row2 = $db->fetchByAssoc($result2);
                                }
                                foreach($dataSet2 as $key2 => $value2){
                                    $return['Leads'][$value2['vin_lead_status_c']] = (float)$return['Leads'][$value2['vin_lead_status_c']] + (float)$value2['total'];
                                }
                            }
                        }
  
                    }
                    else if($sa == 1){
                        $query = "select u.id, u.user_name
                            from users u
                            left join users_cstm uc ON u.id=uc.id_c
                            where u.deleted = 0 AND u.status = 'Active' 
                            AND (uc.user_id_c = '".$current_user->id."' 
                            OR uc.id_c = '".$current_user->id."') ";

                        if($employee != ''){
                            $employee = explode(",",$employee);
                            if(count($employee) > 0){
                                $query .= " AND ( ";
                                foreach($employee as $key => $value){
                                    if($key == 0 || $key == '0'){
                                        $query .= " u.id = '".$value."' ";
                                    }
                                    else{
                                        $query .= " OR u.id = '".$value."' ";
                                    }
                                }
                                $query .= " ) ";
                            }
                        }
                        else{
                            $query .= " AND uc.id_c = '".$current_user->id."' ";
                        }

                        $query .= " ORDER BY u.user_name ";
                        $result = $db->query($query);
                        $dataSet = array();
                        $row = $db->fetchByAssoc($result);
                        while ($row != null){
                            $dataSet[] = $row;
                            $row = $db->fetchByAssoc($result);
                        }
                        if($employee != ''){
                            foreach($dataSet as $key => $value){
                                $query2 = "SELECT vin_lead_status_c, count(l.id) AS total
                                        FROM vin_vin_leads l
                                        JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c 
                                        WHERE l.deleted = 0 
                                        AND (lc.user_id1_c = '".$value['id']."' OR lc.user_id2_c = '".$value['id']."')
                                        AND l.date_entered >= STR_TO_DATE('".$from."', '%m/%d/%Y')
                                        AND l.date_entered <= STR_TO_DATE('".$to." 23:59:59', '%m/%d/%Y %H:%i:%s')
                                        GROUP BY vin_lead_status_c";
                                        
                                $result2 = $db->query($query2);
                                $dataSet2 = array();
                                $row2 = $db->fetchByAssoc($result2);
                                while ($row2 != null){
                                    $dataSet2[] = $row2;
                                    $row2 = $db->fetchByAssoc($result2);
                                }
                                $return[$value['user_name']] = array();
                                foreach($dataSet2 as $key2 => $value2){
                                    $return[$value['user_name']][$value2['vin_lead_status_c']] = $value2['total'];
                                }
                            }
                        }
                        else{
                            $return['Leads'] = array();
                            foreach ($app_list_strings['vin_lead_status_c_list'] as $key => $value){
                                $return['Leads'][$key] = 0;
                            }
                            foreach($dataSet as $key => $value){
                                $query2 = "SELECT vin_lead_status_c, count(l.id) AS total
                                        FROM vin_vin_leads l
                                        JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c 
                                        WHERE l.deleted = 0 
                                        AND (lc.user_id1_c = '".$value['id']."' OR lc.user_id2_c = '".$value['id']."')
                                        AND l.date_entered >= STR_TO_DATE('".$from."', '%m/%d/%Y')
                                        AND l.date_entered <= STR_TO_DATE('".$to." 23:59:59', '%m/%d/%Y %H:%i:%s')
                                        GROUP BY vin_lead_status_c";
                                        
                                $result2 = $db->query($query2);
                                $dataSet2 = array();
                                $row2 = $db->fetchByAssoc($result2);
                                while ($row2 != null){
                                    $dataSet2[] = $row2;
                                    $row2 = $db->fetchByAssoc($result2);
                                }
                                foreach($dataSet2 as $key2 => $value2){
                                    $return['Leads'][$value2['vin_lead_status_c']] = (float)$return['Leads'][$value2['vin_lead_status_c']] + (float)$value2['total'];
                                }
                            }
                        }

                    }
                }
                ob_start();
                ob_clean();
                header('Content-type: application/json');
                echo json_encode($return);
                break;
            case "viewLeadPerformanceReport":
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];
                $employee = $_REQUEST['employee'];
                $return = array();
                if($from != "" && $to != ""){
                    $bm = 0;
                    $sa = 0;
                    $sg = 0;
                    $query = "";
                    $list_roles = getRoleByUser($current_user->id);
                    foreach($list_roles as $key => $value){
                        if($value == 'BUSINESS_MANAGER'){
                            $bm = 1;
                        }
                        else if($value == 'SALE_ADMIN'){
                            $sa = 1;
                        }
                        else if($value == 'SALE_AGENT'){
                            $sg = 1;
                        }
                    }

                    if($bm == 1){
                        $query = "select u.id, u.user_name
                            from users u
                            left join users_cstm uc ON u.id=uc.id_c
                            where u.deleted = 0 AND u.status = 'Active' 
                            AND (uc.user_id_c IN (
                                    select u2.id
                                    from users u2
                                    left join users_cstm uc2 ON u2.id=uc2.id_c
                                    where u2.deleted = 0 AND u2.status = 'Active' 
                                    AND uc2.user_id1_c = '".$current_user->id."'
                                )
                                OR uc.user_id1_c = '".$current_user->id."'
                                OR uc.id_c = '".$current_user->id."'
                            ) ";
                        if($employee != ''){
                            $employee = explode(",",$employee);
                            if(count($employee) > 0){
                                $query .= " AND ( ";
                                foreach($employee as $key => $value){
                                    if($key == 0 || $key == '0'){
                                        $query .= " u.id = '".$value."' ";
                                    }
                                    else{
                                        $query .= " OR u.id = '".$value."' ";
                                    }
                                }
                                $query .= " ) ";
                            }
                        }

                        $query .= " ORDER BY u.user_name ";
                        $result = $db->query($query);
                        $dataSet = array();
                        $row = $db->fetchByAssoc($result);
                        while ($row != null){
                            $dataSet[] = $row;
                            $row = $db->fetchByAssoc($result);
                        }
                        foreach($dataSet as $key => $value){
                            $query2 = "SELECT vin_lead_status_c, count(l.id) AS total
                                    FROM vin_vin_leads l
                                    JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c 
                                    WHERE l.deleted = 0 
                                    AND (lc.user_id1_c = '".$value['id']."' OR lc.user_id2_c = '".$value['id']."'  OR lc.user_id_c = '".$value['id']."')
                                    AND l.date_entered >= STR_TO_DATE('".$from."', '%m/%d/%Y')
                                    AND l.date_entered <= STR_TO_DATE('".$to." 23:59:59', '%m/%d/%Y %H:%i:%s')
                                    GROUP BY vin_lead_status_c";
                                    
                            $result2 = $db->query($query2);
                            $dataSet2 = array();
                            $row2 = $db->fetchByAssoc($result2);
                            while ($row2 != null){
                                $dataSet2[] = $row2;
                                $row2 = $db->fetchByAssoc($result2);
                            }
                            $return[$value['user_name']] = array();
                            foreach($dataSet2 as $key2 => $value2){
                                $return[$value['user_name']][$value2['vin_lead_status_c']] = $value2['total'];
                            }
                        }
                    }
                    else if($sa == 1){
                        $query = "select u.id, u.user_name
                            from users u
                            left join users_cstm uc ON u.id=uc.id_c
                            where u.deleted = 0 AND u.status = 'Active' 
                            AND (uc.user_id_c = '".$current_user->id."' 
                            OR uc.id_c = '".$current_user->id."') ";

                        if($employee != ''){
                            $employee = explode(",",$employee);
                            if(count($employee) > 0){
                                $query .= " AND ( ";
                                foreach($employee as $key => $value){
                                    if($key == 0 || $key == '0'){
                                        $query .= " u.id = '".$value."' ";
                                    }
                                    else{
                                        $query .= " OR u.id = '".$value."' ";
                                    }
                                }
                                $query .= " ) ";
                            }
                        }

                        $query .= " ORDER BY u.user_name ";
                        $result = $db->query($query);
                        $dataSet = array();
                        $row = $db->fetchByAssoc($result);
                        while ($row != null){
                            $dataSet[] = $row;
                            $row = $db->fetchByAssoc($result);
                        }
                        foreach($dataSet as $key => $value){
                            $query2 = "SELECT vin_lead_status_c, count(l.id) AS total
                                    FROM vin_vin_leads l
                                    JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c 
                                    WHERE l.deleted = 0 
                                    AND (lc.user_id1_c = '".$value['id']."' OR lc.user_id2_c = '".$value['id']."')
                                    AND l.date_entered >= STR_TO_DATE('".$from."', '%m/%d/%Y')
                                    AND l.date_entered <= STR_TO_DATE('".$to." 23:59:59', '%m/%d/%Y %H:%i:%s')
                                    GROUP BY vin_lead_status_c";
                                    
                            $result2 = $db->query($query2);
                            $dataSet2 = array();
                            $row2 = $db->fetchByAssoc($result2);
                            while ($row2 != null){
                                $dataSet2[] = $row2;
                                $row2 = $db->fetchByAssoc($result2);
                            }
                            $return[$value['user_name']] = array();
                            foreach($dataSet2 as $key2 => $value2){
                                $return[$value['user_name']][$value2['vin_lead_status_c']] = $value2['total'];
                            }
                        }
                    }
                }
                ob_start();
                ob_clean();
                header('Content-type: application/json');
                echo json_encode($return);
                break;
            case "viewLeadConvertReport":
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];
                if($from == ""){
                    $from = "01/01/0001";
                }
                if($to == ""){
                    $to = date("m/d/Y");
                }

                $employee = $_REQUEST['employee'];
                $return = array();
                $bm = 0;
                $sa = 0;
                $sg = 0;
                $query = "";
                $list_roles = getRoleByUser($current_user->id);
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }
                if($bm == 1){
                    $query = "select u.id, u.user_name
                        from users u
                        left join users_cstm uc ON u.id=uc.id_c
                        where u.deleted = 0 AND u.status = 'Active' 
                        AND (uc.user_id_c IN (
                                select u2.id
                                from users u2
                                left join users_cstm uc2 ON u2.id=uc2.id_c
                                where u2.deleted = 0 AND u2.status = 'Active' 
                                AND uc2.user_id1_c = '".$current_user->id."'
                            )
                            OR uc.user_id1_c = '".$current_user->id."'
                            OR uc.id_c = '".$current_user->id."'
                        ) ";
                    if($employee != ''){
                        $employee = explode(",",$employee);
                        if(count($employee) > 0){
                            $query .= " AND ( ";
                            foreach($employee as $key => $value){
                                if($key == 0 || $key == '0'){
                                    $query .= " u.id = '".$value."' ";
                                }
                                else{
                                    $query .= " OR u.id = '".$value."' ";
                                }
                            }
                            $query .= " ) ";
                        }
                    }
                    else{
                      $query .= " AND uc.id_c = '".$current_user->id."' ";
                    }

                    $query .= " ORDER BY u.user_name ";
                    $result = $db->query($query);
                    $dataSet = array();
                    $row = $db->fetchByAssoc($result);
                    while ($row != null){
                        $dataSet[] = $row;
                        $row = $db->fetchByAssoc($result);
                    }
                    foreach($dataSet as $key => $value){
                        $query2 = "SELECT vin_lead_status_c, count(l.id) AS total
                                FROM vin_vin_leads l
                                JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c 
                                WHERE l.deleted = 0 
                                AND (lc.user_id1_c = '".$value['id']."' OR lc.user_id2_c = '".$value['id']."'  OR lc.user_id_c = '".$value['id']."')
                                AND l.date_entered >= STR_TO_DATE('".$from."', '%m/%d/%Y')
                                AND l.date_entered <= STR_TO_DATE('".$to." 23:59:59', '%m/%d/%Y %H:%i:%s')
                                GROUP BY vin_lead_status_c";
                                
                        $result2 = $db->query($query2);
                        $dataSet2 = array();
                        $row2 = $db->fetchByAssoc($result2);
                        while ($row2 != null){
                            $dataSet2[] = $row2;
                            $row2 = $db->fetchByAssoc($result2);
                        }
                        $return[$value['user_name']] = array();
                        foreach($dataSet2 as $key2 => $value2){
                            $return[$value['user_name']][$value2['vin_lead_status_c']] = $value2['total'];
                        }
                    }
                }
                else if($sa == 1){
                    $query = "select u.id, u.user_name
                        from users u
                        left join users_cstm uc ON u.id=uc.id_c
                        where u.deleted = 0 AND u.status = 'Active' 
                        AND (uc.user_id_c = '".$current_user->id."' 
                        OR uc.id_c = '".$current_user->id."') ";

                    if($employee != ''){
                        $employee = explode(",",$employee);
                        if(count($employee) > 0){
                            $query .= " AND ( ";
                            foreach($employee as $key => $value){
                                if($key == 0 || $key == '0'){
                                    $query .= " u.id = '".$value."' ";
                                }
                                else{
                                    $query .= " OR u.id = '".$value."' ";
                                }
                            }
                            $query .= " ) ";
                        }
                    }

                    $query .= " ORDER BY u.user_name ";
                    $result = $db->query($query);
                    $dataSet = array();
                    $row = $db->fetchByAssoc($result);
                    while ($row != null){
                        $dataSet[] = $row;
                        $row = $db->fetchByAssoc($result);
                    }
                    foreach($dataSet as $key => $value){
                        $query2 = "SELECT vin_lead_status_c, count(l.id) AS total
                                FROM vin_vin_leads l
                                JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c 
                                WHERE l.deleted = 0 
                                AND (lc.user_id1_c = '".$value['id']."' OR lc.user_id2_c = '".$value['id']."')
                                AND l.date_entered >= STR_TO_DATE('".$from."', '%m/%d/%Y')
                                AND l.date_entered <= STR_TO_DATE('".$to." 23:59:59', '%m/%d/%Y %H:%i:%s')
                                GROUP BY vin_lead_status_c";
                                
                        $result2 = $db->query($query2);
                        $dataSet2 = array();
                        $row2 = $db->fetchByAssoc($result2);
                        while ($row2 != null){
                            $dataSet2[] = $row2;
                            $row2 = $db->fetchByAssoc($result2);
                        }
                        $return[$value['user_name']] = array();
                        foreach($dataSet2 as $key2 => $value2){
                            $return[$value['user_name']][$value2['vin_lead_status_c']] = $value2['total'];
                        }
                    }
                }

                ob_start();
                ob_clean();
                header('Content-type: application/json');
                echo json_encode($return);
                break;
            case "export_report":
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];
                $return = array();
                $bm = 0;
                $sa = 0;
                $sg = 0;
                $digital_lead = 0;
                $list_roles = getRoleByUser($current_user->id);
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                    else if($value == 'DIGITAL_MARKETING'){
                        $digital_lead = 1;
                    }
                }

                if($current_user->is_admin == 1 || $digital_lead == 1){
                    $query = "SELECT v.*,vc.*, u1.user_name AS owner,
                            u2.user_name AS sale_in_charge, u3.user_name AS created_by, 
                            u4.user_name AS modified_by, d1.name AS sale_dept,
                            d2.name AS owner_dept
                        FROM vin_vin_leads v
                        LEFT JOIN vin_vin_leads_cstm vc ON v.id=vc.id_c
                        LEFT JOIN users u1 ON u1.id=vc.user_id3_c AND u1.deleted = 0
                        LEFT JOIN users u2 ON u2.id=vc.user_id4_c AND u2.deleted = 0
                        LEFT JOIN users u3 ON u3.id=v.created_by AND u3.deleted = 0
                        LEFT JOIN users u4 ON u4.id=v.modified_user_id AND u4.deleted = 0
                        LEFT JOIN vin_vin_department d1 ON d1.id=vc.vin_vin_department_id_c AND d1.deleted = 0
                        LEFT JOIN vin_vin_department d2 ON d2.id=vc.vin_vin_department_id1_c AND d2.deleted = 0
                        WHERE v.deleted = 0";
                    if($from != ""){
                        $query .= " AND DATE_ADD(v.date_entered, INTERVAL 7 HOUR) >= STR_TO_DATE('".$from."', '%m/%d/%Y') ";
                    }
                    if($to != ""){
                        $query .= " AND DATE_ADD(v.date_entered, INTERVAL 7 HOUR) <= STR_TO_DATE('".$to." 23:59:59', '%m/%d/%Y %H:%i:%s') ";
                    }

                    $result2 = $db->query($query);
                    $dataSet2 = array();
                    $row2 = $db->fetchByAssoc($result2);
                    while ($row2 != null){
                        $dataSet2[] = $row2;
                        $row2 = $db->fetchByAssoc($result2);
                    }

                    // Start Export Excel
                    $export_array = array();
                    $temp_data = array();
                    $temp_data[0] = 'Project';
                    $temp_data[1] = 'Budget';
                    $temp_data[2] = 'House Type';
                    $temp_data[3] = 'Description';
                    $temp_data[4] = 'Lead Status';
                    $temp_data[5] = 'Lead Owner';
                    $temp_data[6] = 'Unqualified Reason';
                    $temp_data[7] = 'Lead Working Detail';
                    $temp_data[8] = 'Rating';
                    $temp_data[9] = 'Lead Scoring';
                    $temp_data[10] = 'Sales In Charge';
                    $temp_data[11] = 'Sales Dept';
                    $temp_data[12] = 'Lead Source';
                    $temp_data[13] = 'Created By';
                    $temp_data[14] = 'Modified By';
                    $temp_data[15] = 'Lead ID Salesforce';
                    $temp_data[16] = 'Sales Dept. of Owner';
                    $temp_data[17] = 'Campaign';
                    $temp_data[18] = 'Phân khu';
                    $temp_data[19] = 'Ads Source';
                    $temp_data[20] = 'Lead ads ID';
                    $temp_data[21] = 'Ads content';
                    $temp_data[22] = 'Agent';
                    $temp_data[23] = 'Ad Term';
                    $temp_data[24] = 'Form Medium';
                    $temp_data[25] = 'UTM Source';
                    array_push($export_array, $temp_data);
                    foreach($dataSet2 as $key2 => $value2){
                        $temp_data = array();
                        $temp_data[0] = $app_list_strings['allocate_dept_level1'][$value2['vin_project_c']];
                        $temp_data[1] = $value2['vin_budget_c'];
                        $temp_data[2] = $app_list_strings['vin_house_type_list_c_list'][$value2['vin_house_type_list_c']];
                        $temp_data[3] = $value2['description'];
                        $temp_data[4] = $app_list_strings['vin_lead_status_c_list'][$value2['vin_lead_status_c']];
                        $temp_data[5] = $value2['owner'];
                        $temp_data[6] = $app_list_strings['vin_unqualified_reason_list'][$value2['vin_unqualified_reason_c']];
                        $temp_data[7] = $app_list_strings['vin_working_substatus_c_list'][$value2['vin_working_substatus_c']];
                        $temp_data[8] = $app_list_strings['vin_rating_c_list'][$value2['vin_rating_c']];
                        $temp_data[9] = $value2['vin_lead_scoring_c'];
                        $temp_data[10] = $value2['sale_in_charge'];
                        $temp_data[11] = $value2['sale_dept'];
                        $temp_data[12] = $app_list_strings['allocate_dept_source'][$value2['vin_lead_source_c']];
                        $temp_data[13] = $value2['created_by'];
                        $temp_data[14] = $value2['modified_by'];
                        $temp_data[15] = $value2['vin_lead_id_salesforce_c'];
                        $temp_data[16] = $value2['owner_dept'];
                        $temp_data[17] = $value2['vin_campaign_c'];
                        $temp_data[18] = $app_list_strings['allocate_dept_level2'][$value2['vin_phankhu_c']];
                        $temp_data[19] = $app_list_strings['allocate_dept_level3'][$value2['vin_ads_source_c']];
                        $temp_data[20] = $value2['vin_lead_ads_id_c'];
                        $temp_data[21] = $value2['vin_ads_content_c'];
                        $temp_data[22] = $value2['vin_agent_c'];
                        $temp_data[23] = $value2['vin_ad_term_c'];
                        $temp_data[24] = $value2['vin_form_medium_c'];
                        $temp_data[25] = $value2['vin_utm_source_c'];
                        array_push($export_array, $temp_data);
                    }

                    $export_name = 'leads_report_'.$current_user->user_name.'_'.date('YmdHis');
                    require "custom/include/library/PHPExcel/Classes/PHPExcel.php";
                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->setTitle('data');
                    $objPHPExcel->getActiveSheet()->fromArray($export_array);
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="'.$export_name.'.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    ob_start();
                    if (!file_exists('leads_report')) {
                        mkdir('leads_report', 0777, true);
                    }
                    $file_name = 'leads_report/'.$export_name.'.xlsx';
                    $objWriter->save($file_name);
                    $xlsData = ob_get_contents();
                    ob_end_clean();
                
                    $response =  array(
                        'fileName' => $export_name.'.xlsx',
                        'file' => $file_name
                    );
                    echo json_encode($response);
                }

                break;
        case "loadSource":
            $id = $_REQUEST['id'];
            $bm = 0;
            $sa = 0;
            $sg = 0;
            $digital_lead = 0;
            $list_roles = getRoleByUser($current_user->id);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
                else if($value == 'DIGITAL_MARKETING'){
                    $digital_lead = 1;
                }
            }
            if($current_user->is_admin != 1 && $digital != 1){
                $id = '';
            }
            if($current_user->is_admin != 1 && $digital != 1 && $bm != 1){
                die;
            }
            if($id == ''){
                $id = $current_user->id;
            }
            if($current_user->is_admin == 1 || $digital == 1){
                echo "<input type='hidden' id='view_as_others' value='1' />";
                //GDKD
                $querySearch = "SELECT u.id, u.user_name AS name 
                            FROM securitygroups_users sgu
                            JOIN securitygroups_acl_roles sgacl ON sgacl.securitygroup_id = sgu.securitygroup_id
                            JOIN acl_roles acl ON acl.id=sgacl.role_id
                            JOIN users u ON u.id = sgu.user_id
                            WHERE u.deleted = 0 AND u.status='Active'
                            AND sgu.deleted=0 AND sgacl.deleted=0  AND acl.deleted=0
                            AND acl.name='BUSINESS_MANAGER'
                            ORDER BY name;";
                $result = $db->query($querySearch);
                $dataSet = array();
                $row = $db->fetchByAssoc($result);
                while ($row != null){
                    $dataSet[] = $row;
                    $row = $db->fetchByAssoc($result);
                }
                $bm_list = 'var bm_list = {';
                $count = 0;
                foreach($dataSet as $key => $value)
                {
                    $bm_list .= $count.':{';
                    $bm_list .= 'id: "'.$value["id"].'",';
                    $bm_list .= 'name: "'.$value["name"].'"';
                    $bm_list .= '},';
                    $count = $count + 1;
                }
                $bm_list .= '};';

                echo "
                    <script>
                        ".$bm_list."
                    </script>
                ";

            }

            require_once('custom/include/global_function/fs_generate_cache.php');
            $fs_generate_cache = new fs_generate_cache;
            echo '<link href="'.$fs_generate_cache->get_cache_version('custom/include/css/departments/departments.css','').'" rel="stylesheet">';
            echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/library/rowspanizer/jquery.rowspanizer.js','').'"></script>';
            echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/library/rowspanizer/jquery.rowspanizer.min.js','').'"></script>';
            if(session_id() != '' && isset($_SESSION)) {
                if(isset($_SESSION["VIN_CSRF_TOKEN"])){
                    echo '
                        <script>
                            var vin_csrf_token = "'.$_SESSION["VIN_CSRF_TOKEN"].'";
                        </script>
                    ';
                }
            }
        
            //Department
            // $querySearch = "SELECT id, name FROM vin_vin_department WHERE deleted = 0 ORDER BY name;";
            $querySearch = "SELECT u.id,u.user_name AS name 
                        FROM users u
                        JOIN users_cstm uc ON u.id=uc.id_c
                        WHERE u.deleted = 0 AND u.status='Active' 
                        AND uc.user_id1_c = '".$id."'
                        AND (uc.user_id_c = '' OR uc.user_id_c IS NULL)
                        ORDER BY name;";
            $result = $db->query($querySearch);
            $dataSet = array();
            $row = $db->fetchByAssoc($result);
            while ($row != null){
                $dataSet[] = $row;
                $row = $db->fetchByAssoc($result);
            }
            $departments_list = 'var departments_list = {';
            $count = 0;
            foreach($dataSet as $key => $value)
            {
                $departments_list .= $count.':{';
                $departments_list .= 'id: "'.$value["id"].'",';
                $departments_list .= 'name: "'.$value["name"].'"';
                $departments_list .= '},';
                $count = $count + 1;
            }
            $departments_list .= '};';
            
            echo "
                <script>
                    ".$departments_list."
                </script>
            ";
        
            //Source
            $allocate_source = 'var allocate_source = {';
            $count = 0;
            foreach($app_list_strings['allocate_dept_source'] as $key => $value)
            {
                $allocate_source .= $count.':{';
                $allocate_source .= 'key: "'.$key.'",';
                $allocate_source .= 'value: "'.$value.'"';
                $allocate_source .= '},';
                $count = $count + 1;
            }
            $allocate_source .= '};';
            echo "
                <script>
                    ".$allocate_source."
                </script>
            ";
        
            //Level 1
            $allocate_level1= 'var allocate_level1 = {';
            $count = 0;
            foreach($app_list_strings['allocate_dept_level1'] as $key => $value)
            {
                $allocate_level1 .= $count.':{';
                $allocate_level1 .= 'key: "'.$key.'",';
                $allocate_level1 .= 'value: "'.$value.'"';
                $allocate_level1 .= '},';
                $count = $count + 1;
            }
            $allocate_level1 .= '};';
            echo "
                <script>
                    ".$allocate_level1."
                </script>
            ";
        
            //Level 2
            $allocate_level2 = 'var allocate_level2 = {';
            $count = 0;
            foreach($app_list_strings['allocate_dept_level2'] as $key => $value)
            {
                $allocate_level2 .= $count.':{';
                $allocate_level2 .= 'key: "'.$key.'",';
                $allocate_level2 .= 'value: "'.$value.'"';
                $allocate_level2 .= '},';
                $count = $count + 1;
            }
            $allocate_level2 .= '};';
            echo "
                <script>
                    ".$allocate_level2."
                </script>
            ";
        
            //Level 3
            $allocate_level3 = 'var allocate_level3 = {';
            $count = 0;
            foreach($app_list_strings['allocate_dept_level3'] as $key => $value)
            {
                $allocate_level3 .= $count.':{';
                $allocate_level3 .= 'key: "'.$key.'",';
                $allocate_level3 .= 'value: "'.$value.'"';
                $allocate_level3 .= '},';
                $count = $count + 1;
            }
            $allocate_level3 .= '};';
            echo "
                <script>
                    ".$allocate_level3."
                </script>
            ";
        
            //Current Allocate
            $dept_id = '';
            $query = "SELECT d.id, d.name FROM vin_vin_department d
                    JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                    JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                    WHERE d.deleted = 0 
                    AND u1.id='".$current_user->id."' LIMIT 1;";
            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $dept_id = $query_result["id"];

            if($dept_id == '' || $dept_id == null){
                $allocate_list = 'var allocate_list = {};';
                echo "
                    <script>
                        ".$allocate_list."
                    </script>
                ";
            }
            else{
                $querySearch = "SELECT a.id, a.source, a.level1, a.level2, a.level3,
                            b.index , b.allocate
                            FROM vin_allocate_matrix a
                            LEFT JOIN vin_allocate_flow_sadmin b ON a.id = b.matrix_id AND b.dept_id = '".$dept_id."'
                            WHERE a.deleted = 0;";
                $result = $db->query($querySearch);
                $dataSet = array();
                $row = $db->fetchByAssoc($result);
                while ($row != null){
                    $dataSet[] = $row;
                    $row = $db->fetchByAssoc($result);
                }
                $allocate_list = 'var allocate_list = {';
                $count = 0;
                foreach($dataSet as $key => $value)
                {
                    $allocate_list .= $count.':{';
                    $allocate_list .= 'id: "'.$value["id"].'",';
                    $allocate_list .= 'index: '.$count.',';
                    $allocate_list .= 'source: "'.$value["source"].'",';
                    $allocate_list .= 'source_disp: "'.$app_list_strings['allocate_dept_source'][$value["source"]].'",';
                    $allocate_list .= 'level1: "'.$value["level1"].'",';
                    $allocate_list .= 'level1_disp: "'.$app_list_strings['allocate_dept_level1'][$value["level1"]].'",';
                    $allocate_list .= 'level2: "'.$value["level2"].'",';
                    $allocate_list .= 'level2_disp: "'.$app_list_strings['allocate_dept_level2'][$value["level2"]].'",';
                    $allocate_list .= 'level3: "'.$value["level3"].'",';
                    $allocate_list .= 'level3_disp: "'.$app_list_strings['allocate_dept_level3'][$value["level3"]].'",';
                    $allocate_list .= 'allocate: "'.$value["allocate"].'",';
                    $allocate_list .= 'current_index: "'.$value["index"].'"';
                    $allocate_list .= '},';
                    $count = $count + 1;
                }
                $allocate_list .= '};';
                echo "
                    <script>
                        ".$allocate_list."
                    </script>
                ";
            }

            break;
        case "loadSourceAsOther":
            $id = $_REQUEST['id'];
            $bm = 0;
            $sa = 0;
            $sg = 0;
            $digital_lead = 0;
            $list_roles = getRoleByUser($current_user->id);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
                else if($value == 'DIGITAL_MARKETING'){
                    $digital_lead = 1;
                }
            }
            if($current_user->is_admin != 1 && $digital != 1){
                $id = '';
            }
            if($current_user->is_admin != 1 && $digital != 1 && $bm != 1){
                die;
            }
            if($id == ''){
                $id = $current_user->id;
            }

            //Sales Admin
            $querySearch = "SELECT u.id,u.user_name AS name 
                        FROM users u
                        JOIN users_cstm uc ON u.id=uc.id_c
                        WHERE u.deleted = 0 AND u.status='Active' 
                        AND uc.user_id1_c = '".$id."'
                        AND (uc.user_id_c = '' OR uc.user_id_c IS NULL)
                        ORDER BY name;";
            $result = $db->query($querySearch);
            $dataSet = array();
            $row = $db->fetchByAssoc($result);
            while ($row != null){
                $dataSet[] = $row;
                $row = $db->fetchByAssoc($result);
            }
            $departments_list = 'var departments_list = {';
            $count = 0;
            foreach($dataSet as $key => $value)
            {
                $departments_list .= $count.':{';
                $departments_list .= 'id: "'.$value["id"].'",';
                $departments_list .= 'name: "'.$value["name"].'"';
                $departments_list .= '},';
                $count = $count + 1;
            }
            $departments_list .= '};';
            
            echo "
                <script>
                    ".$departments_list."
                </script>
            ";

            //Current Allocate
            $dept_id = '';
            $query = "SELECT d.id, d.name FROM vin_vin_department d
                    JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                    JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                    WHERE d.deleted = 0 
                    AND u1.id='".$id."' LIMIT 1;";
            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $dept_id = $query_result["id"];

            if($dept_id == '' || $dept_id == null){
                $allocate_list = 'var allocate_list = {};';
                echo "
                    <script>
                        ".$allocate_list."
                    </script>
                ";
            }
            else{
                $querySearch = "SELECT a.id, a.source, a.level1, a.level2, a.level3,
                            b.index , b.allocate
                            FROM vin_allocate_matrix a
                            LEFT JOIN vin_allocate_flow_sadmin b ON a.id = b.matrix_id AND b.dept_id = '".$dept_id."'
                            WHERE a.deleted = 0;";
                $result = $db->query($querySearch);
                $dataSet = array();
                $row = $db->fetchByAssoc($result);
                while ($row != null){
                    $dataSet[] = $row;
                    $row = $db->fetchByAssoc($result);
                }
                $allocate_list = 'var allocate_list = {';
                $count = 0;
                foreach($dataSet as $key => $value)
                {
                    $allocate_list .= $count.':{';
                    $allocate_list .= 'id: "'.$value["id"].'",';
                    $allocate_list .= 'index: '.$count.',';
                    $allocate_list .= 'source: "'.$value["source"].'",';
                    $allocate_list .= 'source_disp: "'.$app_list_strings['allocate_dept_source'][$value["source"]].'",';
                    $allocate_list .= 'level1: "'.$value["level1"].'",';
                    $allocate_list .= 'level1_disp: "'.$app_list_strings['allocate_dept_level1'][$value["level1"]].'",';
                    $allocate_list .= 'level2: "'.$value["level2"].'",';
                    $allocate_list .= 'level2_disp: "'.$app_list_strings['allocate_dept_level2'][$value["level2"]].'",';
                    $allocate_list .= 'level3: "'.$value["level3"].'",';
                    $allocate_list .= 'level3_disp: "'.$app_list_strings['allocate_dept_level3'][$value["level3"]].'",';
                    $allocate_list .= 'allocate: "'.$value["allocate"].'",';
                    $allocate_list .= 'current_index: "'.$value["index"].'"';
                    $allocate_list .= '},';
                    $count = $count + 1;
                }
                $allocate_list .= '};';
                echo "
                    <script>
                        ".$allocate_list."
                    </script>
                ";
            }
            break;
        case "save_allocate_matrix":
            $return = array();
            $id = $_REQUEST['id'];
            $bm = 0;
            $sa = 0;
            $sg = 0;
            $digital_lead = 0;
            $list_roles = getRoleByUser($current_user->id);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
                else if($value == 'DIGITAL_MARKETING'){
                    $digital_lead = 1;
                }
            }
            if($current_user->is_admin != 1 && $digital != 1){
                $id = '';
            }
            if($current_user->is_admin != 1 && $digital != 1 && $bm != 1){
                die;
            }
            if($id == ''){
                $id = $current_user->id;
            }

            $data = $_REQUEST["data"];
            $dept_id = '';
            $query = "SELECT d.id, d.name FROM vin_vin_department d
                    JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                    JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                    WHERE d.deleted = 0 
                    AND u1.id='".$id."' LIMIT 1;";
            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $dept_id = $query_result["id"];
            if($data == '' || $data == null || !is_array($data)){
            }
            else{
                if($dept_id != '' && $dept_id != null){
                    foreach ($data as $key => $value) {
                        if($value["id"] == "" || $value["id"] == null){
                            // $query_insert = "INSERT INTO vin_allocate_matrix (id,source,level1,level2,
                            //         level3,allocate,date_created,created_by,date_modified,modified_by,deleted)
                            //     VALUES ((SELECT UUID()),'".$value["source"]."','".$value["level1"]."','".$value["level2"]."','".$value["level3"]."',
                            //         '".$value["allocate"]."',NOW(),'".$current_user->id."', NOW(), '".$current_user->id."', 0);";
                            // $db->query($query_insert);
                        }
                        else{
                            // $query_update = " UPDATE vin_allocate_matrix SET 
                            //     allocate = '".$value["allocate"]."' ,date_modified = NOW(),modified_by = '".$current_user->id."'
                            //     WHERE id = '".$value["id"]."' AND allocate <> '".$value["allocate"]."';";
                            // $db->query($query_update);
    
                            $flow_id = '';
                            $query = "SELECT id FROM vin_allocate_flow_sadmin
                                    WHERE matrix_id = '".$value["id"]."' AND dept_id = '".$dept_id."' LIMIT 1;";
    
                            $query_result = $db->query($query);
                            $query_result = $db->fetchByAssoc($query_result);
                            $flow_id = $query_result["id"];
    
                            if($flow_id == '' || $flow_id == null){
                                $query_insert = "INSERT INTO vin_allocate_flow_sadmin (allocate,date_modified,modified_by,dept_id,`matrix_id`)
                                    VALUES ('".$value["allocate"]."', NOW(), '".$current_user->id."', '".$dept_id."','".$value["id"]."');";
                                $db->query($query_insert);
                            }
                            else{
                                $query_update = " UPDATE vin_allocate_flow_sadmin SET 
                                    allocate = '".$value["allocate"]."' ,date_modified = NOW(),modified_by = '".$current_user->id."'
                                    WHERE id = '".$flow_id."' AND allocate <> '".$value["allocate"]."';";
                                $db->query($query_update);
                            }
                        }
                    }
                }
            }

            //Current Allocate
            if($dept_id == '' || $dept_id == null){

            }
            else{
                $querySearch = "SELECT a.id, a.source, a.level1, a.level2, a.level3,
                            b.index , b.allocate
                            FROM vin_allocate_matrix a
                            LEFT JOIN vin_allocate_flow_sadmin b ON a.id = b.matrix_id AND b.dept_id = '".$dept_id."'
                            WHERE a.deleted = 0;";
                $result = $db->query($querySearch);
                $dataSet = array();
                $row = $db->fetchByAssoc($result);
                while ($row != null){
                    $dataSet[] = $row;
                    $row = $db->fetchByAssoc($result);
                }
                $count = 0;
                foreach($dataSet as $key => $value)
                {
                    $return[$count]["id"] = $value["id"];
                    $return[$count]["index"] = $count;
                    $return[$count]["source"] = $value["source"];
                    $return[$count]["source_disp"] = $app_list_strings['allocate_dept_source'][$value["source"]];
                    $return[$count]["level1"] = $value["level1"];
                    $return[$count]["level1_disp"] = $app_list_strings['allocate_dept_level1'][$value["level1"]];
                    $return[$count]["level2"] = $value["level2"];
                    $return[$count]["level2_disp"] = $app_list_strings['allocate_dept_level2'][$value["level2"]];
                    $return[$count]["level3"] = $value["level3"];
                    $return[$count]["level3_disp"] = $app_list_strings['allocate_dept_level3'][$value["level3"]];
                    $return[$count]["allocate"] = $value["allocate"];
                    $return[$count]["current_index"] = $value["index"];
                    $count = $count + 1;
                }
            }

            ob_start();
            ob_clean();
            header('Content-type: application/json');
            echo json_encode($return);
            break;
        case "loadNotification":
            $page = $_REQUEST['page'];
            $per_page = 50;
            if($page == '' || $page == null){
                $page = 0;
            }
            $limit = ((float)$page + 1)*$per_page;

            require_once('custom/include/global_function/fs_generate_cache.php');
            $fs_generate_cache = new fs_generate_cache;
            echo '<link href="'.$fs_generate_cache->get_cache_version('custom/include/css/departments/departments.css','').'" rel="stylesheet">';
            echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/library/rowspanizer/jquery.rowspanizer.js','').'"></script>';
            echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/library/rowspanizer/jquery.rowspanizer.min.js','').'"></script>';
            if(session_id() != '' && isset($_SESSION)) {
                if(isset($_SESSION["VIN_CSRF_TOKEN"])){
                    echo '
                        <script>
                            var vin_csrf_token = "'.$_SESSION["VIN_CSRF_TOKEN"].'";
                        </script>
                    ';
                }
            }
        
            $query = "SELECT COUNT(*) AS count FROM alerts 
                    WHERE assigned_user_id = '".$current_user->id."' 
                    AND deleted = 0";

            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $count_alerts = $query_result["count"];

            $querySearch = "SELECT * FROM alerts 
                    WHERE assigned_user_id = '".$current_user->id."' 
                    AND deleted = 0 ORDER BY date_entered DESC LIMIT ".$limit.";";
            $result = $db->query($querySearch);
            $dataSet = array();
            $row = $db->fetchByAssoc($result);
            while ($row != null){
                $dataSet[] = $row;
                $row = $db->fetchByAssoc($result);
            }
            $allocate_list = 'var allocate_list = {';
            $count = 0;
            foreach($dataSet as $key => $value)
            {
                $allocate_list .= $count.':{';
                $allocate_list .= 'id: "'.$value["id"].'",';
                $allocate_list .= 'index: '.$count.',';
                $allocate_list .= 'name: "'.$value["name"].'",';
                $allocate_list .= 'total: "'.$count_alerts.'",';
                $allocate_list .= 'content: "'.$value["description"].'",';
                $allocate_list .= 'date_entered: "'.$value["date_entered"].'",';
                $allocate_list .= 'is_read: "'.$value["is_read"].'"';
                $allocate_list .= '},';
                $count = $count + 1;
            }
            $allocate_list .= '};';
            echo "
                <script>
                    ".$allocate_list."
                </script>
            ";

            
            break;
        default:
            echo "Something went wrong! Can not find Action!";
    }
    
}

function getSaleAdminUser($id){
    global $db;

    $user_id = '';
    $query = "SELECT user_id_c FROM users_cstm uc
            JOIN users u ON uc.id_c = u.id 
            WHERE u.id = '".$id."' AND u.status = 'Active' LIMIT 1;";

    $query_result = $db->query($query);
    $query_result = $db->fetchByAssoc($query_result);
    $user_id = $query_result["user_id_c"];
    return $user_id;
}

function getBusinessManagementUser($id){
    global $db;

    $user_id = '';
    $query = "SELECT user_id1_c FROM users_cstm uc
            JOIN users u ON uc.id_c = u.id 
            WHERE u.id = '".$id."' AND u.status = 'Active' LIMIT 1;";

    $query_result = $db->query($query);
    $query_result = $db->fetchByAssoc($query_result);
    $user_id = $query_result["user_id1_c"];
    return $user_id;
}

function getRoleByUser($id){
    global $db, $current_user;

    $list_roles = array();
    $query = "SELECT acl.name FROM acl_roles acl 
        JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
        JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
        JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
        WHERE acl.deleted = 0 AND sgu.user_id = '".$id."';";

    $result = $db->query($query);
    $dataSet = array();
    $row = $db->fetchByAssoc($result);
    while ($row != null){
        $dataSet[] = $row;
        $row = $db->fetchByAssoc($result);
    }
    foreach($dataSet as $key => $value){
        array_push($list_roles,$value['name']);
    }
    return $list_roles;
}

function getDepartmentOfUser($user_id){
    global $db, $current_user;

    $bm = 0;
    $sa = 0;
    $sg = 0;
    $department = array();
    $department['id'] = '';
    $department['name'] = '';
    $query = '';
    $list_roles = getRoleByUser($user_id);
    foreach($list_roles as $key => $value){
        if($value == 'BUSINESS_MANAGER'){
            $bm = 1;
        }
        else if($value == 'SALE_ADMIN'){
            $sa = 1;
        }
        else if($value == 'SALE_AGENT'){
            $sg = 1;
        }
    }
    if($bm == 1){
        $query = "SELECT d.id, d.name FROM vin_vin_department d
            JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
            JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
            WHERE d.deleted = 0 
            AND u1.id='".$user_id."' LIMIT 1;";
    }
    else if($sa == 1){
        $query = "SELECT d.id, d.name FROM vin_vin_department d
            JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
            JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
            JOIN users_cstm u3 ON u1.id=u3.user_id1_c 
            WHERE d.deleted = 0
            AND u3.id_c='".$user_id."' LIMIT 1;";
    }
    else if($sg == 1){
        $query = "SELECT d.id, d.name FROM vin_vin_department d
            JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
            JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
            JOIN users_cstm u3 ON u1.id=u3.user_id1_c 
            JOIN users_cstm u4 ON u3.id_c=u4.user_id_c 
            WHERE d.deleted = 0
            AND u4.id_c='".$user_id."' LIMIT 1;";
    }
    if($query != ''){
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $department['id'] = $query_result["id"];
        $department['name'] = $query_result["name"];
    }
    return $department;
}

function importLeads(){
    global $db, $current_user, $app_list_strings;

    $returnValue = array();
    $name_require = "";
    $email_require = "";
    $project_require = "";
    $project_invalid = "";
    $email_invalid = "";
    $phone_invalid = "";
    $valid = 0;
    try {
        require "custom/include/library/PHPExcel/Classes/PHPExcel.php";
        $file = $_FILES['upload_file']['tmp_name'];
        if (isset($_FILES['upload_file']['tmp_name'])) {
            $objReader = PHPExcel_IOFactory::createReaderForFile($file);
            $listWorkSheets = $objReader->listWorkSheetNames($file);
            $is_valid = 1;
            $firstsheet = 1;
            $totalRecord = 0;
            $totalRecord_Hot = 0;
            $totalRecord_Warm = 0;
            $totalRecord_Cold = 0;
            $count = 0;
            $mark_leads_count = 0;
            $mark_leads = array();
            foreach ($listWorkSheets as $sheetName){
                if( $firstsheet == 1){
                    $firstsheet = 0;
                    $objReader->setLoadSheetsOnly($sheetName);
                    $objPHPExcel = $objReader->load($file);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    $highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();
                    for ($row = 2; $row <= $highestRow; $row++){
                        $mark_leads_count = $mark_leads_count + 1;
                        $all_empty = 0;
                        $old_count = $count;
                        $salutation = trim($sheetData[$row]['A']); //dropdown
                        $lastName = trim($sheetData[$row]['B']);
                        $firstName = trim($sheetData[$row]['C']);
                        $middleName = trim($sheetData[$row]['D']);
                        $suffix = trim($sheetData[$row]['E']);
                        $email = trim($sheetData[$row]['F']);
                        $phone = trim($sheetData[$row]['G']);
                        $idNumber = trim($sheetData[$row]['H']);
                        $companyName = trim($sheetData[$row]['I']);
                        $tax = trim($sheetData[$row]['J']);
                        $mobile = trim($sheetData[$row]['K']);
                        $project = trim($sheetData[$row]['L']); //dropdown
                        $budget = floatval(preg_replace('/\,/', '', trim($sheetData[$row]['M'])));
                        $houseType = trim($sheetData[$row]['N']); //dropdown
                        $leadSource = trim($sheetData[$row]['O']); //dropdown
                        $sourceDetails = trim($sheetData[$row]['P']);
                        $address = trim($sheetData[$row]['Q']);
                        $city = trim($sheetData[$row]['R']);
                        $street = trim($sheetData[$row]['S']);
                        $country = trim($sheetData[$row]['T']);
                        $sate = trim($sheetData[$row]['U']);
                        $zip = trim($sheetData[$row]['V']);
                        $idSalesforce = trim($sheetData[$row]['W']);
                        $campaign = trim($sheetData[$row]['X']);

                        $salutationKey = "";
                        $projectKey = "";
                        $houseTypeKey = "";
                        $leadSourceKey = "";

                        if(isNullOrEmpty($salutation) && isNullOrEmpty($lastName)
                        && isNullOrEmpty($firstName) && isNullOrEmpty($middleName)
                        && isNullOrEmpty($suffix) && isNullOrEmpty($email)
                        && isNullOrEmpty($phone) && isNullOrEmpty($idNumber)
                        && isNullOrEmpty($companyName) && isNullOrEmpty($mobile)
                        && isNullOrEmpty($tax) && isNullOrEmpty($project)
                        && isNullOrEmpty($budget) && isNullOrEmpty($houseType)
                        && isNullOrEmpty($leadSource) && isNullOrEmpty($sourceDetails)
                        && isNullOrEmpty($address) && isNullOrEmpty($city)
                        && isNullOrEmpty($street) && isNullOrEmpty($country)
                        && isNullOrEmpty($sate) && isNullOrEmpty($zip)
                        && isNullOrEmpty($idSalesforce)){
                            $all_empty = 1;
                        }
 
                        if($all_empty == 0){
                            foreach ($app_list_strings["vin_salutation_c_list"] as $key => $value) {
                                if(trim(strtolower($value)) == trim(strtolower($salutation))){
                                    $salutationKey = $key;
                                }
                            }
                            foreach ($app_list_strings["allocate_dept_level1"] as $key => $value) {
                                if(trim(strtolower($value)) == trim(strtolower($project))){
                                    $projectKey = $key;
                                }
                            }
                            foreach ($app_list_strings["vin_house_type_list_c_list"] as $key => $value) {
                                if(trim(strtolower($value)) == trim(strtolower($houseType))){
                                    $houseTypeKey = $key;
                                }
                            }
                            foreach ($app_list_strings["allocate_dept_source"] as $key => $value) {
                                if(trim(strtolower($value)) == trim(strtolower($leadSource))){
                                    $leadSourceKey = $key;
                                }
                            }
    
                            //Validate
                            if($lastName == '' || $lastName == null){
                                $name_require = "Last Name is mandatory.";
                                $is_valid = 0;
                            }
    
                            if($email == '' || $email == null){
                                $email_require = "Email is mandatory.";
                                $is_valid = 0;
                            }
                            else{
                                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                                    $email_invalid .= $email.", ";
                                    $is_valid = 0;
                                }
                            }
    
                            if($project == '' || $project == null){
                                $project_require = "Project is mandatory.";
                                $is_valid = 0;
                            }
                            else{
                                if($projectKey == '' || $projectKey == null){
                                    $project_invalid .= $project.", ";
                                    $is_valid = 0;
                                }
                            }
    
                            if($phone != "" && $phone != null){
                                if(!preg_match("/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im", $phone)) {
                                    $phone_invalid .= $phone.", ";
                                    $is_valid = 0;
                                }
                            }

                            if($is_valid == 1){
                                $bean = BeanFactory::newBean('vin_vin_leads');
                                $bean->vin_salutation_c = $salutationKey;
                                $bean->vin_last_name_c = $lastName;
                                $bean->vin_first_name_c = $firstName;
                                $bean->vin_middle_name_c = $middleName;
                                $bean->vin_suffix_c = $suffix;
                                $bean->vin_email_c = $email;
                                $bean->vin_phone_number_c = $phone;
                                $bean->vin_id_number_c = $idNumber;
                                $bean->vin_company_name_c = $companyName;
                                $bean->vin_tax_code_c = $tax;
                                $bean->vin_mobile_c = $mobile;
                                $bean->vin_project_c = $projectKey;
                                $bean->vin_house_type_list_c = $houseTypeKey;
                                $bean->vin_budget_c = $budget;
                                $bean->vin_lead_source_c = $leadSourceKey;
                                $bean->vin_source_details_c = $sourceDetails;
                                $bean->vin_address_c = $address;
                                $bean->vin_city_text_c = $city;
                                $bean->vin_street_c = $street;
                                $bean->vin_country_c = $country;
                                $bean->vin_state_province_c = $sate;
                                $bean->vin_zip_postal_code_c = $zip;
                                $bean->vin_lead_id_salesforce_c = $idSalesforce;
    
                                $bean->vin_vin_currency_id_c = '';
                                $bean->user_id4_c = '';
                                $bean->user_id1_c = '';
                                $bean->ownership = 'import';
    
                                $score = calculateScore($bean);
                                $bean->vin_rating_c = $score['rating'];
                                if($bean->vin_rating_c == 'hot'){
                                    $totalRecord_Hot = $totalRecord_Hot + 1;
                                }
                                else if($bean->vin_rating_c == 'warm'){
                                    $totalRecord_Warm = $totalRecord_Warm + 1;
                                }
                                else{
                                    $totalRecord_Cold = $totalRecord_Cold + 1;
                                }
                                $mark_leads[$mark_leads_count] = $bean->vin_rating_c;
                                $totalRecord = $totalRecord + 1;
                            }
                        }
                    }
                }
            }

            //Import
            if($is_valid == 1){
                //Allocate
                $list_roles = getRoleCurrentUser();
                $bm = 0;
                $sa = 0;
                $sg = 0;
                $list_admins = array();
                $list_sales = array();

                $list_allocated =  array();
                $list_allocated['hot'] =  array();
                $list_allocated['warm'] =  array();
                $list_allocated['cold'] =  array();

                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }
                if($bm == 1){
                    $list_sa = getListSaleAdminAllocate();
                    foreach($list_sa as $key => $value){
                        $list_sales_temp = getListSaleAgentAllocate($value);
                        array_push($list_admins, $value);
                        if (array_key_exists($value,$list_sales)){
                            $list_sales[$value] = $list_sales_temp;
                        }
                        else{
                            $list_sales[$value] = array();
                            $list_sales[$value] = $list_sales_temp;
                        }
                    }
                }
                else if($sa == 1){
                    $list_sales_temp = getListSaleAgentAllocate($current_user->id);
                    $value = $current_user->id;
                    array_push($list_admins, $value);
                    if (array_key_exists($value,$list_sales)){
                        $list_sales[$value] = $list_sales_temp;
                    }
                    else{
                        $list_sales[$value] = array();
                        $list_sales[$value] = $list_sales_temp;
                    }
                }

                $fmod_hot = 0;
                $fmod_warm = 0;
                $fmod_cold = 0;
                if($bm == 1){
                    if(count($list_admins) < $totalRecord_Hot){
                        $fmod_hot = fmod($totalRecord_Hot, count($list_admins));
                    }
                    else{ 
                        $fmod_hot = -1;
                    }
                    if(count($list_admins) < $totalRecord_Warm){
                        $fmod_warm = fmod($totalRecord_Warm, count($list_admins));
                    }
                    else{ 
                        $fmod_warm = -1;
                    }
                    if(count($list_admins) < $totalRecord_Cold){
                        $fmod_cold = fmod($totalRecord_Cold, count($list_admins));
                    }
                    else{ 
                        $fmod_cold = -1;
                    }
                }
                else if($sa == 1){
                    if(count($list_admins) < $totalRecord_Hot){
                        $fmod_hot = fmod($totalRecord_Hot, count($list_admins));
                    }
                    else{ 
                        $fmod_hot = -1;
                    }
                    if(count($list_sales[$current_user->id]) < $totalRecord_Warm){
                        $fmod_warm = fmod($totalRecord_Warm, count($list_sales[$current_user->id]));
                    }
                    else{ 
                        $fmod_warm = -1;
                    }
                    if(count($list_sales[$current_user->id]) < $totalRecord_Cold){
                        $fmod_cold = fmod($totalRecord_Cold, count($list_sales[$current_user->id]));
                    }
                    else{ 
                        $fmod_cold = -1;
                    }
                }
                //Import
                $firstsheet = 1;
                $count = 0;
                $leads_saved = 0;
                $mark_leads_count = 0;
                $hot_saved = 0;
                $warm_saved = 0;
                $cold_saved = 0;
                foreach ($listWorkSheets as $sheetName){
                    if( $firstsheet == 1){
                        $firstsheet = 0;
                        $objReader->setLoadSheetsOnly($sheetName);
                        $objPHPExcel = $objReader->load($file);
                        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                        $highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();
                        for ($row = 2; $row <= $highestRow; $row++){
                            $mark_leads_count = $mark_leads_count + 1;
                            $all_empty = 0;
                            $old_count = $count;
                            $salutation = trim($sheetData[$row]['A']); //dropdown
                            $lastName = trim($sheetData[$row]['B']);
                            $firstName = trim($sheetData[$row]['C']);
                            $middleName = trim($sheetData[$row]['D']);
                            $suffix = trim($sheetData[$row]['E']);
                            $email = trim($sheetData[$row]['F']);
                            $phone = trim($sheetData[$row]['G']);
                            $idNumber = trim($sheetData[$row]['H']);
                            $companyName = trim($sheetData[$row]['I']);
                            $tax = trim($sheetData[$row]['J']);
                            $mobile = trim($sheetData[$row]['K']);
                            $project = trim($sheetData[$row]['L']); //dropdown
                            $budget = floatval(preg_replace('/\,/', '', trim($sheetData[$row]['M'])));
                            $houseType = trim($sheetData[$row]['N']); //dropdown
                            $leadSource = trim($sheetData[$row]['O']); //dropdown
                            $sourceDetails = trim($sheetData[$row]['P']);
                            $address = trim($sheetData[$row]['Q']);
                            $city = trim($sheetData[$row]['R']);
                            $street = trim($sheetData[$row]['S']);
                            $country = trim($sheetData[$row]['T']);
                            $sate = trim($sheetData[$row]['U']);
                            $zip = trim($sheetData[$row]['V']);
                            $idSalesforce = trim($sheetData[$row]['W']);
                            $campaign = trim($sheetData[$row]['X']);
                            $ad_content = trim($sheetData[$row]['Y']);
                            $ad_term = trim($sheetData[$row]['Z']);
                            $form_medium = trim($sheetData[$row]['AA']);
                            $utm_source = trim($sheetData[$row]['AB']);
                            $cs_note = trim($sheetData[$row]['AC']);
                            $lead_ads_id = trim($sheetData[$row]['AD']);
    
                            $salutationKey = "";
                            $projectKey = "";
                            $houseTypeKey = "";
                            $leadSourceKey = "";
                            
                            if(isNullOrEmpty($salutation) && isNullOrEmpty($lastName)
                            && isNullOrEmpty($firstName) && isNullOrEmpty($middleName)
                            && isNullOrEmpty($suffix) && isNullOrEmpty($email)
                            && isNullOrEmpty($phone) && isNullOrEmpty($idNumber)
                            && isNullOrEmpty($companyName) && isNullOrEmpty($mobile)
                            && isNullOrEmpty($tax) && isNullOrEmpty($project)
                            && isNullOrEmpty($budget) && isNullOrEmpty($houseType)
                            && isNullOrEmpty($leadSource) && isNullOrEmpty($sourceDetails)
                            && isNullOrEmpty($address) && isNullOrEmpty($city)
                            && isNullOrEmpty($street) && isNullOrEmpty($country)
                            && isNullOrEmpty($sate) && isNullOrEmpty($zip)
                            && isNullOrEmpty($idSalesforce)){
                                $all_empty = 1;
                            }

                            if($all_empty == 0){
                                foreach ($app_list_strings["vin_salutation_c_list"] as $key => $value) {
                                    if(trim(strtolower($value)) == trim(strtolower($salutation))){
                                        $salutationKey = $key;
                                    }
                                }
                                foreach ($app_list_strings["allocate_dept_level1"] as $key => $value) {
                                    if(trim(strtolower($value)) == trim(strtolower($project))){
                                        $projectKey = $key;
                                    }
                                }
                                foreach ($app_list_strings["vin_house_type_list_c_list"] as $key => $value) {
                                    if(trim(strtolower($value)) == trim(strtolower($houseType))){
                                        $houseTypeKey = $key;
                                    }
                                }
                                foreach ($app_list_strings["allocate_dept_source"] as $key => $value) {
                                    if(trim(strtolower($value)) == trim(strtolower($leadSource))){
                                        $leadSourceKey = $key;
                                    }
                                }
        
                                $bean = BeanFactory::newBean('vin_vin_leads');
                                $bean->vin_salutation_c = $salutationKey;
                                $bean->vin_last_name_c = $lastName;
                                $bean->vin_first_name_c = $firstName;
                                $bean->vin_middle_name_c = $middleName;
                                $bean->vin_suffix_c = $suffix;
                                $bean->vin_email_c = $email;
                                $bean->vin_phone_number_c = $phone;
                                $bean->vin_id_number_c = $idNumber;
                                $bean->vin_company_name_c = $companyName;
                                $bean->vin_tax_code_c = $tax;
                                $bean->vin_mobile_c = $mobile;
                                $bean->vin_project_c = $projectKey;
                                $bean->vin_house_type_list_c = $houseTypeKey;
                                $bean->vin_budget_c = $budget;
                                $bean->vin_lead_source_c = $leadSourceKey;
                                $bean->vin_source_details_c = $sourceDetails;
                                $bean->vin_address_c = $address;
                                $bean->vin_city_text_c = $city;
                                $bean->vin_street_c = $street;
                                $bean->vin_country_c = $country;
                                $bean->vin_state_province_c = $sate;
                                $bean->vin_zip_postal_code_c = $zip;
                                $bean->vin_lead_id_salesforce_c = $idSalesforce;
                                $bean->vin_campaign_c = $campaign;
                                $bean->vin_ads_content_c = $ad_content;
                                $bean->vin_ad_term_c = $ad_term;
                                $bean->vin_form_medium_c = $form_medium;
                                $bean->vin_utm_source_c = $utm_source;
                                $bean->vin_note_c = $cs_note;
                                $bean->vin_lead_ads_id_c = $lead_ads_id;
    
                                $campaign_array =  explode('_',$campaign);
                                if($projectKey == ''){
                                    if (array_key_exists(0,$campaign_array)){
                                        $bean->vin_project_c = $campaign_array[0];
                                    }
                                }
                                if (array_key_exists(1,$campaign_array)){
                                    $bean->vin_phankhu_c = $campaign_array[1];
                                }
                                if (array_key_exists(2,$campaign_array)){
                                    $bean->vin_ads_source_c = $campaign_array[2];
                                }
                                if (array_key_exists(3,$campaign_array)){
                                    $bean->vin_agent_c = $campaign_array[3];
                                }

                                $bean->vin_vin_currency_id_c = '';
                                $bean->user_id4_c = '';
                                $bean->user_id1_c = '';
                                $bean->ownership = 'import';
    
                                if($mark_leads[$mark_leads_count] == 'hot'){
                                    if($fmod_hot == -1){
                                        $bean->ownership = 'import_auto';
                                    }
                                    else if($hot_saved <= $fmod_hot && $fmod_hot != 0){
                                        $bean->ownership = 'import_auto';
                                    }
                                    $hot_saved = $hot_saved + 1;
                                }
                                else if($mark_leads[$mark_leads_count] == 'warm'){
                                    if($fmod_warm == -1){
                                        $bean->ownership = 'import_auto';
                                    }
                                    else if($warm_saved <= $fmod_warm && $fmod_warm != 0){
                                        $bean->ownership = 'import_auto';
                                    }
                                    $warm_saved = $warm_saved + 1;
                                }
                                else{
                                    if($fmod_cold == -1){
                                        $bean->ownership = 'import_auto';
                                    }
                                    else if($cold_saved <= $fmod_cold && $fmod_cold != 0){
                                        $bean->ownership = 'import_auto';
                                    }
                                    $cold_saved = $cold_saved + 1;
                                }

                                $bean->ownership = 'import_allocate';

                                if($bean->ownership == 'import'){
                                    $score = calculateScore($bean);
                                    $bean->vin_lead_scoring_c = $score['score'];
                                    $bean->vin_rating_c = $score['rating'];
    
                                    //Allocate
                                    if($bm == 1){
                                        $bean->user_id_c = $current_user->id;
                                    }
                                    else if($sa == 1){
                                        $bean->user_id1_c = $current_user->id;
                                        $bm_id = getBusinessManagementCurrentUser($current_user->id);
                                        if($bm_id != '' && $bm_id != null){
                                            $bean->user_id_c = $bm_id;
                                        }
                                    }
                                    else if($sg == 1){
                                        $bean->user_id2_c = $current_user->id;
                                        $sa_id = getSaleAdminCurrentUser();
                                        if($sa_id != '' && $sa_id != null){
                                            $bean->user_id1_c = $sa_id;
                                            $bm_id = getBusinessManagementCurrentUser($sa_id);
                                            if($bm_id != '' && $bm_id != null){
                                                $bean->user_id_c = $bm_id;
                                            }
                                        }
                                    }
    
                                    $temp_allowcate = array();
                                    if($bean->vin_rating_c == 'hot'){
                                        $temp_allowcate = $list_allocated['hot'];
                                    }
                                    else if($bean->vin_rating_c == 'warm'){
                                        $temp_allowcate = $list_allocated['warm'];
                                    }
                                    else{
                                        $temp_allowcate = $list_allocated['cold'];
                                    }
    
                                    $last_value = end($temp_allowcate);
                                    $sales_admin_index = 0;
                                    $sales_agent_index = 0;
                                    if(!isNullOrEmpty($last_value)){
                                        if (strpos($last_value, '_') !== false) {
                                            $temp_allo = explode("_",$last_value);
                                            $sales_admin_next_index = intval($temp_allo[0]) + 1;
                                            if (array_key_exists($sales_admin_next_index,$list_admins)){
                                                $sales_admin_index = $sales_admin_next_index;
                                            }
                                            else{
                                                $sales_admin_index = 0;
                                                $sales_admin_next_index = 0;
                                            }
                                            $sales_admin_id = $list_admins[$sales_admin_index];
    
                                            $last_agent_of_admin = 0;
                                            $last_agent = "";
                                            $sales_agent_next_index = 0;
                                            foreach($temp_allowcate as $ka => $va){
                                                if(startsWith($va, $sales_admin_next_index."_")){
                                                    $last_agent = $va;
                                                }
                                            }
    
                                            if(!isNullOrEmpty($last_agent)){
                                                if (strpos($last_agent, '_') !== false) {
                                                    $temp_allo2 = explode("_",$last_agent);
                                                    $sales_agent_next_index = intval($temp_allo2[1]) + 1;
                                                }
                                            }

                                            if (array_key_exists($sales_admin_id,$list_sales)){
                                                if (array_key_exists($sales_agent_next_index,$list_sales[$sales_admin_id])){
                                                    $sales_agent_index = $sales_agent_next_index;
                                                }
                                                else{
                                                    $sales_agent_index = 0;
                                                }
                                            }
    
                                        }
                                    }
                                    else{
                                        $sales_admin_next_index = 0;
                                        $sales_agent_next_index = 0;
                                        if (array_key_exists($sales_admin_next_index,$list_admins)){
                                            $sales_admin_index = $sales_admin_next_index;
                                        }
                                        $sales_admin_id = $list_admins[$sales_admin_index];
                                        if (array_key_exists($sales_admin_id,$list_sales)){
                                            if (array_key_exists($sales_agent_next_index,$list_sales[$sales_admin_id])){
                                                $sales_agent_index = $sales_agent_next_index;
                                            }
                                        }
                                    }
    
                                    if (array_key_exists($sales_admin_index,$list_admins)){
                                        $bean->user_id1_c = $list_admins[$sales_admin_index];
                                    }
    
                                    if (array_key_exists($sales_admin_index,$list_admins)){
                                        if (array_key_exists($list_admins[$sales_admin_index],$list_sales)){
                                            if (array_key_exists($sales_agent_index,$list_sales[$list_admins[$sales_admin_index]])){
                                                if($sa == 1){
                                                    $bean->user_id2_c = $list_sales[$list_admins[$sales_admin_index]][$sales_agent_index];
                                                }
                                            }
                                        } 
                                    }
                                    
                                    if($bean->vin_rating_c == 'hot'){
                                       array_push($list_allocated['hot'],$sales_admin_index."_".$sales_agent_index);
                                    }
                                    else if($bean->vin_rating_c == 'warm'){
                                        array_push($list_allocated['warm'],$sales_admin_index."_".$sales_agent_index);
                                    }
                                    else{
                                        array_push($list_allocated['cold'],$sales_admin_index."_".$sales_agent_index);
                                    }
                                }
                                else if($bean->ownership == 'import_allocate'){
                                    $score = calculateScore($bean);
                                    $bean->vin_lead_scoring_c = $score['score'];
                                    $bean->vin_rating_c = $score['rating'];
                                    //Allocate
                                    if($bm == 1){
                                        $bean->user_id_c = $current_user->id;
                                    }
                                    else if($sa == 1){
                                        $bean->user_id1_c = $current_user->id;
                                        $bm_id = getBusinessManagementCurrentUser($current_user->id);
                                        if($bm_id != '' && $bm_id != null){
                                            $bean->user_id_c = $bm_id;
                                        }
                                    }
                                    else if($sg == 1){
                                        $bean->user_id2_c = $current_user->id;
                                        $sa_id = getSaleAdminCurrentUser();
                                        if($sa_id != '' && $sa_id != null){
                                            $bean->user_id1_c = $sa_id;
                                            $bm_id = getBusinessManagementCurrentUser($sa_id);
                                            if($bm_id != '' && $bm_id != null){
                                                $bean->user_id_c = $bm_id;
                                            }
                                        }
                                    }
                                }

                                $bean->save();
                                $valid = 1;
                                $leads_saved = $leads_saved + 1;
                            }
                        }
                    }
                }
            }
        }
    }
    catch (Exception $e) {
        $GLOBALS['log']->fatal('Import Leads Error: '.$e->getMessage());
    }
    $returnValue['valid'] = $valid;
    $returnValue['name_require'] = $name_require;
    $returnValue['email_require'] = $email_require;
    $returnValue['project_require'] = $project_require;
    $returnValue['email_invalid'] = $email_invalid;
    $returnValue['phone_invalid'] = $phone_invalid;
    $returnValue['project_invalid'] = $project_invalid;
    return $returnValue;
}

function isNullOrEmpty($str){
    if($str == null || $str == ""){
        return true;
    }
    else if(trim($str) == ""){
        return true;
    }
    else return false;
}

function calculateScore($bean){
    global $db, $current_user;
    $result_return = array();

    $score = 0;
    $rating = 'cold';
    //Score
    $query = "SELECT * FROM vin_lead_scoring s
        JOIN vin_lead_scoring_cstm sc ON s.id = sc.id_c
        WHERE s.deleted = 0;";

    $result = $db->query($query);
    $dataSet = array();
    $row = $db->fetchByAssoc($result);
    while ($row != null){
        $dataSet[] = $row;
        $row = $db->fetchByAssoc($result);
    }
    foreach($dataSet as $key => $value){
        if(property_exists($bean, $value['vin_field_c'])){
            if($bean->{$value['vin_field_c']} != null && $bean->{$value['vin_field_c']} != ''){
                $score = $score + (float)$value['vin_score_c'];
            }
        }
    }

    //Rating
    $query = "SELECT sc.vin_rating_c FROM vin_lead_rating_score s
        JOIN vin_lead_rating_score_cstm sc ON s.id = sc.id_c
        WHERE s.deleted = 0 
        AND sc.vin_from_c <= ".$score." AND sc.vin_to_c >= ".$score." LIMIT 1;";

    $query_result = $db->query($query);
    $query_result = $db->fetchByAssoc($query_result);
    $vin_rating_c = $query_result["vin_rating_c"];
    if($vin_rating_c != '' && $vin_rating_c != null){
        $rating = $vin_rating_c;
    }

    $result_return['score'] = $score;
    $result_return['rating'] = $rating;
    return $result_return;
}

function getRoleCurrentUser(){
    global $db, $current_user;

    $list_roles = array();
    $query = "SELECT acl.name FROM acl_roles acl 
        JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
        JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
        JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
        WHERE acl.deleted = 0 AND sgu.user_id = '".$current_user->id."';";

    $result = $db->query($query);
    $dataSet = array();
    $row = $db->fetchByAssoc($result);
    while ($row != null){
        $dataSet[] = $row;
        $row = $db->fetchByAssoc($result);
    }
    foreach($dataSet as $key => $value){
        array_push($list_roles,$value['name']);
    }
    return $list_roles;
}

function getListSaleAgentAllocate($id){
    global $db, $current_user;

    $list_sales = array();
    $query = "SELECT DISTINCT u.id FROM users_cstm uc
        JOIN users u ON uc.id_c = u.id 
        LEFT JOIN vin_vin_leads_cstm lc ON lc.user_id2_c = u.id
        LEFT JOIN vin_vin_leads l ON l.id = lc.id_c AND l.deleted = 0 
        WHERE uc.user_id_c = '".$id."' AND u.status = 'Active' AND u.deleted = 0;";

    $result = $db->query($query);
    $dataSet = array();
    $row = $db->fetchByAssoc($result);
    while ($row != null){
        $dataSet[] = $row;
        $row = $db->fetchByAssoc($result);
    }
    foreach($dataSet as $key => $value){
        array_push($list_sales,$value['id']);
    }
    return $list_sales;
}

function getListSaleAdminAllocate(){
    global $db, $current_user;

    $list_sales = array();
    $query = "SELECT DISTINCT u.id FROM users_cstm uc
        JOIN users u ON uc.id_c = u.id 
        LEFT JOIN vin_vin_leads_cstm lc ON lc.user_id1_c = u.id
        LEFT JOIN vin_vin_leads l ON l.id = lc.id_c AND l.deleted = 0 
        WHERE uc.user_id1_c = '".$current_user->id."' AND u.status = 'Active' AND u.deleted = 0;";

    $result = $db->query($query);
    $dataSet = array();
    $row = $db->fetchByAssoc($result);
    while ($row != null){
        $dataSet[] = $row;
        $row = $db->fetchByAssoc($result);
    }
    foreach($dataSet as $key => $value){
        array_push($list_sales,$value['id']);
    }
    return $list_sales;
}

function getSaleAdminCurrentUser(){
    global $db, $current_user;

    $user_id = '';
    $query = "SELECT user_id_c FROM users_cstm uc
            JOIN users u ON uc.id_c = u.id 
            WHERE u.id = '".$current_user->id."'  AND u.status = 'Active' AND u.deleted = 0 LIMIT 1;";

    $query_result = $db->query($query);
    $query_result = $db->fetchByAssoc($query_result);
    $user_id = $query_result["user_id_c"];
    return $user_id;
}

function getBusinessManagementCurrentUser($id){
    global $db;

    $user_id = '';
    $query = "SELECT user_id1_c FROM users_cstm uc
            JOIN users u ON uc.id_c = u.id 
            WHERE u.id = '".$id."' AND u.status = 'Active' AND u.deleted = 0 LIMIT 1;";

    $query_result = $db->query($query);
    $query_result = $db->fetchByAssoc($query_result);
    $user_id = $query_result["user_id1_c"];
    return $user_id;
}

function startsWith($haystack, $needle){
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function checkPermission(){
    global $db, $current_user;
    return true;
    if($current_user->is_admin == 1){
        return true;
    }
    $bm = 0;
    $sa = 0;
    $sg = 0;
    $query = "";
    $list_roles = getRoleByUser($current_user->id);
    foreach($list_roles as $key => $value){
        if($value == 'BUSINESS_MANAGER'){
            $bm = 1;
        }
        else if($value == 'SALE_ADMIN'){
            $sa = 1;
        }
        else if($value == 'SALE_AGENT'){
            $sg = 1;
        }
    }

    if($bm == 0 && $sa == 0 && $sg == 0){
        return false;
    }
    else return true;
}

function csrf_valid_token(){
    if(session_id() != '' && isset($_SESSION)) {
        if(isset($_SESSION["VIN_CSRF_TOKEN"])){
            if(!isset($_REQUEST)){
                return false;
            }
            if(!isset($_REQUEST['vin_csrf_token'])){
                return false;
            }
            else{
                if($_REQUEST['vin_csrf_token'] == null || $_REQUEST['vin_csrf_token'] == ''){
                    return false;
                }
                else if($_REQUEST['vin_csrf_token'] != $_SESSION["VIN_CSRF_TOKEN"]){
                    return false;
                }
            }
        }
    }
    return true;
}