<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
global $db, $current_user, $app_list_strings;

if(!csrf_valid_token()){
    echo "CSRF BLOCKED.";
    die;
}

$subAction = $_REQUEST["subaction"];
if($subAction != null && $subAction != "")
{
    switch ($subAction) {
        case "createUser":
            $user_name = $_REQUEST['user_name'];
            $last_name = $_REQUEST['last_name'];
            $email = $_REQUEST['email'];
            $password = $_REQUEST['password'];
            $first_name = $_REQUEST['first_name'];
            $sales_admin = $_REQUEST['sales_admin'];
            $type = $_REQUEST['type'];
            $mobile = $_REQUEST['mobile'];
            $employee_id = $_REQUEST['employee_id'];
            $user_status = $_REQUEST['status'];
            $user_id_edit = $_REQUEST['user_id'];
            $user_name = trim($user_name);
            $password = trim($password);
            $first_name = trim($first_name);
            $last_name = trim($last_name);
            $email = trim($email);
            $mobile = trim($mobile);
            $employee_id = trim($employee_id);
            $user_id_edit = trim($user_id_edit);
            $user_status = trim($user_status);
            $status = 0;

            require_once('custom/include/global_function/fs_users.php');
            $fs_users = new fs_users;
            if($user_id_edit == "" || $user_id_edit == null){
                $passCheck = $fs_users->passwordValidationCheck($password);
                if($passCheck != '' && $passCheck != null){
                    echo $passCheck;
                    die;
                }
            }
            else{
                if($password != "" && $password != null){
                    $passCheck = $fs_users->passwordValidationCheck($password);
                    if($passCheck != '' && $passCheck != null){
                        echo $passCheck;
                        die;
                    }
                }
            }

            if(trim($user_name) != '' && trim($last_name) != ''
            && trim($email) != '' 
            && strlen(trim($user_name)) > 2){
                $bm = 0;
                $sa = 0;
                $sg = 0;
                $list_roles = getRoleCurrentUser();
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }

                if($user_id_edit == "" || $user_id_edit == null){
                    if($bm == 0 && $sa == 0){
                        echo 4;
                        break; 
                    }
                    else if($bm == 1 && $type != '1'){
                        $query = "SELECT u.user_name FROM users u
                            JOIN users_cstm uc ON u.id=uc.id_c
                            WHERE u.deleted = 0 AND u.status ='Active' 
                            AND uc.user_id1_c ='".$current_user->id."'
                            AND u.id='".$sales_admin."' LIMIT 1;";
            
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $user_name_check = $query_result["user_name"];
                        if($user_name_check == '' || $user_name_check == null){
                            echo 3;
                            break; 
                        }
                    }
    
                    $query = "SELECT user_name FROM users WHERE user_name = '".$user_name."' LIMIT 1;";
            
                    $query_result = $db->query($query);
                    $query_result = $db->fetchByAssoc($query_result);
                    $user_name_check = $query_result["user_name"];
                    if($user_name_check != '' && $user_name_check != null){
                        $status = 1;
                    }
                    else{
                        $query = "SELECT UUID() AS user_id";
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $user_id = $query_result["user_id"];
                        $password_hash = $fs_users->getPasswordHash($password);
                        if($user_status != 'Active'){
                            $user_status = 'Inactive';
                        }
                        $query_insert = "INSERT INTO users (id, user_name, user_hash, 
                            system_generated_password, sugar_login,first_name,last_name,is_admin,external_auth_only,
                            receive_notifications,date_entered,date_modified,modified_user_id,
                            created_by,status,deleted,portal_only,show_on_employees,employee_status,is_group,factor_auth,phone_mobile)
                            VALUES ('".$user_id."','".$user_name."','".$password_hash."', 0,1,'".$first_name."','".$last_name."',0,0,1,NOW(),NOW(),
                            '".$current_user->id."','".$current_user->id."','".$user_status."',0,0,1,'Active',0,0,'".$mobile."');";
                        $db->query($query_insert);
    
                        if($bm == 1 && $type == '1'){
                            $query_insert = "INSERT INTO users_cstm (id_c,user_id_c,user_id1_c,vin_employee_id_c)
                                VALUES ('".$user_id."',NULL,'".$current_user->id."','".$employee_id."');";
                            $db->query($query_insert);
    
                            $query_insert = "INSERT INTO securitygroups_acl_roles (id,securitygroup_id,role_id,date_modified,deleted)
                                VALUES ((SELECT UUID()), 
                                (SELECT sg.id FROM securitygroups sg
                                    WHERE sg.name = '".strtoupper($user_name)."' AND sg.deleted = 0 LIMIT 1),
                                (SELECT acl.id FROM acl_roles acl
                                    WHERE acl.name = 'SALE_ADMIN' AND acl.deleted = 0 LIMIT 1)
                                    ,NOW(),0
                                );";
                            $db->query($query_insert);
                        }
                        else{
                            if($bm == 1 && $type != '1'){
                                $query_insert = "INSERT INTO users_cstm (id_c,user_id_c,user_id1_c,vin_employee_id_c)
                                    VALUES ('".$user_id."','".$sales_admin."',NULL,'".$employee_id."');";
                                $db->query($query_insert);
                            }
                            else{
                                $query_insert = "INSERT INTO users_cstm (id_c,user_id_c,user_id1_c,vin_employee_id_c)
                                    VALUES ('".$user_id."','".$current_user->id."',NULL,'".$employee_id."');";
                                $db->query($query_insert);
                            }
    
                            $query_insert = "INSERT INTO securitygroups_acl_roles (id,securitygroup_id,role_id,date_modified,deleted)
                                VALUES ((SELECT UUID()), 
                                (SELECT sg.id FROM securitygroups sg
                                    WHERE sg.name = '".strtoupper($user_name)."' AND sg.deleted = 0 LIMIT 1),
                                (SELECT acl.id FROM acl_roles acl
                                    WHERE acl.name = 'SALE_AGENT' AND acl.deleted = 0 LIMIT 1)
                                    ,NOW(),0
                                );";
                            $db->query($query_insert);
                        }
    
                        //Email
                        $query = "SELECT UUID() AS email_id";
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $email_id = $query_result["email_id"];
    
                        $query_insert = "INSERT INTO email_addresses (id,email_address,email_address_caps, 
                                        invalid_email,opt_out,confirm_opt_in,date_created,date_modified,deleted)
                                    VALUES ('".$email_id."','".strtolower($email)."','".strtoupper($email)."',0,0,'confirmed-opt-in', NOW(), NOW(), 0);";
                        $db->query($query_insert);
    
                        $query_insert = "INSERT INTO email_addr_bean_rel (id,email_address_id,bean_id,bean_module,
                                primary_address,reply_to_address,date_created,date_modified,deleted)
                            VALUES ((SELECT UUID()),'".$email_id."','".$user_id."','Users',1,0,NOW(), NOW(), 0);";
                        $db->query($query_insert);
    
                        //Reference
                        $query_insert = "INSERT INTO user_preferences (id, category, deleted, date_entered, date_modified, assigned_user_id, contents)
                            SELECT (SELECT UUID()), category, 0, NOW(), NOW(), '".$user_id."', contents
                            FROM user_preferences
                            WHERE assigned_user_id = '".$current_user->id."' AND deleted = 0 
                            AND category = 'global';";
                        $db->query($query_insert);
    
                        $status = 2;
                    }
                }
                else{
                    $has_permission = 0;
                    if($current_user->is_admin == 1){
                        $has_permission = 1;
                    }
                    else{
                        if($bm == 1){
                            $query = "SELECT u.id FROM users_cstm uc
                                JOIN users u ON uc.id_c = u.id 
                                WHERE (uc.user_id1_c = '".$current_user->id."'
                                OR uc.user_id_c IN (
                                SELECT u.id FROM users_cstm uc
                                JOIN users u ON uc.id_c = u.id 
                                WHERE uc.user_id1_c = '".$current_user->id."' AND u.deleted = 0
                                )) AND u.id = '".$user_id_edit."' LIMIT 1;";
                
                            $query_result = $db->query($query);
                            $query_result = $db->fetchByAssoc($query_result);
                            $user_id = $query_result["id"];
                            if($user_id !='' && $user_id != null){
                                $has_permission = 1;
                            }
                        }
                        else if($sa == 1){
                            $query = "SELECT u.id FROM users_cstm uc
                                    JOIN users u ON uc.id_c = u.id 
                                    WHERE u.id = '".$user_id_edit."' AND uc.user_id_c = '".$current_user->id."' LIMIT 1;";
                
                            $query_result = $db->query($query);
                            $query_result = $db->fetchByAssoc($query_result);
                            $user_id = $query_result["id"];
                            if($user_id !='' && $user_id != null){
                                $has_permission = 1;
                            }
                        }
                    }
    
                    if($has_permission == 1){
                        $user_name = trim($user_name);
                        $password = trim($password);
                        $first_name = trim($first_name);
                        $last_name = trim($last_name);
                        $email = trim($email);
                        $mobile = trim($mobile);
                        $employee_id = trim($employee_id);
                        $user_id = trim($user_id);
                        $user_status = trim($user_status);

                        $beanUser = BeanFactory::getBean('Users',$user_id_edit);
                        if($sales_admin != '' && $sales_admin != null && $bm == 1){
                            if($beanUser->user_id_c == '' || $beanUser->user_id_c == null){
                                $query = "SELECT u.id FROM users_cstm uc
                                        JOIN users u ON uc.id_c = u.id 
                                        WHERE uc.user_id_c = '".$user_id_edit."' AND u.deleted = 0 LIMIT 1;";
                    
                                $query_result = $db->query($query);
                                $query_result = $db->fetchByAssoc($query_result);
                                $user_id = $query_result["id"];
                                if($user_id != '' && $user_id != null){
                                    echo 'Sale Admin has Sale Agent. Please remove all Sale Agent first.';
                                    die;
                                }
                            }
                        }
                        //Audit
                        if($beanUser->first_name != $first_name){
                            $query_insert = "INSERT INTO vin_user_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string)
                                VALUES ((SELECT UUID()),'".$beanUser->id."',NOW(),'".$current_user->id."','first_name','varchar','".$beanUser->first_name."','".$first_name."' );";
                            $db->query($query_insert);
                        }
                        if($beanUser->last_name != $last_name){
                            $query_insert = "INSERT INTO vin_user_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string)
                                VALUES ((SELECT UUID()),'".$beanUser->id."',NOW(),'".$current_user->id."','last_name','varchar','".$beanUser->last_name."','".$last_name."' );";
                            $db->query($query_insert);
                        }
                        if($beanUser->phone_mobile != $mobile){
                            $query_insert = "INSERT INTO vin_user_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string)
                                VALUES ((SELECT UUID()),'".$beanUser->id."',NOW(),'".$current_user->id."','phone_mobile','varchar','".$beanUser->phone_mobile."','".$mobile."' );";
                            $db->query($query_insert);
                        }
                        if($beanUser->status != $status){
                            $query_insert = "INSERT INTO vin_user_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string)
                                VALUES ((SELECT UUID()),'".$beanUser->id."',NOW(),'".$current_user->id."','status','enum','".$beanUser->status."','".$status."' );";
                            $db->query($query_insert);
                        }
                        if($beanUser->vin_employee_id_c != $employee_id){
                            $query_insert = "INSERT INTO vin_user_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string)
                                VALUES ((SELECT UUID()),'".$beanUser->id."',NOW(),'".$current_user->id."','vin_employee_id_c','varchar','".$beanUser->vin_employee_id_c."','".$employee_id."' );";
                            $db->query($query_insert);
                        }
                        if($beanUser->user_id_c != $sales_admin && $bm == 1 && $sales_admin != $user_id_edit){
                            $query_insert = "INSERT INTO vin_user_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string)
                                VALUES ((SELECT UUID()),'".$beanUser->id."',NOW(),'".$current_user->id."','user_id_c','relate','".$beanUser->user_id_c."','".$sales_admin."' );";
                            $db->query($query_insert);
                        }
                        if($beanUser->email1 != $email){
                            $query_insert = "INSERT INTO vin_user_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string)
                                VALUES ((SELECT UUID()),'".$beanUser->id."',NOW(),'".$current_user->id."','email1','varchar','".$beanUser->email1."','".$email."' );";
                            $db->query($query_insert);
                        }


                        $password_hash = $fs_users->getPasswordHash($password);
                        $query = " UPDATE users SET 
                            first_name = '".$first_name."' ,
                            last_name = '".$last_name."' ,
                            phone_mobile = '".$mobile."' ,
                            status = '".$user_status."' ,
                            user_hash = '".$password_hash."' ,
                            date_modified = NOW(),
                            pwd_last_changed = NOW(),
                            modified_user_id = '".$current_user->id."'
                            WHERE id = '".$user_id_edit."';";
                        $db->query($query);

                        if($bm == 1){
                            if($type == '1'){
                                $query = " UPDATE users_cstm SET 
                                    vin_employee_id_c = '".$employee_id."' ,
                                    user_id_c = NULL,
                                    user_id1_c = '".$current_user->id."'
                                    WHERE id_c = '".$user_id_edit."';";
                                $db->query($query);

                                $queryDelete = "UPDATE securitygroups_acl_roles SET
                                            deleted = 1 ,
                                            date_modified = NOW()
                                            WHERE securitygroup_id = (SELECT sg.id FROM securitygroups sg
                                                                    WHERE sg.name = '".strtoupper($user_name)."' AND sg.deleted = 0 LIMIT 1)
                                            AND role_id = (SELECT acl.id FROM acl_roles acl
                                                    WHERE acl.name = 'SALE_ADMIN' AND acl.deleted = 0 LIMIT 1);";
                                $db->query($queryDelete);
                                $queryDelete = "UPDATE securitygroups_acl_roles SET
                                            deleted = 1 ,
                                            date_modified = NOW()
                                            WHERE securitygroup_id = (SELECT sg.id FROM securitygroups sg
                                                                    WHERE sg.name = '".strtoupper($user_name)."' AND sg.deleted = 0 LIMIT 1)
                                            AND role_id = (SELECT acl.id FROM acl_roles acl
                                                    WHERE acl.name = 'SALE_AGENT' AND acl.deleted = 0 LIMIT 1);";
                                $db->query($queryDelete);

                                $query_insert = "INSERT INTO securitygroups_acl_roles (id,securitygroup_id,role_id,date_modified,deleted)
                                    VALUES ((SELECT UUID()), 
                                    (SELECT sg.id FROM securitygroups sg
                                        WHERE sg.name = '".strtoupper($user_name)."' AND sg.deleted = 0 LIMIT 1),
                                    (SELECT acl.id FROM acl_roles acl
                                        WHERE acl.name = 'SALE_ADMIN' AND acl.deleted = 0 LIMIT 1)
                                        ,NOW(),0
                                    );";
                                $db->query($query_insert);
                            }
                            else{
                                if($sales_admin != '' && $sales_admin != null && $sales_admin != $user_id_edit){
                                    $query = " UPDATE users_cstm SET 
                                        vin_employee_id_c = '".$employee_id."' ,
                                        user_id_c = '".$sales_admin."',
                                        user_id1_c = NULL
                                        WHERE id_c = '".$user_id_edit."';";
                                    $db->query($query);

                                    $queryDelete = "UPDATE securitygroups_acl_roles SET
                                                deleted = 1 ,
                                                date_modified = NOW()
                                                WHERE securitygroup_id = (SELECT sg.id FROM securitygroups sg
                                                                        WHERE sg.name = '".strtoupper($user_name)."' AND sg.deleted = 0 LIMIT 1)
                                                AND role_id = (SELECT acl.id FROM acl_roles acl
                                                        WHERE acl.name = 'SALE_ADMIN' AND acl.deleted = 0 LIMIT 1);";
                                    $db->query($queryDelete);
                                    $queryDelete = "UPDATE securitygroups_acl_roles SET
                                                deleted = 1 ,
                                                date_modified = NOW()
                                                WHERE securitygroup_id = (SELECT sg.id FROM securitygroups sg
                                                                        WHERE sg.name = '".strtoupper($user_name)."' AND sg.deleted = 0 LIMIT 1)
                                                AND role_id = (SELECT acl.id FROM acl_roles acl
                                                        WHERE acl.name = 'SALE_AGENT' AND acl.deleted = 0 LIMIT 1);";
                                    $db->query($queryDelete);

                                    $query_insert = "INSERT INTO securitygroups_acl_roles (id,securitygroup_id,role_id,date_modified,deleted)
                                        VALUES ((SELECT UUID()), 
                                        (SELECT sg.id FROM securitygroups sg
                                            WHERE sg.name = '".strtoupper($user_name)."' AND sg.deleted = 0 LIMIT 1),
                                        (SELECT acl.id FROM acl_roles acl
                                            WHERE acl.name = 'SALE_AGENT' AND acl.deleted = 0 LIMIT 1)
                                            ,NOW(),0
                                        );";
                                    $db->query($query_insert);
                                }
                            }
                        }
                        else{
                            $query = " UPDATE users_cstm SET 
                                vin_employee_id_c = '".$employee_id."'
                                WHERE id_c = '".$user_id_edit."';";
                            $db->query($query);
                        }

                        $query = "SELECT e.id FROM email_addresses e
                            JOIN email_addr_bean_rel eb ON e.id = eb.email_address_id 
                            WHERE eb.bean_id = '".$user_id_edit."' AND bean_module='Users' 
                            AND e.deleted = 0 AND eb.deleted = 0 AND primary_address = 1 LIMIT 1";
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $email_id = $query_result["id"];

                        if($email_id == null || $email_id == ''){
                            $query = "SELECT UUID() AS email_id";
                            $query_result = $db->query($query);
                            $query_result = $db->fetchByAssoc($query_result);
                            $email_id = $query_result["email_id"];
        
                            $query_insert = "INSERT INTO email_addresses (id,email_address,email_address_caps, 
                                            invalid_email,opt_out,confirm_opt_in,date_created,date_modified,deleted)
                                        VALUES ('".$email_id."','".strtolower($email)."','".strtoupper($email)."',0,0,'confirmed-opt-in', NOW(), NOW(), 0);";
                            $db->query($query_insert);
        
                            $query_insert = "INSERT INTO email_addr_bean_rel (id,email_address_id,bean_id,bean_module,
                                    primary_address,reply_to_address,date_created,date_modified,deleted)
                                VALUES ((SELECT UUID()),'".$email_id."','".$user_id_edit."','Users',1,0,NOW(), NOW(), 0);";
                            $db->query($query_insert);
                        }
                        else{
                            $query_update = " UPDATE email_addresses SET 
                                email_address = '".strtolower($email)."' ,
                                email_address_caps = '".strtoupper($email)."'
                                WHERE id = '".$email_id."';";
                            $db->query($query_update);
                        }

                        $status = 2;
                    }
                }
            }
            
            echo $status;
            break;
        case "getSalesAdmin":
            $query = "SELECT u.id, u.user_name as text 
                FROM users_cstm uc
                JOIN users u ON uc.id_c = u.id 
                WHERE uc.user_id1_c = '".$current_user->id."' AND u.status = 'Active';";

            $bean = new SugarBean();
            $result = $bean->db->query($query,true," Error filling in additional detail fields: ");
            $rows = array();

            while($row=$bean->db->fetchByAssoc($result) ) 
            {
                $rows[] = $row;
            }

            ob_start();
            ob_clean();
            header('Content-type: application/json');
            echo json_encode($rows);
            break;
        case "deleteUser":
            die;
            $id = $_REQUEST['id'];
            $status = 0;
            if($id !='' && $id != null){
                $bm = 0;
                $sa = 0;
                $sg = 0;
                $list_roles = getRoleCurrentUser();
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }
                $has_permission = 0;
                if($current_user->is_admin == 1){
                    $has_permission = 1;
                }
                else{
                    if($bm == 1){
                        $query = "SELECT u.id FROM users_cstm uc
                            JOIN users u ON uc.id_c = u.id 
                            WHERE (uc.user_id1_c = '".$current_user->id."'
                            OR uc.user_id_c IN (
                            SELECT u.id FROM users_cstm uc
                            JOIN users u ON uc.id_c = u.id 
                            WHERE uc.user_id1_c = '".$current_user->id."' AND u.deleted = 0
                            )) AND u.id = '".$id."' LIMIT 1;";
            
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $user_id = $query_result["id"];
                        if($user_id !='' && $user_id != null){
                            $has_permission = 1;
                        }
                    }
                    else if($sa == 1){
                        $query = "SELECT u.id FROM users_cstm uc
                                JOIN users u ON uc.id_c = u.id 
                                WHERE u.id = '".$id."' AND uc.user_id_c = '".$current_user->id."' LIMIT 1;";
            
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $user_id = $query_result["id"];
                        if($user_id !='' && $user_id != null){
                            $has_permission = 1;
                        }
                    }
                }

                if($has_permission == 1){
                    $query = "SELECT u.id FROM users_cstm uc
                            JOIN users u ON uc.id_c = u.id 
                            WHERE uc.user_id_c = '".$id."' AND u.deleted = 0 LIMIT 1;";

                    $query_result = $db->query($query);
                    $query_result = $db->fetchByAssoc($query_result);
                    $user_id = $query_result["id"];
                    if($user_id != '' && $user_id != null){
                        echo 'Sale Admin has Sale Agent. Please remove all Sale Agent first.';
                        die;
                    }

                    $query = " UPDATE users SET 
                        deleted = 1 ,
                        date_modified = NOW(),
                        modified_user_id = '".$current_user->id."'
                        WHERE id = '".$id."';";
                    $db->query($query);
                    $status = 1;
                }
            }
            echo $status;
            break;
        case "loadUser":
            $id = $_REQUEST['id'];
            $return = array();
            if($id !='' && $id != null){
                $bm = 0;
                $sa = 0;
                $sg = 0;
                $list_roles = getRoleCurrentUser();
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }
                $has_permission = 0;
                if($current_user->is_admin == 1){
                    $has_permission = 1;
                }
                else{
                    if($bm == 1){
                        $query = "SELECT u.id FROM users_cstm uc
                            JOIN users u ON uc.id_c = u.id 
                            WHERE (uc.user_id1_c = '".$current_user->id."'
                            OR uc.user_id_c IN (
                            SELECT u.id FROM users_cstm uc
                            JOIN users u ON uc.id_c = u.id 
                            WHERE uc.user_id1_c = '".$current_user->id."' AND u.deleted = 0
                            )) AND u.id = '".$id."' LIMIT 1;";
            
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $user_id = $query_result["id"];
                        if($user_id !='' && $user_id != null){
                            $has_permission = 1;
                        }
                    }
                    else if($sa == 1){
                        $query = "SELECT u.id FROM users_cstm uc
                                JOIN users u ON uc.id_c = u.id 
                                WHERE u.id = '".$id."' AND uc.user_id_c = '".$current_user->id."' LIMIT 1;";
            
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $user_id = $query_result["id"];
                        if($user_id !='' && $user_id != null){
                            $has_permission = 1;
                        }
                    }
                }

                if($has_permission == 1){
                    // $query = "SELECT u.id AS id,
                    //         u.user_name AS user_name,
                    //         u.first_name AS first_name,
                    //         u.last_name AS last_name,
                    //         u.phone_mobile AS mobile,
                    //         u.status AS status,
                    //         uc.vin_employee_id_c AS employee_id,
                    //         uc.user_id_c AS user_id_c,
                    //         uc.user_id1_c AS user_id1_c
                    //         FROM users_cstm uc
                    //         JOIN users u ON uc.id_c = u.id 
                    //         WHERE u.id = '".$id."' LIMIT 1;";
                    // $query_result = $db->query($query);
                    // $query_result = $db->fetchByAssoc($query_result);

                    $beanUser = BeanFactory::getBean('Users',$id);
                    $sea = new SugarEmailAddress;
                    $user_email = $sea->getPrimaryAddress($beanUser);
                    $return['id'] = $beanUser->id;
                    $return['user_name'] = $beanUser->user_name;
                    $return['first_name'] = $beanUser->first_name;
                    $return['last_name'] = $beanUser->last_name;
                    $return['phone_mobile'] = $beanUser->phone_mobile;
                    $return['status'] = $beanUser->status;
                    $return['vin_employee_id_c'] = $beanUser->vin_employee_id_c;
                    $return['user_id_c'] = $beanUser->user_id_c;
                    $return['user_id1_c'] = $beanUser->user_id1_c;
                    $return['user_email'] = $user_email;
                    $return['vin_business_management_c'] = $beanUser->vin_business_management_c;
                    $return['vin_sale_admin_c'] = $beanUser->vin_sale_admin_c;
                }
            }
            ob_start();
            ob_clean();
            header('Content-type: application/json');
            echo json_encode($return);
            break;
        default:
            echo "Something went wrong! Can not find Action!";
    }
}

function getRoleCurrentUser(){
    global $db, $current_user;

    $list_roles = array();
    $query = "SELECT acl.name FROM acl_roles acl 
        JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
        JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
        JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
        WHERE acl.deleted = 0 AND sgu.user_id = '".$current_user->id."';";

    $result = $db->query($query);
    $dataSet = array();
    $row = $db->fetchByAssoc($result);
    while ($row != null){
        $dataSet[] = $row;
        $row = $db->fetchByAssoc($result);
    }
    foreach($dataSet as $key => $value){
        array_push($list_roles,$value['name']);
    }
    return $list_roles;
}

function csrf_valid_token(){
    if(session_id() != '' && isset($_SESSION)) {
        if(isset($_SESSION["VIN_CSRF_TOKEN"])){
            if(!isset($_REQUEST)){
                return false;
            }
            if(!isset($_REQUEST['vin_csrf_token'])){
                return false;
            }
            else{
                if($_REQUEST['vin_csrf_token'] == null || $_REQUEST['vin_csrf_token'] == ''){
                    return false;
                }
                else if($_REQUEST['vin_csrf_token'] != $_SESSION["VIN_CSRF_TOKEN"]){
                    return false;
                }
            }
        }
    }
    return true;
}