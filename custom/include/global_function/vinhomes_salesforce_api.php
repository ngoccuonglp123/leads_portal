<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
* CUSTOM 2020/04/18
* QUANKT
* Salesforce API
*/
class vinhomes_salesforce_api {

    public function get_access_token(){
        // $api_url = $GLOBALS['sugar_config']['salesforce_api_url'];
        $api_url = 'https://login.salesforce.com';
        $client_id = $GLOBALS['sugar_config']['salesforce_api_client_id'];
        $client_secret = $GLOBALS['sugar_config']['salesforce_api_client_secret'];
        $username = $GLOBALS['sugar_config']['salesforce_api_username'];
        $password = $GLOBALS['sugar_config']['salesforce_api_password'];

        if($api_url == '' || $api_url == null
        || $client_id == '' || $client_id == null
        || $client_secret == '' || $client_secret == null
        || $username == '' || $username == null
        || $password == '' || $password == null){
            return '';
        }
        require_once('custom/include/global_function/encrypt_decrypt.php');
        $f = new encrypt_decrypt;
        $client_secret = $f->dec_enc('decrypt', $client_secret);
        $password = $f->dec_enc('decrypt', $password);

        $post_fields = array(
            'grant_type' => 'password',
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'username' => $username,
            'password' => $password,
        );
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url.'/services/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        $token_request_body = curl_exec($ch);
        $token_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (($token_response_code<200)||($token_response_code>=300)||empty($token_request_body)){
            curl_close($ch);
            $GLOBALS['log']->fatal("Salesforce API Get Token: Call to get token failed");
            return '';
        }
        $token_request_data = json_decode($token_request_body, true);
        if (empty($token_request_data)){
            curl_close($ch);
            $GLOBALS['log']->fatal("Salesforce API Get Token: Couldn't decode '".$token_request_data."' as a JSON object");
            return '';
        }

        if (!isset($token_request_data['access_token'])){
            curl_close($ch);
            $GLOBALS['log']->fatal("Salesforce API Get Token: Missing expected data (access_token)");
            return '';
        }
        curl_close($ch);
        if($token_request_data['access_token'] != '' && $token_request_data['access_token'] != null){
            return $token_request_data['access_token'];
        }
        return '';
    }

    public function get_instance_url(){
        $api_url = 'https://login.salesforce.com';
        $client_id = $GLOBALS['sugar_config']['salesforce_api_client_id'];
        $client_secret = $GLOBALS['sugar_config']['salesforce_api_client_secret'];
        $username = $GLOBALS['sugar_config']['salesforce_api_username'];
        $password = $GLOBALS['sugar_config']['salesforce_api_password'];

        if($api_url == '' || $api_url == null
        || $client_id == '' || $client_id == null
        || $client_secret == '' || $client_secret == null
        || $username == '' || $username == null
        || $password == '' || $password == null){
            return '';
        }
        require_once('custom/include/global_function/encrypt_decrypt.php');
        $f = new encrypt_decrypt;
        $client_secret = $f->dec_enc('decrypt', $client_secret);
        $password = $f->dec_enc('decrypt', $password);

        $post_fields = array(
            'grant_type' => 'password',
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'username' => $username,
            'password' => $password,
        );
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url.'/services/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        $token_request_body = curl_exec($ch);
        $token_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (($token_response_code<200)||($token_response_code>=300)||empty($token_request_body)){
            curl_close($ch);
            $GLOBALS['log']->fatal("Salesforce API Get Token: Call to get token failed");
            return '';
        }
        $token_request_data = json_decode($token_request_body, true);
        if (empty($token_request_data)){
            curl_close($ch);
            $GLOBALS['log']->fatal("Salesforce API Get Token: Couldn't decode '".$token_request_data."' as a JSON object");
            return '';
        }

        if (!isset($token_request_data['instance_url'])){
            curl_close($ch);
            $GLOBALS['log']->fatal("Salesforce API Get Token: Missing expected data (instance_url)");
            return '';
        }
        curl_close($ch);
        if($token_request_data['instance_url'] != '' && $token_request_data['instance_url'] != null){
            return $token_request_data['instance_url'];
        }
        return '';
    }

    public function get_leads_from_salesforce($access_token){
        global $db, $app_list_strings;
        $return = array();
        //$api_url = $GLOBALS['sugar_config']['salesforce_api_url'];
        $api_url = $this->get_instance_url();
        $date = new DateTime();
        $date->sub(new DateInterval('P1D'));
        $d = $date->format('Y-m-d');
        $h = date('H:i:s');
        $query = "SELECT  Id, LastName, FirstName, Name,
                Salutation, Company, Street, City, State, 
                PostalCode, Country, Phone, MobilePhone,
                Email, CurrencyIsoCode, LeadSource, Status,
                Rating, Priority__c, ID_Number__c,
                Project__c, House_Type__c, Budget__c,
                Lead_Scoring__c, Source_Details__c, 
                Tax_Code__c,Campaign__c,
                Area_of_Project__c,
                Ads_Source__c,
                Lead_ads_ID__c,
                Ads_Content__c,
                Agent__c,
                Ad_Term__c,
                form_medium__c,
                Utm_Source__c,
                dob__c,
                Department_Name__c,
                sign_status__c,
                Lead_Duplicate__c
                FROM Lead 
                WHERE CreatedDate >= ".$d."T".$h."Z"."
                AND IsDeleted = false";
        $url = $api_url."/services/data/v20.0/query?q=" . urlencode($query);
    
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: OAuth $access_token"));
    
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ( $status != 200 ) {
          $GLOBALS['log']->fatal("Salesforce API Get Leads: Error: call to URL ".$url." failed with status ".$status.", response ".$json_response.", curl_error ".curl_error($curl). ", curl_errno " . curl_errno($curl));
        }
        curl_close($curl);
        $response = json_decode($json_response, true);
        $total_size = $response['totalSize'];
        if ($total_size > 0) {
            foreach ((array) $response['records'] as $record) {
                $save_project = $record['Project__c'];
                //Campaign
                $campaign = $record['Campaign__c'];
                $campaign =  explode('_',$campaign);
                if (array_key_exists(0,$campaign)){
                    $record['Project__c'] = $campaign[0];
                }
                if (array_key_exists(1,$campaign)){
                    $record['Area_of_Project__c'] = $campaign[1];
                }
                if (array_key_exists(2,$campaign)){
                    $record['Ads_Source__c'] = $campaign[2];
                }
                if (array_key_exists(3,$campaign)){
                    $record['Agent__c'] = $campaign[3];
                }

                $temp = array();
                $temp['vin_lead_id_salesforce_c'] = $record['Id'];
                $temp['vin_last_name_c'] = $record['LastName'];
                $temp['vin_first_name_c'] = $record['FirstName'];
                $temp['name'] = $record['Name'];
                $temp['vin_full_name_c'] = $record['Name'];
                $temp['vin_company_name_c'] = $record['Company'];
                $temp['vin_street_c'] = $record['Street'];
                $temp['vin_city_text_c'] = $record['City'];
                $temp['vin_state_province_c'] = $record['State'];
                $temp['vin_zip_postal_code_c'] = $record['PostalCode'];
                $temp['vin_country_c'] = $record['Country'];
                $temp['vin_phone_number_c'] = $record['Phone'];
                $temp['vin_mobile_c'] = $record['MobilePhone'];
                $temp['vin_email_c'] = $record['Email'];
                $temp['vin_vin_currency_id_c'] = $record['CurrencyIsoCode'];
                if(strtolower($record['CurrencyIsoCode']) == "vnd"){
                    $temp['vin_vin_currency_id_c'] = '';
                }
                $temp['vin_id_number_c'] = $record['ID_Number__c'];
                $temp['vin_project_c'] = $record['Project__c'];
                $temp['vin_house_type_list_c'] = $record['House_Type__c'];
                $temp['vin_budget_c'] = $record['Budget__c'];
                $temp['vin_lead_scoring_c'] = $record['Lead_Scoring__c'];
                $temp['vin_source_details_c'] = $record['Source_Details__c'];
                $temp['vin_tax_code_c'] = $record['Tax_Code__c'];
                $temp['vin_campaign_c'] = $record['Campaign__c'];
                $temp['vin_lead_ads_id_c'] = $record['Lead_ads_ID__c'];
                $temp['vin_ads_content_c'] = $record['Ads_Content__c'];
                $temp['vin_agent_c'] = $record['Agent__c'];
                $temp['vin_ad_term_c'] = $record['Ad_Term__c'];
                $temp['vin_form_medium_c'] = $record['form_medium__c'];
                $temp['vin_utm_source_c'] = $record['Utm_Source__c'];
                $temp['vin_dob_c'] = $record['dob__c'];
                $temp['vin_vin_department_id2_c'] = trim(strtolower($record['Department_Name__c']));

                $temp['vin_salutation_c'] = '';
                foreach ($app_list_strings["vin_salutation_c_list"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['Salutation']))){
                        $temp['vin_salutation_c'] = $key;
                    }
                }
                if($temp['vin_salutation_c'] == ''){
                    $temp['vin_salutation_c'] = $record['Salutation'];
                }

                $temp['vin_house_type_list_c'] = '';
                foreach ($app_list_strings["vin_house_type_list_c_list"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['House_Type__c']))){
                        $temp['vin_house_type_list_c'] = $key;
                    }
                }
                if($temp['vin_house_type_list_c'] == ''){
                    $temp['vin_house_type_list_c'] = $record['House_Type__c'];
                }

                //Source
                $temp['vin_lead_source_c'] = '';
                foreach ($app_list_strings["allocate_dept_source"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['LeadSource']))){
                        $temp['vin_lead_source_c'] = $key;
                    }
                }
                if($temp['vin_lead_source_c'] == ''){
                    foreach ($app_list_strings["allocate_dept_source"] as $key => $value) {
                        if(trim(strtolower($key)) == trim(strtolower($record['LeadSource']))){
                            $temp['vin_lead_source_c'] = $key;
                        }
                    }
                }
                if($temp['vin_lead_source_c'] == ''){
                    $temp['vin_lead_source_c'] = $record['LeadSource'];
                }

                $temp['vin_lead_status_c'] = '';
                foreach ($app_list_strings["vin_lead_status_c_list"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['Status']))){
                        $temp['vin_lead_status_c'] = $key;
                    }
                }
                if($temp['vin_lead_status_c'] == ''){
                    $temp['vin_lead_status_c'] = $record['Status'];
                }

                $temp['vin_rating_c'] = '';
                foreach ($app_list_strings["vin_rating_c_list"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['Rating']))){
                        $temp['vin_rating_c'] = $key;
                    }
                }
                if($temp['vin_rating_c'] == ''){
                    $temp['vin_rating_c'] = $record['Rating'];
                }

                //Project
                $temp['vin_project_c'] = '';
                foreach ($app_list_strings["allocate_dept_level1"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['Project__c']))){
                        $temp['vin_project_c'] = $key;
                    }
                }
                if($temp['vin_project_c'] == ''){
                    foreach ($app_list_strings["allocate_dept_level1"] as $key => $value) {
                        if(trim(strtolower($key)) == trim(strtolower($record['Project__c']))){
                            $temp['vin_project_c'] = $key;
                        }
                    }
                }
                if($temp['vin_project_c'] == ''){
                    if($save_project != ''){
                        $record['Project__c'] = $save_project;
                        foreach ($app_list_strings["allocate_dept_level1"] as $key => $value) {
                            if(trim(strtolower($value)) == trim(strtolower($record['Project__c']))){
                                $temp['vin_project_c'] = $key;
                            }
                        }
                        if($temp['vin_project_c'] == ''){
                            foreach ($app_list_strings["allocate_dept_level1"] as $key => $value) {
                                if(trim(strtolower($key)) == trim(strtolower($record['Project__c']))){
                                    $temp['vin_project_c'] = $key;
                                }
                            }
                        }
                        if($temp['vin_project_c'] == ''){
                            $temp['vin_project_c'] = $record['Project__c'];
                        }
                    }
                    else{
                        $temp['vin_project_c'] = $record['Project__c'];
                    }
                }

                foreach ($app_list_strings["vin_house_type_list_c_list"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['House_Type__c']))){
                        $temp['vin_house_type_list_c'] = $key;
                    }
                }
                
                //Phan Khu
                $temp['vin_phankhu_c'] = '';
                foreach ($app_list_strings["allocate_dept_level2"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['Area_of_Project__c']))){
                        $temp['vin_phankhu_c'] = $key;
                    }
                }
                if($temp['vin_phankhu_c'] == ''){
                    foreach ($app_list_strings["allocate_dept_level2"] as $key => $value) {
                        if(trim(strtolower($key)) == trim(strtolower($record['Area_of_Project__c']))){
                            $temp['vin_phankhu_c'] = $key;
                        }
                    }
                }
                if($temp['vin_phankhu_c'] == ''){
                    $temp['vin_phankhu_c'] = $record['Area_of_Project__c'];
                }

                //Ads Source
                $temp['vin_ads_source_c'] = '';
                foreach ($app_list_strings["allocate_dept_level3"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['Ads_Source__c']))){
                        $temp['vin_ads_source_c'] = $key;
                    }
                }
                if($temp['vin_ads_source_c'] == ''){
                    foreach ($app_list_strings["allocate_dept_level3"] as $key => $value) {
                        if(trim(strtolower($key)) == trim(strtolower($record['Ads_Source__c']))){
                            $temp['vin_ads_source_c'] = $key;
                        }
                    }
                }
                if($temp['vin_ads_source_c'] == ''){
                    $temp['vin_ads_source_c'] = $record['Ads_Source__c'];
                }

                //vin_sign_status_c
                $temp['vin_sign_status_c'] = '';
                foreach ($app_list_strings["vin_sign_status_list"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['sign_status__c']))){
                        $temp['vin_sign_status_c'] = $key;
                    }
                }
                if($temp['vin_sign_status_c'] == ''){
                    foreach ($app_list_strings["vin_sign_status_list"] as $key => $value) {
                        if(trim(strtolower($key)) == trim(strtolower($record['sign_status__c']))){
                            $temp['vin_sign_status_c'] = $key;
                        }
                    }
                }
                if($temp['vin_sign_status_c'] == ''){
                    $temp['vin_sign_status_c'] = $record['sign_status__c'];
                }

                //vin_lead_duplicate_c
                $temp['vin_lead_duplicate_c'] = '';
                foreach ($app_list_strings["vin_lead_duplicate_list"] as $key => $value) {
                    if(trim(strtolower($value)) == trim(strtolower($record['Lead_Duplicate__c']))){
                        $temp['vin_lead_duplicate_c'] = $key;
                    }
                }
                if($temp['vin_lead_duplicate_c'] == ''){
                    foreach ($app_list_strings["vin_lead_duplicate_list"] as $key => $value) {
                        if(trim(strtolower($key)) == trim(strtolower($record['Lead_Duplicate__c']))){
                            $temp['vin_lead_duplicate_c'] = $key;
                        }
                    }
                }
                if($temp['vin_lead_duplicate_c'] == ''){
                    $temp['vin_lead_duplicate_c'] = $record['Lead_Duplicate__c'];
                }

                array_push($return, $temp);
            }
        }
        return $return;
    }

    public function sync_new_lead_to_salesforce($access_token, $data){
        global $db, $app_list_strings;
        //$api_url = $GLOBALS['sugar_config']['salesforce_api_url'];
        $api_url = $this->get_instance_url();
        $url = $api_url."/services/data/v20.0/sobjects/Lead/";

        $content = json_encode(array("FirstName" => $data['vin_first_name_c'],
                "LastName" => $data['vin_last_name_c'],
                "Salutation" => $app_list_strings["vin_salutation_c_list"][$data['vin_salutation_c']],
                "Phone" => $data['vin_phone_number_c'],
                "Email" => $data['vin_email_c'],
                "Company" => $data['vin_company_name_c'],
                "Country" => $data['vin_country_c'],
                "LeadSource" => $app_list_strings["allocate_dept_source"][$data['vin_lead_source_c']],
                "Street" => $data['vin_street_c'],
                "City" => $data['vin_city_text_c'],
                "State" => $data['vin_state_province_c'],
                "MobilePhone" => $data['vin_mobile_c'],
                "CurrencyIsoCode" => $data['vin_currency_c'],
                "Status" => $app_list_strings["vin_lead_status_c_list"][$data['vin_lead_status_c']],
                "Rating" => $app_list_strings["vin_rating_c_list"][$data['vin_rating_c']],
                "ID_Number__c" => $data['vin_id_number_c'],
                // "Project__c" => $data['vin_project_c'],
                "Project__c" => $app_list_strings["allocate_dept_level1"][$data['vin_project_c']],
                "House_Type__c" => $app_list_strings["vin_house_type_list_c_list"][$data['vin_house_type_list_c']],
                "Budget__c" => $data['vin_budget_c'],
                "Source_Details__c" => $data['vin_source_details_c'],
                "Tax_Code__c" => $data['vin_tax_code_c'],
                "Campaign__c" => $data['vin_campaign_c'],
                // "Area_of_Project__c" => $app_list_strings["allocate_dept_level2"][$data['vin_phankhu_c']],
                "Area_of_Project__c" => $data['vin_phankhu_c'],
                // "Ads_Source__c" => $app_list_strings["allocate_dept_level3"][$data['vin_ads_source_c']],
                "Ads_Source__c" => $data['vin_ads_source_c'],
                "Lead_ads_ID__c" => $data['vin_lead_ads_id_c'],
                "Ads_Content__c" => $data['vin_ads_content_c'],
                "Agent__c" => $data['vin_agent_c'],
                "Ad_Term__c" => $data['vin_ad_term_c'],
                "form_medium__c" => $data['vin_form_medium_c'],
                "Utm_Source__c" => $data['vin_utm_source_c'],
                "PostalCode" => $data['vin_zip_postal_code_c']));


        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
        array("Authorization: OAuth $access_token", "Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ( $status != 201 ) {
            $GLOBALS['log']->fatal("Salesforce API Create/Update Leads: Error: call to URL ".$url." failed with status ".$status.", response ".$json_response.", curl_error ".curl_error($curl). ", curl_errno " . curl_errno($curl));
        }
        curl_close($curl);
        $response = json_decode($json_response, true);
        $id = $response["id"];
        return $id;
    }

    public function sync_update_lead_to_salesforce($access_token, $data, $id) {
        global $db, $app_list_strings;
        //$api_url = $GLOBALS['sugar_config']['salesforce_api_url'];
        $api_url = $this->get_instance_url();
        $url = $api_url."/services/data/v20.0/sobjects/Lead/".$id;
    
        $content = json_encode(array("FirstName" => $data['vin_first_name_c'],
                "LastName" => $data['vin_last_name_c'],
                "Phone" => $data['vin_phone_number_c'],
                "Salutation" => $app_list_strings["vin_salutation_c_list"][$data['vin_salutation_c']],
                "Email" => $data['vin_email_c'],
                "Company" => $data['vin_company_name_c'],
                "Country" => $data['vin_country_c'],
                "LeadSource" => $app_list_strings["allocate_dept_source"][$data['vin_lead_source_c']],
                "Street" => $data['vin_street_c'],
                "City" => $data['vin_city_text_c'],
                "State" => $data['vin_state_province_c'],
                "MobilePhone" => $data['vin_mobile_c'],
                "CurrencyIsoCode" => $data['vin_currency_c'],
                "Status" => $app_list_strings["vin_lead_status_c_list"][$data['vin_lead_status_c']],
                "Rating" => $app_list_strings["vin_rating_c_list"][$data['vin_rating_c']],
                "ID_Number__c" => $data['vin_id_number_c'],
                // "Project__c" => $data['vin_project_c'],
                "Project__c" => $app_list_strings["allocate_dept_level1"][$data['vin_project_c']],
                "House_Type__c" => $app_list_strings["vin_house_type_list_c_list"][$data['vin_house_type_list_c']],
                "Budget__c" => $data['vin_budget_c'],
                "Source_Details__c" => $data['vin_source_details_c'],
                "Tax_Code__c" => $data['vin_tax_code_c'],
                "Campaign__c" => $data['vin_campaign_c'],
                // "Area_of_Project__c" => $app_list_strings["allocate_dept_level2"][$data['vin_phankhu_c']],
                "Area_of_Project__c" => $data['vin_phankhu_c'],
                // "Ads_Source__c" => $app_list_strings["allocate_dept_level3"][$data['vin_ads_source_c']],
                "Ads_Source__c" => $data['vin_ads_source_c'],
                "Lead_ads_ID__c" => $data['vin_lead_ads_id_c'],
                "Ads_Content__c" => $data['vin_ads_content_c'],
                "Agent__c" => $data['vin_agent_c'],
                "Ad_Term__c" => $data['vin_ad_term_c'],
                "form_medium__c" => $data['vin_form_medium_c'],
                "Utm_Source__c" => $data['vin_utm_source_c'],
                "PostalCode" => $data['vin_zip_postal_code_c']));
    
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: OAuth $access_token","Content-type: application/json"));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ( $status != 204 ) {
            $GLOBALS['log']->fatal("Salesforce API Create/Update Leads: Error: call to URL ".$url." failed with status ".$status.", curl_error ".curl_error($curl). ", curl_errno " . curl_errno($curl));
        }
        curl_close($curl);
    }

    public function sync_delete_lead_to_salesforce($access_token, $id) {
        //$api_url = $GLOBALS['sugar_config']['salesforce_api_url'];
        $api_url = $this->get_instance_url();
        $url = $api_url."/services/data/v20.0/sobjects/Lead/".$id;
    
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: OAuth $access_token"));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ( $status != 204 ) {
            $GLOBALS['log']->fatal("Salesforce API Create/Update Leads: Error: call to URL ".$url." failed with status ".$status.", curl_error ".curl_error($curl). ", curl_errno " . curl_errno($curl));
        }
        curl_close($curl);
    }
}