<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
* CUSTOM 2018/06/08
* QUANKT
* encrypt_decrypt
*/
class encrypt_decrypt {

    public function dec_enc($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = "1233BHKMN!@hmhaG45J";
        $secret_iv = "HJBJNKJ545#!@@VHBJHHJBvjhbhjb%u8";
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}