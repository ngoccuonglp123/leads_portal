<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/* 
 * FSO_Custom
 * Custom Job
 */
class fs_generate_cache {
    public function get_cache_version($path, $additional_attrs = '')
    {
        if (empty($GLOBALS['sugar_config']['js_custom_version'])) {
            $GLOBALS['sugar_config']['js_custom_version'] = 1;
        }
        $js_version_key = isset($GLOBALS['js_version_key']) ? $GLOBALS['js_version_key'] : '';
        $dev = '';
        if (is_array($additional_attrs)) {
            $additional_attrs = implode('|', $additional_attrs);
        }
        // cutting 2 last chars here because since md5 is 32 chars, it's always ==
        $str = substr(base64_encode(md5("$js_version_key|{$GLOBALS['sugar_config']['js_custom_version']}|$dev|$additional_attrs", true)), 0, -2);
        // remove / - it confuses some parsers
        $str = strtr($str, '/+', '-_');
        if (empty($path)) {
            return $str;
        }
    
        return $path . "?v=$str";
    }
}