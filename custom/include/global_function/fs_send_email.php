<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
* CUSTOM 2018/06/08
* QUANKT
* Custom call send email notification
*/
require_once('include/SugarPHPMailer.php');

class fs_send_email {
    public function send_email($toAddress, $ccAddress, $subject, $body, $template)
    {
        try {
            global $current_user, $db;
            $tempToAddress = array();
            $tempCCAddress = array();
            foreach($toAddress as $key => $value)
            {
                if(!in_array($value,$tempToAddress))
                {
                    array_push($tempToAddress,$value);
                }
            }
            foreach($ccAddress as $key => $value)
            {
                if(!in_array($value,$tempCCAddress))
                {
                    array_push($tempCCAddress,$value);
                }
            }

            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: '.$current_user->user_name.'-'.date("Y-m-d h:i:sa"));
            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: Subject: '.$subject);
            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: To: '.implode(",",$tempToAddress));
            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: CC: '.implode(",",$tempCCAddress));
            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: Content: '.$body);
            
            $emailObj = new Email();  
            $defaults = $emailObj->getSystemDefaultEmail();  
            $mail = new SugarPHPMailer();  
            $mail->setMailerForSystem();  
            $mail->From = $defaults['email'];  
            $mail->FromName = $defaults['name'];  
            $mail->Subject = $subject;
            $mail->IsHTML(true);
            $mail->Body = $body;
            $mail->prepForOutbound();
            foreach($tempToAddress as $key => $value)
            {
                if($value != null && $value != '')
                {
                    $mail->AddAddress($value);
                }
            }
            if(count($tempCCAddress) > 0)
            {
                foreach($tempCCAddress as $key => $value)
                {
                    if(!in_array($value,$tempToAddress) && $value != null && $value != '')
                    {
                        $mail->addCC($value);  
                    }
                }
            }

            // $bcc = $GLOBALS['sugar_config']['bcc_fs_send_mail'];
            // if($bcc != null && $bcc != '')
            // {
            //     $mail->addBCC($bcc);  
            // }

            $mail->Send();

            $mailStatus = "";
            $mailMsg = "";
            if($mail->ErrorInfo == null || $mail->ErrorInfo == "")
            {
                $mailStatus = "1";
            }
            else
            {
                $mailStatus = "0";
                $mailMsg = $mail->ErrorInfo;
            }
            $queryInsert = "INSERT INTO fs_email_history (`date`,`from`,`to`,cc,template,`status`,error_message,`subject`,`content`) VALUES ";
            $queryInsert .= "(NOW(),'".$current_user->user_name."','".implode(";",$tempToAddress)."','".implode(";",$tempCCAddress)."','".$template."','".$mailStatus."','".$mailMsg."','".$subject."','".$body."')";
            $db->query($queryInsert);
        } catch (Exception $e) {
            $GLOBALS['log']->fatal('FSO_CUSTOM_fs_send_email_ERROR: '.$e->getMessage());
        }
    }
    public function send_email_attach($toAddress, $ccAddress, $subject, $body, $template,$file_url,$file_name)
    {
        try {
            global $current_user, $db;
            $tempToAddress = array();
            $tempCCAddress = array();
            foreach($toAddress as $key => $value)
            {
                if(!in_array($value,$tempToAddress))
                {
                    array_push($tempToAddress,$value);
                }
            }
            foreach($ccAddress as $key => $value)
            {
                if(!in_array($value,$tempCCAddress))
                {
                    array_push($tempCCAddress,$value);
                }
            }

            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: '.$current_user->user_name.'-'.date("Y-m-d h:i:sa"));
            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: Subject: '.$subject);
            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: To: '.implode(",",$tempToAddress));
            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: CC: '.implode(",",$tempCCAddress));
            $GLOBALS['log']->debug('FSO_CUSTOM_fs_send_email: Content: '.$body);
            
            $emailObj = new Email();  
            $defaults = $emailObj->getSystemDefaultEmail();  
            $mail = new SugarPHPMailer();  
            $mail->setMailerForSystem();  
            $mail->From = $defaults['email'];  
            $mail->FromName = $defaults['name'];  
            $mail->Subject = $subject;
            $mail->IsHTML(true);
            $mail->Body = $body;
            
            
            $mail->fsHandleAttachments($file_url,$file_name);
            
            $mail->prepForOutbound();
            foreach($tempToAddress as $key => $value)
            {
                if($value != null && $value != '')
                {
                    $mail->AddAddress($value);
                }
            }
            if(count($tempCCAddress) > 0)
            {
                foreach($tempCCAddress as $key => $value)
                {
                    if(!in_array($value,$tempToAddress) && $value != null && $value != '')
                    {
                        $mail->addCC($value);  
                    }
                }
            }

            $bcc = $GLOBALS['sugar_config']['bcc_fs_send_mail'];
            if($bcc != null && $bcc != '')
            {
                $mail->addBCC($bcc);  
            }

            $mail->Send();

            $mailStatus = "";
            $mailMsg = "";
            if($mail->ErrorInfo == null || $mail->ErrorInfo == "")
            {
                $mailStatus = "1";
            }
            else
            {
                $mailStatus = "0";
                $mailMsg = $mail->ErrorInfo;
            }
            $queryInsert = "INSERT INTO fs_email_history (`date`,`from`,`to`,cc,template,`status`,error_message,`subject`,`content`) VALUES ";
            $queryInsert .= "(NOW(),'".$current_user->user_name."','".implode(";",$tempToAddress)."','".implode(";",$tempCCAddress)."','".$template."','".$mailStatus."','".$mailMsg."','".$subject."','".$body."')";
            $db->query($queryInsert);
        } catch (Exception $e) {
            $GLOBALS['log']->fatal('FSO_CUSTOM_fs_send_email_ERROR: '.$e->getMessage());
        }
    }
    /*
    * CUSTOM 2018/10/18
    * QUANKT
    * ReSend email For Job
    */
    public function re_send_email()
    {
        try {
            global $current_user, $db;

            $querySearch = " SELECT * FROM fs_email_history WHERE `status` = 0 AND TIMESTAMPDIFF(DAY,`date`,NOW()) <= 2 ";
            $result = $db->query($querySearch);
            $dataSet = array();
            $row = $db->fetchByAssoc($result);
            while ($row != null){
                $dataSet[] = $row;
                $row = $db->fetchByAssoc($result);
            }
            if(count($dataSet)>0)
            {
                foreach ($dataSet as $key => $values)
                {
                    $tempToAddress = array();
                    $tempCCAddress = array();
                    if($values["to"] != null && $values["to"] != "")
                    {
                        $tempToAddress = explode(";",$values["to"]);
                    }
                    if($values["cc"] != null && $values["cc"] != "")
                    {
                        $tempCCAddress = explode(";",$values["cc"]);
                    }
                    
                    $emailObj = new Email();  
                    $defaults = $emailObj->getSystemDefaultEmail();  
                    $mail = new SugarPHPMailer();  
                    $mail->setMailerForSystem();  
                    $mail->From = $defaults['email'];  
                    $mail->FromName = $defaults['name'];  
                    $mail->Subject = $values["subject"];
                    $mail->IsHTML(true);
                    $mail->Body = $values["content"];
                    $mail->prepForOutbound();
                    foreach($tempToAddress as $key => $value)
                    {
                        if($value != null && $value != '')
                        {
                            $mail->AddAddress($value);
                        }
                    }
                    if(count($tempCCAddress) > 0)
                    {
                        foreach($tempCCAddress as $key => $value)
                        {
                            if(!in_array($value,$tempToAddress) && $value != null && $value != '')
                            {
                                $mail->addCC($value);  
                            }
                        }
                    }
                    $mail->Send();
        
                    $mailStatus = "";
                    $mailMsg = "";
                    if($mail->ErrorInfo == null || $mail->ErrorInfo == "")
                    {
                        $queryInsert = " UPDATE fs_email_history SET `status` = 1 WHERE id = ".$values["id"];
                        $db->query($queryInsert); 
                    }
                }
            }
        } catch (Exception $e) {
            $GLOBALS['log']->fatal('FSO_CUSTOM_fs_send_email_ERROR: '.$e->getMessage());
        }
    }
}