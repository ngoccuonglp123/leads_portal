<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
* CUSTOM 2018/06/08
* QUANKT
* Custom Users
*/
require_once('modules/Users/User.php');
class fs_users {
    public function getPasswordHash($password){
        $f = new User;
        return $f->getPasswordHash($password);
    }

    public function checkPassword($user_id, $password){
        global $db;

        $query = "SELECT user_hash FROM users
                WHERE id = '".$user_id."' LIMIT 1;";
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $user_hash = $query_result["user_hash"];

        $f = new User;
        $result = $f->checkPassword($password,$user_hash);
        if($result ==  1 || $result == true){
            return true;
        }
        else return false;
    }

    public function passwordValidationCheck($newPassword){
        global $sugar_config, $app_list_strings;

        $messages = array();

        if (!isset($sugar_config['passwordsetting']['minpwdlength'])) {
            $sugar_config['passwordsetting']['minpwdlength'] = null;
        }
        $minpwdlength = $sugar_config['passwordsetting']['minpwdlength'];


        if (!isset($sugar_config['passwordsetting']['oneupper'])) {
            $sugar_config['passwordsetting']['oneupper'] = null;
        }
        $oneupper = $sugar_config['passwordsetting']['oneupper'];


        if (!isset($sugar_config['passwordsetting']['onelower'])) {
            $sugar_config['passwordsetting']['onelower'] = null;
        }
        $onelower = $sugar_config['passwordsetting']['onelower'];


        if (!isset($sugar_config['passwordsetting']['onenumber'])) {
            $sugar_config['passwordsetting']['onenumber'] = null;
        }
        $onenumber = $sugar_config['passwordsetting']['onenumber'];


        if (!isset($sugar_config['passwordsetting']['onespecial'])) {
            $sugar_config['passwordsetting']['onespecial'] = null;
        }
        $onespecial = $sugar_config['passwordsetting']['onespecial'];


        if ($minpwdlength && strlen($newPassword) < $minpwdlength) {
            $messages[] = sprintf(translate('ERR_PASSWORD_MINPWDLENGTH', 'Users'), $minpwdlength);
        }

        if ($oneupper && strtolower($newPassword) === $newPassword) {
            $messages[] = translate('ERR_PASSWORD_ONEUPPER', 'Users');
        }

        if ($onelower && strtoupper($newPassword) === $newPassword) {
            $messages[] = translate('ERR_PASSWORD_ONELOWER', 'Users');
        }

        if ($onenumber && !preg_match('/[0-9]/', $newPassword)) {
            $messages[] = translate('ERR_PASSWORD_ONENUMBER', 'Users');
        }

        if ($onespecial && false === strpbrk($newPassword, "#$%^&*()+=-[]';,./{}|:<>?~")) {
            $messages[] = translate('ERR_PASSWORD_SPECCHARS', 'Users');
        }

        $message = implode('<br>', $messages);

        return $message;
    }
}