<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class vin_vin_leads_jobs {
    public function lead_expiry_warning(){
        global $db, $app_list_strings;

        $query = " SELECT * FROM vin_vin_leads a
                JOIN vin_vin_leads_cstm b ON a.id = b.id_c
                WHERE a.deleted = 0 AND b.vin_lead_status_c = 'assigned' 
                AND b.vin_expiry_warning_c <> 1 
                AND b.vin_lead_expiry_date_c <= CURRENT_TIMESTAMP + INTERVAL 1 HOUR";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
           $query_update = "UPDATE vin_vin_leads_cstm SET vin_expiry_warning_c = 1 WHERE id_c = '".$value['id']."';";
           $db->query($query_update);

           $bean = BeanFactory::getBean('vin_vin_leads',$value['id']);
            //Email
            require_once('custom/include/global_function/fs_send_email.php');
            require_once('modules/EmailTemplates/EmailTemplate.php');
            $template_name = "Leads_Warning_Expiry";
            $template = new EmailTemplate();
            $template->retrieve_by_string_fields(array('name' => $template_name));
            $sendMail = new fs_send_email;
            $toAddress = array();
            $ccAddress = array();
            $subject = $template->subject;
            $body = from_html($template->body_html);
            
            $subject = str_replace('$lead_name',$bean->name,$subject);

            $beanAssignee = BeanFactory::getBean('Users',$bean->user_id4_c);
            $sea = new SugarEmailAddress;
            $emailAssignee = $sea->getPrimaryAddress($beanAssignee);
            array_push($toAddress, $emailAssignee);

            if($beanAssignee->full_name != '' && $beanAssignee->full_name != null){
                $body = str_replace('$assignee',$beanAssignee->full_name,$body);
            }
            else{
                $body = str_replace('$assignee',$beanAssignee->user_name,$body);
            }
            $body = str_replace('$lead_name',$bean->name,$body);
            $body = str_replace('$lead_time_expiry','<span style="color:red;">60 phút</span>',$body);
            $body = str_replace('$lead_phone',$bean->vin_phone_number_c,$body);
            $body = str_replace('$lead_email',$bean->vin_email_c,$body);
            $body = str_replace('$lead_note',$bean->vin_note_c,$body);
            $body = str_replace('$lead_status',$app_list_strings['vin_lead_status_c_list'][$bean->vin_lead_status_c],$body);
            $body = str_replace('$lead_rating',$app_list_strings['vin_rating_c_list'][$bean->vin_rating_c],$body);
            $body = str_replace('$lead_score',$bean->vin_lead_scoring_c,$body);
            $body = str_replace('$lead_project',$app_list_strings['allocate_dept_level1'][$bean->vin_project_c],$body);
            $body = str_replace('$lead_house_type',$app_list_strings['vin_house_type_list_c_list'][$bean->vin_house_type_list_c],$body);

            $date_now = new DateTime($bean->vin_assign_date_c);
            $date_now = $date_now->format('d/m/Y h:i a');
            $date_now = '<span style="color:red;">'.$date_now.'</span>';
            $body = str_replace('$lead_date_assign',$date_now,$body);
            $expiry_time = new DateTime($bean->vin_lead_expiry_date_c);
            $expiry_time_subject = $expiry_time->format('h:i a');
            $expiry_time_alert = $expiry_time->format('h:i a');
            $expiry_time = $expiry_time->format('d/m/Y h:i a');
            $expiry_time = '<span style="color:red;">'.$expiry_time.'</span>';
            $body = str_replace('$lead_date_expiry',$expiry_time,$body);
            $subject = str_replace('$expiry_time',$expiry_time_subject,$subject);

            $siteURL = $GLOBALS['sugar_config']['site_url'];
            $url = '<a href="'.$siteURL.'/index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id.'">đây</a>';
            $body = str_replace('$lead_url',$url,$body);

            $paramSendMail = $sendMail->send_email($toAddress, $ccAddress, $subject, $body, $template_name);

            //Alert
            $is_read = 0;
            $type = 'info';
            $target_module= 'vin_vin_leads';
            $name = "Lead Warning: ".$bean->name;
            $description = "[Chú ý] Hãy liên hệ với lead ".$bean->name." trước ".$expiry_time_alert;
            $url_redirect = 'index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id;
            $beanAlerts = BeanFactory::newBean('Alerts');
            $beanAlerts->name = $name;
            $beanAlerts->description = $description;
            $beanAlerts->url_redirect = $url_redirect;
            // $beanAlerts->target_module = $target_module;
            $beanAlerts->target_module = "Thông báo mới";
            $beanAlerts->is_read = $is_read;
            $beanAlerts->assigned_user_id = $beanAssignee->id;
            $beanAlerts->type = $type;
            $beanAlerts->save();
        }
    }

    public function lead_expiry_warning_180(){
        global $db, $app_list_strings;

        $query = " SELECT * FROM vin_vin_leads a
                JOIN vin_vin_leads_cstm b ON a.id = b.id_c
                WHERE a.deleted = 0 AND b.vin_lead_status_c = 'assigned' 
                AND b.vin_expiry_warning_c <> 2
                AND b.vin_expiry_warning_c <> 1
                AND b.vin_assign_date_c + INTERVAL 3 HOUR >= CURRENT_TIMESTAMP";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
           $query_update = "UPDATE vin_vin_leads_cstm SET vin_expiry_warning_c = 2 WHERE id_c = '".$value['id']."';";
           $db->query($query_update);

           $bean = BeanFactory::getBean('vin_vin_leads',$value['id']);
            //Email
            require_once('custom/include/global_function/fs_send_email.php');
            require_once('modules/EmailTemplates/EmailTemplate.php');
            $template_name = "Leads_Warning_Assign";
            $template = new EmailTemplate();
            $template->retrieve_by_string_fields(array('name' => $template_name));
            $sendMail = new fs_send_email;
            $toAddress = array();
            $ccAddress = array();
            $subject = $template->subject;
            $body = from_html($template->body_html);
            
            $subject = str_replace('$lead_name',$bean->name,$subject);

            $beanAssignee = BeanFactory::getBean('Users',$bean->user_id4_c);
            $sea = new SugarEmailAddress;
            $emailAssignee = $sea->getPrimaryAddress($beanAssignee);
            array_push($toAddress, $emailAssignee);

            if($beanAssignee->full_name != '' && $beanAssignee->full_name != null){
                $body = str_replace('$assignee',$beanAssignee->full_name,$body);
            }
            else{
                $body = str_replace('$assignee',$beanAssignee->user_name,$body);
            }
            $body = str_replace('$lead_name',$bean->name,$body);
            $body = str_replace('$lead_time_expiry','<span style="color:red;">60 phút</span>',$body);
            $body = str_replace('$lead_phone',$bean->vin_phone_number_c,$body);
            $body = str_replace('$lead_email',$bean->vin_email_c,$body);
            $body = str_replace('$lead_note',$bean->vin_note_c,$body);
            $body = str_replace('$lead_status',$app_list_strings['vin_lead_status_c_list'][$bean->vin_lead_status_c],$body);
            $body = str_replace('$lead_rating',$app_list_strings['vin_rating_c_list'][$bean->vin_rating_c],$body);
            $body = str_replace('$lead_score',$bean->vin_lead_scoring_c,$body);
            $body = str_replace('$lead_project',$app_list_strings['allocate_dept_level1'][$bean->vin_project_c],$body);
            $body = str_replace('$lead_house_type',$app_list_strings['vin_house_type_list_c_list'][$bean->vin_house_type_list_c],$body);

            $date_now = new DateTime($bean->vin_assign_date_c);
            $date_now = $date_now->format('d/m/Y h:i a');
            $date_now = '<span style="color:red;">'.$date_now.'</span>';
            $body = str_replace('$lead_date_assign',$date_now,$body);
            $expiry_time = new DateTime($bean->vin_lead_expiry_date_c);
            $expiry_time_subject = $expiry_time->format('h:i a');
            $expiry_time_alert = $expiry_time->format('h:i a');
            $expiry_time = $expiry_time->format('d/m/Y h:i a');
            $expiry_time = '<span style="color:red;">'.$expiry_time.'</span>';
            $body = str_replace('$lead_date_expiry',$expiry_time,$body);
            $subject = str_replace('$expiry_time',$expiry_time_subject,$subject);

            $siteURL = $GLOBALS['sugar_config']['site_url'];
            $url = '<a href="'.$siteURL.'/index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id.'">đây</a>';
            $body = str_replace('$lead_url',$url,$body);

            $paramSendMail = $sendMail->send_email($toAddress, $ccAddress, $subject, $body, $template_name);

            //Alert
            $is_read = 0;
            $type = 'info';
            $target_module= 'vin_vin_leads';
            $name = "Lead Warning: ".$bean->name;
            $description = "[Chú ý] Hãy liên hệ với lead ".$bean->name;
            $url_redirect = 'index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id;
            $beanAlerts = BeanFactory::newBean('Alerts');
            $beanAlerts->name = $name;
            $beanAlerts->description = $description;
            $beanAlerts->url_redirect = $url_redirect;
            //$beanAlerts->target_module = $target_module;
            $beanAlerts->target_module = "Thông báo mới";
            $beanAlerts->is_read = $is_read;
            $beanAlerts->assigned_user_id = $beanAssignee->id;
            $beanAlerts->type = $type;
            $beanAlerts->save();
        }
    }

    public function lead_expiry_inform(){
        global $db, $app_list_strings;

        $query = " SELECT * FROM vin_vin_leads a
                JOIN vin_vin_leads_cstm b ON a.id = b.id_c
                WHERE a.deleted = 0 AND b.vin_lead_status_c = 'assigned' 
                AND b.vin_lead_lock_c <> 1 
                AND b.vin_lead_expiry_date_c < NOW() ";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            $query_update = "UPDATE vin_vin_leads_cstm SET vin_lead_lock_c = 1 WHERE id_c = '".$value['id']."';";
            $db->query($query_update);
 
            $bean = BeanFactory::getBean('vin_vin_leads',$value['id']);
            require_once('custom/include/global_function/fs_send_email.php');
            require_once('modules/EmailTemplates/EmailTemplate.php');

            $bm = 0;
            $sa = 0;
            $sg = 0;
            $leader = array();
            $list_roles = $this->getRoleByUser($bean->user_id4_c);
            foreach($list_roles as $key => $value){
                if($value == 'BUSINESS_MANAGER'){
                    $bm = 1;
                }
                else if($value == 'SALE_ADMIN'){
                    $sa = 1;
                }
                else if($value == 'SALE_AGENT'){
                    $sg = 1;
                }
            }
            if($sa == 1){
                $leader = $this->getBusinessManagementCurrentUser($bean->user_id4_c);
            }
            else if($sg == 1){
                $leader = $this->getSaleAdminCurrentUser($bean->user_id4_c);
            }
            //Email
            $template_name = "Leads_Notice_Expiry";
            $template = new EmailTemplate();
            $template->retrieve_by_string_fields(array('name' => $template_name));
            $sendMail = new fs_send_email;
            $toAddress = array();
            $ccAddress = array();
            $subject = $template->subject;
            $body = from_html($template->body_html);
            
            $subject = str_replace('$lead_name',$bean->name,$subject);
            $beanAssignee = BeanFactory::getBean('Users',$bean->user_id4_c);
            $sea = new SugarEmailAddress;
            $emailAssignee = $sea->getPrimaryAddress($beanAssignee);
            array_push($toAddress, $emailAssignee);

            if($beanAssignee->full_name != '' && $beanAssignee->full_name != null){
                $body = str_replace('$assignee',$beanAssignee->full_name,$body);
            }
            else{
                $body = str_replace('$assignee',$beanAssignee->user_name,$body);
            }
            $body = str_replace('$lead_name',$bean->name,$body);
            $body = str_replace('$assigner',$leader['name'],$body);
            $body = str_replace('$lead_phone',$bean->vin_phone_number_c,$body);
            $body = str_replace('$lead_email',$bean->vin_email_c,$body);
            $body = str_replace('$lead_note',$bean->vin_note_c,$body);
            $body = str_replace('$lead_status',$app_list_strings['vin_lead_status_c_list'][$bean->vin_lead_status_c],$body);
            $body = str_replace('$lead_rating',$app_list_strings['vin_rating_c_list'][$bean->vin_rating_c],$body);
            $body = str_replace('$lead_score',$bean->vin_lead_scoring_c,$body);
            $body = str_replace('$lead_project',$app_list_strings['allocate_dept_level1'][$bean->vin_project_c],$body);
            $body = str_replace('$lead_house_type',$app_list_strings['vin_house_type_list_c_list'][$bean->vin_house_type_list_c],$body);

            $date_now = new DateTime($bean->vin_assign_date_c);
            $date_now = $date_now->format('d/m/Y h:i a');
            $date_now = '<span style="color:red;">'.$date_now.'</span>';
            $body = str_replace('$lead_date_assign',$date_now,$body);
            $expiry_time = new DateTime($bean->vin_lead_expiry_date_c);
            $expiry_time_subject = $expiry_time->format('h:i a');
            $expiry_time_alert = $expiry_time->format('h:i a');
            $expiry_time_subject = '<span style="color:red;">'.$expiry_time_subject.'</span>';
            $expiry_time = $expiry_time->format('d/m/Y h:i a');
            $expiry_time = '<span style="color:red;">'.$expiry_time.'</span>';
            $body = str_replace('$lead_date_expiry',$expiry_time,$body);
            $subject = str_replace('$expiry_time ',$expiry_time_subject,$subject);

            $siteURL = $GLOBALS['sugar_config']['site_url'];
            $url = '<a href="'.$siteURL.'/index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id.'">đây</a>';
            $body = str_replace('$lead_url',$url,$body);

            $paramSendMail = $sendMail->send_email($toAddress, $ccAddress, $subject, $body, $template_name);

            //Alert
            $is_read = 0;
            $type = 'info';
            $target_module= 'vin_vin_leads';
            $name = "Lead Warning: ".$bean->name;
            $description = "[Cảnh báo] Quá hạn liên hệ với lead ".$bean->name;
            $url_redirect = 'index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id;
            $beanAlerts = BeanFactory::newBean('Alerts');
            $beanAlerts->name = $name;
            $beanAlerts->description = $description;
            $beanAlerts->url_redirect = $url_redirect;
            //$beanAlerts->target_module = $target_module;
            $beanAlerts->target_module = "Thông báo mới";
            $beanAlerts->is_read = $is_read;
            $beanAlerts->assigned_user_id = $beanAssignee->id;
            $beanAlerts->type = $type;
            $beanAlerts->save();

            //Email
            $template_name = "Leads_Expiry_Inform ";
            $template = new EmailTemplate();
            $template->retrieve_by_string_fields(array('name' => $template_name));
            $toAddress = array();
            $ccAddress = array();
            $subject = $template->subject;
            $body = from_html($template->body_html);
            
            $subject = str_replace('$lead_name',$bean->name,$subject);
            $emailAssignee = $sea->getPrimaryAddress($beanAssignee);
            array_push($toAddress, $emailAssignee);

            $assignee = '';
            if($beanAssignee->full_name != '' && $beanAssignee->full_name != null){
                $body = str_replace('$assignee',$beanAssignee->full_name,$body);
                $subject = str_replace('$assignee',$beanAssignee->full_name,$subject);
                $assignee = $beanAssignee->full_name;
            }
            else{
                $body = str_replace('$assignee',$beanAssignee->user_name,$body);
                $subject = str_replace('$assignee',$beanAssignee->user_name,$subject);
                $assignee = $beanAssignee->user_name;
            }
            $body = str_replace('$lead_name',$bean->name,$body);
            $body = str_replace('$assigner',$leader['name'],$body);
            $body = str_replace('$lead_phone',$bean->vin_phone_number_c,$body);
            $body = str_replace('$lead_email',$bean->vin_email_c,$body);
            $body = str_replace('$lead_note',$bean->vin_note_c,$body);
            $body = str_replace('$lead_status',$app_list_strings['vin_lead_status_c_list'][$bean->vin_lead_status_c],$body);
            $body = str_replace('$lead_rating',$app_list_strings['vin_rating_c_list'][$bean->vin_rating_c],$body);
            $body = str_replace('$lead_score',$bean->vin_lead_scoring_c,$body);
            $body = str_replace('$lead_project',$app_list_strings['allocate_dept_level1'][$bean->vin_project_c],$body);
            $body = str_replace('$lead_house_type',$app_list_strings['vin_house_type_list_c_list'][$bean->vin_house_type_list_c],$body);

            $date_now = new DateTime($bean->vin_assign_date_c);
            $date_now = $date_now->format('d/m/Y h:i a');
            $date_now = '<span style="color:red;">'.$date_now.'</span>';
            $body = str_replace('$lead_date_assign',$date_now,$body);
            $expiry_time = new DateTime($bean->vin_lead_expiry_date_c);
            $expiry_time_subject = $expiry_time->format('h:i a');
            $expiry_time_alert = $expiry_time->format('h:i a');
            $expiry_time_subject = '<span style="color:red;">'.$expiry_time_subject.'</span>';
            $expiry_time = $expiry_time->format('d/m/Y h:i a');
            $expiry_time = '<span style="color:red;">'.$expiry_time.'</span>';
            $body = str_replace('$lead_date_expiry',$expiry_time,$body);
            $subject = str_replace('$expiry_time ',$expiry_time_subject,$subject);

            $siteURL = $GLOBALS['sugar_config']['site_url'];
            $url = '<a href="'.$siteURL.'/index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id.'">đây</a>';
            $body = str_replace('$lead_url',$url,$body);

            $paramSendMail = $sendMail->send_email($toAddress, $ccAddress, $subject, $body, $template_name);

            //Alert
            $is_read = 0;
            $type = 'info';
            $target_module= 'vin_vin_leads';
            $name = "Lead Warning: ".$bean->name;
            $description = "[Cảnh báo] ".$assignee." đã quá hạn liên hệ với lead ".$bean->name;
            $url_redirect = 'index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id;
            $beanAlerts = BeanFactory::newBean('Alerts');
            $beanAlerts->name = $name;
            $beanAlerts->description = $description;
            $beanAlerts->url_redirect = $url_redirect;
            //$beanAlerts->target_module = $target_module;
            $beanAlerts->target_module = "Thông báo mới";
            $beanAlerts->is_read = $is_read;
            $beanAlerts->assigned_user_id = $leader['id'];
            $beanAlerts->type = $type;
            $beanAlerts->save();

        }
    }

    private function getRoleByUser($id){
        global $db, $current_user;

        $list_roles = array();
        $query = "SELECT acl.name FROM acl_roles acl 
            JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
            JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
            WHERE acl.deleted = 0 AND sgu.user_id = '".$id."';";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($list_roles,$value['name']);
        }
        return $list_roles;
    }

    private function getBusinessManagementCurrentUser($id){
        global $db;

        $return = array();
        $name = '';
        $user_id = '';
        $query = "SELECT  u2.user_name, u2.id,
                CONCAT(IFNULL(u2.first_name,''),' ',IFNULL(u2.last_name,'')) AS fullname
                FROM users_cstm uc
                JOIN users u ON uc.id_c = u.id 
                JOIN users u2 ON uc.user_id1_c = u2.id 
                WHERE u.id = '".$id."' AND u.status = 'Active'
                AND u2.deleted = 0 LIMIT 1;";

        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $user_name = $query_result["user_name"];
        $fullname = $query_result["fullname"];
        $user_id = $query_result["id"];
        if($fullname != '' && $fullname != null){
            $name = $fullname;
        }
        else{
            $name = $user_name;
        }
        $return['name'] = $name;
        $return['id'] = $user_id;

        return $return;
    }

    private function getSaleAdminCurrentUser($id){
        global $db, $current_user;

        $return = array();
        $name = '';
        $user_id = '';
        $query = "SELECT u2.user_name,  u2.id,
                CONCAT(IFNULL(u2.first_name,''),' ',IFNULL(u2.last_name,'')) AS fullname
                FROM users_cstm uc
                JOIN users u ON uc.id_c = u.id 
                JOIN users u2 ON uc.user_id_c = u2.id 
                WHERE u.id = '".$id."'  AND u.status = 'Active' 
                AND u2.deleted = 0 LIMIT 1;";

        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $user_name = $query_result["user_name"];
        $fullname = $query_result["fullname"];
        $user_id = $query_result["id"];
        if($fullname != '' && $fullname != null){
            $name = $fullname;
        }
        else{
            $name = $user_name;
        }
        $return['name'] = $name;
        $return['id'] = $user_id;

        return $return;
    }

    public function sync_leads_from_salesforce(){
        global $db;

        //Get data to vin_leads_salesforce

        //Prepare person
        $list_bm = $this->getListBusinessManagement();
        $count_bm = count($list_bm);

        //Sync to Leads Portal
        $query = "SELECT DISTINCT batch_id FROM vin_leads_salesforce WHERE is_update = 0;";
        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            $queryBatch = "SELECT * FROM vin_leads_salesforce WHERE batch_id = '".$value['batch_id']."';";
            $resultBatch = $db->query($queryBatch);
            $dataSetBatch = array();
            $rowBatch = $db->fetchByAssoc($resultBatch);
            while ($rowBatch != null){
                $dataSetBatch[] = $rowBatch;
                $rowBatch = $db->fetchByAssoc($resultBatch);
            }
            $count_leads = count($dataSetBatch);
            $leads_hot = array();
            $leads_warm = array();
            $leads_cold = array();
            $totalRecord_Hot = 0;
            $totalRecord_Warm = 0;
            $totalRecord_Cold = 0;
            $fmod_hot = 0;
            $fmod_warm = 0;
            $fmod_cold = 0;
            $mark_leads_count = 0;
            $mark_leads = array();
            $mark_leads_score = array();

            //Caculate Rating
            foreach($dataSetBatch as $k => $v){
                $score = calculateScore($v);
                if($score['rating'] == 'hot'){
                    $totalRecord_Hot = $totalRecord_Hot + 1;
                }
                else if($score['rating'] == 'warm'){
                    $totalRecord_Warm = $totalRecord_Warm + 1;
                }
                else{
                    $totalRecord_Cold = $totalRecord_Cold + 1;
                }
                $mark_leads_count = $mark_leads_count + 1;
                $mark_leads[$mark_leads_count] = $score['rating'];
                $mark_leads_score[$mark_leads_count] = $score['score'];
            }

            //Caculate mod
            if($count_bm < $totalRecord_Hot){
                $fmod_hot = fmod($totalRecord_Hot, $count_bm);
            }
            else{ 
                $fmod_hot = -1;
            }
            if($count_bm < $totalRecord_Warm){
                $fmod_warm = fmod($totalRecord_Warm, $count_bm);
            }
            else{ 
                $fmod_warm = -1;
            }
            if($count_bm < $totalRecord_Cold){
                $fmod_cold = fmod($totalRecord_Cold, $count_bm);
            }
            else{ 
                $fmod_cold = -1;
            }

            //Save Leads
            $mark_leads_count = 0;
            $hot_saved = 0;
            $warm_saved = 0;
            $cold_saved = 0;
            $list_allocated =  array();
            $list_allocated['hot'] =  array();
            $list_allocated['warm'] =  array();
            $list_allocated['cold'] =  array();
            
            foreach($dataSetBatch as $k => $v){
                $auto_assign = 0;
                $mark_leads_count = $mark_leads_count + 1;
                $bean = BeanFactory::newBean('vin_vin_leads');

                // $bean->vin_salutation_c = $salutationKey;
                // $bean->vin_last_name_c = $lastName;
                // $bean->vin_first_name_c = $firstName;
                // $bean->vin_middle_name_c = $middleName;
                // $bean->vin_suffix_c = $suffix;
                // $bean->vin_email_c = $email;
                // $bean->vin_phone_number_c = $phone;
                // $bean->vin_id_number_c = $idNumber;
                // $bean->vin_company_name_c = $companyName;
                // $bean->vin_tax_code_c = $tax;
                // $bean->vin_mobile_c = $mobile;
                // $bean->vin_project_c = $projectKey;
                // $bean->vin_house_type_list_c = $houseTypeKey;
                // $bean->vin_budget_c = $budget;
                // $bean->vin_lead_source_c = $leadSourceKey;
                // $bean->vin_source_details_c = $sourceDetails;
                // $bean->vin_address_c = $address;
                // $bean->vin_city_text_c = $city;
                // $bean->vin_street_c = $street;
                // $bean->vin_country_c = $country;
                // $bean->vin_state_province_c = $sate;
                // $bean->vin_zip_postal_code_c = $zip;
                // $bean->vin_lead_id_salesforce_c = $idSalesforce;

                $bean->vin_vin_currency_id_c = '';
                $bean->user_id4_c = '';
                $bean->user_id1_c = '';
                $bean->ownership = 'salesforce';
                $bean->vin_lead_scoring_c = $mark_leads_score[$mark_leads_count];
                $bean->vin_rating_c = $mark_leads[$mark_leads_count];

                if($mark_leads[$mark_leads_count] == 'hot'){
                    if($fmod_hot == -1){
                        $auto_assign = 1;
                    }
                    else if($hot_saved <= $fmod_hot && $fmod_hot != 0){
                        $auto_assign = 1;
                    }
                    $hot_saved = $hot_saved + 1;
                }
                else if($mark_leads[$mark_leads_count] == 'warm'){
                    if($fmod_warm == -1){
                        $auto_assign = 1;
                    }
                    else if($warm_saved <= $fmod_warm && $fmod_warm != 0){
                        $auto_assign = 1;
                    }
                    $warm_saved = $warm_saved + 1;
                }
                else{
                    if($fmod_cold == -1){
                        $auto_assign = 1;
                    }
                    else if($cold_saved <= $fmod_cold && $fmod_cold != 0){
                        $auto_assign = 1;
                    }
                    $cold_saved = $cold_saved + 1;
                }

                if($auto_assign == 1){
                    $bean->user_id_c = $this->getBusinessManagementAllocate();
                }
                else{
                    $temp_allowcate = array();
                    if($bean->vin_rating_c == 'hot'){
                        $temp_allowcate = $list_allocated['hot'];
                    }
                    else if($bean->vin_rating_c == 'warm'){
                        $temp_allowcate = $list_allocated['warm'];
                    }
                    else{
                        $temp_allowcate = $list_allocated['cold'];
                    }

                    $last_value = end($temp_allowcate);
                    $bm_index = 0;
                    $bm_id = '';
                    if(!isNullOrEmpty($last_value)){
                        if (strpos($last_value, '_') !== false) {
                            $temp_allo = explode("_",$last_value);
                            $bm_next_index = intval($temp_allo[0]) + 1;
                            if (array_key_exists($bm_next_index,$list_bm)){
                                $bm_index = $bm_next_index;
                            }
                            else{
                                $bm_index = 0;
                                $bm_next_index = 0;
                            }
                            $bm_id = $list_bm[$bm_index];
                        }
                        else{
                            $bm_next_index = 0;
                            if (array_key_exists($bm_next_index,$list_bm)){
                                $bm_index = $bm_next_index;
                            }
                            $bm_id = $list_bm[$bm_index];
                        }

                        $bean->user_id_c = $bm_id;
                        $sales_agent_index = 0;
                        if($bean->vin_rating_c == 'hot'){
                            array_push($list_allocated['hot'],$bm_id."_".$sales_agent_index);
                        }
                        else if($bean->vin_rating_c == 'warm'){
                            array_push($list_allocated['warm'],$bm_id."_".$sales_agent_index);
                        }
                        else{
                            array_push($list_allocated['cold'],$bm_id."_".$sales_agent_index);
                        }
                    }
                }
                $bean->save();

                //Delete sync data
                $query = "DELETE FROM vin_leads_salesforce WHERE id = '".$v['id']."';";
                $db->query($query);
            }
        }

        return true;
    }

    public function sync_leads_to_salesforce(){
        global $db;

        return true;
    }

    private function getListBusinessManagement(){
        global $db;

        $return = array();
        $query = "SELECT u.id, u.user_name FROM users u
            JOIN securitygroups_users su ON u.id = su.user_id AND su.deleted = 0
            JOIN securitygroups sg ON sg.id = su.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_acl_roles sgacl ON sg.id = sgacl.securitygroup_id AND sgacl.deleted = 0
            JOIN acl_roles acl ON acl.id = sgacl.role_id AND acl.deleted = 0
            WHERE u.deleted = 0 AND u.status = 'Active' AND acl.name = 'BUSINESS_MANAGER';";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($return, $value['id']);
        }
        return $return;
    }

    private function calculateScore($bean){
        global $db, $current_user;
        $result_return = array();
    
        $score = 0;
        $rating = 'cold';
        //Score
        $query = "SELECT * FROM vin_lead_scoring s
            JOIN vin_lead_scoring_cstm sc ON s.id = sc.id_c
            WHERE s.deleted = 0;";
    
        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            if(array_key_exists ($value['vin_field_c'],$bean)){
                if($bean[$value['vin_field_c']] != null && $bean[$value['vin_field_c']] != ''){
                    $score = $score + (float)$value['vin_score_c'];
                }
            }
        }
    
        //Rating
        $query = "SELECT sc.vin_rating_c FROM vin_lead_rating_score s
            JOIN vin_lead_rating_score_cstm sc ON s.id = sc.id_c
            WHERE s.deleted = 0 
            AND sc.vin_from_c <= ".$score." AND sc.vin_to_c >= ".$score." LIMIT 1;";
    
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $vin_rating_c = $query_result["vin_rating_c"];
        if($vin_rating_c != '' && $vin_rating_c != null){
            $rating = $vin_rating_c;
        }
    
        $result_return['score'] = $score;
        $result_return['rating'] = $rating;
        return $result_return;
    }
    
    private function getBusinessManagementAllocate($rating){
        global $db, $current_user;

        $user_id = '';
        $query = "SELECT u.id, u.user_name, COUNT(l.id) FROM users u
            JOIN securitygroups_users su ON u.id = su.user_id AND su.deleted = 0
            JOIN securitygroups sg ON sg.id = su.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_acl_roles sgacl ON sg.id = sgacl.securitygroup_id AND sgacl.deleted = 0
            JOIN acl_roles acl ON acl.id = sgacl.role_id AND acl.deleted = 0
            LEFT JOIN vin_vin_leads_cstm lc ON lc.user_id_c = u.id AND lc.vin_rating_c = '".$rating."'
            LEFT JOIN vin_vin_leads l ON l.id = lc.id_c AND l.deleted = 0 
            WHERE u.deleted = 0 AND u.status = 'Active' AND acl.name = 'BUSINESS_MANAGER'
            GROUP BY u.id, u.user_name ORDER BY COUNT(l.id) LIMIT 1;";

        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $user_id = $query_result["id"];
        return $user_id;
    }

    public function salesforce_sync_leads_to_portal(){
        global $db;

        require_once('custom/include/global_function/vinhomes_salesforce_api.php');
        $f = new vinhomes_salesforce_api;
        $token = $f->get_access_token();
        $leads = $f->get_leads_from_salesforce($token);

        //Dummy data
        // $leads = array();
        // $data = array();
        // $data['vin_first_name_c'] = 'FName223';
        // $data['vin_last_name_c'] = 'LNam223';
        // $data['vin_email_c'] = 'abc2134453751@gmail.com';
        // $data['vin_company_name_c'] = 'Company Name';
        // $data['vin_country_c'] = 'VN';
        // $data['vin_lead_source_c'] = 'Advertisement';
        // $data['name'] = 'FName1 Mname LNam1 Suffix';
        // $data['vin_street_c'] = 'HN2';
        // $data['vin_city_text_c'] = 'HN';
        // $data['vin_state_province_c'] = '23123';
        // $data['vin_mobile_c'] = '0977261899';
        // $data['vin_phone_number_c'] = '0977261890';
        // $data['vin_vin_currency_id_c'] = 'VND';
        // $data['vin_lead_status_c'] = 'New';
        // $data['vin_rating_c'] = 'Hot';
        // $data['vin_zip_postal_code_c'] = '312312312';
        // $data['vin_id_number_c'] = '213331231231231';
        // $data['vin_project_c'] = 'Vinhomes Grand Park';
        // $data['vin_house_type_list_c'] = 'Shop';
        // $data['vin_budget_c'] = 100000000;
        // $data['vin_lead_scoring_c'] = 90;
        // $data['vin_source_details_c'] = 'abv';
        // $data['vin_tax_code_c'] = '213123123132121';
        // array_push($leads,$data);
        // array_push($leads,$data);
        // array_push($leads,$data);
        // array_push($leads,$data);
        // array_push($leads,$data);


        // $query = "SELECT UUID() AS batch_id";
        // $query_result = $db->query($query);
        // $query_result = $db->fetchByAssoc($query_result);
        // $batch_id = $query_result["batch_id"];

        // foreach($leads as $key => $temp){
        //     $query_insert = "INSERT INTO vin_leads_salesforce 
        //                 (
        //                     id,
        //                     batch_id,
        //                     vin_lead_id_salesforce_c,
        //                     vin_last_name_c,
        //                     vin_first_name_c,
        //                     name,
        //                     vin_full_name_c,
        //                     vin_company_name_c,
        //                     vin_street_c,
        //                     vin_city_text_c,
        //                     vin_state_province_c,
        //                     vin_zip_postal_code_c,
        //                     vin_country_c,
        //                     vin_phone_number_c,
        //                     vin_mobile_c,
        //                     vin_email_c,
        //                     vin_vin_currency_id_c,
        //                     vin_id_number_c,
        //                     vin_project_c,
        //                     vin_house_type_list_c,
        //                     vin_budget_c,
        //                     vin_lead_scoring_c,
        //                     vin_source_details_c,
        //                     vin_tax_code_c,
        //                     date_entered
        //                 )
        //                 VALUES 
        //                 (
        //                     (SELECT UUID()),
        //                     '".$batch_id."',
        //                     '".$temp['vin_lead_id_salesforce_c']."',
        //                     '".$temp['vin_last_name_c']."',
        //                     '".$temp['vin_first_name_c']."',
        //                     '".$temp['name']."',
        //                     '".$temp['vin_full_name_c']."',
        //                     '".$temp['vin_company_name_c']."',
        //                     '".$temp['vin_street_c']."',
        //                     '".$temp['vin_city_text_c']."',
        //                     '".$temp['vin_state_province_c']."',
        //                     '".$temp['vin_zip_postal_code_c']."',
        //                     '".$temp['vin_country_c']."',
        //                     '".$temp['vin_phone_number_c']."',
        //                     '".$temp['vin_mobile_c']."',
        //                     '".$temp['vin_email_c']."',
        //                     '".$temp['vin_vin_currency_id_c']."',
        //                     '".$temp['vin_id_number_c']."',
        //                     '".$temp['vin_project_c']."',
        //                     '".$temp['vin_house_type_list_c']."',
        //                     '".$temp['vin_budget_c']."',
        //                     '".$temp['vin_lead_scoring_c']."',
        //                     '".$temp['vin_source_details_c']."',
        //                     '".$temp['vin_tax_code_c']."',
        //                     NOW()
        //                 );";

        //     $db->query($query_insert);
        // }
        $list_admin = array();
        $bm_id = '';
        $query = "SELECT user_id_c FROM vin_vin_department a
            JOIN vin_vin_department_cstm b ON a.id = b.id_c
            JOIN users u ON u.id = b.user_id_c AND u.deleted = 0 AND u.status = 'Active'
            WHERE a.deleted = 0 AND a.description LIKE 'ALLOCATE_DEFAULT%' LIMIT 1;";
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $bm_id = $query_result['user_id_c'];
        foreach($leads as $key => $temp){
            $query = "SELECT id_c FROM vin_vin_leads_cstm 
                WHERE vin_lead_id_salesforce_c ='".$temp['vin_lead_id_salesforce_c']."' LIMIT 1;";
            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            // $query_result = array();
            // $query_result['id_c'] = "";
            if($query_result['id_c'] == '' || $query_result['id_c'] == null){
                $bean = BeanFactory::newBean('vin_vin_leads');
                $bean->vin_lead_id_salesforce_c = $temp['vin_lead_id_salesforce_c'];
                $bean->vin_last_name_c = $temp['vin_last_name_c'];
                $bean->vin_first_name_c = $temp['vin_first_name_c'];
                $bean->name = $temp['name'];
                $bean->vin_full_name_c = $temp['vin_full_name_c'];
                $bean->vin_salutation_c = $temp['vin_salutation_c'];
                $bean->vin_company_name_c = $temp['vin_company_name_c'];
                $bean->vin_street_c = $temp['vin_street_c'];
                $bean->vin_city_text_c = $temp['vin_city_text_c'];
                $bean->vin_state_province_c = $temp['vin_state_province_c'];
                $bean->vin_zip_postal_code_c = $temp['vin_zip_postal_code_c'];
                $bean->vin_country_c = $temp['vin_country_c'];
                $bean->vin_phone_number_c = $temp['vin_phone_number_c'];
                $bean->vin_mobile_c = $temp['vin_mobile_c'];
                $bean->vin_email_c = $temp['vin_email_c'];
                $bean->vin_id_number_c = $temp['vin_id_number_c'];
                $bean->vin_project_c = $temp['vin_project_c'];
                $bean->vin_house_type_list_c = $temp['vin_house_type_list_c'];
                $bean->vin_budget_c = $temp['vin_budget_c'];
                $bean->vin_lead_scoring_c = $temp['vin_lead_scoring_c'];
                $bean->vin_source_details_c = $temp['vin_source_details_c'];
                $bean->vin_tax_code_c = $temp['vin_tax_code_c'];
                $bean->vin_phankhu_c = $temp['vin_phankhu_c'];
                $bean->vin_ads_source_c = $temp['vin_ads_source_c'];
                $bean->vin_lead_ads_id_c = $temp['vin_lead_ads_id_c'];
                $bean->vin_ads_content_c = $temp['vin_ads_content_c'];
                $bean->vin_agent_c = $temp['vin_agent_c'];
                $bean->vin_ad_term_c = $temp['vin_ad_term_c'];
                $bean->vin_form_medium_c = $temp['vin_form_medium_c'];
                $bean->vin_campaign_c = $temp['vin_campaign_c'];
                $bean->vin_utm_source_c = $temp['vin_utm_source_c'];
                $bean->vin_lead_source_c = $temp['vin_lead_source_c'];
                $bean->vin_lead_status_c = $temp['vin_lead_status_c'];
                $bean->vin_rating_c = $temp['vin_rating_c'];
                $bean->vin_dob_c = $temp['vin_dob_c'];
                $bean->vin_sign_status_c = $temp['vin_sign_status_c'];
                $bean->vin_lead_duplicate_c = $temp['vin_lead_duplicate_c'];
                if($temp['vin_vin_department_id2_c'] != '' && $temp['vin_vin_department_id2_c'] != null){
                    $query = "SELECT id FROM vin_vin_department 
                        WHERE LOWER(name) = '".$temp['vin_vin_department_id2_c']."' 
                        AND deleted = 0 LIMIT 1;";
                    $query_result = $db->query($query);
                    $query_result = $db->fetchByAssoc($query_result);
                    $bean->vin_vin_department_id2_c = $query_result['id'];
                }
                if($temp['vin_rating_c'] == null){
                    $bean->vin_rating_c = '';
                }
                $bean->ownership = "salesforce";
    
                if($temp['vin_vin_currency_id_c'] != '' && $temp['vin_vin_currency_id_c']!= null){
                    $query = "SELECT * FROM vin_vin_currency 
                        WHERE deleted = 0 AND name='".$temp['vin_vin_currency_id_c']."'
                        LIMIT 1;";
                    $query_result = $db->query($query);
                    $query_result = $db->fetchByAssoc($query_result);
                    $bean->vin_vin_currency_id_c = $query_result['id'];
                }
    
                $is_digital = 1;
                $sc_source = $bean->vin_lead_source_c;
                $sc_level1 = $bean->vin_project_c;
                $sc_level2 = $bean->vin_phankhu_c;
                $sc_level3 = $bean->vin_ads_source_c;
				$sc_level3 = "";
                //$app_list_strings['allocate_dept_source']
                //$app_list_strings['allocate_dept_level1']
                //$app_list_strings['allocate_dept_level2']
                //$app_list_strings['allocate_dept_level3']
                
                //Get allocate matrix
                $matrix_id = '';
                $allocate = '';
                $query = "SELECT * FROM vin_allocate_matrix
                    WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = '".$sc_level1."' ";
                if($sc_level2 == "" || $sc_level2 == null){
                    $query .= " AND (level2 = '' OR level2 IS NULL) ";
                }
                else{
                    $query .= " AND level2 = '".$sc_level2."' ";
                }
                if($sc_level3 == "" || $sc_level3 == null){
                    $query .= " AND (level3 = '' OR level3 IS NULL) ";
                }
                else{
                    $query .= " AND level3 = '".$sc_level3."' ";
                }
                $query .= " LIMIT 1;";
                $query_result = $db->query($query);
                $query_result = $db->fetchByAssoc($query_result);
                $matrix_id = $query_result['id'];
                $allocate = $query_result['allocate'];
                $allocate_index = '';
                $new_index = 0;
                
                //No result
                if($matrix_id == '' || $matrix_id == null
                || $allocate == '' || $allocate == null){
                    $query = "SELECT * FROM vin_allocate_matrix
                        WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = '".$sc_level1."' ";
                    if($sc_level2 == "" || $sc_level2 == null){
                        $query .= " AND (level2 = '' OR level2 IS NULL) ";
                    }
                    else{
                        $query .= " AND level2 = '".$sc_level2."' ";
                    }
                    $query .= " AND level3 = 'OT' ";
                    $query .= " LIMIT 1;";
                    $query_result = $db->query($query);
                    $query_result = $db->fetchByAssoc($query_result);
                    $matrix_id = $query_result['id'];
                    $allocate = $query_result['allocate'];
                    //No result
                    if($matrix_id == '' || $matrix_id == null
                    || $allocate == '' || $allocate == null){
                        $query = "SELECT * FROM vin_allocate_matrix
                            WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = '".$sc_level1."' ";
                        $query .= " AND (level2 = 'OT' OR level2 = '' OR level2 IS NULL) ORDER BY level2 ";
                        $query .= " LIMIT 1;";
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $matrix_id = $query_result['id'];
                        $allocate = $query_result['allocate'];
                        //No result
                        if($matrix_id == '' || $matrix_id == null
                        || $allocate == '' || $allocate == null){
                            $query = "SELECT * FROM vin_allocate_matrix
                                WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = 'OTH' ";
                            $query .= " LIMIT 1;";
                            $query_result = $db->query($query);
                            $query_result = $db->fetchByAssoc($query_result);
                            $matrix_id = $query_result['id'];
                            $allocate = $query_result['allocate'];
                        }
                    }
                }

                if($matrix_id != '' && $matrix_id != null
                && $allocate != '' && $allocate != null){
                    //Get current index
                    $allocate = explode(",",$allocate);
                    $temp = array();
                    foreach($allocate as $k => $v){
                        if(trim($v) != "" && $v != null){
                            array_push($temp,$v);
                        }
                    }
                    $allocate = $temp;
                    $allocate_length = count($allocate);
                    if($allocate_length > 0){
                        $query = "SELECT * FROM vin_allocate_flow
                            WHERE matrix_id = '".$matrix_id."' LIMIT 1;";
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $allocate_index = $query_result['index'];
                        $count_check = 0;
                        while($count_check <= $allocate_length){
                            $count_check++;
                            if($allocate_index != '' && $query_result['id'] != ''
                            && $allocate_index != null && $query_result['id'] != null){
                                $allocate_index = (float)$allocate_index;
                                if((float)$allocate_index >= ((float)$allocate_length -1)){
                                    $allocate_index = 0;
                                }
                                else{
                                    $allocate_index = (float)$allocate_index + 1;
                                }
                            }
                            else{
                                $new_index = 1;
                                $allocate_index = 0;
                            }
                            //Allocate
                            if($allocate_index >= 0){
                                $dept_id = $allocate[$allocate_index];
                                if($dept_id != "" && $dept_id != null){
                                    $query = "SELECT user_id_c FROM vin_vin_department a
                                        JOIN vin_vin_department_cstm b ON a.id = b.id_c
                                        JOIN users u ON u.id = b.user_id_c AND u.deleted = 0 AND u.status = 'Active'
                                        WHERE a.deleted = 0 AND a.id = '".$dept_id."' LIMIT 1;";
                                    $query_result = $db->query($query);
                                    $query_result = $db->fetchByAssoc($query_result);
                                    if($query_result['user_id_c'] != "" && $query_result['user_id_c'] != null){
                                        $is_digital = 0;
                                        $count_check = $allocate_length + 1;
                                        $bean->user_id_c = $query_result['user_id_c'];
                                    }
                                }
                            }
                        }
                    }
                }
                //Digital Maketing
                if($is_digital == 1){
                    $bean->user_id_c = $bm_id;
                }
                else{
                    if($new_index == 1){
                        $query_insert = "INSERT INTO vin_allocate_flow (matrix_id,`index`)
                            VALUES ('".$matrix_id."','".$allocate_index."');";
                        $db->query($query_insert);
                    }
                    else{
                        $query_update = " UPDATE vin_allocate_flow SET 
                            `index` = '".$allocate_index."'
                            WHERE matrix_id = '".$matrix_id."';";
                        $db->query($query_update);
                    }

                    //Phân bổ tự động
                    //$matrix_id = $this->getMatrixAllocate($bean);
                    if($matrix_id != '' && $matrix_id != null){
                        $allocate = $this->getSalesAdminByAllocate($bean->user_id_c,$matrix_id);
                        if($allocate['id'] != '' && $allocate['id'] != null){
                            $bean->user_id1_c = $allocate['id'];
                            $this->markSalesAdminAllocated($allocate['flow_id'], $allocate['index']);
                            if(!in_array($allocate['id'],$list_admin)){
                                array_push($list_admin, $allocate['id']);
                            }
                        }
                    }
                    //End
                }
                $bean->save();
            }
        }
        $this->notice_sales_admin_after_sync($list_admin);
        return true;
    }

    public function salesforce_sync_leads_to_salesforce(){
        global $db;

        $query = "SELECT a.*,b.*, c.name AS vin_currency_c 
            FROM vin_vin_leads a
            JOIN vin_vin_leads_cstm b ON a.id = b.id_c
            LEFT JOIN vin_vin_currency c ON b.vin_vin_currency_id_c = c.id AND c.deleted = 0
            WHERE a.date_modified >= NOW() - INTERVAL 50 HOUR;";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        require_once('custom/include/global_function/vinhomes_salesforce_api.php');
        $f = new vinhomes_salesforce_api;
        $token = $f->get_access_token();
        if($token != '' && $token != null){
            foreach($dataSet as $key => $value){
                if($value['vin_lead_id_salesforce_c'] == '' || $value['vin_lead_id_salesforce_c'] == null){
                    $id = $f->sync_new_lead_to_salesforce($token, $value);
                    $query = "UPDATE  vin_vin_leads_cstm 
                        SET vin_lead_id_salesforce_c = '".$id."' 
                        WHERE id_c = '".$value['id']."';";
                    $db->query($query);
                }
                else{
                    if($value['deleted'] == '1' || $value['deleted'] == 1){
                        $f->sync_delete_lead_to_salesforce($token, $value['vin_lead_id_salesforce_c']);
                    }
                    else{
                        $f->sync_update_lead_to_salesforce($token, $value, $value['vin_lead_id_salesforce_c']);
                    }
                }
            }
        }

    }

    public function lead_unqualified_refresh(){
        global $db, $app_list_strings;

        $bm_id = '';
        $query = "SELECT user_id_c FROM vin_vin_department a
            JOIN vin_vin_department_cstm b ON a.id = b.id_c
            JOIN users u ON u.id = b.user_id_c AND u.deleted = 0 AND u.status = 'Active'
            WHERE a.deleted = 0 AND a.description LIKE 'ALLOCATE_DEFAULT%' LIMIT 1;";
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $bm_id = $query_result['user_id_c'];

        $query = " SELECT * FROM vin_vin_leads a
                JOIN vin_vin_leads_cstm b ON a.id = b.id_c
                WHERE a.deleted = 0 AND b.vin_lead_status_c = 'unqualified' ";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
           $query_update = "UPDATE vin_vin_leads_cstm 
                        SET vin_expiry_warning_c = 0 ,
                        vin_lead_lock_c = 0,
                        vin_lead_status_c = 'new',
                        user_id1_c = '',
                        user_id2_c = '',
                        user_id3_c = '',
                        user_id4_c = '',
                        vin_vin_department_id_c = '',
                        vin_vin_department_id1_c = '',
                        vin_working_substatus_c = '',
                        vin_unqualified_reason_c = '',
                        user_id_c = '".$bm_id."'
                        WHERE id_c = '".$value['id']."';";
           $db->query($query_update);
        }
        return true;
    }

    private function getMatrixAllocate($bean){
        global $db;

        $sc_source = $bean->vin_lead_source_c;
        $sc_level1 = $bean->vin_project_c;
        $sc_level2 = $bean->vin_phankhu_c;
        $sc_level3 = $bean->vin_ads_source_c;
        $sc_level3 = "";
        $matrix_id = '';

        $query = "SELECT * FROM vin_allocate_matrix
            WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = '".$sc_level1."' ";
        if($sc_level2 == "" || $sc_level2 == null){
            $query .= " AND (level2 = '' OR level2 IS NULL) ";
        }
        else{
            $query .= " AND level2 = '".$sc_level2."' ";
        }
        if($sc_level3 == "" || $sc_level3 == null){
            $query .= " AND (level3 = '' OR level3 IS NULL) ";
        }
        else{
            $query .= " AND level3 = '".$sc_level3."' ";
        }
        $query .= " LIMIT 1;";
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $matrix_id = $query_result['id'];
        //No result
        if($matrix_id == '' || $matrix_id == null){
            $query = "SELECT * FROM vin_allocate_matrix
                WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = '".$sc_level1."' ";
            if($sc_level2 == "" || $sc_level2 == null){
                $query .= " AND (level2 = '' OR level2 IS NULL) ";
            }
            else{
                $query .= " AND level2 = '".$sc_level2."' ";
            }
            $query .= " AND level3 = 'OT' ";
            $query .= " LIMIT 1;";
            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $matrix_id = $query_result['id'];
            //No result
            if($matrix_id == '' || $matrix_id == null){
                $query = "SELECT * FROM vin_allocate_matrix
                    WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = '".$sc_level1."' ";
                $query .= " AND (level2 = 'OT' OR level2 = '' OR level2 IS NULL) ORDER BY level2 ";
                $query .= " LIMIT 1;";
                $query_result = $db->query($query);
                $query_result = $db->fetchByAssoc($query_result);
                $matrix_id = $query_result['id'];
                //No result
                if($matrix_id == '' || $matrix_id == null){
                    $query = "SELECT * FROM vin_allocate_matrix
                        WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = 'OTH' ";
                    $query .= " LIMIT 1;";
                    $query_result = $db->query($query);
                    $query_result = $db->fetchByAssoc($query_result);
                    $matrix_id = $query_result['id'];
                }
            }
        }
        return $matrix_id;
    }

    private function getSalesAdminByAllocate($id, $matrix_id){
        global $db;

        $dept_id = '';
        $return = array();
        $return['id'] = '';
        $return['flow_id'] = '';
        $return['index'] = '';
        $query = "SELECT d.id, d.name FROM vin_vin_department d
                JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                WHERE d.deleted = 0 
                AND u1.id='".$id."' LIMIT 1;";
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $dept_id = $query_result["id"];
        if($dept_id != '' && $dept_id != null){
            $flow_id = '';
            $query = "SELECT id,allocate,`index` FROM vin_allocate_flow_sadmin
                    WHERE matrix_id = '".$matrix_id."' AND dept_id = '".$dept_id."' LIMIT 1;";

            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $flow_id = $query_result["id"];

            if($flow_id != '' && $flow_id != null){
                $allocate = $query_result["allocate"];
                $allocate_index = $query_result["index"];
                if($allocate != '' && $allocate != null){
                    //Get current index
                    $allocate = explode(",",$allocate);
                    $temp = array();
                    foreach($allocate as $k => $v){
                        if(trim($v) != "" && $v != null){
                            array_push($temp,$v);
                        }
                    }
                    $allocate = $temp;
                    $allocate_length = count($allocate);
                    if($allocate_length > 0){
                        $count_check = 0;
                        while($count_check <= $allocate_length){
                            $count_check++;
                            if($allocate_index != '' && $query_result['id'] != ''
                            && $allocate_index != null && $query_result['id'] != null){
                                $allocate_index = (float)$allocate_index;
                                if((float)$allocate_index >= ((float)$allocate_length -1)){
                                    $allocate_index = 0;
                                }
                                else{
                                    $allocate_index = (float)$allocate_index + 1;
                                }
                            }
                            else{
                                $new_index = 1;
                                $allocate_index = 0;
                            }
                            //Allocate
                            if($allocate_index >= 0){
                                $sadmin = $allocate[$allocate_index];
                                $return['id'] = $sadmin;
                                $return['flow_id'] = $flow_id;
                                $return['index'] = $allocate_index;
                                $count_check = $allocate_length + 1;
                            }
                        }
                    }
                }
            }
        }
        return $return;
    }

    private function markSalesAdminAllocated($flow_id, $allocate_index){
        global $db;

        $query_update = " UPDATE vin_allocate_flow_sadmin SET 
            `index` = '".$allocate_index."'
            WHERE id = '".$flow_id."';";
        $db->query($query_update);
    }

    public function notice_sales_admin_after_sync($list_admin){
        require_once('custom/include/global_function/fs_send_email.php');
        require_once('modules/EmailTemplates/EmailTemplate.php');
        foreach($list_admin as $key => $value){
            $template_name = "Notice_Admin_After_Sync";
            $template = new EmailTemplate();
            $template->retrieve_by_string_fields(array('name' => $template_name));
            $sendMail = new fs_send_email;
            $toAddress = array();
            $ccAddress = array();
            $subject = $template->subject;
            $body = from_html($template->body_html);
            $beanAssignee = BeanFactory::getBean('Users',$value);
            $sea = new SugarEmailAddress;
            $emailAssignee = $sea->getPrimaryAddress($beanAssignee);
            array_push($toAddress, $emailAssignee);
            if($beanAssignee->full_name != '' && $beanAssignee->full_name != null){
                $body = str_replace('$assignee',$beanAssignee->full_name,$body);
            }
            else{
                $body = str_replace('$assignee',$beanAssignee->user_name,$body);
            }
            $siteURL = $GLOBALS['sugar_config']['site_url'];
            $url = '<a href="'.$siteURL.'">đây</a>';
            $body = str_replace('$lead_url',$url,$body);

            $paramSendMail = $sendMail->send_email($toAddress, $ccAddress, $subject, $body, $template_name);
        }
    }
}