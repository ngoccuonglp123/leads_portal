<?php
$module_name = 'vin_lead_scoring';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'vin_field_c',
            'label' => 'LBL_VIN_FIELD_C',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'vin_score_c',
            'label' => 'LBL_VIN_SCORE_C',
          ),
          1 => 'description',
        ),
      ),
    ),
  ),
);
;
?>
