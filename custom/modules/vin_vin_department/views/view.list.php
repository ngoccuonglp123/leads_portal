<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/global_function/fs_generate_cache.php');

class vin_vin_departmentViewList extends ViewList
{
    function display()
    {
        global $db, $app_list_strings, $current_user;
        $fs_generate_cache = new fs_generate_cache;

        $digital = 0;
        $list_roles = $this->getRoleByUser($current_user->id);
        foreach($list_roles as $key => $value){
            if($value == 'DIGITAL_MARKETING'){
                $digital = 1;
            }
        }

        if($current_user->is_admin == 1 || $digital == 1){
            echo '<script>
                    var btn_allocate_button = '."'".'<button type="button" id="btn_allocate_button" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="allocate_matrix();">Allocate</button>'."'".';
                </script>';
            echo '<script>
                    $(document).ready(function () {
                        if(document.getElementById("fs_custom_header_buttons") == null){
                            $( '."'".'<div id="fs_custom_header_buttons"></div>'."'".' ).insertBefore( ".moduleTitle .clear" );
                        }

                        if(document.getElementById("btn_allocate_button") == null){
                            $("#fs_custom_header_buttons").append(btn_allocate_button);
                        }
                    });
                    function allocate_matrix(){
                        window.location.href = "index.php?module=vin_vin_department&action=allocate_matrix";
                    }
                </script>
            ';
        }

        parent::display();
    }

    function getRoleByUser($id){
        global $db, $current_user;
    
        $list_roles = array();
        $query = "SELECT acl.name FROM acl_roles acl 
            JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
            JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
            WHERE acl.deleted = 0 AND sgu.user_id = '".$id."';";
    
        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($list_roles,$value['name']);
        }
        return $list_roles;
    }
}