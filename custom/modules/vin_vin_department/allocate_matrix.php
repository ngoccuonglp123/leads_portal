<script>
    $(document).ready(function () {
        $("#loaderModal").modal('show');
        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: "index.php?entryPoint=departments&subaction=loadSource",
                success: function(result){
                    $('#allocate_matrix_body').append(result);
                    $("#allocate_matrix_body").css('display','block');
                    $("#allocate_matrix_header").css('display','block');

                    if(allocate_source != undefined && typeof allocate_source === 'object'){
                        var temp = "";
                        for (var key in allocate_source) {
                            if (!allocate_source.hasOwnProperty(key)) continue;
                            var obj = allocate_source[key];
                            temp = temp + "<option value='"+obj.key+"'>"+obj.value+"</option>";
                        }
                        $('#allocate_source_select').html('');
                        $('#allocate_source_select').append(temp);
                    }

                    if(allocate_level1 != undefined && typeof allocate_level1 === 'object'){
                        var temp = "";
                        for (var key in allocate_level1) {
                            if (!allocate_level1.hasOwnProperty(key)) continue;
                            var obj = allocate_level1[key];
                            temp = temp + "<option value='"+obj.key+"'>"+obj.value+"</option>";
                        }
                        $('#allocate_level1_select').html('');
                        $('#allocate_level1_select').append(temp);
                    }

                    if(allocate_level2 != undefined && typeof allocate_level2 === 'object'){
                        var temp = "";
                        for (var key in allocate_level2) {
                            if (!allocate_level2.hasOwnProperty(key)) continue;
                            var obj = allocate_level2[key];
                            temp = temp + "<option value='"+obj.key+"'>"+obj.value+"</option>";
                        }
                        $('#allocate_level2_select').html('');
                        $('#allocate_level2_select').append(temp);
                    }

                    if(allocate_level3 != undefined && typeof allocate_level3 === 'object'){
                        var temp = "";
                        for (var key in allocate_level3) {
                            if (!allocate_level3.hasOwnProperty(key)) continue;
                            var obj = allocate_level3[key];
                            temp = temp + "<option value='"+obj.key+"'>"+obj.value+"</option>";
                        }
                        $('#allocate_level3_select').html('');
                        $('#allocate_level3_select').append(temp);
                    }
                    rebuid_allocate_list();
                    reload_departments_drag();
                    $("#loaderModal").modal('hide');
                },
                error: function() {
                    $("#loaderModal").modal('hide');
                }
            });
        }, 0);
        //$("#loaderModal").modal('hide');
    });

    function reload_departments_drag(){
        if(departments_list != undefined && typeof departments_list === 'object'){
            var temp = "<span id='allocate_choose_departments_header'>Kéo phòng KD vào phân bổ</span><hr id='allocate_choose_departments_hr' /><p></p>";
            for (var key in departments_list) {
                if (!departments_list.hasOwnProperty(key)) continue;
                var obj = departments_list[key];
                temp = temp + '<div class="drag_item_dept" draggable="true" id="dept_'+obj.id+'" data-id="'+obj.id+'">'+obj.name+'</div>';
            }
            $('#allocate_choose_departments_content').html('');
            $('#allocate_choose_departments_content').append(temp);
        }

        //Reload value
        for (var key in allocate_list) {
            if (!allocate_list.hasOwnProperty(key)) continue;
            var obj = allocate_list[key];
            var temp_id = "";
            $('#allocate_value_'+obj.index).children().each(function(){
                temp_id = temp_id + $(this).attr('data-id') + ",";
            });
            obj.allocate = temp_id;
        }
    }

    function addNewObject(){
        $("#allocate_source_select").css("border-color", "#a5e8d6");
        $("#allocate_level1_select").css("border-color", "#a5e8d6");
        $("#allocate_source_select").val('');
        $("#allocate_level1_select").val('');
        $("#allocate_level2_select").val('');
        $("#allocate_level3_select").val('');
        $("#allocate_msg").html('');
        $("#allocate_msg2").html('');
        $("#create_modal").modal('show');
    }

    function onSubmitAddObject(x){
        var pass = 1;
        $("#allocate_source_select").css("border-color", "#a5e8d6");
        $("#allocate_level1_select").css("border-color", "#a5e8d6");
        $("#allocate_msg").html('');
        $("#allocate_msg2").html('');
        if($("#allocate_source_select").val() == ""){
            $("#allocate_source_select").css("border-color", "red");
            pass = 0;
        }
        if($("#allocate_level1_select").val() == ""){
            $("#allocate_level1_select").css("border-color", "red");
            pass = 0;
        }
        for (var key in allocate_list) {
            if (!allocate_list.hasOwnProperty(key)) continue;
            var obj = allocate_list[key];
            if(obj.source == $("#allocate_source_select").val()
            && obj.level1 == $("#allocate_level1_select").val()
            && obj.level2 == $("#allocate_level2_select").val()
            && obj.level3 == $("#allocate_level3_select").val()){
                $("#allocate_msg").html('Phân bổ đã tồn tại!!!');
                pass = 0;
            }
        }
        
        if(pass == 1){
            var last_index = allocate_list_last_index();
            allocate_list[last_index+1]= {source: $("#allocate_source_select").val(), source_disp: $("#allocate_source_select option:selected").text(),
                    level1: $("#allocate_level1_select").val(), level1_disp: $("#allocate_level1_select option:selected").text(),
                    level2: $("#allocate_level2_select").val(), level2_disp: $("#allocate_level2_select option:selected").text(),
                    level3: $("#allocate_level3_select").val(), level3_disp: $("#allocate_level3_select option:selected").text(),
                    allocate: "", index: last_index+1, id: "", current_index: "" };
            if(x == 0){
                rebuid_allocate_list();
                $("#create_modal").modal('hide');
            }
            else{
                rebuid_allocate_list();
                $("#allocate_source_select").css("border-color", "#a5e8d6");
                $("#allocate_level1_select").css("border-color", "#a5e8d6");
                $("#allocate_source_select").val('');
                $("#allocate_level1_select").val('');
                $("#allocate_level2_select").val('');
                $("#allocate_level3_select").val('');
                $("#allocate_msg2").html('Tạo phân bổ thành công!!!');
            }
        }
    }

    function rebuid_allocate_list(){
        $("#allocate_matrix_table").html('');
        var temp = "";
        temp += '<tr style="background-color: #c9e6ff;">';
        temp += '<th style="width: 10%">NGUỒN LEAD</th>';
        temp += '<th style="width: 10%">LEVEL 1 <p></p> DỰ ÁN</th>';
        temp += '<th style="width: 10%">LEVEL 2 <p></p> PHÂN KHU</th>';
        temp += '<th style="width: 10%;display: none;">LEVEL 3 <p></p> Ads source</th>';
        temp += '<th style="width: 60%">PHÂN BỔ</th>';
        temp += '</tr>';

        var data = [];
        for (var key in allocate_list) {
            if (!allocate_list.hasOwnProperty(key)) continue;
            var obj = allocate_list[key];
            var temp_data = {};
            temp_data.allocate = obj.allocate;
            temp_data.index = obj.index;
            temp_data.id = obj.id;
            temp_data.current_index = obj.current_index;
            temp_data.level1 = obj.level1;
            temp_data.level1_disp = obj.level1_disp;
            temp_data.level2 = obj.level2;
            temp_data.level2_disp = obj.level2_disp;
            temp_data.level3 = obj.level3;
            temp_data.level3_disp = obj.level3_disp;
            temp_data.source = obj.source;
            temp_data.source_disp = obj.source_disp;
            data.push(temp_data);
        }

        data = data.sort(fieldSorter(['-source', 'level1', 'level2', 'level3']));

        for (var key in data) {
            if (!data.hasOwnProperty(key)) continue;
            var obj = data[key];
            temp += '<tr style="">';
            temp += '<td style="width: 10%"><div>'+obj.source_disp+'</div></td>';
            temp += '<td style="width: 10%"><div>'+obj.level1_disp+'</div></td>';
            temp += '<td style="width: 10%"><div>'+obj.level2_disp+'</div></td>';
            temp += '<td style="width: 10%;display: none;" name="allocate_level3"><div>'+obj.level3_disp+'&nbsp;</div></td>';
            temp += '<td style="width: 60%" name="allocate_value"><div id="allocate_value_'+obj.index+'" data-id="'+obj.id+'" data-index="'+obj.index+'" class="droptarget_allocate">&nbsp;';
            
            var current_index = obj.current_index;
            var myStringArray = obj.allocate.split(",");
            var arrayLength = myStringArray.length;
            for (var i = 0; i < arrayLength; i++) {
                var dept_name = "";
                if(myStringArray[i] != ""){
                    if(departments_list != undefined && typeof departments_list === 'object'){
                        for (var key2 in departments_list) {
                            if (!departments_list.hasOwnProperty(key2)) continue;
                            var obj2 = departments_list[key2];
                            if(obj2.id == myStringArray[i]){
                                dept_name = obj2.name;
                            }
                        }
                    }
                    var current_dept = '';
                    if(current_index != "" && current_index != undefined && current_index == i){
                        current_dept = 'style="background-color: #5cc2ff;"';
                    }
                    temp = temp + '<div class="drag_item_allocate allocate_value_name_'+obj.index+'" '+current_dept+' draggable="true" id="allocate_value_'+obj.index+'_'+i+'" data-id="'+myStringArray[i]+'">'+dept_name+'</div>';
                }
            }
            temp += '</div></td>';
            temp += '</tr>';
        }

        $("#allocate_matrix_table").html(temp);
        auto_row_span();
    }

    function allocate_list_last_index(){
        var last_index = -1;
        if(allocate_list != undefined && typeof allocate_list === 'object'){
            for (var key in allocate_list) {
                if (!allocate_list.hasOwnProperty(key)) continue;
                if(Number(key) > Number(last_index)){
                    last_index = Number(key);
                }
            }
        }
        return last_index;
    }

    function generateTable(data) {
        $("#allocate_matrix_table").html();
        $.each(data.result, function(key, elem) {
            var baseHtml = "";
            var childrenHtml = "";
            var maxRowSpan = 0;
            $.each(elem, function(index, inner_elem) {
            var innerElemKey = Object.keys(inner_elem)[0];
            var arr = inner_elem[innerElemKey];
            var elemRowSpan = arr.length;
            maxRowSpan += arr.length;

            if (index === 0) {
                childrenHtml += ('<td rowspan="' + elemRowSpan + '">' + innerElemKey + '</td>');
                $.each(arr, function(indx, child) {
                if (indx === 0) {
                    childrenHtml += ('<td rowspan="1">' + child + '</td>' + '</tr>');
                } else {
                    childrenHtml += ('<tr><td rowspan="1">' + child + '</td>' + '</tr>');
                }
                });
            } else {
                childrenHtml += ('<tr><td rowspan="' + elemRowSpan + '">' + innerElemKey + '</td>');
                $.each(arr, function(indx, child) {
                if (indx === 0) {
                    childrenHtml += ('<td rowspan="1">' + child + '</td>' + '</tr>');
                } else {
                    childrenHtml += ('<tr><td rowspan="1">' + child + '</td>' + '</tr>');
                }
                });
            }
            });
            baseHtml += ('<tr><td rowspan="' + maxRowSpan + '">' + key + '</td>');
            $("#allocate_matrix_table").append(baseHtml + childrenHtml);
        });
    }

    function fieldSorter(fields) {
        return function (a, b) {
            return fields
                .map(function (o) {
                    var dir = 1;
                    if (o[0] === '-') {
                    dir = -1;
                    o=o.substring(1);
                    }
                    if (a[o] > b[o]) return dir;
                    if (a[o] < b[o]) return -(dir);
                    return 0;
                })
                .reduce(function firstNonZeroValue (p,n) {
                    return p ? p : n;
                }, 0);
        };
    }

    function auto_row_span(){
        $("#allocate_matrix_table").each(function() {
            $("#allocate_matrix_table").find('td').each(function() {
                var $this = $(this);
                if($(this).attr('name') != 'allocate_value' && $(this).attr('name') != 'allocate_level4'){
                    var col = $this.index();
                    var html = $this.html();
                    var row = $(this).parent()[0].rowIndex; 
                    var span = 1;
                    var cell_above = $($this.parent().prev().children()[col]);

                    // look for cells one above another with the same text
                    while (cell_above.html() === html) { // if the text is the same
                        span += 1; // increase the span
                        cell_above_old = cell_above; // store this cell
                        cell_above = $(cell_above.parent().prev().children()[col]); // and go to the next cell above
                    }

                    // if there are at least two columns with the same value,
                    // set a new span to the first and hide the other
                    if (span > 1) {
                        $(cell_above_old).attr('rowspan', span);
                        $this.hide();
                    }
                }
            });
        }); 
    }

    function addSaveAllocateMatrix(){
        $(".droptarget_allocate").css('background-color','#FFFFFF');
        var in_valid = 0;
        for (var key in allocate_list) {
            if (!allocate_list.hasOwnProperty(key)) continue;
            var obj = allocate_list[key];
            if(obj.allocate == "" || obj.allocate == null){
                in_valid = 1;
                //$("#allocate_value_"+obj.index).animate({backgroundColor: 'red'},{duration:2000});
                $("#allocate_value_"+obj.index).css('background-color','red');
                // setTimeout(function(){
                //     $("#allocate_value_"+obj.index).animate({backgroundColor: '#FFFFFF'},{duration:2000});
                // },2000);
            }
        }
        setTimeout(function(){
            for (var key in allocate_list) {
                if (!allocate_list.hasOwnProperty(key)) continue;
                var obj = allocate_list[key];
                if(obj.allocate == "" || obj.allocate == null){
                        $("#allocate_value_"+obj.index).animate({backgroundColor: '#FFFFFF'},{duration:3000});
                }
            }
        },2000);

        if(in_valid == 0){
            $("#loaderModal").modal('show');
            setTimeout(function(){
                var data = new Array();
                for (var key in allocate_list) {
                    if (!allocate_list.hasOwnProperty(key)) continue;
                    var obj = allocate_list[key];
                    var collection = {
                            'allocate' : obj.allocate,
                            'id' : obj.id,
                            'source' : obj.source,
                            'level1' : obj.level1,
                            'level2' : obj.level2,
                            'level3' : obj.level3
                    };
                    data.push( collection );
                }
                $.ajax({
                    type: 'POST',
                    url: "index.php?entryPoint=departments&subaction=save_allocate_matrix",
                    data: { vin_csrf_token : vin_csrf_token, data : data },
                    success: function(result){
                        allocate_list = {};
                        var index = 0;
                        for (var key in result) {
                            if (!result.hasOwnProperty(key)) continue;
                            var obj = result[key];
                            allocate_list[index]= {source: obj.source, source_disp: obj.source_disp,
                                level1: obj.level1, level1_disp: obj.level1_disp,
                                level2: obj.level2, level2_disp: obj.level2_disp,
                                level3: obj.level3, level3_disp: obj.level3_disp,
                                allocate: obj.allocate, index: index, current_index: obj.current_index, id: obj.id };
                            index = index + 1;
                        }
                        rebuid_allocate_list();
                        $("#loaderModal").modal('hide');
                    },
                    error: function() {
                        $("#loaderModal").modal('hide');
                    }
                });
            }, 0);
            //$("#loaderModal").modal('hide');
        }
        else if(in_valid == 1){
            alert("Hãy thêm Phòng Kinh Doanh vào các phân bổ còn trống!");
        }
    }

</script>
<div id="allocate_matrix_header" style="display: none;">
    <span>Sales Cloud Lead Allocate</span>
    <div id="allocate_matrix_header_button">
        <button type="button" id="" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="addNewObject()">Create</button>
        <button type="button" id="" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="addSaveAllocateMatrix()">Save</button>
    </div>
</div>
<div id="allocate_matrix_body" style="display: none;">
    <p>&nbsp</p>
    <span style="padding-left: 10px;font-weight: bold;">*Kéo thả các Phòng Kinh Doanh để thay đổi vị trí, kéo ra ngoài để xóa phòng KD.</span>
    <div style="display: -webkit-box;">
        <div style="background-color: #5cc2ff;width: 40px;height: 23px;border: 1px solid #5c52ee;margin: 5px 5px 5px 10px;box-shadow: 2px 2px rgba(0, 0, 0, 0.16);">&nbsp;</div>
        <span style="line-height: 35px;font-weight: bold;">Phân bổ hiện tại</span>
    </div>
    <table id="allocate_matrix_table">
        <tr style="background-color: #c9e6ff;">
            <th style="width: 10%">NGUỒN LEAD</th>
            <th style="width: 10%">LEVEL 1 <p></p> DỰ ÁN</th>
            <th style="width: 10%">LEVEL 2 <p></p> PHÂN KHU</th>
            <th style="width: 10%;display: none;">LEVEL 3 <p></p> Ads source</th>
            <th style="width: 60%">PHÂN BỔ</th>
        </tr>
    </table>
</div>

<div class="modal fade bd-example-modal-lg" id="loaderModal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="z-index: 100000000 !important;">
    <div class="modal-dialog modal-sm" style="padding-top: 17%;">
        <div class="" style="width: 0;">
            <div class="spinner-border text-primary"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="create_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="encrypt_modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
        <div class="modal-content" style="max-width: 100%;">
            <div class="modal-header" style="padding: 10px; ">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size: 15px; ">Create</h4>
            </div>
            <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                <span id="allocate_msg" style="color:red;"></span>
                <span id="allocate_msg2" style="color:green;"></span>
                <table style="width: 100%;table-layout: fixed;" id="">
                    <tr style="">
                        <td style="width: 30%;font-weight: bold;padding-left: 5px;line-height: 30px;">Nguồn:<span style="color:red;">*</span></td>
                        <td><select id="allocate_source_select" style="min-width: 50%;"></select></td>
                    </tr>
                    <tr style="">
                        <td style="width: 30%;font-weight: bold;padding-left: 5px;line-height: 30px;">Level 1:<span style="color:red;">*</span></td>
                        <td style=""><select id="allocate_level1_select" style="min-width: 50%;"></select></td>
                    </tr>
                    <tr style="">
                        <td style="width: 30%;font-weight: bold;padding-left: 5px;line-height: 30px;">Level 2: </td>
                        <td style=""><select id="allocate_level2_select" style="min-width: 50%;"></select></td>
                    </tr>
                    <tr style="display:none;">
                        <td style="width: 30%;font-weight: bold;padding-left: 5px;line-height: 30px;">Level 3: </td>
                        <td style=""><select id="allocate_level3_select" style="min-width: 50%;"></select></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button style="background-color: #3d9dd5;" type="button" class="btn btn-primary" data="" onclick="onSubmitAddObject(0);" id="btn_encrypt">Create</button>
                <button style="background-color: #3d9dd5;" type="button" class="btn btn-primary" data="" onclick="onSubmitAddObject(1);" id="btn_encrypt">Create And Continue</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="droptarget" id="drag1" style="display: none;">
  <div class="drag_item" draggable="true" id="dragtarget1">Drag me 1!</div>
  <div class="drag_item" draggable="true" id="dragtarget2">Drag me 2!</div>
  <div class="drag_item" draggable="true" id="dragtarget3">Drag me 3!</div>
  <div class="drag_item" draggable="true" id="dragtarget4">Drag me 4!</div>
  <div class="drag_item" draggable="true" id="dragtarget5">Drag me 5!</div>
</div>

<script>
document.addEventListener("dragstart", function(event) {
  event.dataTransfer.setData("Text", event.target.id);
  if(event.target.style != undefined){
    event.target.style.opacity = "0.4";
  }
});

document.addEventListener("drag", function(event) {

});

document.addEventListener("dragend", function(event) {
  if(event.target.style != undefined){
    event.target.style.opacity = "1";
  }
});

document.addEventListener("dragenter", function(event) {
  if ( event.target.className == "droptarget_allocate" ) {
    if(event.target.style != undefined){
        event.target.style.border = "1px solid blue";
    }
  }
});

document.addEventListener("dragover", function(event) {
  event.preventDefault();
});

document.addEventListener("dragleave", function(event) {
  if ( event.target.className == "droptarget_allocate" ) {
    if(event.target.style != undefined){
        event.target.style.border = "";
    }
  }
});

document.addEventListener("drop", function(event) {
  event.preventDefault();
  var data = event.dataTransfer.getData("Text");
  var dragElement = document.getElementById(data);
  if(dragElement.className == "drag_item_dept"){
    if ( event.target.className == "droptarget_allocate" ) {
        event.target.style.border = "";
        // var divRect = document.getElementById('drag1').getBoundingClientRect();
        var divRect = document.getElementById(event.target.id).getBoundingClientRect();
        if (event.clientX >= divRect.left && event.clientX <= divRect.right &&
        event.clientY >= divRect.top && event.clientY <= divRect.bottom) {
            //inside element.
            var x = event.pageX;
            var y = event.pageY;
            var eleName = "allocate_value_name_"+document.getElementById(event.target.id).getAttribute("data-index");
            var element = getClosest(x,y, dragElement, eleName);
            var countChild = document.getElementById(event.target.id).childElementCount + 1;
            while(true){
                if(document.getElementById(event.target.id+"_"+countChild) != null){
                    countChild++;
                }
                else{
                    break;
                }
            }
            if(element == 1){
                // $( "#drag1" ).append( $( "#"+dragElement.id ) );
                $( "#"+event.target.id ).append( $( "#"+dragElement.id ) );
                dragElement.id = event.target.id+"_"+countChild;
                dragElement.className = "drag_item_allocate"+" "+eleName;
                dragElement.name = eleName;
            }
            else{
                var clientRect = element.getBoundingClientRect();
                var xclientX = clientRect.left;
                var yclientY = clientRect.to;

                if(xclientX > x){
                    $( "#"+dragElement.id ).insertBefore( $( "#"+element.id ) );
                    dragElement.id = event.target.id+"_"+countChild;
                    dragElement.className = "drag_item_allocate"+" "+eleName;
                    dragElement.name = eleName;
                }
                else{
                    $( "#"+dragElement.id ).insertAfter( $( "#"+element.id ) );
                    dragElement.id = event.target.id+"_"+countChild;
                    dragElement.className = "drag_item_allocate"+" "+eleName;
                    dragElement.name = eleName;
                }
            }
        }
        else{

        }
    }
  }
  else{
    if ( event.target.className == "droptarget_allocate" ) {
        event.target.style.border = "";
        // var divRect = document.getElementById('drag1').getBoundingClientRect();
        var divRect = document.getElementById(event.target.id).getBoundingClientRect();
        if (event.clientX >= divRect.left && event.clientX <= divRect.right &&
        event.clientY >= divRect.top && event.clientY <= divRect.bottom) {
            //inside element.
            var x = event.pageX;
            var y = event.pageY;
            var eleName = "allocate_value_name_"+document.getElementById(event.target.id).getAttribute("data-index");
            //console.log(event);
            var element = getClosest(x,y, dragElement, eleName);
            var countChild = document.getElementById(event.target.id).childElementCount + 1;
            while(true){
                if(document.getElementById(event.target.id+"_"+countChild) != null){
                    countChild++;
                }
                else{
                    break;
                }
            }

            if(element == 1){
                // $( "#drag1" ).append( $( "#"+dragElement.id ) );
                $( "#"+event.target.id ).append( $( "#"+dragElement.id ) );
                dragElement.id = event.target.id+"_"+countChild;
                dragElement.className = "drag_item_allocate"+" "+eleName;
                dragElement.name = eleName;
            }
            else{
                var clientRect = element.getBoundingClientRect();
                var xclientX = clientRect.left;
                var yclientY = clientRect.to;
                //console.log(xclientX+" - "+x);
                //console.log(dragElement.id+" - "+element.id);
                if(xclientX > x){
                    if(dragElement.parentElement.id  == event.target.id){
                        $( "#"+dragElement.id ).insertBefore( $( "#"+element.id ) );
                        dragElement.className = "drag_item_allocate"+" "+eleName;
                        dragElement.name = eleName;
                    }
                    else{
                        $( "#"+dragElement.id ).insertBefore( $( "#"+element.id ) );
                        dragElement.id = event.target.id+"_"+countChild;
                        dragElement.className = "drag_item_allocate"+" "+eleName;
                        dragElement.name = eleName;
                    }
                }
                else{
                    if(dragElement.parentElement.id  == event.target.id){
                        $( "#"+dragElement.id ).insertAfter( $( "#"+element.id ) );
                        dragElement.className = "drag_item_allocate"+" "+eleName;
                        dragElement.name = eleName;
                    }
                    else{
                        $( "#"+dragElement.id ).insertAfter( $( "#"+element.id ) );
                        dragElement.id = event.target.id+"_"+countChild;
                        dragElement.className = "drag_item_allocate"+" "+eleName;
                        dragElement.name = eleName;
                    }
                }
            }
        }
        else{

        }
    }
    else{
        // var divRect = document.getElementById('drag1').getBoundingClientRect();
        var parentElement = dragElement.parentElement;
        var divRect = parentElement.getBoundingClientRect();
        if (event.clientX >= divRect.left && event.clientX <= divRect.right &&
        event.clientY >= divRect.top && event.clientY <= divRect.bottom) {
            //inside element.

        }
        else{
            var data = event.dataTransfer.getData("Text");
            // document.getElementById('drag1').removeChild(document.getElementById(data));
            parentElement.removeChild(document.getElementById(data));
        }
    
    }
  }
  reload_departments_drag();
});

function getClosest(x, y, dragElement, name_fs){
    // var divs = document.getElementsByClassName('drag_item_allocate');
    //var divs = document.getElementsByName(name);
    var divs = document.getElementsByClassName(name_fs);
    if(divs.length == 0){
        return 1;
    }
    var minDist = 99999,
        maxLastX = 0,
        maxLastY = 0,
        maxLastY_2 = 0,
        closestElement = {};
    for(var i = 0; i < divs.length; i++){
        if(divs[i] !== this && divs[i].id != dragElement.id){
            //Top-Left
            // var x2 = divs[i].offsetLeft,
            //     y2 = divs[i].offsetTop;
            var x2 = $("#"+divs[i].id).offset().left,
                y2 = $("#"+divs[i].id).offset().top;
            dist = Math.sqrt((x2 - x) *(x2-x) + (y2 - y) * (y2-y));
            if(dist < minDist){
                minDist = dist;
                closestElement = divs[i];
            }
            //console.log($("#"+divs[i].id).offset());
            //console.log(x+" - "+y+" - "+x2+" - "+y2+" - "+divs[i].id);
            //console.log(minDist+" - "+divs[i].id);
            //Top-Right
            x2 = $("#"+divs[i].id).offset().left + $("#"+divs[i].id).outerWidth();
            y2 = $("#"+divs[i].id).offset().top;
            dist = Math.sqrt((x2 - x) *(x2-x) + (y2 - y) * (y2-y));
            if(dist < minDist){
                minDist = dist;
                closestElement = divs[i];
            }
            //console.log(minDist+" - "+divs[i].id);
            //Bot-Left
            x2 = $("#"+divs[i].id).offset().left;
            y2 = $("#"+divs[i].id).offset().top + $("#"+divs[i].id).outerHeight();
            dist = Math.sqrt((x2 - x) *(x2-x) + (y2 - y) * (y2-y));
            if(dist < minDist){
                minDist = dist;
                closestElement = divs[i];
            }
            //console.log(minDist+" - "+divs[i].id);
            if(y2 > maxLastY){
                if(maxLastY > 0){
                    maxLastY_2 = maxLastY;
                }
                maxLastY = y2;
                maxLastX = 0;
            }
            //Bot-Right
            x2 = $("#"+divs[i].id).offset().left + $("#"+divs[i].id).outerWidth();
            y2 = $("#"+divs[i].id).offset().top + $("#"+divs[i].id).outerHeight();
            dist = Math.sqrt((x2 - x) *(x2-x) + (y2 - y) * (y2-y));
            if(dist < minDist){
                minDist = dist;
                closestElement = divs[i];
            }
            if(x2 > maxLastX){
                maxLastX = x2;
            }
            //console.log(minDist+" - "+divs[i].id);

        }
    }
    //Last Element
    if(x >= maxLastX && y >= maxLastY_2){
        //return 1;
    }
    return closestElement;
}

    function allocate_choose_departments_btn_click(e){
        if(e == 0){
            $("#allocate_choose_departments_btn_hide").css('display','none');
            $("#allocate_choose_departments_btn_show").css('display','block');
            $("#allocate_choose_departments").animate({right: '-190px'});
        }
        else{
            $("#allocate_choose_departments_btn_hide").css('display','block');
            $("#allocate_choose_departments_btn_show").css('display','none');
            $("#allocate_choose_departments").animate({right: '0px'});
        }
    }
</script>

<div id="allocate_choose_departments">
    <div id="allocate_choose_departments_btn">
        <a href="javascript:void(0)"  id="allocate_choose_departments_btn_hide" class="glyphicon glyphicon-chevron-right" style="" onclick="allocate_choose_departments_btn_click(0)" title="Delete"></a>
        <a href="javascript:void(0)"  id="allocate_choose_departments_btn_show" class="glyphicon glyphicon-chevron-left" style="display: none;" onclick="allocate_choose_departments_btn_click(1)" title="Delete"></a>
    </div>
    <div id="allocate_choose_departments_content" class="droptarget_dept">
    
    </div>
</div>