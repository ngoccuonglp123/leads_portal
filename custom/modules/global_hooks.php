<?php

class global_hooks {
    function custom_after_ui_frame(){
        global $current_user;
        //var_dump($_REQUEST);die;

        if(isset($_REQUEST)){
            if(isset($_REQUEST['action']) && isset($_REQUEST['module'])){
                if(strtolower($_REQUEST['action']) != "index"
                && strtolower($_REQUEST['action']) != "detailview"
                && strtolower($_REQUEST['action']) != "editview"
                && strtolower($_REQUEST['action']) != "listview"
                && strtolower($_REQUEST['action']) != "popup"
                && strtolower($_REQUEST['action']) != "passwordmanager"
                && strtolower($_REQUEST['action']) != "subpanelcreates"
                && strtolower($_REQUEST['module']) != "import"
                && strtolower($_REQUEST['action']) != "changepassword"){
                    if(strtolower($_REQUEST['action']) != "config" && strtolower($_REQUEST['module']) != "securitygroups" ){
                        return;
                    }
                }
                if(strtolower($_REQUEST['action']) != "index" && strtolower($_REQUEST['module']) == "modulebuilder"){
                    return;
                }
                else if(strtolower($_REQUEST['action']) != "index" && strtolower($_REQUEST['module']) == "modulebuilder"){

                }
                else if(strtolower($_REQUEST['action']) == "popup" 
                    && strtolower($_REQUEST['module']) == "audit"
                    && strtolower($_REQUEST['module_name']) != ""
                    && strtolower($_REQUEST['record']) != ""){
                        // $beanLead = BeanFactory::getBean($_REQUEST['module_name'],$_REQUEST['record']);
                        // if($beanLead != null && $beanLead != false && $beanLead->ACLAccess('DetailView')){

                        // }
                        // else{
                        //     header("Location: index.php?module=vin_vin_leads&action=index&parentTab=Sales"); 
                        //     exit();
                        // }
                }
            }
        }

        if(session_id() != '' && isset($_SESSION)) {
            if(isset($_SESSION["VIN_CSRF_TOKEN"])){
                echo '
                    <script>
                        var vin_csrf_token = "'.$_SESSION["VIN_CSRF_TOKEN"].'";
                    </script>
                ';
            }
        }
        require_once('custom/include/global_function/fs_generate_cache.php');
        $fs_generate_cache = new fs_generate_cache;
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/global/global.js','').'"></script>';
        echo '<input type="hidden" id="fs_current_user_name" value="'.$current_user->user_name.'" />';
        echo '<input type="hidden" id="fs_current_user_id" value="'.$current_user->id.'" />';

        if(isset($_REQUEST)){
            if(isset($_REQUEST['module'])){
                if(strtolower($_REQUEST['module']) == "users" && 
                (strtolower($_REQUEST['action']) == "detailview" || strtolower($_REQUEST['action']) == "editview" )){
                    if($current_user->is_admin != 1){
                        header("Location: index.php?module=vin_vin_leads&action=index&parentTab=Sales"); 
                        exit();
                    }
                }
            }
        }

    }

    function custom_after_login(){
        global $db, $current_user;
        $str = $current_user->id.date('Y-m-d H:i:s');
        $str2 = $current_user->user_name.date('Y-m-d H:i:s');
        $_SESSION["VIN_CSRF_TOKEN"] = md5($str).sha1($str2);

        $ipaddress = "";
        if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        if($ipaddress == ""){
            if(isset($_SERVER['REMOTE_ADDR'])){
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            }
        }
        if($ipaddress != ""){
            $query = " DELETE FROM vin_login_failed WHERE ip = '".$ipaddress."';";
            $result = $db->query($query);
        }
    }

    function custom_before_save(){
        if(isset($_REQUEST)){
            if(isset($_REQUEST['action'])){
                if(strtolower($_REQUEST['action']) == "save"
                    || strtolower($_REQUEST['action']) == "changepassword"
                    || strtolower($_REQUEST['action']) == "passwordmanager"){
                    if(session_id() != '' && isset($_SESSION)) {
                        if(isset($_SESSION["VIN_CSRF_TOKEN"])){
                            if(!isset($_REQUEST['vin_csrf_token'])){
                                echo "CSRF BLOCKED.";
                                die;
                            }
                            else{
                                if($_REQUEST['vin_csrf_token'] == null || $_REQUEST['vin_csrf_token'] == ''){
                                    echo "CSRF BLOCKED.";
                                    die;
                                }
                                else if($_REQUEST['vin_csrf_token'] != $_SESSION["VIN_CSRF_TOKEN"]){
                                    echo "CSRF BLOCKED.";
                                    die;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function custom_before_delete(){
        if(isset($_REQUEST)){
            if(isset($_REQUEST['action'])){
                if(strtolower($_REQUEST['action']) == "delete" || strtolower($_REQUEST['action']) == "massupdate"){
                    if(session_id() != '' && isset($_SESSION)) {
                        if(isset($_SESSION["VIN_CSRF_TOKEN"])){
                            if(!isset($_REQUEST['vin_csrf_token'])){
                                echo "CSRF BLOCKED.";
                                die;
                            }
                            else{
                                if($_REQUEST['vin_csrf_token'] == null || $_REQUEST['vin_csrf_token'] == ''){
                                    echo "CSRF BLOCKED.";
                                    die;
                                }
                                else if($_REQUEST['vin_csrf_token'] != $_SESSION["VIN_CSRF_TOKEN"]){
                                    echo "CSRF BLOCKED.";
                                    die;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function custom_after_entry_point(){
        //var_dump($_REQUEST);die;
        if(isset($_REQUEST)){
            //Dev Tool
            if( $GLOBALS['sugar_config']['development_tool_enable'] == 0){
                if(isset($_REQUEST['module'])){
                    if(strtolower($_REQUEST['module']) == "modulebuilder"
                    ||strtolower($_REQUEST['module']) == "studio"
                    ||strtolower($_REQUEST['action']) == "upgradewizard"
                    ||strtolower($_REQUEST['action']) == "upgradewizard_prepare"
                    ||strtolower($_REQUEST['action']) == "savetabs"
                    ||strtolower($_REQUEST['action']) == "configuretabs"
                    ||strtolower($_REQUEST['module']) == "configurator"){
                        echo "Development Tool has been disabled.";
                        die;
                    }
                }
            }

            //CSRF
            if(isset($_REQUEST['action'])){
                if(strtolower($_REQUEST['action']) == "importvcard" 
                || strtolower($_REQUEST['module']) == "import"
                ||(strtolower($_REQUEST['action']) == "aodadmin"
                    && strtolower($_REQUEST['module']) == "administration"
                    && isset($_REQUEST['save']) && trim(strtolower($_REQUEST['save'])) == "save")
                ||(strtolower($_REQUEST['action']) == "aopadmin"
                 && strtolower($_REQUEST['module']) == "administration"
                    && isset($_REQUEST['save']) && trim(strtolower($_REQUEST['save'])) == "save")
                ||(strtolower($_REQUEST['action']) == "businesshours"
                    && strtolower($_REQUEST['module']) == "administration"
                    && isset($_REQUEST['save']) && trim(strtolower($_REQUEST['save'])) == "save")
                ||(strtolower($_REQUEST['action']) == "aosadmin" 
                    && strtolower($_REQUEST['module']) == "administration"
                    && isset($_REQUEST['save']) && trim(strtolower($_REQUEST['save'])) == "save")){
                    echo "Action has been disabled.";
                    die;
                }
            }

            if(isset($_REQUEST['action']) && isset($_REQUEST['module'])){
                if((strtolower($_REQUEST['action']) == "passwordmanager" 
                    && strtolower($_REQUEST['module']) == "administration"
                    && isset($_REQUEST['save']) && strtolower($_REQUEST['save']) == "save")
                    ||(strtolower($_REQUEST['action']) == "saveconfig" 
                    && strtolower($_REQUEST['module']) == "configurator"
                    && isset($_REQUEST['save']) && trim(strtolower($_REQUEST['save'])) == "save")
                    ||(strtolower($_REQUEST['action']) == "saveconfig" 
                    && strtolower($_REQUEST['module']) == "securitygroups"
                    && isset($_REQUEST['button']) && trim(strtolower($_REQUEST['button'])) == "save")
                    ){
                        if(session_id() == '' || !isset($_SESSION) || $_SESSION == null) {
                            session_start();
                        }
                        if(session_id() != '' && isset($_SESSION)) {
                            if(isset($_SESSION["VIN_CSRF_TOKEN"])){
                                if(!isset($_REQUEST['vin_csrf_token'])){
                                    echo "CSRF BLOCKED.";
                                    die;
                                }
                                else{
                                    if($_REQUEST['vin_csrf_token'] == null || $_REQUEST['vin_csrf_token'] == ''){
                                        echo "CSRF BLOCKED.";
                                        die;
                                    }
                                    else if($_REQUEST['vin_csrf_token'] != $_SESSION["VIN_CSRF_TOKEN"]){
                                        echo "CSRF BLOCKED.";
                                        die;
                                    }
                                }
                            }
                        }
                }
                else if(strtolower($_REQUEST['action']) == "login" && strtolower($_REQUEST['module']) == "users"){
                    global $db;
                    $ipaddress = "";
                    if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
                        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    }
                    if($ipaddress == ""){
                        if(isset($_SERVER['REMOTE_ADDR'])){
                            $ipaddress = $_SERVER['REMOTE_ADDR'];
                        }
                    }
                    if($ipaddress != ""){
                        $query = " SELECT * FROM vin_login_failed 
                            WHERE ip = '".$ipaddress."' AND login_date + INTERVAL 15 MINUTE >= CURRENT_TIMESTAMP;";
                
                        $result = $db->query($query);
                        $dataSet = array();
                        $row = $db->fetchByAssoc($result);
                        while ($row != null){
                            $dataSet[] = $row;
                            $row = $db->fetchByAssoc($result);
                        }
                        if(count($dataSet) >= 5){
                            echo '
                                <html lang="en_us" class="yui3-js-enabled">
                                    <head>
                                        <script type="text/javascript" src="cache/themes/SuiteP/js/style.js"></script>
                                        <link rel="stylesheet" type="text/css" href="cache/themes/SuiteP/css/Dawn/style.css">
                                        <link rel="stylesheet" type="text/css" media="all" href="modules/Users/login.css">
                                    </head>
                                    <body>
                                        <div class="p_login">
                                            <div class="p_login_top">
                                                <a title="SuiteCRM" href="#">SuiteCRM</a>
                                            </div>
                                            <div class="p_login_middle">
                                                <div id="loginform">
                                                    <form class="form-signin" role="form" action="" method="post" name="DetailView" style="width: 400px" id="form" onsubmit="" autocomplete="off">
                                                        <div class="companylogo"><img src="themes/default/images/company_logo.png" alt="SuiteCRM" style="margin: 5px 0;"></div>
                                                        <span style="color:red;font-size: 15px;line-height: 30px;">You have been blocked for loging too many failed attempts.</span>
                                                        <br />
                                                        <span style="color:red;font-size: 15px;line-height: 30px;">Please wait a few minutes before trying again.</span>
                                                    </form> 
                                                </div>
                                            </div>
                                            <div class="p_login_bottom">
                                                <a id="admin_options">© Supercharged by SuiteCRM</a>
                                                <a id="powered_by">© Powered By SugarCRM</a>
                                            </div>
                                        </div>
                                    </body>
                                </html>
                            ';
                            die;
                        }
                    }
                }
                else if(strtolower($_REQUEST['action']) == "authenticate" && strtolower($_REQUEST['module']) == "users"){
                    if($_REQUEST['user_name'] !=null && strtolower($_REQUEST['user_name']) != ""){
                        global $db;
                        //Block Admin base on IP
                        $query = " SELECT is_admin FROM users WHERE LOWER(user_name) = '".strtolower(trim($_REQUEST['user_name']))."' AND deleted = 0 LIMIT 1;";
                        $query_result = $db->query($query);
                        $query_result = $db->fetchByAssoc($query_result);
                        $is_admin = $query_result["is_admin"];
                        $ipaddress = "";
                        if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
                            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                        }
                        if($ipaddress == ""){
                            if(isset($_SERVER['REMOTE_ADDR'])){
                                $ipaddress = $_SERVER['REMOTE_ADDR'];
                            }
                        }
                        if( is_array($GLOBALS['sugar_config']['admin_access_ip']) 
                            && count($GLOBALS['sugar_config']['admin_access_ip']) > 0
                            && $ipaddress != "" && $is_admin == '1'){
                                $ipaddress = explode(":",$ipaddress);
                                $ipaddress = $ipaddress[0];
                                if(!in_array($ipaddress,$GLOBALS['sugar_config']['admin_access_ip'])){
                                    echo "Login blocked by Admin.";
                                    echo "<p>";
                                    echo "Click <a href='index.php?module=Users&action=Login'>here</a> to login with other user.";
                                    die;
                                }
                        }
                    }
                }
            }
        }
    }

    function custom_login_failed(){
        if(isset($_REQUEST)){
            if(isset($_REQUEST['action']) && isset($_REQUEST['module'])){
                if(strtolower($_REQUEST['action']) == "authenticate" && strtolower($_REQUEST['module']) == "users"){
                    global $db;
                    $ipaddress = "";
                    if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
                        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    }
                    if($ipaddress == ""){
                        if(isset($_SERVER['REMOTE_ADDR'])){
                            $ipaddress = $_SERVER['REMOTE_ADDR'];
                        }
                    }
                    if($ipaddress != ""){
                        $query_insert = "INSERT INTO vin_login_failed (ip,login_date)
                            VALUES ('".$ipaddress."',NOW());";
                        $db->query($query_insert);
                    }
                }
            }
        }
    }

    function custom_after_retrieve(){
        if(isset($_REQUEST)){
            //Audit Permission
            if(isset($_REQUEST['action']) && isset($_REQUEST['module'])){
                if(strtolower($_REQUEST['action']) == "popup" 
                && strtolower($_REQUEST['module']) == "audit"
                && strtolower($_REQUEST['module_name']) != ""
                && strtolower($_REQUEST['record']) != ""){
                    if(!isset($_REQUEST['custom_check_audit']) || $_REQUEST['custom_check_audit'] != 1){
                        $_REQUEST['custom_check_audit'] = 1;
                        $beanLead = BeanFactory::getBean($_REQUEST['module_name'],$_REQUEST['record']);
                        if($beanLead != null && $beanLead != false && $beanLead->ACLAccess('DetailView')){
    
                        }
                        else{
                            echo "You dont have permission to access this site.";
                            die;
                        }
                    }
                }
            }
        }
    }
}