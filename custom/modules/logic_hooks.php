<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(1, 'AOD Index Changes', 'modules/AOD_Index/AOD_LogicHooks.php','AOD_LogicHooks', 'saveModuleChanges'); 
$hook_array['after_save'][] = Array(1, 'ElasticSearch Index Changes', 'lib/Search/ElasticSearch/ElasticSearchHooks.php','SuiteCRM\Search\ElasticSearch\ElasticSearchHooks', 'beanSaved'); 
$hook_array['after_save'][] = Array(30, 'popup_select', 'modules/SecurityGroups/AssignGroups.php','AssignGroups', 'popup_select'); 
$hook_array['after_delete'] = Array(); 
$hook_array['after_delete'][] = Array(1, 'AOD Index changes', 'modules/AOD_Index/AOD_LogicHooks.php','AOD_LogicHooks', 'saveModuleDelete'); 
$hook_array['after_delete'][] = Array(1, 'ElasticSearch Index Changes', 'lib/Search/ElasticSearch/ElasticSearchHooks.php','SuiteCRM\Search\ElasticSearch\ElasticSearchHooks', 'beanDeleted'); 
$hook_array['after_restore'] = Array(); 
$hook_array['after_restore'][] = Array(1, 'AOD Index changes', 'modules/AOD_Index/AOD_LogicHooks.php','AOD_LogicHooks', 'saveModuleRestore'); 
$hook_array['after_ui_footer'] = Array(); 
$hook_array['after_ui_footer'][] = Array(10, 'popup_onload', 'modules/SecurityGroups/AssignGroups.php','AssignGroups', 'popup_onload'); 
$hook_array['after_ui_frame'] = Array(); 
$hook_array['after_ui_frame'][] = Array(20, 'mass_assign', 'modules/SecurityGroups/AssignGroups.php','AssignGroups', 'mass_assign'); 
$hook_array['after_ui_frame'][] = Array(1, 'Load Social JS', 'include/social/hooks.php','hooks', 'load_js'); 
$hook_array['after_ui_frame'][] = Array(11, 'custom_after_ui_frame', 'custom/modules/global_hooks.php','global_hooks', 'custom_after_ui_frame'); 
$hook_array['after_login'][] = Array(11, 'custom_after_login', 'custom/modules/global_hooks.php','global_hooks', 'custom_after_login'); 
$hook_array['before_save'][] = Array(11, 'custom_before_save', 'custom/modules/global_hooks.php','global_hooks', 'custom_before_save'); 
$hook_array['before_delete'][] = Array(11, 'custom_before_delete', 'custom/modules/global_hooks.php','global_hooks', 'custom_before_delete'); 
$hook_array['after_entry_point'][] = Array(11, 'custom_after_entry_point', 'custom/modules/global_hooks.php','global_hooks', 'custom_after_entry_point'); 
$hook_array['login_failed'][] = Array(11, 'custom_login_failed', 'custom/modules/global_hooks.php','global_hooks', 'custom_login_failed'); 
$hook_array['after_retrieve'][] = Array(11, 'custom_after_retrieve', 'custom/modules/global_hooks.php','global_hooks', 'custom_after_retrieve'); 

?>