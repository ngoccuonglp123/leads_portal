<?php
$module_name = 'vin_lead_rating_score';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'vin_rating_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_RATING_C',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'vin_from_c',
            'label' => 'LBL_VIN_FROM_C',
          ),
          1 => 
          array (
            'name' => 'vin_to_c',
            'label' => 'LBL_VIN_TO_C',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'vin_task_hours_c',
            'label' => 'LBL_VIN_TASK_HOURS_C',
          ),
          1 => 'description',
        ),
      ),
    ),
  ),
);
;
?>
