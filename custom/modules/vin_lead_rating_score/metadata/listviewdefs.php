<?php
$module_name = 'vin_lead_rating_score';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'VIN_RATING_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_VIN_RATING_C',
    'width' => '10%',
  ),
  'VIN_FROM_C' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_VIN_FROM_C',
    'width' => '10%',
  ),
  'VIN_TO_C' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_VIN_TO_C',
    'width' => '10%',
  ),
  'VIN_TASK_HOURS_C' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_VIN_TASK_HOURS_C',
    'width' => '10%',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
