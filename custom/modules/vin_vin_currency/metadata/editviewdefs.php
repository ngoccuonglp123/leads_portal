<?php
$module_name = 'vin_vin_currency';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'sin_symbol_c',
            'label' => 'LBL_SIN_SYMBOL',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'vin_exchange_rate_c',
            'label' => 'LBL_VIN_EXCHANGE_RATE',
          ),
          1 => 'description',
        ),
      ),
    ),
  ),
);
;
?>
