<?php
// created: 2020-04-15 23:40:01
$mod_strings = array (
  'LNK_NEW_CASE' => 'Create Case',
  'LNK_CASE_LIST' => 'View Cases',
  'LNK_IMPORT_CASES' => 'Import Cases',
  'LBL_LIST_FORM_TITLE' => 'Case List',
  'LBL_SEARCH_FORM_TITLE' => 'Case Search',
  'LBL_LIST_MY_CASES' => 'My Open Cases',
  'LBL_MODULE_NAME' => 'Cases',
);