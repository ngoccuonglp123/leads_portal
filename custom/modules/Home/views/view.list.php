<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/global_function/fs_generate_cache.php');

class HomeViewList extends ViewList
{
    function display()
    {
        global $db, $app_list_strings;
        $fs_generate_cache = new fs_generate_cache;

        header("Location: index.php?module=vin_vin_leads&action=index&parentTab=Sales"); 
        exit();
        parent::display();
    }
}