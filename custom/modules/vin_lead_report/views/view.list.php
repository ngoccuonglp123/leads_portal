<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/global_function/fs_generate_cache.php');

class vin_lead_reportViewList extends ViewList
{
    function display()
    {
        global $db, $app_list_strings, $current_user, $mod_strings;
        $fs_generate_cache = new fs_generate_cache;

        echo '<link rel="stylesheet" type="text/css" href="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.css','').'">';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.js','').'"></script>';
        echo '<link href="'.$fs_generate_cache->get_cache_version('custom/include/css/vin_lead_report/vin_lead_report.css','').'" rel="stylesheet">';
        echo '<script src="'.$fs_generate_cache->get_cache_version('custom/include/js/notify/notify.min.js','').'"></script>';
        echo '<style>.notifyjs-corner{top:60px !important; font-size: 16px;}</style>';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/vin_lead_report/custom_listview.js','').'"></script>';
        echo '<script src="'.$fs_generate_cache->get_cache_version('custom/include/library/angular-chart/angular.min.js','').'"></script>';
        echo '<script src="'.$fs_generate_cache->get_cache_version('custom/include/library/angular-chart/Chart.bundle.js','').'"></script>';
        echo '<script src="'.$fs_generate_cache->get_cache_version('custom/include/library/angular-chart/angular-chart.js','').'"></script>';
        echo '<script src="'.$fs_generate_cache->get_cache_version('custom/include/library/angular-chart/angular-chart.min.js','').'"></script>';
        // echo '<script src="'.$fs_generate_cache->get_cache_version('custom/include/library/angular-chart/angular-chart.min.js.map','').'"></script>';
     
        $status_list = "{";
        foreach ($app_list_strings['vin_lead_status_c_list'] as $key => $value){
            $status_list .= '"'.$key.'": "'.$value.'",';
        }
        $status_list .= "}";

        echo "<script> var status_list = ".$status_list.";</script>";

        $bm = 0;
        $sa = 0;
        $role = -1;
        $list_roles = $this->getRoleCurrentUser();
        foreach($list_roles as $key => $value){
            if($value == 'BUSINESS_MANAGER'){
                $bm = 1;
                $role = 1;
            }
            else if($value == 'SALE_ADMIN'){
                $sa = 1;
                $role = 2;
            }
            else if($value == 'SALE_AGENT'){
                $sg = 1;
                $role = 3;
            }
        }

        echo '
        <script>
            function openTab(evt, tabName) {
                var i, x, tablinks;
                x = document.getElementsByClassName("city");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablink");
                for (i = 0; i < x.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" w3-indigo", "");
                }
                document.getElementById(tabName).style.display = "block";
                evt.currentTarget.className += " w3-indigo";
            }
        </script>
        <div id="custom_list_view_report">
            <div id="custom_headers" style="display: none;">

            </div>
            <div id="custom_contents">
     
                <div class="w3-bar w3-light-blue">
                    <button class="w3-bar-item w3-button tablink w3-indigo" onclick="openTab(event,'."'"."tab_lead_funel"."'".')">Lead Funel</button>
                    <button class="w3-bar-item w3-button tablink" onclick="openTab(event,'."'"."tab_convert"."'".')">Convert</button>
                    <button class="w3-bar-item w3-button tablink" onclick="openTab(event,'."'"."tab_performance"."'".')">Performance</button>
                </div>
                
                <div id="tab_lead_funel" class="w3-container city">
                    <table style="width: 100%; margin-top: 10px;">
                        <tr style="display:none;">
                            <td style="width: 20%; font-weight: bold;">From: </td>
                            <td style="width: 30%;"> <input type="text" id="lead_funel_from"/></td>
                            <td style="width: 20%; font-weight: bold;">To: </td>
                            <td style="width: 30%;"><input type="text" id="lead_funel_to" /></td>
                        </tr>
                        <tr style="display:none;">
                            <td style="font-weight: bold;">Department: </td>
                            <td colspan="3"><input type="text" /></td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Employee: </td>
                            <td colspan="3"><input type="text" id="lead_funel_employee" style="width: 100%;height: 60px;"/></td>
                        </tr>
                    </table>
                    <input type="button" value="View Report" onclick="viewReportLeadFunel()" />
                    <br />
                    <div id="chart1" style="width: 90%;">
                        <div ng-controller="lead_funel">
                            <canvas class="chart chart-bar" chart-data="data" chart-labels="labels"  chart-options="options"
                            chart-series="series" chart-click="onClick"></canvas> 
                        </div>
                    </div>
                </div>
                
                <div id="tab_convert" class="w3-container w3-border city" style="display:none">
                    <table style="width: 100%; margin-top: 10px;">
                        <tr style="">
                            <td style="width: 20%; font-weight: bold;">From: </td>
                            <td style="width: 30%;"> <input type="text" id="lead_convert_from"/></td>
                            <td style="width: 20%; font-weight: bold;">To: </td>
                            <td style="width: 30%;"><input type="text" id="lead_convert_to" /></td>
                        </tr>
                        <tr style="display:none;">
                            <td style="font-weight: bold;">Department: </td>
                            <td colspan="3"><input type="text" /></td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 10%;">Employee: </td>
                            <td colspan="3"><input type="text" id="lead_convert_employee" style="width: 100%;height: 60px;"/></td>
                        </tr>
                    </table>
                    <input type="button" value="View Report" onclick="viewReportLeadConvert()" />
                    <br />
                    <table style="width: 100%; margin-top: 10px;">
                        <tr>
                            <td style="width: 50%;">
                                <div id="chart2_1" style="width: 90%;">
                                    <div ng-controller="lead_convert_1">
                                        <canvas class="chart chart-pie" chart-data="data" chart-labels="labels"  chart-options="options" chart-colors="colors"
                                        chart-series="series" chart-click="onClick"></canvas> 
                                    </div>
                                </div>
                            </td>
                            <td style="width: 50%;">
                                <div id="chart2_2" style="width: 90%;">
                                    <div ng-controller="lead_convert_2">
                                        <canvas class="chart chart-pie" chart-data="data" chart-labels="labels"  chart-options="options" chart-colors="colors"
                                        chart-series="series" chart-click="onClick"></canvas> 
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                
                <div id="tab_performance" class="w3-container w3-border city" style="display:none">
                    <table style="width: 100%; margin-top: 10px;">
                        <tr>
                            <td style="width: 20%; font-weight: bold;">From: </td>
                            <td style="width: 30%;"> <input type="text" id="lead_performance_from"/></td>
                            <td style="width: 20%; font-weight: bold;">To: </td>
                            <td style="width: 30%;"><input type="text" id="lead_performance_to" /></td>
                        </tr>
                        <tr style="display:none;">
                            <td style="font-weight: bold;">Department: </td>
                            <td colspan="3"><input type="text" /></td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Employee: </td>
                            <td colspan="3"><input type="text" id="lead_performance_employee" style="width: 100%;height: 60px;"/></td>
                        </tr>
                    </table>
                    <input type="button" value="View Report" onclick="viewReportLeadPerformance()" />
                    <br />
                    <div id="chart3" style="width: 90%;">
                        <div ng-controller="lead_performance">
                            <canvas class="chart chart-bar" chart-data="data" chart-labels="labels"  chart-options="options"
                            chart-series="series" chart-click="onClick"></canvas> 
                        </div>
                    </div>
                </div>

            </div>
        </div>';


        parent::display();
    }

    private function getRoleCurrentUser(){
        global $db, $current_user;

        $list_roles = array();
        $query = "SELECT acl.name FROM acl_roles acl 
            JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
            JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
            WHERE acl.deleted = 0 AND sgu.user_id = '".$current_user->id."';";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($list_roles,$value['name']);
        }
        return $list_roles;
    }
}