<?php
$mod_strings = array (
  'LBL_DEPARTMENT' => 'Deparment',
  'LBL_BUSINESS_MANAGEMENT' => 'Business Management',
  'LBL_SALES_ADMIN' => 'Sales Admin',
  'LBL_SALES_AGENT' => 'Sales Agent',
  'LBL_FULLNAME' => 'Fullname',
  'LBL_EMAIL' => 'Email',
  'LBL_NO_RECORD' => 'No records found',
  'LBL_CREATE_USER' => 'Create User',
  'LBL_SAVE' => 'Save',
  'LBL_SAVE_CONTINUE' => 'Save And Continue',
  'LBL_CANCEL' => 'Cancel',
  'LBL_STATUS' => 'Status',
  'LBL_EDIT_USER' => 'Edit User',
);