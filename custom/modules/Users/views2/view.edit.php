<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/global_function/fs_generate_cache.php');

class UsersViewEdit extends ViewEdit {

    function display()
    {
        global $db, $app_list_strings, $current_user;
        $fs_generate_cache = new fs_generate_cache;

        if($current_user->is_admin != 1){
            echo "<span style='color:red;'>You dont have permission to access this site!</span>";
            die;
        }

        parent::display();
    }
}