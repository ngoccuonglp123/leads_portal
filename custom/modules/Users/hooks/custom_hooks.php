<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class users_custom_hooks {

    function custom_before_save($bean, $event, $arguments){
        global $db, $app_list_strings, $current_user;

        if($current_user->is_admin != 1){
            echo "<span style='color:red;'>You dont have permission to access this site!</span>";
            die;
        }
    }
}