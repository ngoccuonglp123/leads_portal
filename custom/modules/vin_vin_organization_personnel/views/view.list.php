<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/global_function/fs_generate_cache.php');

class vin_vin_organization_personnelViewList extends ViewList
{
    function display()
    {
        global $db, $app_list_strings, $current_user, $mod_strings;
        $fs_generate_cache = new fs_generate_cache;

        echo '<link rel="stylesheet" type="text/css" href="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.css','').'">';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.js','').'"></script>';
        echo '<link href="'.$fs_generate_cache->get_cache_version('custom/include/css/organization_personnel/organization_personnel.css','').'" rel="stylesheet">';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/organization_personnel/custom_listview.js','').'"></script>';
        echo '<script src="'.$fs_generate_cache->get_cache_version('custom/include/js/notify/notify.min.js','').'"></script>';
        echo '<style>.notifyjs-corner{top:60px !important; font-size: 16px;}</style>';

        $bm = 0;
        $sa = 0;
        $role = -1;
        $list_roles = $this->getRoleCurrentUser();
        foreach($list_roles as $key => $value){
            if($value == 'BUSINESS_MANAGER'){
                $bm = 1;
                $role = 1;
            }
            else if($value == 'SALE_ADMIN'){
                $sa = 1;
                $role = 2;
            }
            else if($value == 'SALE_AGENT'){
                $sg = 1;
                $role = 3;
            }
        }

        if($current_user->is_admin != 1 && $bm != 1 && $sa != 1){
            echo "<span style='color:red;'>You dont have permission to access this site!</span>";
            die;
        }

        echo '<input type="hidden" id="custom_role" value="'.$role.'" />';

        $list_admins = array();
        $list_sales = array();
        $edit_sa_display ='';
        if($bm == 1){
            $list_sa = $this->getListSaleAdminAllocate();
            foreach($list_sa as $key => $value){
                $list_sales_temp = $this->getListSaleAgentAllocate($value['id']);
                array_push($list_admins, $value);
                if (array_key_exists($value['id'],$list_sales)){
                    $list_sales[$value['id']] = $list_sales_temp;
                }
                else{
                    $list_sales[$value['id']] = array();
                    $list_sales[$value['id']] = $list_sales_temp;
                }
            }
        }
        else if($sa == 1){
            $list_sales_temp = $this->getListSaleAgentAllocate($current_user->id);
            $value = array();
            $value['id'] = $current_user->id;
            $value['user_name'] = $current_user->user_name;
            $value['status'] = $current_user->status;
            $value['fullname'] = $current_user->first_name.' '.$current_user->last_name;
            $value['email'] = $current_user->email1;
            $edit_sa_display ='display:none';
            array_push($list_admins, $value);
            if (array_key_exists($value['id'],$list_sales)){
                $list_sales[$value['id']] = $list_sales_temp;
            }
            else{
                $list_sales[$value['id']] = array();
                $list_sales[$value['id']] = $list_sales_temp;
            }
        }

        $data = '';
        $no_record = '                        
                        <tr>
                            <td colspan="6" style="font-style: italic;text-align: center;"> '.$mod_strings['LBL_NO_RECORD'].'</td>
                        </tr>';

        foreach($list_admins as $key => $value){
            $agent_tr = '';
            $adm_tr = '';
            $rows_span = 1;
            $no_record ='';
            foreach($list_sales[$value['id']] as $k => $v){
                $action_btn ='
                <a href="javascript:void(0)" style="color: #351dd1;" class="glyphicon glyphicon glyphicon-pencil" onclick="custom_edit_user('."'".$v['id']."'".')" title="Edit User"></a>
                <a style="display:none" href="javascript:void(0)" style="color: #f20b0b;" class="glyphicon glyphicon-trash" onclick="custom_delete_user('."'".$v['id']."'".', '."'".$v['user_name']."'".')" title="Delete User"></a>
                ';

                $agent_tr .= '<tr>';
                $agent_tr .= '<td>'.$v['user_name'].'</td>';
                $agent_tr .= '<td>'.$v['status'].'</td>';
                $agent_tr .= '<td>'.$v['fullname'].'</td>';
                $agent_tr .= '<td>'.$v['email'].'</td>';
                $agent_tr .= '<td>&nbsp'.$action_btn.'</td>';
                $agent_tr .= '</tr>';
                $rows_span++;
            }
            $action_btn ='
            <a style="'.$edit_sa_display.'" href="javascript:void(0)" style="color: #351dd1;" class="glyphicon glyphicon glyphicon-pencil" onclick="custom_edit_user('."'".$value['id']."'".')" title="Edit User"></a>
            <a style="display:none" href="javascript:void(0)" style="color: #f20b0b;" class="glyphicon glyphicon-trash" onclick="custom_delete_user('."'".$value['id']."'".', '."'".$value['user_name']."'".')" title="Delete User"></a>
            ';

            $adm_tr .= '<tr>';
            $adm_tr .= '<td rowspan="'.$rows_span.'" style="vertical-align: text-top;">'.$value['user_name'].'</td>';
            $adm_tr .= '<td style="background-color: #c4f4ff;"></td>';
            $adm_tr .= '<td style="background-color: #c4f4ff;">'.$value['status'].'</td>';
            $adm_tr .= '<td style="background-color: #c4f4ff;">'.$value['fullname'].'</td>';
            $adm_tr .= '<td style="background-color: #c4f4ff;">'.$value['email'].'</td>';
            $adm_tr .= '<td style="background-color: #c4f4ff;">&nbsp'.$action_btn.'</td>';
            $adm_tr .= '</tr>';
            $data .= $adm_tr.$agent_tr;
        }

        $deparment = $this->getDepartmentNameOfUser($current_user->id, $list_roles);
        $bm_name = "";
        $sa_name = "";
        $sa_display = "";
        if($bm == 1){
            $bm_name = $current_user->user_name;
            $sa_display = "none";
        }
        if($sa == 1){
            $bm_name = $this->getBusinessManagementNameByUser($current_user->id);
            $sa_name = $current_user->user_name;
        }

        echo '
            <div id="custom_list_view_organization_personnel">
                <div id="custom_headers" >
                    <div style="font-weight: bold;">'.$mod_strings['LBL_DEPARTMENT'].': </div>
                    <div>'.$deparment.'</div>
                    <div style="font-weight: bold;">'.$mod_strings['LBL_BUSINESS_MANAGEMENT'].': </div>
                    <div>'.$bm_name.'</div>
                    <div style="font-weight: bold; display: '.$sa_display.'">'.$mod_strings['LBL_SALES_ADMIN'].': </div>
                    <div style="display: '.$sa_display.'">'.$sa_name.'</div>
                </div>
                <div id="custom_contents">
                    <table>
                        <tr>
                            <th valign="middle" style="width: 20%;">'.$mod_strings['LBL_SALES_ADMIN'].'</th>
                            <th valign="middle" style="width: 20%;">'.$mod_strings['LBL_SALES_AGENT'].'</th>
                            <th valign="middle" style="width: 10%;">'.$mod_strings['LBL_STATUS'].'</th>
                            <th valign="middle" style="width: 20%;">'.$mod_strings['LBL_FULLNAME'].'</th>
                            <th valign="middle" style="width: 20%;">'.$mod_strings['LBL_EMAIL'].'</th>
                            <th style="width: 4%;"><a href="javascript:void(0)" style="color: #FFF;" class="glyphicon glyphicon-plus" onclick="custom_create_user()" title="Create User"></a></th>
                        </tr>
                        '.$data.'
                        '.$no_record.'
                    </table>
                </div>
            </div>';

        echo $this->create_user_modal();
        echo $this->drawLoader();

        $check_permission_create_user = 0;
        if($bm == 1 || $sa == 1){
            $check_permission_create_user = 1;
        }
        if($check_permission_create_user == 1){
            echo '<script>
                var btn_create_user_button = '."'".'<button type="button" id="btn_create_user_button" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="custom_create_user();">Create User</button>'."'".';
            </script>';
        }
        else{
            echo '<script>
                var btn_create_user_button = "";
            </script>';
        }

        parent::display();
    }

    private function getRoleCurrentUser(){
        global $db, $current_user;

        $list_roles = array();
        $query = "SELECT acl.name FROM acl_roles acl 
            JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
            JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
            WHERE acl.deleted = 0 AND sgu.user_id = '".$current_user->id."';";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($list_roles,$value['name']);
        }
        return $list_roles;
    }

    private function getDepartmentNameOfUser($user_id, $list_roles){
        global $db, $current_user;

        $bm = 0;
        $sa = 0;
        $sg = 0;
        $department_id = '';
        $query = '';
        foreach($list_roles as $key => $value){
            if($value == 'BUSINESS_MANAGER'){
                $bm = 1;
            }
            else if($value == 'SALE_ADMIN'){
                $sa = 1;
            }
            else if($value == 'SALE_AGENT'){
                $sg = 1;
            }
        }
        if($bm == 1){
            $query = "SELECT d.name FROM vin_vin_department d
                JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                WHERE d.deleted = 0 
                AND u1.id='".$user_id."' LIMIT 1;";
        }
        else if($sa == 1){
            $query = "SELECT d.name FROM vin_vin_department d
                JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                JOIN users_cstm u3 ON u1.id=u3.user_id1_c 
                WHERE d.deleted = 0
                AND u3.id_c='".$user_id."' LIMIT 1;";
        }
        else if($sg == 1){
            $query = "SELECT d.name FROM vin_vin_department d
                JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                JOIN users_cstm u3 ON u1.id=u3.user_id1_c 
                JOIN users_cstm u4 ON u3.id_c=u4.user_id_c 
                WHERE d.deleted = 0
                AND u4.id_c='".$user_id."' LIMIT 1;";
        }
        if($query != ''){
            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $department_id = $query_result["name"];
        }
        return $department_id;
    }

    private function getBusinessManagementNameByUser($id){
        global $db;

        $user_id = '';
        $query = "SELECT u2.user_name FROM users_cstm uc
                JOIN users u ON uc.id_c = u.id 
                JOIN users u2 ON uc.user_id1_c = u2.id 
                WHERE u.id = '".$id."' AND u.status <> '' AND u.deleted = 0 LIMIT 1;";

        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $user_id = $query_result["user_name"];
        return $user_id;
    }

    private function getListSaleAdminAllocate(){
        global $db, $current_user;
    
        $list_sales = array();
        $query = "SELECT DISTINCT u.id, u.user_name, 
                CONCAT(IFNULL(u.first_name,''),' ',IFNULL(u.last_name,'')) AS fullname,
                e.email_address AS email, u.status AS status
            FROM users_cstm uc
            JOIN users u ON uc.id_c = u.id 
            LEFT JOIN vin_vin_leads_cstm lc ON lc.user_id1_c = u.id
            LEFT JOIN vin_vin_leads l ON l.id = lc.id_c AND l.deleted = 0 
            LEFT JOIN email_addr_bean_rel eb ON eb.bean_id = u.id AND eb.bean_module = 'Users' AND eb.primary_address = 1 AND eb.deleted = 0
            LEFT JOIN email_addresses e ON e.id = eb.email_address_id AND e.deleted = 0
            WHERE uc.user_id1_c = '".$current_user->id."' AND u.status <> '' AND u.deleted = 0;";
    
        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            $temp = array();
            $temp['id'] = $value['id'];
            $temp['user_name'] = $value['user_name'];
            $temp['status'] = $value['status'];
            $temp['fullname'] = $value['fullname'];
            $temp['email'] = $value['email'];
            array_push($list_sales,$temp);
        }
        return $list_sales;
    }

    private function getListSaleAgentAllocate($id){
        global $db, $current_user;
    
        $list_sales = array();
        $query = "SELECT DISTINCT u.id, u.user_name, 
                CONCAT(IFNULL(u.first_name,''),' ',IFNULL(u.last_name,'')) AS fullname,
                e.email_address AS email, u.status AS status
            FROM users_cstm uc
            JOIN users u ON uc.id_c = u.id 
            LEFT JOIN email_addr_bean_rel eb ON eb.bean_id = u.id AND eb.bean_module = 'Users' AND eb.primary_address = 1 AND eb.deleted = 0
            LEFT JOIN email_addresses e ON e.id = eb.email_address_id AND e.deleted = 0
            WHERE uc.user_id_c = '".$id."' AND u.status <> '' AND u.deleted = 0;";
    
        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            $temp = array();
            $temp['id'] = $value['id'];
            $temp['user_name'] = $value['user_name'];
            $temp['status'] = $value['status'];
            $temp['fullname'] = $value['fullname'];
            $temp['email'] = $value['email'];
            array_push($list_sales,$temp);
        }
        return $list_sales;
    }

    private function drawLoader()
    {
        return '
            <div class="modal fade bd-example-modal-lg" id="loaderModal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="z-index: 100000000 !important;">
                <div class="modal-dialog modal-sm" style="padding-top: 17%;">
                    <div class="" style="width: 0;">
                        <div class="spinner-border text-primary"></div>
                    </div>
                </div>
            </div>';
    }

    private function create_user_modal()
    {
        global $db, $app_list_strings, $current_user, $mod_strings;

        return '
            <style>
            </style>
            <div class="modal fade" id="create_user_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="fs_quote_approval_checking" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 50px;">
                    <div class="modal-content" style="max-width: 100%;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; " id="cs-modal-title">'.$mod_strings['LBL_CREATE_USER'].'</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <span id="create_user_msg" style="color:red;"></span>
                            <form action="" style="border:1px solid #ccc">
                                <div class="create_user_container">
                                    <input class="create_user_text" style="border: none;" id="create_user_id" type="hidden" >
                                    <label id="create_user_type_lable" for="type" style="width: 45px"><b>Type</b></label>
                                    <select id="create_user_type" onchange="onChangeType()"> 
                                        <option value="1" >Sales Admin</option>
                                        <option value="2" >Sale Agent</option>
                                    </select>
                                    <p style="margin-top: 10px;"></p>

                                    <label for="username" style=""  id="create_user_sales_admin_lable"><b>Sales Admin</b></label>
                                    <input class="create_user_text" style="border: none;" id="create_user_sales_admin" type="text" >

                                    <label for="username"><b>Username</b></label>
                                    <input class="create_user_text" style="border: none;" id="create_user_name" type="text" placeholder="Enter Username" name="create_user_name" required>

                                    <label for="first_name"><b>First Name</b></label>
                                    <input class="create_user_text" style="border: none;" id="create_user_first_name" type="text" placeholder="Enter First Name" name="create_user_first_name" required>

                                    <label for="last_name"><b>Last Name</b></label>
                                    <input class="create_user_text" style="border: none;" id="create_user_last_name" type="text" placeholder="Enter Last Name" name="create_user_last_name" required>

                                    <label for="email"><b>Email</b></label>
                                    <input id="create_user_email" style="border: none;" type="text" placeholder="Enter Email" name="email" required>

                                    <label for="mobile"><b>Mobile</b></label>
                                    <input class="create_user_text" id="create_user_mobile" style="border: none;" type="text" placeholder="Enter Mobile Phone" name="create_user_mobile" >

                                    <label for="employee_id"><b>Employee ID</b></label>
                                    <input class="create_user_text" id="create_user_employee_id" style="border: none;" type="text" placeholder="Enter Employee ID" name="create_user_employee_id" >

                                    <label id="create_user_status_lable" for="type" style="width: 45px"><b>Status</b></label>
                                    <select id="create_user_status" onchange="onChangeType()"> 
                                        <option value="Active" >Active</option>
                                        <option value="Inactive" >Inactive</option>
                                    </select>
                                    <p style="margin-top: 10px;"></p>

                                    <label for="psw"><b>Password</b></label>
                                    <input id="create_user_password" style="border: none;" type="password" placeholder="Enter Password" name="psw" required>

                                    <label for="psw-repeat"><b>Repeat Password</b></label>
                                    <input id="create_user_repassword" style="border: none;" type="password" placeholder="Repeat Password" name="psw-repeat" required>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="background-color: #3d9dd5;" class="btn btn-primary" onclick="create_user_submit(1);">'.$mod_strings['LBL_SAVE'].'</button>
                            <button type="button" style="background-color: #3d9dd5;" class="btn btn-primary" onclick="create_user_submit(0);">'.$mod_strings['LBL_SAVE_CONTINUE'].'</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">'.$mod_strings['LBL_CANCEL'].'</button>
                        </div>
                    </div>
                </div>
            </div>';
    }

}