<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2020-03-29 12:06:20
$dictionary["Document"]["fields"]["vin_vin_leads_documents_1"] = array (
  'name' => 'vin_vin_leads_documents_1',
  'type' => 'link',
  'relationship' => 'vin_vin_leads_documents_1',
  'source' => 'non-db',
  'module' => 'vin_vin_leads',
  'bean_name' => 'vin_vin_leads',
  'vname' => 'LBL_VIN_VIN_LEADS_DOCUMENTS_1_FROM_VIN_VIN_LEADS_TITLE',
  'id_name' => 'vin_vin_leads_documents_1vin_vin_leads_ida',
);
$dictionary["Document"]["fields"]["vin_vin_leads_documents_1_name"] = array (
  'name' => 'vin_vin_leads_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_VIN_VIN_LEADS_DOCUMENTS_1_FROM_VIN_VIN_LEADS_TITLE',
  'save' => true,
  'id_name' => 'vin_vin_leads_documents_1vin_vin_leads_ida',
  'link' => 'vin_vin_leads_documents_1',
  'table' => 'vin_vin_leads',
  'module' => 'vin_vin_leads',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["vin_vin_leads_documents_1vin_vin_leads_ida"] = array (
  'name' => 'vin_vin_leads_documents_1vin_vin_leads_ida',
  'type' => 'link',
  'relationship' => 'vin_vin_leads_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_VIN_VIN_LEADS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);

?>