<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class vin_leads_custom_hooks {

    function custom_before_save($bean, $event, $arguments){
        global $db, $current_user, $app_list_strings;

        if($bean->name == '' || $bean->name == null){
            $bean->name = trim($app_list_strings['vin_salutation_c_list'][$bean->vin_salutation_c].' '.$bean->vin_first_name_c.' '.$bean->vin_middle_name_c.' '.$bean->vin_last_name_c);
        }
        if($bean->vin_full_name_c != '' && $bean->vin_full_name_c != null){
            $bean->name = trim($app_list_strings['vin_salutation_c_list'][$bean->vin_salutation_c].' '.$bean->vin_full_name_c);
        }
        else{
            $bean->vin_full_name_c = $bean->name;
        }
        $bean->email1 = $bean->vin_email_c;

        //Currency
        if($bean->vin_vin_currency_id_c == null || $bean->vin_vin_currency_id_c == ''){
            $currency = $this->getDefaultCurrency();
            if(is_array($currency)){
                if($currency['id'] != null &&  $currency['id'] != ''){
                    $bean->vin_vin_currency_id_c = $currency['id'];
                    if($currency['vin_exchange_rate_c'] != null &&  $currency['vin_exchange_rate_c'] != ''
                    && $currency['vin_exchange_rate_c'] != 0 &&  $currency['vin_exchange_rate_c'] != '0'){
                        $bean->vin_budget_usd_c = $bean->vin_budget_c/(float)$currency['vin_exchange_rate_c'];
                    }
                }
            }
        }

        //Scoring
        if($bean->vin_auto_scoring_c == 1 && $bean->ownership != "import" && $bean->ownership != "import_allocate" && $bean->ownership != "salesforce"){
            $score = $this->calculateScore($bean);
            $bean->vin_lead_scoring_c = $score['score'];
            $bean->vin_rating_c = $score['rating'];
        }

        if(empty($bean->fetched_row)){
            if($bean->ownership != "salesforce"){
                $bean->user_id3_c = $current_user->id;
                $bean->vin_lead_id_salesforce_c = '';
            }
            $bean->vin_vin_department_id1_c = $this->getDepartmentOfUser($bean->user_id3_c);
            $bm = 0;
            $sa = 0;
            $sg = 0;
            if($bean->vin_lead_status_c == null || $bean->vin_lead_status_c == ''){
                $bean->vin_lead_status_c = 'new';
            }

            if($bean->user_id4_c != '' && $bean->user_id4_c != null){
                $bean->vin_lead_status_c = 'assigned';
                $bean->vin_vin_department_id_c = $this->getDepartmentOfUser($bean->user_id4_c);
            }
            if($bean->ownership != "import" && $bean->ownership != "import_allocate" && $bean->ownership != "salesforce"){
                $list_roles = $this->getRoleCurrentUser();
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }
                if($bm == 1){
                    $bean->user_id_c = $current_user->id;
                }
                if($sa == 1){
                    $bean->user_id1_c = $current_user->id;
                    $bm_id = $this->getBusinessManagementCurrentUser($current_user->id);
                    if($bm_id != '' && $bm_id != null){
                        $bean->user_id_c = $bm_id;
                    }
                }
                if($sg == 1){
                    $bean->user_id2_c = $current_user->id;
                    $sa_id = $this->getSaleAdminCurrentUser();
                    if($sa_id != '' && $sa_id != null){
                        $bean->user_id1_c = $sa_id;
                        $bm_id = $this->getBusinessManagementCurrentUser($sa_id);
                        if($bm_id != '' && $bm_id != null){
                            $bean->user_id_c = $bm_id;
                        }
                    }
                }

                //Auto Allocate
                $rating = $bean->vin_rating_c;
                if($bm == 1 && $bean->user_id1_c == '' && $bean->user_id1_c == null){
                    $sa_id = $this->getSaleAdminAllocate($rating);
                    if($sa_id != '' && $sa_id != null){
                        $bean->user_id1_c = $sa_id;
                        // $sg_id = $this->getSaleAgentAllocate($sa_id, $rating);
                        // if($sg_id != '' && $sg_id != null){
                        //     $bean->user_id2_c = $sg_id;
                        // }
                    }
                }
                if($sa == 1 && $bean->user_id2_c == '' && $bean->user_id2_c == null){
                    // $sg_id = $this->getSaleAgentAllocate($bean->user_id1_c, $rating);
                    // if($sg_id != '' && $sg_id != null){
                    //     $bean->user_id2_c = $sg_id;
                    // }
                }
            }
            else if($bean->ownership == "import_allocate"){
                if($bean->user_id_c != null && $bean->user_id_c != ''
                && ($bean->user_id1_c == null || $bean->user_id1_c == '')
                && ($bean->user_id2_c == null || $bean->user_id2_c == '')){
                    //Phân bổ tự động
                    $matrix_id = $this->getMatrixAllocate($bean);
                    $allocate = $this->getSalesAdminByAllocate($bean->user_id_c,$matrix_id);
                    if($allocate['id'] != '' && $allocate['id'] != null){
                        $bean->user_id1_c = $allocate['id'];
                        $this->markSalesAdminAllocated($allocate['flow_id'], $allocate['index']);
                    }
                    //End
                }
            }
        }
        else{
            //Department
            if($bean->fetched_row['user_id4_c'] != $bean->user_id4_c){
                $bean->vin_vin_department_id_c = $this->getDepartmentOfUser($bean->user_id4_c);
                $sales_incharge_id = $bean->user_id4_c;
                $list_roles = $this->getRoleByUser($sales_incharge_id);
                $bm = 0;
                $sa = 0;
                $sg = 0;
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }
                if($bm == 1){
                    $bean->user_id_c = $sales_incharge_id;
                    $bean->user_id1_c = '';
                    $bean->user_id2_c = '';
                    //Phân bổ tự động
                    $matrix_id = $this->getMatrixAllocate($bean);
                    $allocate = $this->getSalesAdminByAllocate($bean->user_id_c,$matrix_id);
                    if($allocate['id'] != '' && $allocate['id'] != null){
                        $bean->user_id1_c = $allocate['id'];
                        $this->markSalesAdminAllocated($allocate['flow_id'], $allocate['index']);
                    }
                    //End
                }
                else if($sa == 1){
                    $bean->user_id1_c = $sales_incharge_id;
                    $bean->user_id2_c = '';
                }
                else if($sg == 1){
                    $bean->user_id2_c = $sales_incharge_id;
                }
            }
            if($bean->fetched_row['user_id3_c'] != $bean->user_id3_c){
                $bean->vin_vin_department_id1_c = $this->getDepartmentOfUser($bean->user_id3_c);
                $bean->user_id4_c = '';
                $bean->vin_vin_department_id_c = '';
                $ownerId = $bean->user_id3_c;

                $list_roles = $this->getRoleByUser($ownerId);
                $bm = 0;
                $sa = 0;
                $sg = 0;
                foreach($list_roles as $key => $value){
                    if($value == 'BUSINESS_MANAGER'){
                        $bm = 1;
                    }
                    else if($value == 'SALE_ADMIN'){
                        $sa = 1;
                    }
                    else if($value == 'SALE_AGENT'){
                        $sg = 1;
                    }
                }
                if($bm == 1){
                    $bean->user_id_c = $ownerId;
                    $bean->user_id1_c = '';
                    $bean->user_id2_c = '';
                }
                else if($sa == 1){
                    $bean->user_id1_c = $ownerId;
                    $bm_id = $this->getBusinessManagementCurrentUser($current_user->id);
                    if($bm_id != '' && $bm_id != null){
                        $bean->user_id_c = $bm_id;
                    }
                    $bean->user_id2_c = '';
                }
                else if($sg == 1){
                    $bean->user_id2_c = $ownerId;
                    $sa_id = $this->getSaleAdminCurrentUser();
                    if($sa_id != '' && $sa_id != null){
                        $bean->user_id1_c = $sa_id;
                        $bm_id = $this->getBusinessManagementCurrentUser($sa_id);
                        if($bm_id != '' && $bm_id != null){
                            $bean->user_id_c = $bm_id;
                        }
                    }
                }
            }
        }

        //Notice
        if(empty($bean->fetched_row)){
            if($bean->user_id4_c != '' && $bean->user_id4_c != null){
                $bean->vin_assign_date_c = date("Y-m-d H:i:s");
                $bean->vin_expiry_warning_c = 0;

                $query = "SELECT sc.vin_task_hours_c FROM vin_lead_rating_score s
                    JOIN vin_lead_rating_score_cstm sc ON s.id = sc.id_c
                    WHERE s.deleted = 0 
                    AND sc.vin_rating_c = '".$bean->vin_rating_c."' LIMIT 1;";
                $query_result = $db->query($query);
                $query_result = $db->fetchByAssoc($query_result);
                $vin_task_hours_c = $query_result["vin_task_hours_c"];
                $add_hours = 0;
                if($vin_task_hours_c != '' && $vin_task_hours_c != null){
                    $add_hours = round((float)$vin_task_hours_c,0);
                }
                $bean->vin_lead_expiry_date_c = date("Y-m-d H:i:s", strtotime('+'.$add_hours.' hours'));

                $this->noticeAssignerGotLead($bean);
            }
        }
        else{
            if($bean->fetched_row['user_id4_c'] != $bean->user_id4_c){
                $bean->vin_assign_date_c = date("Y-m-d H:i:s");
                $bean->vin_expiry_warning_c = 0;

                $query = "SELECT sc.vin_task_hours_c FROM vin_lead_rating_score s
                    JOIN vin_lead_rating_score_cstm sc ON s.id = sc.id_c
                    WHERE s.deleted = 0 
                    AND sc.vin_rating_c = '".$bean->vin_rating_c."' LIMIT 1;";
                $query_result = $db->query($query);
                $query_result = $db->fetchByAssoc($query_result);
                $vin_task_hours_c = $query_result["vin_task_hours_c"];
                $add_hours = 0;
                if($vin_task_hours_c != '' && $vin_task_hours_c != null){
                    $add_hours = round((float)$vin_task_hours_c,0);
                }
                $bean->vin_lead_expiry_date_c = date("Y-m-d H:i:s", strtotime('+'.$add_hours.' hours'));

                $this->noticeAssignerGotLead($bean);
            }
        }
    }

    private function getRoleCurrentUser(){
        global $db, $current_user;

        $list_roles = array();
        $query = "SELECT acl.name FROM acl_roles acl 
            JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
            JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
            WHERE acl.deleted = 0 AND sgu.user_id = '".$current_user->id."';";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($list_roles,$value['name']);
        }
        return $list_roles;
    }

    private function getRoleByUser($id){
        global $db, $current_user;

        $list_roles = array();
        $query = "SELECT acl.name FROM acl_roles acl 
            JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
            JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
            WHERE acl.deleted = 0 AND sgu.user_id = '".$id."';";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($list_roles,$value['name']);
        }
        return $list_roles;
    }

    private function getSaleAdminCurrentUser(){
        global $db, $current_user;

        $user_id = '';
        $query = "SELECT user_id_c FROM users_cstm uc
                JOIN users u ON uc.id_c = u.id 
                WHERE u.id = '".$current_user->id."'  AND u.status = 'Active' AND u.deleted = 0 LIMIT 1;";

        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $user_id = $query_result["user_id_c"];
        return $user_id;
    }

    private function getBusinessManagementCurrentUser($id){
        global $db;

        $user_id = '';
        $query = "SELECT user_id1_c FROM users_cstm uc
                JOIN users u ON uc.id_c = u.id WHERE u.id = '".$id."' AND u.status = 'Active'  AND u.deleted = 0 LIMIT 1;";

        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $user_id = $query_result["user_id1_c"];
        return $user_id;
    }

    private function getSaleAdminAllocate($rating){
        global $db, $current_user;

        $user_id = '';
        $query = "SELECT u.id, COUNT(l.id) FROM users_cstm uc
            JOIN users u ON uc.id_c = u.id 
            LEFT JOIN vin_vin_leads_cstm lc ON lc.user_id1_c = u.id
            LEFT JOIN vin_vin_leads l ON l.id = lc.id_c AND l.deleted = 0 
            WHERE uc.user_id1_c = '".$current_user->id."' AND lc.vin_rating_c = '".$rating."' AND u.status = 'Active' AND u.deleted = 0
            GROUP BY u.id ORDER BY COUNT(l.id) LIMIT 1;";

        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $user_id = $query_result["id"];
        return $user_id;
    }

    private function getDefaultCurrency(){
        global $db, $current_user;

        $default = 'VND';
        if(isset($GLOBALS['sugar_config']['vin_default_currency']) 
        && $GLOBALS['sugar_config']['vin_default_currency'] != null 
        && $GLOBALS['sugar_config']['vin_default_currency'] != ''){
            $default = $GLOBALS['sugar_config']['vin_default_currency'];
        }
        $user_id = '';
        $query = "SELECT * FROM vin_vin_currency c
            JOIN vin_vin_currency_cstm cc ON cc.id_c = c.id 
            WHERE deleted = 0 
            AND (c.name='".$default."' OR cc.sin_symbol_c = '".$default."')
            LIMIT 1;";
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        return $query_result;
    }

    private function getSaleAgentAllocate($id, $rating){
        global $db, $current_user;

        $user_id = '';
        $query = "SELECT u.id, COUNT(l.id) FROM users_cstm uc
            JOIN users u ON uc.id_c = u.id 
            LEFT JOIN vin_vin_leads_cstm lc ON lc.user_id2_c = u.id
            LEFT JOIN vin_vin_leads l ON l.id = lc.id_c AND l.deleted = 0 
            WHERE uc.user_id_c = '".$id."' AND lc.vin_rating_c = '".$rating."' AND u.status = 'Active' AND u.deleted = 0
            GROUP BY u.id ORDER BY COUNT(l.id) LIMIT 1;";

        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $user_id = $query_result["id"];
        return $user_id;
    }

    private function calculateScore($bean){
        global $db, $current_user;
        $result_return = array();

        $score = 0;
        $rating = 'cold';
        //Score
        $query = "SELECT * FROM vin_lead_scoring s
            JOIN vin_lead_scoring_cstm sc ON s.id = sc.id_c
            WHERE s.deleted = 0;";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            if(property_exists($bean, $value['vin_field_c'])){
                if($bean->{$value['vin_field_c']} != null && $bean->{$value['vin_field_c']} != ''){
                    $score = $score + (float)$value['vin_score_c'];
                }
            }
        }

        //Rating
        $query = "SELECT sc.vin_rating_c FROM vin_lead_rating_score s
            JOIN vin_lead_rating_score_cstm sc ON s.id = sc.id_c
            WHERE s.deleted = 0 
            AND sc.vin_from_c <= ".$score." AND sc.vin_to_c >= ".$score." LIMIT 1;";

        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $vin_rating_c = $query_result["vin_rating_c"];
        if($vin_rating_c != '' && $vin_rating_c != null){
            $rating = $vin_rating_c;
        }

        $result_return['score'] = $score;
        $result_return['rating'] = $rating;
        return $result_return;
    }

    private function getDepartmentOfUser($user_id){
        global $db, $current_user;

        $bm = 0;
        $sa = 0;
        $sg = 0;
        $department_id = '';
        $query = '';
        $list_roles = $this->getRoleByUser($user_id);
        foreach($list_roles as $key => $value){
            if($value == 'BUSINESS_MANAGER'){
                $bm = 1;
            }
            else if($value == 'SALE_ADMIN'){
                $sa = 1;
            }
            else if($value == 'SALE_AGENT'){
                $sg = 1;
            }
        }
        if($bm == 1){
            $query = "SELECT d.id FROM vin_vin_department d
                JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                WHERE d.deleted = 0 
                AND u1.id='".$user_id."' LIMIT 1;";
        }
        else if($sa == 1){
            $query = "SELECT d.id FROM vin_vin_department d
                JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                JOIN users_cstm u3 ON u1.id=u3.user_id1_c 
                WHERE d.deleted = 0
                AND u3.id_c='".$user_id."' LIMIT 1;";
        }
        else if($sg == 1){
            $query = "SELECT d.id FROM vin_vin_department d
                JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                JOIN users_cstm u3 ON u1.id=u3.user_id1_c 
                JOIN users_cstm u4 ON u3.id_c=u4.user_id_c 
                WHERE d.deleted = 0
                AND u4.id_c='".$user_id."' LIMIT 1;";
        }
        if($query != ''){
            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $department_id = $query_result["id"];
        }
        return $department_id;
    }

    private function noticeAssignerGotLead($bean){
        global $db, $current_user, $app_list_strings;

        //Email
        require_once('custom/include/global_function/fs_send_email.php');
        require_once('modules/EmailTemplates/EmailTemplate.php');
        $template_name = "Assigned_Leads";
        $template = new EmailTemplate();
        $template->retrieve_by_string_fields(array('name' => $template_name));
        $sendMail = new fs_send_email;
        $toAddress = array();
        $ccAddress = array();
        $subject = $template->subject;
        $body = from_html($template->body_html);
        
        $beanAssigner = BeanFactory::getBean('Users',$current_user->id);
        $assigner = '';
        if($beanAssigner->full_name != '' && $beanAssigner->full_name != null){
            $subject = str_replace('$assigner',$beanAssigner->full_name,$subject);
            $body = str_replace('$assigner',$beanAssigner->full_name,$body);
            $assigner = $beanAssigner->full_name;
        }
        else{
            $subject = str_replace('$assigner',$beanAssigner->user_name,$subject);
            $body = str_replace('$assigner',$beanAssigner->user_name,$body);
            $assigner = $beanAssigner->user_name;
        }
        $subject = str_replace('$lead_name',$bean->name,$subject);

        $beanAssignee = BeanFactory::getBean('Users',$bean->user_id4_c);
        $sea = new SugarEmailAddress;
        $emailAssignee = $sea->getPrimaryAddress($beanAssignee);
        array_push($toAddress, $emailAssignee);

        if($beanAssignee->full_name != '' && $beanAssignee->full_name != null){
            $body = str_replace('$assignee',$beanAssignee->full_name,$body);
        }
        else{
            $body = str_replace('$assignee',$beanAssignee->user_name,$body);
        }
        $body = str_replace('$lead_name',$bean->name,$body);
        $body = str_replace('$lead_phone',$bean->vin_phone_number_c,$body);
        $body = str_replace('$lead_email',$bean->vin_email_c,$body);
        $body = str_replace('$lead_note',$bean->vin_note_c,$body);
        $body = str_replace('$lead_status',$app_list_strings['vin_lead_status_c_list'][$bean->vin_lead_status_c],$body);
        $body = str_replace('$lead_rating',$app_list_strings['vin_rating_c_list'][$bean->vin_rating_c],$body);
        $body = str_replace('$lead_score',$bean->vin_lead_scoring_c,$body);
        $body = str_replace('$lead_project',$app_list_strings['allocate_dept_level1'][$bean->vin_project_c],$body);
        $body = str_replace('$lead_house_type',$app_list_strings['vin_house_type_list_c_list'][$bean->vin_house_type_list_c],$body);

        $date_now = '<span style="color:red;">'.date("d/m/Y h:i a").'</span>';
        $body = str_replace('$lead_date_assign',$date_now,$body);

        $query = "SELECT sc.vin_task_hours_c FROM vin_lead_rating_score s
            JOIN vin_lead_rating_score_cstm sc ON s.id = sc.id_c
            WHERE s.deleted = 0 
            AND sc.vin_rating_c = '".$bean->vin_rating_c."' LIMIT 1;";
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $vin_task_hours_c = $query_result["vin_task_hours_c"];
        $add_hours = 0;
        if($vin_task_hours_c != '' && $vin_task_hours_c != null){
            $add_hours = round((float)$vin_task_hours_c,0);
        }
        $expiry_time = '<span style="color:red;">'.date("d/m/Y h:i a", strtotime('+'.$add_hours.' hours')).'</span>';
        $body = str_replace('$lead_date_expiry',$expiry_time,$body);

        $siteURL = $GLOBALS['sugar_config']['site_url'];
        $url = '<a href="'.$siteURL.'/index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id.'">đây</a>';
        $body = str_replace('$lead_url',$url,$body);

        $paramSendMail = $sendMail->send_email($toAddress, $ccAddress, $subject, $body, $template_name);

        //Alert
        $is_read = 0;
        $type = 'info';
        $target_module= 'vin_vin_leads';
        $name = "Lead Assign: ".$bean->name;
        $description = "[Lead mới] ".$assigner." vừa giao cho bạn một lead mới";
        $url_redirect = 'index.php?module=vin_vin_leads&action=DetailView&record='.$bean->id;
        $beanAlerts = BeanFactory::newBean('Alerts');
        $beanAlerts->name = $name;
        $beanAlerts->description = $description;
        $beanAlerts->url_redirect = $url_redirect;
        $beanAlerts->target_module = $target_module;
        $beanAlerts->is_read = $is_read;
        $beanAlerts->assigned_user_id = $beanAssignee->id;
        $beanAlerts->type = $type;
        $beanAlerts->reminder_id = $beanAssigner->id;
        $beanAlerts->save();
    }

    private function getMatrixAllocate($bean){
        global $db;

        $sc_source = $bean->vin_lead_source_c;
        $sc_level1 = $bean->vin_project_c;
        $sc_level2 = $bean->vin_phankhu_c;
        $sc_level3 = $bean->vin_ads_source_c;
        $sc_level3 = "";
        $matrix_id = '';

        $query = "SELECT * FROM vin_allocate_matrix
            WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = '".$sc_level1."' ";
        if($sc_level2 == "" || $sc_level2 == null){
            $query .= " AND (level2 = '' OR level2 IS NULL) ";
        }
        else{
            $query .= " AND level2 = '".$sc_level2."' ";
        }
        if($sc_level3 == "" || $sc_level3 == null){
            $query .= " AND (level3 = '' OR level3 IS NULL) ";
        }
        else{
            $query .= " AND level3 = '".$sc_level3."' ";
        }
        $query .= " LIMIT 1;";
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $matrix_id = $query_result['id'];
        //No result
        if($matrix_id == '' || $matrix_id == null){
            $query = "SELECT * FROM vin_allocate_matrix
                WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = '".$sc_level1."' ";
            if($sc_level2 == "" || $sc_level2 == null){
                $query .= " AND (level2 = '' OR level2 IS NULL) ";
            }
            else{
                $query .= " AND level2 = '".$sc_level2."' ";
            }
            $query .= " AND level3 = 'OT' ";
            $query .= " LIMIT 1;";
            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $matrix_id = $query_result['id'];
            //No result
            if($matrix_id == '' || $matrix_id == null){
                $query = "SELECT * FROM vin_allocate_matrix
                    WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = '".$sc_level1."' ";
                $query .= " AND (level2 = 'OT' OR level2 = '' OR level2 IS NULL) ORDER BY level2 ";
                $query .= " LIMIT 1;";
                $query_result = $db->query($query);
                $query_result = $db->fetchByAssoc($query_result);
                $matrix_id = $query_result['id'];
                //No result
                if($matrix_id == '' || $matrix_id == null){
                    $query = "SELECT * FROM vin_allocate_matrix
                        WHERE deleted = 0 AND source = '".$sc_source."' AND level1 = 'OTH' ";
                    $query .= " LIMIT 1;";
                    $query_result = $db->query($query);
                    $query_result = $db->fetchByAssoc($query_result);
                    $matrix_id = $query_result['id'];
                }
            }
        }
        return $matrix_id;
    }

    private function getSalesAdminByAllocate($id, $matrix_id){
        global $db;

        $dept_id = '';
        $return = array();
        $return['id'] = '';
        $return['flow_id'] = '';
        $return['index'] = '';
        $query = "SELECT d.id, d.name FROM vin_vin_department d
                JOIN vin_vin_department_cstm dc ON d.id=dc.id_c
                JOIN users u1 ON u1.id = dc.user_id_c AND u1.deleted = 0
                WHERE d.deleted = 0 
                AND u1.id='".$id."' LIMIT 1;";
        $query_result = $db->query($query);
        $query_result = $db->fetchByAssoc($query_result);
        $dept_id = $query_result["id"];
        if($dept_id != '' && $dept_id != null){
            $flow_id = '';
            $query = "SELECT id,allocate,`index` FROM vin_allocate_flow_sadmin
                    WHERE matrix_id = '".$matrix_id."' AND dept_id = '".$dept_id."' LIMIT 1;";

            $query_result = $db->query($query);
            $query_result = $db->fetchByAssoc($query_result);
            $flow_id = $query_result["id"];

            if($flow_id != '' && $flow_id != null){
                $allocate = $query_result["allocate"];
                $allocate_index = $query_result["index"];
                if($allocate != '' && $allocate != null){
                    //Get current index
                    $allocate = explode(",",$allocate);
                    $temp = array();
                    foreach($allocate as $k => $v){
                        if(trim($v) != "" && $v != null){
                            array_push($temp,$v);
                        }
                    }
                    $allocate = $temp;
                    $allocate_length = count($allocate);
                    if($allocate_length > 0){
                        $count_check = 0;
                        while($count_check <= $allocate_length){
                            $count_check++;
                            if($allocate_index != '' && $query_result['id'] != ''
                            && $allocate_index != null && $query_result['id'] != null){
                                $allocate_index = (float)$allocate_index;
                                if((float)$allocate_index >= ((float)$allocate_length -1)){
                                    $allocate_index = 0;
                                }
                                else{
                                    $allocate_index = (float)$allocate_index + 1;
                                }
                            }
                            else{
                                $new_index = 1;
                                $allocate_index = 0;
                            }
                            //Allocate
                            if($allocate_index >= 0){
                                $sadmin = $allocate[$allocate_index];
                                $return['id'] = $sadmin;
                                $return['flow_id'] = $flow_id;
                                $return['index'] = $allocate_index;
                                $count_check = $allocate_length + 1;
                            }
                        }
                    }
                }
            }
        }
        return $return;
    }

    private function markSalesAdminAllocated($flow_id, $allocate_index){
        global $db;

        $query_update = " UPDATE vin_allocate_flow_sadmin SET 
            `index` = '".$allocate_index."'
            WHERE id = '".$flow_id."';";
        $db->query($query_update);
    }

    public function custom_before_save_check_duplicate($bean)
    {
        global $db;

        $email = $bean->vin_email_c;
        $phone = $bean->vin_phone_number_c;
        $fullName = $bean->vin_fullName_c;
        $dob = $bean->vin_dob_c;

        if ($email != '' || $phone != '' || $fullName != '' || $dob != '') {
            $query = "SELECT l.id id FROM vin_vin_leads l
                    JOIN vin_vin_leads_cstm lc ON l.id = lc.id_c
                    WHERE l.deleted = 0 ";
            if (($email != '' && $phone != '') || ($email != '' && $fullName != '') || ($email != '' && $dob != '')
                || ($fullName != '' && $phone != '') || ($fullName != '' && $dob != '') || ($phone != '' && $dob != '')) {
                $query .= " AND (";
                $checkOr = 0;
                if ($email != '' && $phone != '') {
                    $checkOr++;
                    $query .= " (lower(lc.vin_email_c) = '" . $email . "' AND lc.vin_phone_number_c = '" . $phone . "')";
                }if ($email != '' && $fullName != '') {
                    if ($checkOr > 0) {$query .= " OR ";}
                    $checkOr++;
                    $query .= "(lower(lc.vin_email_c) = lower('" . $email . "') AND lower(lc.vin_full_name_c) = lower('" . $fullName . "'))";
                }if ($email != '' && $dob != '') {
                    if ($checkOr > 0) {$query .= " OR ";}
                    $checkOr++;
                    $query .= "(lower(lc.vin_email_c) = lower('" . $email . "') AND lc.vin_dob_c = '" . $dob . "')";
                }if ($fullName != '' && $phone != '') {
                    if ($checkOr > 0) {$query .= " OR ";}
                    $checkOr++;
                    $query .= "(lower(lc.vin_full_name_c) = lower('" . $fullName . "') AND lc.vin_phone_number_c = '" . $phone . "')";
                }if ($fullName != '' && $dob != '') {
                    if ($checkOr > 0) {$query .= " OR ";}
                    $checkOr++;
                    $query .= "(lower(lc.vin_full_name_c) = lower('" . $fullName . "') AND lc.vin_dob_c = '" . $dob . "')";
                }if ($phone != '' && $dob != '') {
                    if ($checkOr > 0) {$query .= " OR ";}
                    $query .= "(lc.vin_phone_number_c = '" . $phone . "' AND lc.vin_dob_c = '" . $dob . "')";
                }
                $query .= " ) AND datediff(now(), l.date_entered) <= 20";
                $data = array();
                $result = $db->query($query);
                $row = $db->fetchByAssoc($result);
                while ($row != null) {
                    $data[] = $row;
                    $row = $db->fetchByAssoc($result);
                }
                if (count($data) > 0) { // Check duplicate leads
                    $bean->vin_lead_duplicate_c = 1; // Set Lead Duplicate of current record = 1
                    $str_id = '';
                    foreach ($data as $key => $value) {
                       if($key == 0) {
                        $str_id .= '\''.$value['id'].'\'';
                       }
                       $str_id.= ', \''. $value['id'] .'\'';
                    }
                    $update_query = "update vin_vin_leads_cstm set vin_lead_duplicate_c = 1 where id_c in (".$str_id.")"; // Update all duplicate lead in database = 1
                    $db->query($update_query);
                }
            }
        }
    }

}