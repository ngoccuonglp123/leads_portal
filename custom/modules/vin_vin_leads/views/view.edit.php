<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/global_function/fs_generate_cache.php');

class vin_vin_leadsViewEdit extends ViewEdit {

    function display()
    {
        global $db, $app_list_strings, $current_user;
        $fs_generate_cache = new fs_generate_cache;
        echo '<link rel="stylesheet" type="text/css" href="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.css','').'">';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.js','').'"></script>';
        echo '<link href="'.$fs_generate_cache->get_cache_version('custom/include/css/vinhomes_leads/vinhomes_leads.css','').'" rel="stylesheet">';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/vinhomes_leads/custom_editview.js','').'"></script>';
        echo '<script src="'.$fs_generate_cache->get_cache_version('custom/include/js/notify/notify.min.js','').'"></script>';
        echo '<style>.notifyjs-corner{top:60px !important; font-size: 16px;}</style>';
        echo $this->list_duplicate_modal();

        echo '<input type="hidden" id="cs_vin_other_fb_c" value="'.$this->bean->vin_other_fb_c.'" />';
        echo '<input type="hidden" id="cs_vin_primary_fb_c" value="'.$this->bean->vin_primary_fb_c.'" />';
        echo '<input type="hidden" id="cs_vin_other_zalo_c" value="'.$this->bean->vin_other_zalo_c.'" />';
        echo '<input type="hidden" id="cs_vin_primary_zalo_c" value="'.$this->bean->vin_primary_zalo_c.'" />';
        echo '<input type="hidden" id="cs_vin_other_email_c" value="'.$this->bean->vin_other_email_c.'" />';
        echo '<input type="hidden" id="cs_vin_other_phone_c" value="'.$this->bean->vin_other_phone_c.'" />';

        $bm = 0;
        $sa = 0;
        $sg = 0;
        $list_roles = $this->getRoleCurrentUser();
        foreach($list_roles as $key => $value){
            if($value == 'BUSINESS_MANAGER'){
                $bm = 1;
            }
            else if($value == 'SALE_ADMIN'){
                $sa = 1;
            }
            else if($value == 'SALE_AGENT'){
                $sg = 1;
            }
        }
        if($current_user->is_admin == 1 || $bm == 1 || $sa == 1){

        }
        else{
            if($_REQUEST['record'] != ""){
                if(!empty($_REQUEST['isDuplicate']) && (string)$_REQUEST['isDuplicate'] == 'true'){
                }
                else{
                    if($this->bean->vin_lead_lock_c == 1){
                        echo 'You dont have permission to access this site!';
                        die;
                    }
                }
            }
        }

        parent::display();
    }

    private function list_duplicate_modal()
    {
        return '
            <style>
                .select2-container{
                    width: 282px;
                }
            </style>
            <div class="modal fade" id="list_duplicate_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="fs_quote_approval_checking" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
                    <div class="modal-content" style="max-width: 150%;width: 700px;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; ">List Duplicate Leads</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <table style="" id="list_duplicate_modal_table">
                                <tr style="width:100%; height:28px;" class="list_duplicate_tr_header">
                                    <th style="width:5%;"></th>
                                    <th style="width:30%;">Name</th>
                                    <th style="width:23%;">Owner</th>
                                    <th style="width:25%;">Email</th>
                                    <th style="width:17%;">Phone Number</th>
                                </tr>
                            </table>
                            <table style="" id="list_duplicate_modal_data">
        
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>';
    }

    private function getRoleCurrentUser(){
        global $db, $current_user;

        $list_roles = array();
        $query = "SELECT acl.name FROM acl_roles acl 
            JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
            JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
            WHERE acl.deleted = 0 AND sgu.user_id = '".$current_user->id."';";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($list_roles,$value['name']);
        }
        return $list_roles;
    }
}