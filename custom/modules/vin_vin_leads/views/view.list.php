<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/global_function/fs_generate_cache.php');

class vin_vin_leadsViewList extends ViewList
{
    function display()
    {
        global $db, $app_list_strings, $current_user;
        $fs_generate_cache = new fs_generate_cache;
        
        echo '<link rel="stylesheet" type="text/css" href="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.css','').'">';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.js','').'"></script>';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/vinhomes_leads/custom_listview.js','').'"></script>';
        echo '<link href="'.$fs_generate_cache->get_cache_version('custom/include/css/vinhomes_leads/vinhomes_leads.css','').'" rel="stylesheet">';
        echo '<script src="'.$fs_generate_cache->get_cache_version('custom/include/js/notify/notify.min.js','').'"></script>';
        echo '<style>.notifyjs-corner{top:60px !important; font-size: 16px;}</style>';
        
        $check_permission = 1;
        if($check_permission == 1)
        {
            echo '<script>
                var assign_leads_button = '."'".'<button type="button" id="btn_assign_leads_button" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="showAssignLeadsModal();">Assign Sales</button>'."'".';
            </script>';
        }
        else
        {
            echo '<script>
                var assign_leads_button = "";
            </script>';
        }

        $check_permission_owner = 0;
        $bm = 0;
        $sa = 0;
        $sg = 0;
        $digital_lead = 0;
        $list_roles = $this->getRoleCurrentUser();
        foreach($list_roles as $key => $value){
            if($value == 'BUSINESS_MANAGER'){
                $bm = 1;
            }
            else if($value == 'SALE_ADMIN'){
                $sa = 1;
            }
            else if($value == 'SALE_AGENT'){
                $sg = 1;
            }
            else if($value == 'DIGITAL_MARKETING'){
                $digital_lead = 1;
            }
        }
        if($bm == 1 || $sa == 1){
            $check_permission_owner = 1;
        }
        if($check_permission_owner == 1)
        {
            echo '<script>
                var btn_change_owner_button = '."'".'<button type="button" id="btn_change_owner_button" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="onclickChangeOwner();">Change Owner</button>'."'".';
            </script>';
        }
        else
        {
            echo '<script>
                var btn_change_owner_button = "";
            </script>';
        }

        $check_permission_changeStatus = 1;
        if($check_permission_changeStatus == 1)
        {
            echo '<script>
                var btn_change_status_button = '."'".'<button type="button" id="btn_change_status_button" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="showChangeStatusModal();">Change Status</button>'."'".';
            </script>';
        }
        else
        {
            echo '<script>
                var btn_change_status_button = "";
            </script>';
        }

        echo $this->assign_leads_modal();
        echo $this->change_owner_modal();
        echo $this->change_status_modal();
        echo $this->master_import_modal();
        echo $this->drawLoader();

        if($current_user->is_admin == 1){
            echo '<script>
                var btn_encode_modal = '."'".'<button type="button" id="btn_encode_modal" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="onclickEncrypt();">Encrypt</button>'."'".';
            </script>';
            echo $this->encode_modal();
        }
        else{
            echo '<script>
                var btn_encode_modal = "";
            </script>';
        }
        
        if($current_user->is_admin == 1 || $digital_lead == 1){
            echo '<script>
                    var btn_lead_report = '."'".'<button type="button" id="btn_lead_report" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="showLeadsReportModal();">Leads Report</button>'."'".';
                </script>';
            echo $this->leads_report_modal();
        }
        else{
            echo '<script>
                    var btn_lead_report = "";
                </script>';
        }

        if($current_user->is_admin == 1 || $digital_lead == 1 || $bm == 1){
            echo '<script>
                var btn_allocate_button = '."'".'<button type="button" id="btn_allocate_button" class="button" style="margin-right: 2px;float: right;margin-top: 0px;" onclick="allocate_matrix();">Allocate</button>'."'".';
            </script>';
        }
        else{
            echo '<script>
                    var btn_allocate_button = "";
                </script>';
        }

        parent::display();
    }

    function listViewPrepare() {
		if(!isset($_REQUEST['orderBy'])) {
			$_REQUEST['orderBy'] = 'date_entered';
		}
		if(!isset($_REQEUST['sortOrder'])) {
			$_REQUEST['sortOrder'] = 'DESC';
		}
		parent::listViewPrepare();
	}

    private function assign_leads_modal()
    {
        return '
            <style>
                #fs_quote_approval_checking_table tr td{
                    padding-left:3px;
                    text-overflow: ellipsis;
                    overflow: hidden; 
                    white-space: nowrap;
                }
                #fs_quote_approval_checking_table tr th{
                    padding-left:3px;
                }

                .moduleTitle{
                    background-color: #FFFFFF;
                }

                .select2-container{
                    width: 200px;
                }
            </style>
            <div class="modal fade" id="sale_agent_select_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="fs_quote_approval_checking" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
                    <div class="modal-content" style="max-width: 70%;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; ">Assign Sales</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <table style="width:100%;" id="sale_agent_select_modal_table">
                                <tr style="width:100%; height:35px;">
                                    <th style="width:30%;">Sale In Charge: </th>
                                    <th style="width:70%; display: inline;">&nbsp<input id="sale_agent_select" /></th>
                                </tr>
                                <tr style="width:100%; height:35px;line-height: 30px;">
                                    <td style="width:30%; font-weight: bold;">Email: </td>
                                    <td style="width:70%; display: inline;">&nbsp<span id="sale_agent_select_email"></span></td>
                                </tr>
                                <tr style="width:100%; height:35px;line-height: 30px;">
                                    <td style="width:30%; font-weight: bold;">Sale Dept.: </td>
                                    <td style="width:70%; display: inline;">&nbsp<span id="sale_agent_select_department"></span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="background-color: #3d9dd5;" class="btn btn-primary" onclick="assignLeadsSubmit();">Submit</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>';
    }

    private function change_owner_modal()
    {
        return '
            <style>
                .select2-container{
                    width: 200px;
                }
            </style>
            <div class="modal fade" id="change_owner_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="fs_quote_approval_checking" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
                    <div class="modal-content" style="max-width: 100%;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; ">Change Lead Owner</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <table style="" id="change_owner_select_modal_table">
                                <tr style="width:100%; height:35px;" class="change_owner_tr">
                                    <th style="width:20%;">Lead Owner: </th>
                                    <th style="width:80%; display: inline;">&nbsp<input id="change_owner_select" /></th>
                                </tr>
                                <tr style="width:100%; height:35px;line-height: 30px;" class="change_owner_tr">
                                    <td style="width:20%; font-weight: bold;"></td>
                                    <td style="width:80%; display: inline;">&nbsp<input type="checkbox"/> &nbspSend notification email</td>
                                </tr>
                                <tr style="width:100%; height:35px;line-height: 30px;" class="change_owner_tr">
                                    <td colspan="2">
                                        <strong>The new owner</strong> will also become the owner of these records relate to that are owned by <strong>the current record owner.</strong>
                                        <ul class="fs_workflow_desc_ul">
                                            <li>Notes and attachments</li>
                                            <li>Open activities</li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="background-color: #3d9dd5;" class="btn btn-primary" onclick="change_owner_submit();">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>';
    }

    private function change_status_modal()
    {
        global $app_list_strings;

        $option_status = '';
        foreach ($app_list_strings['vin_lead_status_c_list'] as $key => $value){
            $option_status .= '<option value="'.$key.'">'.$value.'</option>';
        }

        $option_unqualified = '';
        foreach ($app_list_strings['vin_unqualified_reason_list'] as $key => $value){
            $option_unqualified .= '<option value="'.$key.'">'.$value.'</option>';
        }

        $option_working_status = '';
        foreach ($app_list_strings['vin_working_substatus_c_list'] as $key => $value){
            $option_working_status .= '<option value="'.$key.'">'.$value.'</option>';
        }

        return '
            <style>
            </style>
            <div class="modal fade" id="change_status_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="fs_quote_approval_checking" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
                    <div class="modal-content" style="max-width: 100%;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; ">Change Leads Status</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <table style="" id="change_status_select_modal_table">
                                <tr style="width:100%; height:35px;" class="change_owner_tr">
                                    <th style="width:25%;"><span style="color:red;">*</span>Change Status </th>
                                    <th style="width:75%; display: inline;">&nbsp<select id="status_change" onchange="working_status_change()">'.$option_status.'</select></th>
                                </tr>
                                <tr style="width:100%; height:35px;display:none;" id="working_status_tr">
                                    <th style="width:25%;">Working Status: </th>
                                    <th style="width:75%; display: inline;">&nbsp<select id="working_status">'.$option_working_status.'</select></th>
                                </tr>
                                <tr style="width:100%; height:35px;display:none;" id="reason_unqualified_tr">
                                    <th style="width:25%;">Reason Unqualified: </th>
                                    <th style="width:75%; display: inline;">&nbsp<select id="reason_unqualified">'.$option_unqualified.'</select></th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="background-color: #3d9dd5;" class="btn btn-primary" onclick="change_status_submit();">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>';
    }

    private function master_import_modal()
    {
        global $mod_strings, $db, $app_list_strings;

        return '<div class="modal fade" id="master_import_modal_popup" role="dialog" aria-labelledby="editEstimate" style="padding-top: 50px;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xs" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Import Leads</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <span style="font-weight: bold;">Select an excel file to Import</span>
                            <br />
                            <span style="font-weight: bold; color: red;">Note: </span>
                            <ul class="fs_workflow_desc_ul">
                                <li>Do not change order of column template. </li>
                                <li>Required fields marked as red. </li>
                                <li>Some fields must choose in list data. </li>
                            <ul>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 30%; font-weight: bold;">Upload file</td>
                                    <td style="width: 70%;"><input class="form-control" type="file" name="upload_file3" id="upload_file3" value="" style="width:100% !important; height: auto;" onchange="validateFile3()"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><span id="lblError3" style="color: red; font-weight: normal"></span></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><span id="lblMsg3" style="color: green; font-weight: normal"></span></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="custom/include/files/templates/import_leads_template.xlsx" download="" style="font-weight: normal"><p>Download Template</p></a></td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button style="background-color: #3d9dd5;" type="button" class="btn btn-primary" data="" onclick="onClickVerifyMasterImportInvoices();" id="btn_verify_master_import_invoices">Import</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>';
    }

    private function drawLoader()
    {
        return '
            <div class="modal fade bd-example-modal-lg" id="loaderModal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="z-index: 100000000 !important;">
                <div class="modal-dialog modal-sm" style="padding-top: 17%;">
                    <div class="" style="width: 0;">
                        <div class="spinner-border text-primary"></div>
                    </div>
                </div>
            </div>';
    }

    private function getRoleCurrentUser(){
        global $db, $current_user;

        $list_roles = array();
        $query = "SELECT acl.name FROM acl_roles acl 
            JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
            JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
            WHERE acl.deleted = 0 AND sgu.user_id = '".$current_user->id."';";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($list_roles,$value['name']);
        }
        return $list_roles;
    }

    private function encode_modal()
    {
        return '
            <style>
            </style>
            <div class="modal fade" id="encrypt_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="encrypt_modal" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
                    <div class="modal-content" style="max-width: 100%;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; ">Encrypt</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <table style="width: 100%;table-layout: fixed;" id="">
                                <tr style="line-height: 35px;">
                                    <td style="width: 20%;font-weight: bold;">Value: </td>
                                    <td><input type="text" id="encrypt_value_raw" style="width: 90%;" /></td>
                                </tr>
                                <tr style="">
                                    <td style="width: 20%;font-weight: bold;">Encrypt: </td>
                                    <td style="word-wrap: break-word;"><span id="encrypt_value"></span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button style="background-color: #3d9dd5;" type="button" class="btn btn-primary" data="" onclick="onClickEncrypt();" id="btn_encrypt">Encrypt</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>';
    }

    private function leads_report_modal()
    {
        return '
            <div class="modal fade" id="leads_report_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="fs_quote_approval_checking" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
                    <div class="modal-content" style="max-width: 70%;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; ">Leads Report</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <table style="width:100%;" id="leads_report_modal_table">
                                <tr style="width:100%; height:35px;line-height: 30px;">
                                    <td style="width:30%; font-weight: bold;">From:</td>
                                    <td style="width:70%; display: inline;">&nbsp<input type="text" id="lead_report_from"/></td>
                                </tr>
                                <tr style="width:100%; height:35px;line-height: 30px;">
                                    <td style="width:30%; font-weight: bold;">To: </td>
                                    <td style="width:70%; display: inline;">&nbsp<input type="text" id="lead_report_to"/></td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="background-color: #3d9dd5;" class="btn btn-primary" onclick="export_report();">Export</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>';
    }
}