<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/global_function/fs_generate_cache.php');

class vin_vin_leadsViewDetail extends ViewDetail {

    function display()
    {
        global $db, $app_list_strings, $current_user;
        $fs_generate_cache = new fs_generate_cache;

        echo '<link rel="stylesheet" type="text/css" href="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.css','').'">';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/select2/select2.min.js','').'"></script>';
        echo '<link href="'.$fs_generate_cache->get_cache_version('custom/include/css/vinhomes_leads/vinhomes_leads.css','').'" rel="stylesheet">';
        echo '<script type="text/javascript" src="'.$fs_generate_cache->get_cache_version('custom/include/js/vinhomes_leads/custom_detailview.js','').'"></script>';
        echo $this->custom_header();
        echo '<input type="hidden" id="qkt_record_id" value="'.$this->bean->id.'" />';
        echo $this->workflow_action_modal();

        $check_permission = 0;
        $bm = 0;
        $sa = 0;
        $sg = 0;
        $digital_lead = 0;
        $list_roles = $this->getRoleCurrentUser();
        foreach($list_roles as $key => $value){
            if($value == 'BUSINESS_MANAGER'){
                $bm = 1;
            }
            else if($value == 'SALE_ADMIN'){
                $sa = 1;
            }
            else if($value == 'SALE_AGENT'){
                $sg = 1;
            }
            else if($value == 'DIGITAL_MARKETING'){
                $digital_lead = 1;
            }
        }
        if($bm == 1 || $sa == 1){
            $check_permission = 1;
        }
        if($check_permission == 1){
            echo '<script>
                var lead_change_owner_btn = '."'".'<button type="button" id="btn_lead_change_owner" class="button" style="margin-right: 2px;margin-top: 0px;" onclick="onclickChangeOwner();">Change Owner</button>'."'".';
            </script>';
        }
        else{
            echo '<script>
                var lead_change_owner_btn = "";
            </script>';
        }

        //Lock
        $lock_permission = 0;
        if($bm == 1 || $current_user->is_admin == 1){
            $lock_permission = 1;
        }
        else if($sa == 1){
            $lock_permission = 1;
            // if($this->bean->user_id4_c != '' && $this->bean->user_id4_c != null){
            //     $user_in_charge = $this->bean->user_id4_c;
            //     $query = "SELECT uc.user_id_c 
            //         FROM users_cstm uc
            //         JOIN users u ON uc.id_c = u.id 
            //         WHERE u.id = '".$user_in_charge."' 
            //         AND uc.user_id_c = '".$current_user->id."' 
            //         AND u.deleted = 0 LIMIT 1;";

            //     $query_result = $db->query($query);
            //     $query_result = $db->fetchByAssoc($query_result);
            //     $user_id = $query_result["user_id_c"];
            //     if($user_id != '' && $user_id != null){
            //         $lock_permission = 1;
            //     }
            // }
        }
        if($lock_permission == 1){
            echo '<input type="hidden" id="fs_lock_permission" value="1" />';
        }
        else{
            echo '<input type="hidden" id="fs_lock_permission" value="0" />';
        }
        echo '<input type="hidden" id="fs_lock_status" value="'.$this->bean->vin_lead_lock_c.'" />';

        echo $this->change_owner_modal();
        echo $this->list_duplicate_modal();
        $created_by = $this->bean->created_by_name.' '.$this->bean->date_entered;
        $modified_by = $this->bean->modified_by_name.' '.$this->bean->date_modified;
        echo '<input type="hidden" id="fs_created_by" value="'.$created_by.'" />';
        echo '<input type="hidden" id="fs_modified_by" value="'.$modified_by.'" />';
        echo '<input type="hidden" id="fs_currency" value="'.$this->bean->vin_currency_c.'" />';
        echo '<input type="hidden" id="fs_order_id" value="'.$this->bean->vin_order_id_c.'" />';
        echo '<input type="hidden" id="vin_other_fb_c" value="'.$this->bean->vin_other_fb_c.'" />';
        echo '<input type="hidden" id="vin_primary_fb_c" value="'.$this->bean->vin_primary_fb_c.'" />';
        echo '<input type="hidden" id="vin_other_zalo_c" value="'.$this->bean->vin_other_zalo_c.'" />';
        echo '<input type="hidden" id="vin_primary_zalo_c" value="'.$this->bean->vin_primary_zalo_c.'" />';

        $field_permission = 0;
        if($current_user->is_admin == 1 || $digital_lead == 1){
            $field_permission = 1;
        }
        if($field_permission == 0){
            $this->bean->vin_agent_c = '';
            $this->bean->vin_ad_term_c = '';
            $this->bean->vin_form_medium_c = '';
            $this->bean->vin_utm_source_c = '';
            echo '<input type="hidden" id="lead_hidden_fields" value="vin_agent_c,vin_ad_term_c,vin_form_medium_c,vin_utm_source_c" />';
        }

        parent::display();
    }

    private function custom_header(){
        global $app_list_strings;

        $beanLead = $this->bean;
        $new = '';
        $assigned = '';
        $working = '';
        $unqualified = '';
        $converted = '';
        $action_new = '';
        $action_assigned = '';
        $action_working = '';
        $action_unqualified = '';
        $action_converted = '';
        echo '<input type="hidden" id="qkt_current_status" value="'.$beanLead->vin_lead_status_c.'" />';
        if($beanLead->vin_lead_status_c == '' || $beanLead->vin_lead_status_c == null || $beanLead->vin_lead_status_c == 'new'){
            $new = 'fs_workflow_current';
            $step = 1;
            $action_assigned = 'onclick="workflowChangeStatus('."'assigned', 'Assigned', 2".')"';
            $action_working = 'onclick="workflowChangeStatus('."'working', 'Working', 3".')"';
            $action_unqualified = 'onclick="workflowChangeStatus('."'unqualified', 'Unqualified', 4".')"';
            $action_converted = 'onclick="workflowChangeStatus('."'converted', 'Converted', 5".')"';
        }
        else if($beanLead->vin_lead_status_c == 'assigned'){
            $new = 'fs_workflow_completed';
            $assigned = 'fs_workflow_current';
            $step = 2;
            $action_working = 'onclick="workflowChangeStatus('."'working', 'Working', 3".')"';
            $action_unqualified = 'onclick="workflowChangeStatus('."'unqualified', 'Unqualified', 4".')"';
            $action_converted = 'onclick="workflowChangeStatus('."'converted', 'Converted', 5".')"';

            $action_assigned = 'onclick="workflowChangeStatus('."'assigned', 'Assigned', 2".')"';
            $action_working = 'onclick="workflowChangeStatus('."'working', 'Working', 3".')"';
            $action_unqualified = 'onclick="workflowChangeStatus('."'unqualified', 'Unqualified', 4".')"';
            $action_converted = 'onclick="workflowChangeStatus('."'converted', 'Converted', 5".')"';
        }
        else if($beanLead->vin_lead_status_c == 'working'){
            $new = 'fs_workflow_completed';
            $assigned = 'fs_workflow_completed';
            $working = 'fs_workflow_current';
            $step = 3;
            $action_unqualified = 'onclick="workflowChangeStatus('."'unqualified', 'Unqualified', 4".')"';
            $action_converted = 'onclick="workflowChangeStatus('."'converted', 'Converted', 5".')"';

            $action_assigned = 'onclick="workflowChangeStatus('."'assigned', 'Assigned', 2".')"';
            $action_working = 'onclick="workflowChangeStatus('."'working', 'Working', 3".')"';
            $action_unqualified = 'onclick="workflowChangeStatus('."'unqualified', 'Unqualified', 4".')"';
            $action_converted = 'onclick="workflowChangeStatus('."'converted', 'Converted', 5".')"';
        }
        else if($beanLead->vin_lead_status_c == 'unqualified'){
            $new = 'fs_workflow_completed';
            $assigned = 'fs_workflow_completed';
            $working = 'fs_workflow_completed';
            $unqualified = 'fs_workflow_current';
            $step = 4;
            $action_converted = 'onclick="workflowChangeStatus('."'converted', 'Converted', 5".')"';

            //$action_assigned = 'onclick="workflowChangeStatus('."'assigned', 'Assigned', 2".')"';
            //$action_working = 'onclick="workflowChangeStatus('."'working', 'Working', 3".')"';
            //$action_unqualified = 'onclick="workflowChangeStatus('."'unqualified', 'Unqualified', 4".')"';
            //$action_converted = 'onclick="workflowChangeStatus('."'converted', 'Converted', 5".')"';
        }
        else if($beanLead->vin_lead_status_c == 'converted'){
            $new = 'fs_workflow_completed';
            $assigned = 'fs_workflow_completed';
            $working = 'fs_workflow_completed';
            $unqualified = 'fs_workflow_completed';
            $converted = 'fs_workflow_current';
            $step = 5;

            //$action_assigned = 'onclick="workflowChangeStatus('."'assigned', 'Assigned', 2".')"';
            //$action_working = 'onclick="workflowChangeStatus('."'working', 'Working', 3".')"';
            //$action_unqualified = 'onclick="workflowChangeStatus('."'unqualified', 'Unqualified', 4".')"';
            //$action_converted = 'onclick="workflowChangeStatus('."'converted', 'Converted', 5".')"';
        }
        return '
            <div id="custom_header_above" style="display: none;">
                <div id="custom_header_info">
                    <div>
                        <div>
                            Phone
                            <p>'.$beanLead->vin_phone_number_c.'</p>
                        </div>
                    </div>
                    <div>
                        <div>
                            Email
                            <p class="fs_color_link">'.$beanLead->vin_email_c.'</p>
                        </div>
                    </div>
                    <div>
                        <div>
                            Project
                            <p>'.$app_list_strings['allocate_dept_level1'][$beanLead->vin_project_c].'</p>
                        </div>
                    </div>
                    <div>
                        <div>
                            Sales In Charge
                            <p class="fs_color_link" id="header_sales_in_charge">'.$beanLead->vin_sales_in_charge_c.'</p>
                        </div>
                    </div>
                    <div>
                        <div>
                            Sales Dept
                            <p>'.$beanLead->vin_sales_dept_c.'</p>
                        </div>
                    </div>
                    <div>
                        <div>
                            Lead ID Salesforce
                            <p>'.$beanLead->vin_lead_id_salesforce_c.'</p>
                        </div>
                    </div>
                </div>
                <div id="custom_header_workflow">
                    <div class="fs_workflow_container">	
                        <div class="fs_workflow_wrapper">	
                            <div class="fs_workflow_arrow-steps fs_workflow_clearfix">
                                <div id="fs_workflow_step_1" class="fs_workflow_step '.$new.'" '.$action_new.'> <span>New</span> </div>
                                <div id="fs_workflow_step_2" class="fs_workflow_step '.$assigned.'" '.$action_assigned.'> <span>Assigned</span> </div>
                                <div id="fs_workflow_step_3" class="fs_workflow_step '.$working.'" '.$action_working.'> <span>Working</span> </div>
                                <div id="fs_workflow_step_4" class="fs_workflow_step '.$unqualified.'" '.$action_unqualified.'> <span>Unqualified</span> </div>
                                <div id="fs_workflow_step_5" class="fs_workflow_step '.$converted.'" '.$action_converted.'> <span>Converted</span> </div>
                                <div class="fs_workflow_mark_status" onclick="workflowMarkStatus()"> <span>✔ Mark Status as Complete</span> </div>
                            </div>
                        </div>
                    </div>
                    <div id="fs_workflow_desc">
                        <span>Guidance for Success</span>
                        <br />
                        <p>&nbsp</p>
                        <div>
                            <span>Determine which leads are qualified or unqualified</span>
                            <ul class="fs_workflow_desc_ul">
                                <li>Assign the lead to a representative</li>
                                <li>Gather key details on the lead from their company website</li>
                                <li>Create a plan to build a connection with this lead</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="wf_status" value="'.$beanLead->vin_lead_status_c.'" />
            <input type="hidden" id="wf_status_display" value="'.$app_list_strings['vin_lead_status_c_list'][$beanLead->vin_lead_status_c].'" />
            <input type="hidden" id="wf_step" value="'.$step.'" />
        ';
    }

    private function workflow_action_modal()
    {
        global $app_list_strings;

        $option_unqualified = '';
        foreach ($app_list_strings['vin_unqualified_reason_list'] as $key => $value){
            $option_unqualified .= '<option value="'.$key.'">'.$value.'</option>';
        }

        $option_working_status = '';
        foreach ($app_list_strings['vin_working_substatus_c_list'] as $key => $value){
            $option_working_status .= '<option value="'.$key.'">'.$value.'</option>';
        }

        return '
            <style>
                .select2-container{
                    width: 200px;
                }
            </style>
            <div class="modal fade" id="workflow_action_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="fs_quote_approval_checking" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
                    <div class="modal-content" style="max-width: 100%;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; ">Change Status Action</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <table style="width: 80%;" id="sale_agent_select_modal_table">
                                <tr style="width:100%; height:35px;" class="sales_in_charge_tr">
                                    <th style="width:40%;">Sale In Charge: </th>
                                    <th style="width:60%; display: inline;">&nbsp<input id="sale_agent_select" /></th>
                                </tr>
                                <tr style="width:100%; height:35px;line-height: 30px;" class="sales_in_charge_tr">
                                    <td style="width:40%; font-weight: bold;">Email: </td>
                                    <td style="width:60%; display: inline;">&nbsp<span id="sale_agent_select_email"></span></td>
                                </tr>
                                <tr style="width:100%; height:35px;line-height: 30px;" class="sales_in_charge_tr">
                                    <td style="width:40%; font-weight: bold;">Sale Dept.: </td>
                                    <td style="width:60%; display: inline;">&nbsp<span id="sale_agent_select_department"></span></td>
                                </tr>
                                <tr style="width:100%; height:35px;" id="working_status_tr">
                                    <th style="width:40%;">Working Status: </th>
                                    <th style="width:60%; display: inline;">&nbsp<select id="working_status">'.$option_working_status.'</select></th>
                                </tr>
                                <tr style="width:100%; height:35px;" id="reason_unqualified_tr">
                                    <th style="width:40%;">Reason Unqualified: </th>
                                    <th style="width:60%; display: inline;">&nbsp<select id="reason_unqualified">'.$option_unqualified.'</select></th>
                                </tr>
                                <tr style="width:100%; height:35px;" id="converted_status_change">
                                    <th style="width:40%;">Order ID: </th>
                                    <th style="width:60%; display: inline;">&nbsp<input id="converted_status_change_input" type="text"/></th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="background-color: #3d9dd5;" class="btn btn-primary" onclick="workflow_change_status_save();">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>';
    }

    private function change_owner_modal()
    {
        return '
            <style>
                .select2-container{
                    width: 200px;
                }
            </style>
            <div class="modal fade" id="change_owner_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="fs_quote_approval_checking" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
                    <div class="modal-content" style="max-width: 100%;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; ">Change Lead Owner</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <table style="" id="change_owner_select_modal_table">
                                <tr style="width:100%; height:35px;" class="change_owner_tr">
                                    <th style="width:20%;">Lead Owner: </th>
                                    <th style="width:80%; display: inline;">&nbsp<input id="change_owner_select" /></th>
                                </tr>
                                <tr style="width:100%; height:35px;line-height: 30px;" class="change_owner_tr">
                                    <td style="width:20%; font-weight: bold;"></td>
                                    <td style="width:80%; display: inline;">&nbsp<input type="checkbox"/> &nbspSend notification email</td>
                                </tr>
                                <tr style="width:100%; height:35px;line-height: 30px;" class="change_owner_tr">
                                    <td colspan="2">
                                        <strong>The new owner</strong> will also become the owner of these records relate to <strong><span id="change_owner_current">'.$this->bean->vin_lead_owner_c.'</span></strong> that are owned by <strong>the current record owner.</strong>
                                        <ul class="fs_workflow_desc_ul">
                                            <li>Notes and attachments</li>
                                            <li>Open activities</li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="background-color: #3d9dd5;" class="btn btn-primary" onclick="change_owner_submit();">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>';
    }

    private function getRoleCurrentUser(){
        global $db, $current_user;

        $list_roles = array();
        $query = "SELECT acl.name FROM acl_roles acl 
            JOIN securitygroups_acl_roles sgacl ON acl.id = sgacl.role_id AND sgacl.deleted = 0
            JOIN securitygroups sg ON sg.id = sgacl.securitygroup_id AND sg.deleted = 0
            JOIN securitygroups_users sgu ON sg.id = sgu.securitygroup_id AND sgu.deleted = 0
            WHERE acl.deleted = 0 AND sgu.user_id = '".$current_user->id."';";

        $result = $db->query($query);
        $dataSet = array();
        $row = $db->fetchByAssoc($result);
        while ($row != null){
            $dataSet[] = $row;
            $row = $db->fetchByAssoc($result);
        }
        foreach($dataSet as $key => $value){
            array_push($list_roles,$value['name']);
        }
        return $list_roles;
    }

    private function list_duplicate_modal()
    {
        return '
            <style>
                .select2-container{
                    width: 282px;
                }
            </style>
            <div class="modal fade" id="list_duplicate_modal" style="overflow-x: auto;" role="dialog" aria-labelledby="fs_quote_approval_checking" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-nm" role="document" style="margin-top: 70px;">
                    <div class="modal-content" style="max-width: 150%;width: 700px;">
                        <div class="modal-header" style="padding: 10px; ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size: 15px; ">List Duplicate Leads</h4>
                        </div>
                        <div class="tab-content modal-body" style="overflow-y: auto; max-height: calc(100vh - 212px);">
                            <table style="" id="list_duplicate_modal_table">
                                <tr style="width:100%; height:28px;" class="list_duplicate_tr_header">
                                    <th style="width:5%;"></th>
                                    <th style="width:30%;">Name</th>
                                    <th style="width:23%;">Owner</th>
                                    <th style="width:25%;">Email</th>
                                    <th style="width:17%;">Phone Number</th>
                                </tr>
                            </table>
                            <table style="" id="list_duplicate_modal_data">
        
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>';
    }
}