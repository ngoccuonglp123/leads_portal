<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2020-03-29 12:06:20
$dictionary["vin_vin_leads"]["fields"]["vin_vin_leads_documents_1"] = array (
  'name' => 'vin_vin_leads_documents_1',
  'type' => 'link',
  'relationship' => 'vin_vin_leads_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_VIN_VIN_LEADS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


 // created: 2020-03-29 16:09:23
$dictionary['vin_vin_leads']['fields']['annual_revenue']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['annual_revenue']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:10:56
$dictionary['vin_vin_leads']['fields']['billing_address_city']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['billing_address_city']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:11:58
$dictionary['vin_vin_leads']['fields']['billing_address_country']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['billing_address_country']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:11:39
$dictionary['vin_vin_leads']['fields']['billing_address_postalcode']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['billing_address_postalcode']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:11:14
$dictionary['vin_vin_leads']['fields']['billing_address_state']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['billing_address_state']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:10:37
$dictionary['vin_vin_leads']['fields']['billing_address_street']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['billing_address_street']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-27 00:37:10
$dictionary['vin_vin_leads']['fields']['description']['audited']=true;
$dictionary['vin_vin_leads']['fields']['description']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['description']['comments']='Full text of the note';
$dictionary['vin_vin_leads']['fields']['description']['merge_filter']='disabled';

 

 // created: 2020-03-29 16:17:11
$dictionary['vin_vin_leads']['fields']['email1']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['email1']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:15:42
$dictionary['vin_vin_leads']['fields']['employees']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['employees']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:06:54
$dictionary['vin_vin_leads']['fields']['industry']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['industry']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-28 13:51:29
$dictionary['vin_vin_leads']['fields']['name']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['name']['comments']='Name of the Company';

 

 // created: 2020-03-29 16:15:16
$dictionary['vin_vin_leads']['fields']['ownership']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['ownership']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:14:09
$dictionary['vin_vin_leads']['fields']['phone_alternate']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['phone_alternate']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:09:56
$dictionary['vin_vin_leads']['fields']['phone_fax']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['phone_fax']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:13:43
$dictionary['vin_vin_leads']['fields']['phone_office']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['phone_office']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:12:23
$dictionary['vin_vin_leads']['fields']['rating']['inline_edit']=true;
$dictionary['vin_vin_leads']['fields']['rating']['comments']='An arbitrary rating for this company for use in comparisons with others';
$dictionary['vin_vin_leads']['fields']['rating']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['rating']['duplicate_merge_dom_value']='0';
$dictionary['vin_vin_leads']['fields']['rating']['merge_filter']='disabled';

 

 // created: 2020-03-29 16:16:15
$dictionary['vin_vin_leads']['fields']['shipping_address_city']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['shipping_address_city']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:17:07
$dictionary['vin_vin_leads']['fields']['shipping_address_country']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['shipping_address_country']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:16:45
$dictionary['vin_vin_leads']['fields']['shipping_address_postalcode']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['shipping_address_postalcode']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:16:39
$dictionary['vin_vin_leads']['fields']['shipping_address_state']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['shipping_address_state']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:16:11
$dictionary['vin_vin_leads']['fields']['shipping_address_street']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['shipping_address_street']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-29 16:15:47
$dictionary['vin_vin_leads']['fields']['ticker_symbol']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['ticker_symbol']['duplicate_merge_dom_value']='0';

 

 // created: 2020-03-25 20:09:21
$dictionary['vin_vin_leads']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2020-03-25 20:10:27
$dictionary['vin_vin_leads']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2020-03-26 21:16:31
$dictionary['vin_vin_leads']['fields']['user_id3_c']['inline_edit']=1;

 

 // created: 2020-03-26 21:22:59
$dictionary['vin_vin_leads']['fields']['user_id4_c']['inline_edit']=1;

 

 // created: 2020-03-25 20:08:39
$dictionary['vin_vin_leads']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2020-03-29 16:24:31
$dictionary['vin_vin_leads']['fields']['vin_address_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_address_c']['labelValue']='Address';

 

 // created: 2020-05-21 22:42:11
$dictionary['vin_vin_leads']['fields']['vin_ads_content_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_ads_content_c']['labelValue']='Ads Content';

 

 // created: 2020-05-21 22:40:03
$dictionary['vin_vin_leads']['fields']['vin_ads_source_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_ads_source_c']['labelValue']='Ads Source';

 

 // created: 2020-05-21 22:43:52
$dictionary['vin_vin_leads']['fields']['vin_ad_term_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_ad_term_c']['labelValue']='Ad Term';

 

 // created: 2020-05-21 22:43:23
$dictionary['vin_vin_leads']['fields']['vin_agent_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_agent_c']['labelValue']='Agent';

 

 // created: 2020-03-25 20:01:48
$dictionary['vin_vin_leads']['fields']['vin_assign_date_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_assign_date_c']['labelValue']='Assign Date';

 

 // created: 2020-03-29 14:57:16
$dictionary['vin_vin_leads']['fields']['vin_auto_scoring_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_auto_scoring_c']['labelValue']='Auto Scoring ';

 

 // created: 2020-03-29 16:31:22
$dictionary['vin_vin_leads']['fields']['vin_budget_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_budget_c']['labelValue']='Budget';

 

 // created: 2020-03-29 16:31:34
$dictionary['vin_vin_leads']['fields']['vin_budget_usd_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_budget_usd_c']['labelValue']='Budget USD';

 

 // created: 2020-03-25 22:29:29
$dictionary['vin_vin_leads']['fields']['vin_business_manager_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_business_manager_c']['labelValue']='Business Manager';

 

 // created: 2020-05-21 22:40:56
$dictionary['vin_vin_leads']['fields']['vin_campaign_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_campaign_c']['labelValue']='Campaign';

 

 // created: 2020-03-25 20:03:21
$dictionary['vin_vin_leads']['fields']['vin_change_status_date_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_change_status_date_c']['labelValue']='Change Status Date';

 

 // created: 2020-03-26 20:57:32
$dictionary['vin_vin_leads']['fields']['vin_city_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_city_c']['labelValue']='(Deleted) City';

 

 // created: 2020-03-29 16:25:19
$dictionary['vin_vin_leads']['fields']['vin_city_text_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_city_text_c']['labelValue']='City';

 

 // created: 2020-03-29 16:20:46
$dictionary['vin_vin_leads']['fields']['vin_company_name_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_company_name_c']['labelValue']='Company Name';

 

 // created: 2020-03-25 19:57:02
$dictionary['vin_vin_leads']['fields']['vin_content_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_content_c']['labelValue']='Content';

 

 // created: 2020-03-29 16:25:38
$dictionary['vin_vin_leads']['fields']['vin_country_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_country_c']['labelValue']='Country';

 

 // created: 2020-03-29 11:23:17
$dictionary['vin_vin_leads']['fields']['vin_currency_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_currency_c']['labelValue']='Currency';

 

 // created: 2020-06-18 22:04:12
$dictionary['vin_vin_leads']['fields']['vin_department_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_department_c']['labelValue']='Phòng KD';

 

 // created: 2020-06-18 22:03:14
$dictionary['vin_vin_leads']['fields']['vin_dob_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_dob_c']['labelValue']='Date of Birth';

 

 // created: 2020-03-29 16:01:58
$dictionary['vin_vin_leads']['fields']['vin_email_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_email_c']['labelValue']='Email';

 

 // created: 2020-04-07 23:03:01
$dictionary['vin_vin_leads']['fields']['vin_expiry_warning_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_expiry_warning_c']['labelValue']='Expiry Warning';

 

 // created: 2020-04-10 00:25:12
$dictionary['vin_vin_leads']['fields']['vin_facebook_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_facebook_c']['labelValue']='Facebook';

 

 // created: 2020-06-01 22:10:18
$dictionary['vin_vin_leads']['fields']['vin_first_name_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_first_name_c']['labelValue']='First Name';

 

 // created: 2020-05-21 22:44:24
$dictionary['vin_vin_leads']['fields']['vin_form_medium_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_form_medium_c']['labelValue']='Form Medium';

 

 // created: 2020-03-25 19:34:16
$dictionary['vin_vin_leads']['fields']['vin_full_name_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_full_name_c']['labelValue']='Full Name';

 

 // created: 2020-03-25 19:40:10
$dictionary['vin_vin_leads']['fields']['vin_gender_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_gender_c']['labelValue']='Gender';

 

 // created: 2020-03-25 20:14:56
$dictionary['vin_vin_leads']['fields']['vin_google_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_google_c']['labelValue']='Google';

 

 // created: 2020-03-26 20:56:50
$dictionary['vin_vin_leads']['fields']['vin_house_type_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_house_type_c']['labelValue']='(Deleted) House Type';

 

 // created: 2020-03-29 16:22:14
$dictionary['vin_vin_leads']['fields']['vin_house_type_list_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_house_type_list_c']['labelValue']='House Type';

 

 // created: 2020-03-29 16:20:30
$dictionary['vin_vin_leads']['fields']['vin_id_number_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_id_number_c']['labelValue']='ID Number';

 

 // created: 2020-03-29 16:19:14
$dictionary['vin_vin_leads']['fields']['vin_last_name_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_last_name_c']['labelValue']='Last Name';

 

 // created: 2020-05-21 22:41:40
$dictionary['vin_vin_leads']['fields']['vin_lead_ads_id_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_lead_ads_id_c']['labelValue']='Lead ads ID';

 

 // created: 2020-06-18 22:13:59
$dictionary['vin_vin_leads']['fields']['vin_lead_duplicate_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_lead_duplicate_c']['labelValue']='Lead Duplicate';

 

 // created: 2020-04-07 23:11:12
$dictionary['vin_vin_leads']['fields']['vin_lead_expiry_date_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_lead_expiry_date_c']['labelValue']='Expiry Date';

 

 // created: 2020-03-29 16:31:52
$dictionary['vin_vin_leads']['fields']['vin_lead_id_salesforce_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_lead_id_salesforce_c']['labelValue']='Lead ID Salesforce';

 

 // created: 2020-04-07 23:04:32
$dictionary['vin_vin_leads']['fields']['vin_lead_lock_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_lead_lock_c']['labelValue']='Lead Lock';

 

 // created: 2020-03-29 16:23:01
$dictionary['vin_vin_leads']['fields']['vin_lead_owner_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_lead_owner_c']['labelValue']='Lead Owner';

 

 // created: 2020-03-26 21:22:35
$dictionary['vin_vin_leads']['fields']['vin_lead_scoring_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_lead_scoring_c']['labelValue']='Lead Scoring';

 

 // created: 2020-05-21 22:46:52
$dictionary['vin_vin_leads']['fields']['vin_lead_source_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_lead_source_c']['labelValue']='Lead Source';

 

 // created: 2020-03-29 16:22:43
$dictionary['vin_vin_leads']['fields']['vin_lead_status_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_lead_status_c']['labelValue']='Lead Status';

 

 // created: 2020-03-29 13:46:47
$dictionary['vin_vin_leads']['fields']['vin_lead_task_id_c']['inline_edit']=1;

 

 // created: 2020-03-25 19:56:20
$dictionary['vin_vin_leads']['fields']['vin_medium_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_medium_c']['labelValue']='Medium';

 

 // created: 2020-03-29 16:19:54
$dictionary['vin_vin_leads']['fields']['vin_middle_name_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_middle_name_c']['labelValue']='Middle Name';

 

 // created: 2020-03-29 16:21:30
$dictionary['vin_vin_leads']['fields']['vin_mobile_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_mobile_c']['labelValue']='Mobile';

 

 // created: 2020-03-27 00:37:42
$dictionary['vin_vin_leads']['fields']['vin_note_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_note_c']['labelValue']='Note';

 

 // created: 2020-03-29 16:32:09
$dictionary['vin_vin_leads']['fields']['vin_order_id_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_order_id_c']['labelValue']='Order ID';

 

 // created: 2020-03-25 20:02:44
$dictionary['vin_vin_leads']['fields']['vin_order_status_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_order_status_c']['labelValue']='Order Status';

 

 // created: 2020-04-10 22:25:10
$dictionary['vin_vin_leads']['fields']['vin_other_email_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_other_email_c']['labelValue']='Other Email';

 

 // created: 2020-04-05 21:12:42
$dictionary['vin_vin_leads']['fields']['vin_other_fb_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_other_fb_c']['labelValue']='Other Facebook';

 

 // created: 2020-03-27 00:37:56
$dictionary['vin_vin_leads']['fields']['vin_other_note_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_other_note_c']['labelValue']='Other Note';

 

 // created: 2020-04-10 22:26:04
$dictionary['vin_vin_leads']['fields']['vin_other_phone_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_other_phone_c']['labelValue']='Other Phone';

 

 // created: 2020-04-05 21:13:04
$dictionary['vin_vin_leads']['fields']['vin_other_zalo_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_other_zalo_c']['labelValue']='Other Zalo';

 

 // created: 2020-05-21 22:37:04
$dictionary['vin_vin_leads']['fields']['vin_phankhu_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_phankhu_c']['labelValue']='Phân Khu';

 

 // created: 2020-06-18 22:20:03
$dictionary['vin_vin_leads']['fields']['vin_phone_number_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_phone_number_c']['labelValue']='Phone';

 

 // created: 2020-04-05 21:11:37
$dictionary['vin_vin_leads']['fields']['vin_primary_fb_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_primary_fb_c']['labelValue']='Primary Facebook';

 

 // created: 2020-04-05 21:12:12
$dictionary['vin_vin_leads']['fields']['vin_primary_zalo_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_primary_zalo_c']['labelValue']='Primary Zalo';

 

 // created: 2020-03-26 21:19:32
$dictionary['vin_vin_leads']['fields']['vin_priority_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_priority_c']['labelValue']='Priority';

 

 // created: 2020-05-21 22:48:53
$dictionary['vin_vin_leads']['fields']['vin_project_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_project_c']['labelValue']='Project';

 

 // created: 2020-03-25 19:59:34
$dictionary['vin_vin_leads']['fields']['vin_project_follow_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_project_follow_c']['labelValue']='Project Follow';

 

 // created: 2020-03-29 16:23:23
$dictionary['vin_vin_leads']['fields']['vin_rating_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_rating_c']['labelValue']='Rating';

 

 // created: 2020-03-26 21:24:26
$dictionary['vin_vin_leads']['fields']['vin_sales_dept_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_sales_dept_c']['labelValue']='Sales Dept.';

 

 // created: 2020-03-26 21:28:35
$dictionary['vin_vin_leads']['fields']['vin_sales_dept_of_owner_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_sales_dept_of_owner_c']['labelValue']='Sales Dept. of Owner';

 

 // created: 2020-03-26 21:22:59
$dictionary['vin_vin_leads']['fields']['vin_sales_in_charge_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_sales_in_charge_c']['labelValue']='Sales In Charge';

 

 // created: 2020-06-08 22:10:50
$dictionary['vin_vin_leads']['fields']['vin_sale_admin_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_sale_admin_c']['labelValue']='Sales Admin';

 

 // created: 2020-06-08 22:11:22
$dictionary['vin_vin_leads']['fields']['vin_sale_agent_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_sale_agent_c']['labelValue']='Sales Agent';

 

 // created: 2020-03-29 16:18:57
$dictionary['vin_vin_leads']['fields']['vin_salutation_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_salutation_c']['labelValue']='Salutation';

 

 // created: 2020-06-18 22:12:50
$dictionary['vin_vin_leads']['fields']['vin_sign_status_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_sign_status_c']['labelValue']='Đăng ký tư vấn offline';

 

 // created: 2020-03-25 19:55:49
$dictionary['vin_vin_leads']['fields']['vin_source_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_source_c']['labelValue']='Source';

 

 // created: 2020-03-29 16:24:12
$dictionary['vin_vin_leads']['fields']['vin_source_details_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_source_details_c']['labelValue']='Source Details';

 

 // created: 2020-03-29 16:25:54
$dictionary['vin_vin_leads']['fields']['vin_state_province_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_state_province_c']['labelValue']='State/Province';

 

 // created: 2020-03-29 16:24:51
$dictionary['vin_vin_leads']['fields']['vin_street_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_street_c']['labelValue']='Street';

 

 // created: 2020-03-29 16:20:14
$dictionary['vin_vin_leads']['fields']['vin_suffix_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_suffix_c']['labelValue']='Suffix';

 

 // created: 2020-03-29 13:46:47
$dictionary['vin_vin_leads']['fields']['vin_task_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_task_c']['labelValue']='Task';

 

 // created: 2020-03-29 16:21:09
$dictionary['vin_vin_leads']['fields']['vin_tax_code_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_tax_code_c']['labelValue']='Tax Code';

 

 // created: 2020-03-25 19:57:59
$dictionary['vin_vin_leads']['fields']['vin_term_ads_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_term_ads_c']['labelValue']='Term Advertisement';

 

 // created: 2020-03-27 00:39:23
$dictionary['vin_vin_leads']['fields']['vin_unqualified_reason_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_unqualified_reason_c']['labelValue']='Unqualified Reason';

 

 // created: 2020-05-21 22:44:55
$dictionary['vin_vin_leads']['fields']['vin_utm_source_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_utm_source_c']['labelValue']='UTM Source';

 

 // created: 2020-03-25 19:51:07
$dictionary['vin_vin_leads']['fields']['vin_viber_phone_c']['inline_edit']='1';
$dictionary['vin_vin_leads']['fields']['vin_viber_phone_c']['labelValue']='Viber Phone';

 

 // created: 2020-03-29 11:23:17
$dictionary['vin_vin_leads']['fields']['vin_vin_currency_id_c']['inline_edit']=1;

 

 // created: 2020-03-26 21:28:35
$dictionary['vin_vin_leads']['fields']['vin_vin_department_id1_c']['inline_edit']=1;

 

 // created: 2020-06-18 22:04:12
$dictionary['vin_vin_leads']['fields']['vin_vin_department_id2_c']['inline_edit']=1;

 

 // created: 2020-03-26 21:24:26
$dictionary['vin_vin_leads']['fields']['vin_vin_department_id_c']['inline_edit']=1;

 

 // created: 2020-03-29 16:08:15
$dictionary['vin_vin_leads']['fields']['vin_vin_leads_type']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['vin_vin_leads_type']['duplicate_merge_dom_value']='0';

 

 // created: 2020-05-30 12:42:36
$dictionary['vin_vin_leads']['fields']['vin_working_substatus_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_working_substatus_c']['labelValue']='Lead Working Detail';

 

 // created: 2020-04-10 00:24:40
$dictionary['vin_vin_leads']['fields']['vin_zalo_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_zalo_c']['labelValue']='Zalo';

 

 // created: 2020-03-29 16:26:09
$dictionary['vin_vin_leads']['fields']['vin_zip_postal_code_c']['inline_edit']='';
$dictionary['vin_vin_leads']['fields']['vin_zip_postal_code_c']['labelValue']='Zip/Postal Code';

 

 // created: 2020-03-29 16:14:31
$dictionary['vin_vin_leads']['fields']['website']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['website']['duplicate_merge_dom_value']='0';

 
?>