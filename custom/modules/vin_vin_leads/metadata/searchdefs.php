<?php
$module_name = 'vin_vin_leads';
$_module_name = 'vin_vin_leads';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'vin_email_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_VIN_EMAIL_C',
        'width' => '10%',
        'name' => 'vin_email_c',
      ),
      'vin_phone_number_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_VIN_PHONE_NUMBER_C',
        'width' => '10%',
        'name' => 'vin_phone_number_c',
      ),
      'vin_lead_source_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_LEAD_SOURCE_C',
        'width' => '10%',
        'name' => 'vin_lead_source_c',
      ),
      'vin_project_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_PROJECT_C',
        'width' => '10%',
        'name' => 'vin_project_c',
      ),
      'vin_phankhu_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_PHANKHU_C',
        'width' => '10%',
        'name' => 'vin_phankhu_c',
      ),
      'vin_lead_status_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_LEAD_STATUS_C',
        'width' => '10%',
        'name' => 'vin_lead_status_c',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'vin_budget_c' => 
      array (
        'type' => 'decimal',
        'default' => true,
        'label' => 'LBL_VIN_BUDGET_C',
        'width' => '10%',
        'name' => 'vin_budget_c',
      ),
      'vin_email_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_VIN_EMAIL_C',
        'width' => '10%',
        'name' => 'vin_email_c',
      ),
      'vin_sales_dept_c' => 
      array (
        'type' => 'relate',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_SALES_DEPT_C',
        'id' => 'VIN_VIN_DEPARTMENT_ID_C',
        'link' => true,
        'width' => '10%',
        'name' => 'vin_sales_dept_c',
      ),
      'vin_sales_in_charge_c' => 
      array (
        'type' => 'relate',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_SALES_IN_CHARGE_C',
        'id' => 'USER_ID4_C',
        'link' => true,
        'width' => '10%',
        'name' => 'vin_sales_in_charge_c',
      ),
      'vin_sales_dept_of_owner_c' => 
      array (
        'type' => 'relate',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_SALES_DEPT_OF_OWNER_C',
        'id' => 'VIN_VIN_DEPARTMENT_ID1_C',
        'link' => true,
        'width' => '10%',
        'name' => 'vin_sales_dept_of_owner_c',
      ),
      'vin_rating_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_RATING_C',
        'width' => '10%',
        'name' => 'vin_rating_c',
      ),
      'vin_mobile_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_VIN_MOBILE_C',
        'width' => '10%',
        'name' => 'vin_mobile_c',
      ),
      'vin_phone_number_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_VIN_PHONE_NUMBER_C',
        'width' => '10%',
        'name' => 'vin_phone_number_c',
      ),
      'vin_order_id_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_VIN_ORDER_ID_C',
        'width' => '10%',
        'name' => 'vin_order_id_c',
      ),
      'vin_lead_status_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_LEAD_STATUS_C',
        'width' => '10%',
        'name' => 'vin_lead_status_c',
      ),
      'vin_lead_scoring_c' => 
      array (
        'type' => 'decimal',
        'default' => true,
        'label' => 'LBL_VIN_LEAD_SCORING_C',
        'width' => '10%',
        'name' => 'vin_lead_scoring_c',
      ),
      'vin_lead_source_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_LEAD_SOURCE_C',
        'width' => '10%',
        'name' => 'vin_lead_source_c',
      ),
      'vin_project_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_PROJECT_C',
        'width' => '10%',
        'name' => 'vin_project_c',
      ),
      'vin_phankhu_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_PHANKHU_C',
        'width' => '10%',
        'name' => 'vin_phankhu_c',
      ),
      'vin_house_type_list_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_HOUSE_TYPE_LIST_C',
        'width' => '10%',
        'name' => 'vin_house_type_list_c',
      ),
      'vin_business_manager_c' => 
      array (
        'type' => 'relate',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_BUSINESS_MANAGER',
        'id' => 'USER_ID_C',
        'link' => true,
        'width' => '10%',
        'name' => 'vin_business_manager_c',
      ),
      'vin_sale_admin_c' => 
      array (
        'type' => 'relate',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_SALE_ADMIN_C',
        'id' => 'USER_ID1_C',
        'link' => true,
        'width' => '10%',
        'name' => 'vin_sale_admin_c',
      ),
      'vin_sale_agent_c' => 
      array (
        'type' => 'relate',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_SALE_AGENT_C',
        'id' => 'USER_ID2_C',
        'link' => true,
        'width' => '10%',
        'name' => 'vin_sale_agent_c',
      ),
      'vin_campaign_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_VIN_CAMPAIGN_C',
        'width' => '10%',
        'name' => 'vin_campaign_c',
      ),
      'vin_company_name_c' => 
      array (
        'type' => 'text',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_VIN_COMPANY_NAME_C',
        'sortable' => false,
        'width' => '10%',
        'name' => 'vin_company_name_c',
      ),
      'vin_id_number_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_VIN_ID_NUMBER_C',
        'width' => '10%',
        'name' => 'vin_id_number_c',
      ),
      'vin_lead_lock_c' => 
      array (
        'type' => 'int',
        'default' => true,
        'label' => 'LBL_VIN_LEAD_LOCK',
        'width' => '10%',
        'name' => 'vin_lead_lock_c',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
