<?php
$module_name = 'vin_vin_leads';
$OBJECT_NAME = 'VIN_VIN_LEADS';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '40%',
    'label' => 'LBL_ACCOUNT_NAME',
    'link' => true,
    'default' => true,
  ),
  'VIN_EMAIL_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_VIN_EMAIL_C',
    'width' => '10%',
  ),
  'VIN_PHONE_NUMBER_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_VIN_PHONE_NUMBER_C',
    'width' => '10%',
  ),
  'VIN_LEAD_STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_VIN_LEAD_STATUS_C',
    'width' => '10%',
  ),
  'VIN_BUSINESS_MANAGER_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_VIN_BUSINESS_MANAGER',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'VIN_SALE_ADMIN_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_VIN_SALE_ADMIN_C',
    'id' => 'USER_ID1_C',
    'link' => true,
    'width' => '10%',
  ),
  'VIN_SALE_AGENT_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_VIN_SALE_AGENT_C',
    'id' => 'USER_ID2_C',
    'link' => true,
    'width' => '10%',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => true,
  ),
  'VIN_OTHER_NOTE_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_OTHER_NOTE_C',
    'sortable' => false,
    'width' => '10%',
  ),
  'VIN_FULL_NAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_FULL_NAME_C',
    'width' => '10%',
  ),
  'VIN_CHANGE_STATUS_DATE_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => false,
    'label' => 'LBL_VIN_CHANGE_STATUS_DATE_C',
    'width' => '10%',
  ),
  'VIN_MOBILE_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_MOBILE_C',
    'width' => '10%',
  ),
  'VIN_STREET_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_STREET_C',
    'sortable' => false,
    'width' => '10%',
  ),
  'VIN_STATE_PROVINCE_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_STATE_PROVINCE_C',
    'width' => '10%',
  ),
  'VIN_SUFFIX_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_SUFFIX_C',
    'width' => '10%',
  ),
  'VIN_ZIP_POSTAL_CODE_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_ZIP_POSTAL_CODE_C',
    'width' => '10%',
  ),
  'VIN_TAX_CODE_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_TAX_CODE_C',
    'width' => '10%',
  ),
  'VIN_SOURCE_DETAILS_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_SOURCE_DETAILS_C',
    'width' => '10%',
  ),
  'VIN_SALES_IN_CHARGE_C' => 
  array (
    'type' => 'relate',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_SALES_IN_CHARGE_C',
    'id' => 'USER_ID4_C',
    'link' => true,
    'width' => '10%',
  ),
  'VIN_SALES_DEPT_OF_OWNER_C' => 
  array (
    'type' => 'relate',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_SALES_DEPT_OF_OWNER_C',
    'id' => 'VIN_VIN_DEPARTMENT_ID1_C',
    'link' => true,
    'width' => '10%',
  ),
  'VIN_SALES_DEPT_C' => 
  array (
    'type' => 'relate',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_SALES_DEPT_C',
    'id' => 'VIN_VIN_DEPARTMENT_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'VIN_RATING_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_RATING_C',
    'width' => '10%',
  ),
  'VIN_PROJECT_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_PROJECT_C',
    'width' => '10%',
  ),
  'VIN_PRIORITY_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_PRIORITY_C',
    'width' => '10%',
  ),
  'VIN_LEAD_SCORING_C' => 
  array (
    'type' => 'decimal',
    'default' => false,
    'label' => 'LBL_VIN_LEAD_SCORING_C',
    'width' => '10%',
  ),
  'VIN_LEAD_SOURCE_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_LEAD_SOURCE_C',
    'width' => '10%',
  ),
  'VIN_LEAD_OWNER_C' => 
  array (
    'type' => 'relate',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_LEAD_OWNER_C',
    'id' => 'USER_ID3_C',
    'link' => true,
    'width' => '10%',
  ),
  'VIN_LEAD_ID_SALESFORCE_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_LEAD_ID_SALESFORCE_C',
    'width' => '10%',
  ),
  'VIN_ID_NUMBER_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_ID_NUMBER_C',
    'width' => '10%',
  ),
  'VIN_HOUSE_TYPE_LIST_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_HOUSE_TYPE_LIST_C',
    'width' => '10%',
  ),
  'VIN_COUNTRY_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_COUNTRY_C',
    'width' => '10%',
  ),
  'VIN_ADDRESS_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_ADDRESS_C',
    'width' => '10%',
  ),
  'VIN_BUDGET_C' => 
  array (
    'type' => 'decimal',
    'default' => false,
    'label' => 'LBL_VIN_BUDGET_C',
    'width' => '10%',
  ),
  'VIN_COMPANY_NAME_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_COMPANY_NAME_C',
    'sortable' => false,
    'width' => '10%',
  ),
  'VIN_CITY_TEXT_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_CITY_TEXT_C',
    'width' => '10%',
  ),
  'VIN_ORDER_ID_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_ORDER_ID_C',
    'width' => '10%',
  ),
  'VIN_NOTE_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_NOTE_C',
    'sortable' => false,
    'width' => '10%',
  ),
  'VIN_ASSIGN_DATE_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => false,
    'label' => 'LBL_VIN_ASSIGN_DATE_C',
    'width' => '10%',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'VIN_LEAD_ADS_ID_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_LEAD_ADS_ID_C',
    'width' => '10%',
  ),
  'VIN_CAMPAIGN_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_CAMPAIGN_C',
    'width' => '10%',
  ),
  'VIN_UNQUALIFIED_REASON_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_UNQUALIFIED_REASON_C',
    'width' => '10%',
  ),
  'VIN_ADS_CONTENT_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VIN_ADS_CONTENT_C',
    'width' => '10%',
  ),
  'VIN_WORKING_SUBSTATUS_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_WORKING_SUBSTATUS_C',
    'width' => '10%',
  ),
  'VIN_PHANKHU_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_PHANKHU_C',
    'width' => '10%',
  ),
  'VIN_DEPARTMENT_C' => 
  array (
    'type' => 'relate',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_DEPARTMENT_C',
    'id' => 'VIN_VIN_DEPARTMENT_ID2_C',
    'link' => true,
    'width' => '10%',
  ),
  'VIN_SIGN_STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_SIGN_STATUS_C',
    'width' => '10%',
  ),
  'VIN_LEAD_DUPLICATE_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_LEAD_DUPLICATE_C',
    'width' => '10%',
  ),
  'VIN_DOB_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_VIN_DOB_C',
    'width' => '10%',
  ),
  'VIN_ADS_SOURCE_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VIN_ADS_SOURCE_C',
    'width' => '10%',
  ),
);
;
?>
