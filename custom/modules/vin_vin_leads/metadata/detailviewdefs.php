<?php
$module_name = 'vin_vin_leads';
$_object_name = 'vin_vin_leads';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_ACCOUNT_INFORMATION' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_account_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'vin_salutation_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_SALUTATION_C',
          ),
          1 => 
          array (
            'name' => 'vin_company_name_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_COMPANY_NAME_C',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'vin_full_name_c',
            'label' => 'LBL_VIN_FULL_NAME_C',
          ),
          1 => 
          array (
            'name' => 'vin_lead_duplicate_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_LEAD_DUPLICATE_C',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'vin_suffix_c',
            'label' => 'LBL_VIN_SUFFIX_C',
          ),
          1 => 
          array (
            'name' => 'vin_dob_c',
            'label' => 'LBL_VIN_DOB_C',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'vin_email_c',
            'label' => 'LBL_VIN_EMAIL_C',
          ),
          1 => 
          array (
            'name' => 'vin_tax_code_c',
            'label' => 'LBL_VIN_TAX_CODE_C',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'vin_phone_number_c',
            'label' => 'LBL_VIN_PHONE_NUMBER_C',
          ),
          1 => 
          array (
            'name' => 'vin_mobile_c',
            'label' => 'LBL_VIN_MOBILE_C',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'vin_id_number_c',
            'label' => 'LBL_VIN_ID_NUMBER_C',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'vin_other_email_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_OTHER_EMAIL_C',
          ),
          1 => 
          array (
            'name' => 'vin_other_phone_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_OTHER_PHONE_C',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'vin_facebook_c',
            'label' => 'LBL_VIN_FACEBOOK_C',
          ),
          1 => 
          array (
            'name' => 'vin_zalo_c',
            'label' => 'LBL_VIN_ZALO_C',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'vin_project_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_PROJECT_C',
          ),
          1 => 
          array (
            'name' => 'vin_budget_c',
            'label' => 'LBL_VIN_BUDGET_C',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'vin_house_type_list_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_HOUSE_TYPE_LIST_C',
          ),
          1 => 
          array (
            'name' => 'vin_department_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_DEPARTMENT_C',
          ),
        ),
        2 => 
        array (
          0 => 'description',
          1 => 
          array (
            'name' => 'vin_sign_status_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_SIGN_STATUS_C',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'vin_lead_status_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_LEAD_STATUS_C',
          ),
          1 => 
          array (
            'name' => 'vin_lead_owner_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_LEAD_OWNER_C',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'vin_working_substatus_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_WORKING_SUBSTATUS_C',
          ),
          1 => 
          array (
            'name' => 'vin_unqualified_reason_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_UNQUALIFIED_REASON_C',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'vin_rating_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_RATING_C',
          ),
          1 => 
          array (
            'name' => 'vin_priority_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_PRIORITY_C',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'vin_lead_scoring_c',
            'label' => 'LBL_VIN_LEAD_SCORING_C',
          ),
          1 => 
          array (
            'name' => 'vin_sales_in_charge_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_SALES_IN_CHARGE_C',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'vin_auto_scoring_c',
            'label' => 'LBL_VIN_AUTO_SCORING_C',
          ),
          1 => 
          array (
            'name' => 'vin_sales_dept_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_SALES_DEPT_C',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'vin_lead_source_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_LEAD_SOURCE_C',
          ),
          1 => 
          array (
            'name' => 'vin_source_details_c',
            'label' => 'LBL_VIN_SOURCE_DETAILS_C',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'vin_note_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_NOTE_C',
          ),
          1 => 
          array (
            'name' => 'vin_other_note_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_OTHER_NOTE_C',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'vin_phankhu_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_PHANKHU_C',
          ),
          1 => 
          array (
            'name' => 'vin_ads_content_c',
            'label' => 'LBL_VIN_ADS_CONTENT_C',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'vin_ads_source_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_ADS_SOURCE_C',
          ),
          1 => 
          array (
            'name' => 'vin_lead_ads_id_c',
            'label' => 'LBL_VIN_LEAD_ADS_ID_C',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'vin_campaign_c',
            'label' => 'LBL_VIN_CAMPAIGN_C',
          ),
          1 => 
          array (
            'name' => 'vin_business_manager_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_BUSINESS_MANAGER',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'vin_agent_c',
            'label' => 'LBL_VIN_AGENT_C',
          ),
          1 => 
          array (
            'name' => 'vin_ad_term_c',
            'label' => 'LBL_VIN_AD_TERM_C',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'vin_form_medium_c',
            'label' => 'LBL_VIN_FORM_MEDIUM_C',
          ),
          1 => 
          array (
            'name' => 'vin_utm_source_c',
            'label' => 'LBL_VIN_UTM_SOURCE_C',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'vin_address_c',
            'label' => 'LBL_VIN_ADDRESS_C',
          ),
          1 => 
          array (
            'name' => 'vin_city_text_c',
            'label' => 'LBL_VIN_CITY_TEXT_C',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'vin_street_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_STREET_C',
          ),
          1 => 
          array (
            'name' => 'vin_country_c',
            'label' => 'LBL_VIN_COUNTRY_C',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'vin_state_province_c',
            'label' => 'LBL_VIN_STATE_PROVINCE_C',
          ),
          1 => 
          array (
            'name' => 'vin_zip_postal_code_c',
            'label' => 'LBL_VIN_ZIP_POSTAL_CODE_C',
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'vin_lead_id_salesforce_c',
            'label' => 'LBL_VIN_LEAD_ID_SALESFORCE_C',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
          1 => 
          array (
            'name' => 'vin_sales_dept_of_owner_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_SALES_DEPT_OF_OWNER_C',
          ),
        ),
      ),
    ),
  ),
);
;
?>
