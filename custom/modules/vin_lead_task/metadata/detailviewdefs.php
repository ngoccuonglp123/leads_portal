<?php
$module_name = 'vin_lead_task';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'vin_sale_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_SALE_C',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'vin_lead_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_LEAD_C',
          ),
          1 => 
          array (
            'name' => 'vin_deadline_c',
            'label' => 'LBL_VIN_DEADLINE_C',
          ),
        ),
        2 => 
        array (
          0 => 'description',
          1 => 
          array (
            'name' => 'vin_task_status_c',
            'studio' => 'visible',
            'label' => 'LBL_VIN_TASK_STATUS_C',
          ),
        ),
      ),
    ),
  ),
);
;
?>
