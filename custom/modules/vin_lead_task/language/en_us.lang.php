<?php
// created: 2020-03-29 13:42:30
$mod_strings = array (
  'LBL_VIN_LEAD_C_VIN_VIN_LEADS_ID' => 'Lead (related Vinhomes Leads ID)',
  'LBL_VIN_LEAD_C' => 'Lead',
  'LBL_VIN_DEADLINE_C' => 'Deadline',
  'LBL_VIN_SALE_C_USER_ID' => 'Sale (related User ID)',
  'LBL_VIN_SALE_C' => 'Sale',
  'LBL_VIN_TASK_STATUS_C' => 'Task Status',
);