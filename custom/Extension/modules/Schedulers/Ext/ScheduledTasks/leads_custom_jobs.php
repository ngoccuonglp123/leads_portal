<?php

require_once('custom/include/custom_jobs/vin_vin_leads/vin_vin_leads_jobs.php');

array_push($job_strings, 'lead_expiry_warning');
function lead_expiry_warning(){
    $f = new vin_vin_leads_jobs;
    $f->lead_expiry_warning();
    return true;
}

array_push($job_strings, 'lead_expiry_inform');
function lead_expiry_inform(){
    $f = new vin_vin_leads_jobs;
    $f->lead_expiry_inform();
    return true;
}

array_push($job_strings, 'lead_assign_warning');
function lead_assign_warning(){
    $f = new vin_vin_leads_jobs;
    $f->lead_expiry_warning_180();
    return true;
}

array_push($job_strings, 'salesforce_sync_leads_to_portal');
function salesforce_sync_leads_to_portal(){
    $f = new vin_vin_leads_jobs;
    $f->salesforce_sync_leads_to_portal();
    return true;
}

array_push($job_strings, 'salesforce_sync_leads_to_salesforce');
function salesforce_sync_leads_to_salesforce(){
    $f = new vin_vin_leads_jobs;
    $f->salesforce_sync_leads_to_salesforce();
    return true;
}

array_push($job_strings, 'lead_unqualified_refresh');
function lead_unqualified_refresh(){
    $f = new vin_vin_leads_jobs;
    $f->lead_unqualified_refresh();
    return true;
}