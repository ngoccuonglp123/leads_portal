<?php
 // created: 2020-03-29 16:12:23
$dictionary['vin_vin_leads']['fields']['rating']['inline_edit']=true;
$dictionary['vin_vin_leads']['fields']['rating']['comments']='An arbitrary rating for this company for use in comparisons with others';
$dictionary['vin_vin_leads']['fields']['rating']['duplicate_merge']='disabled';
$dictionary['vin_vin_leads']['fields']['rating']['duplicate_merge_dom_value']='0';
$dictionary['vin_vin_leads']['fields']['rating']['merge_filter']='disabled';

 ?>