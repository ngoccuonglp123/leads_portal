<?php
// created: 2020-03-29 12:06:20
$dictionary["vin_vin_leads"]["fields"]["vin_vin_leads_documents_1"] = array (
  'name' => 'vin_vin_leads_documents_1',
  'type' => 'link',
  'relationship' => 'vin_vin_leads_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_VIN_VIN_LEADS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
