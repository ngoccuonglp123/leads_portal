<?php 
 //WARNING: The contents of this file are auto-generated


$entry_point_registry['vinhomes_leads'] = array(
    'file' => 'custom/include/entryPoints/vinhomes_leads/vinhomes_leads.php',
    'auth' => true,
);

$entry_point_registry['organization_personnel'] = array(
  'file' => 'custom/include/entryPoints/organization_personnel/organization_personnel.php',
  'auth' => true,
);

$entry_point_registry['departments'] = array(
  'file' => 'custom/include/entryPoints/departments/departments.php',
  'auth' => true,
);

$entry_point_registry['dangkytuvan'] = array(
    'file' => 'custom/include/entryPoints/advice/regis_advice.php',
    'auth' => false,
);

$entry_point_registry['kenhtuvan_GD_BDS_Vinhomes'] = array(
    'file' => 'custom/include/entryPoints/advice/information.php',
    'auth' => false,
);

?>